#pragma once
#include "pch.h"
#include "../LUOptimization/IDijkstra.h"

using namespace ::testing;

class DijkstraMock : public IDijkstra
{
	public:
		MOCK_METHOD((std::vector<Station*>*), dijkstra, (Station*, Station*), (override));
};
