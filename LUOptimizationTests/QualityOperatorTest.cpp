#include "pch.h"
#include "../LUOptimization/QualityOperator.h"
#include "../LUOptimization/MultiCriteriaNetwork.h"
#include "TestNetworkCreator.h"

using namespace std;

TEST(QualityOperatorTest, getQualitiesOneGenerationIntegrationTest)
{
	//given
	TestNetworkCreator networkCreator;
	vector<MultiCriteriaValue*>* trueParetoFront = new vector<MultiCriteriaValue*>();
	trueParetoFront->push_back(networkCreator.createValue(1, 17, 1));
	trueParetoFront->push_back(networkCreator.createValue(2, 14, 1));
	trueParetoFront->push_back(networkCreator.createValue(4, 13, 1));
	trueParetoFront->push_back(networkCreator.createValue(5, 9, 1));
	trueParetoFront->push_back(networkCreator.createValue(6, 7, 1));
	trueParetoFront->push_back(networkCreator.createValue(7, 6, 1));
	trueParetoFront->push_back(networkCreator.createValue(9, 5, 1));
	trueParetoFront->push_back(networkCreator.createValue(11, 3, 1));
	trueParetoFront->push_back(networkCreator.createValue(12, 2, 1));
	QualityOperator qualityOperator = QualityOperator(trueParetoFront);

	vector<AbstractNetwork<MultiCriteriaValue>*>* population = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	population->push_back(networkCreator.createSimpleNetwork(2, 19, 1, 1));
	population->push_back(networkCreator.createSimpleNetwork(3, 16, 1, 2));
	population->push_back(networkCreator.createSimpleNetwork(5, 14, 1, 3));
	population->push_back(networkCreator.createSimpleNetwork(6, 13, 1, 4));
	population->push_back(networkCreator.createSimpleNetwork(7, 9, 1, 5));
	population->push_back(networkCreator.createSimpleNetwork(10, 7, 1, 6));
	population->push_back(networkCreator.createSimpleNetwork(13, 5, 1, 7));
	population->push_back(networkCreator.createSimpleNetwork(16, 3, 1, 8));
	population->push_back(networkCreator.createSimpleNetwork(50, 50, 2, 9));
	population->push_back(networkCreator.createSimpleNetwork(60, 60, 2, 10));

	vector<QualityEnum> qualityTypes;
	qualityTypes.push_back(QualityEnum::HV);
	qualityTypes.push_back(QualityEnum::IGD);

	//when
	std::string result = qualityOperator.getQualites(population, qualityTypes);

	//then
	std::string expected = "340282366920938463334247398915801350057;340282366920938463334247398915801350057;2.3721322508096470293378504879985464123545604671002404381634;2.3721322508096470293378504879985464123545604671002404381634";
	EXPECT_EQ(result, expected);

	//clean
	for (vector<AbstractNetwork<MultiCriteriaValue>*>::iterator it = population->begin(); it != population->end(); ++it)
	{
		delete (*it);
	}

	delete population;
}

TEST(QualityOperatorTest, getQualitiesTwoGenerationsIntegrationTest)
{
	//given
	TestNetworkCreator networkCreator;
	vector<MultiCriteriaValue*>* trueParetoFront = new vector<MultiCriteriaValue*>();
	trueParetoFront->push_back(networkCreator.createValue(1, 17, 1));
	trueParetoFront->push_back(networkCreator.createValue(2, 14, 1));
	trueParetoFront->push_back(networkCreator.createValue(4, 13, 1));
	trueParetoFront->push_back(networkCreator.createValue(5, 9, 1));
	trueParetoFront->push_back(networkCreator.createValue(6, 7, 1));
	trueParetoFront->push_back(networkCreator.createValue(7, 6, 1));
	trueParetoFront->push_back(networkCreator.createValue(9, 5, 1));
	trueParetoFront->push_back(networkCreator.createValue(11, 3, 1));
	trueParetoFront->push_back(networkCreator.createValue(12, 2, 1));
	QualityOperator qualityOperator = QualityOperator(trueParetoFront);

	vector<AbstractNetwork<MultiCriteriaValue>*>* firstGeneration = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	firstGeneration->push_back(networkCreator.createSimpleNetwork(2, 19, 1, 1));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(3, 16, 1, 2));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(5, 14, 1, 3));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(6, 13, 1, 4));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(7, 9, 1, 5));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(10, 7, 1, 6));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(13, 5, 1, 7));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(16, 3, 1, 8));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(50, 50, 2, 9));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(60, 60, 2, 10));

	vector<AbstractNetwork<MultiCriteriaValue>*>* secondGeneration = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	secondGeneration->push_back(networkCreator.createSimpleNetwork(1, 19, 1, 11));
	secondGeneration->push_back(networkCreator.createSimpleNetwork(3, 16, 1, 2));
	secondGeneration->push_back(networkCreator.createSimpleNetwork(7, 7, 1, 12));
	secondGeneration->push_back(networkCreator.createSimpleNetwork(12, 2, 1, 13));
	secondGeneration->push_back(networkCreator.createSimpleNetwork(50, 50, 2, 9));
	secondGeneration->push_back(networkCreator.createSimpleNetwork(60, 60, 2, 10));

	vector<QualityEnum> qualityTypes;
	qualityTypes.push_back(QualityEnum::HV);
	qualityTypes.push_back(QualityEnum::IGD);

	//when
	std::string firstResult = qualityOperator.getQualites(firstGeneration, qualityTypes);
	std::string secondResult = qualityOperator.getQualites(secondGeneration, qualityTypes);

	//then
	std::string firstExpectedResult = "340282366920938463334247398915801350057;340282366920938463334247398915801350057;2.3721322508096470293378504879985464123545604671002404381634;2.3721322508096470293378504879985464123545604671002404381634";
	std::string secondExpectedResult = "340282366920938463371140887063220453267;340282366920938463371140887063220453272;1.8299348277259604747129456482458316846676147639801647685568;1.6357054835264844432465895571099405229842944013192460181478";
	EXPECT_EQ(firstResult, firstExpectedResult);
	EXPECT_EQ(secondResult, secondExpectedResult);

	//clean
	for (vector<AbstractNetwork<MultiCriteriaValue>*>::iterator it = firstGeneration->begin(); it != firstGeneration->end(); ++it)
	{
		delete (*it);
	}
	for (vector<AbstractNetwork<MultiCriteriaValue>*>::iterator it = secondGeneration->begin(); it != secondGeneration->end(); ++it)
	{
		delete (*it);
	}

	delete firstGeneration;
	delete secondGeneration;
}

TEST(QualityOperatorTest, getQualitiesOnlyHVIntegrationTest)
{
	//given
	TestNetworkCreator networkCreator;
	vector<MultiCriteriaValue*>* trueParetoFront = new vector<MultiCriteriaValue*>();
	trueParetoFront->push_back(networkCreator.createValue(1, 17, 1));
	trueParetoFront->push_back(networkCreator.createValue(2, 14, 1));
	trueParetoFront->push_back(networkCreator.createValue(4, 13, 1));
	trueParetoFront->push_back(networkCreator.createValue(5, 9, 1));
	trueParetoFront->push_back(networkCreator.createValue(6, 7, 1));
	trueParetoFront->push_back(networkCreator.createValue(7, 6, 1));
	trueParetoFront->push_back(networkCreator.createValue(9, 5, 1));
	trueParetoFront->push_back(networkCreator.createValue(11, 3, 1));
	trueParetoFront->push_back(networkCreator.createValue(12, 2, 1));
	QualityOperator qualityOperator = QualityOperator(trueParetoFront);

	vector<AbstractNetwork<MultiCriteriaValue>*>* population = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	population->push_back(networkCreator.createSimpleNetwork(2, 19, 1, 1));
	population->push_back(networkCreator.createSimpleNetwork(3, 16, 1, 2));
	population->push_back(networkCreator.createSimpleNetwork(5, 14, 1, 3));
	population->push_back(networkCreator.createSimpleNetwork(6, 13, 1, 4));
	population->push_back(networkCreator.createSimpleNetwork(7, 9, 1, 5));
	population->push_back(networkCreator.createSimpleNetwork(10, 7, 1, 6));
	population->push_back(networkCreator.createSimpleNetwork(13, 5, 1, 7));
	population->push_back(networkCreator.createSimpleNetwork(16, 3, 1, 8));
	population->push_back(networkCreator.createSimpleNetwork(50, 50, 2, 9));
	population->push_back(networkCreator.createSimpleNetwork(60, 60, 2, 10));

	vector<QualityEnum> qualityTypes;
	qualityTypes.push_back(QualityEnum::HV);

	//when
	std::string result = qualityOperator.getQualites(population, qualityTypes);

	//then
	std::string expected = "340282366920938463334247398915801350057;340282366920938463334247398915801350057";
	EXPECT_EQ(result, expected);

	//clean
	for (vector<AbstractNetwork<MultiCriteriaValue>*>::iterator it = population->begin(); it != population->end(); ++it)
	{
		delete (*it);
	}

	delete population;
}

TEST(QualityOperatorTest, getQualitiesOnlyIGDIntegrationTest)
{
	//given
	TestNetworkCreator networkCreator;
	vector<MultiCriteriaValue*>* trueParetoFront = new vector<MultiCriteriaValue*>();
	trueParetoFront->push_back(networkCreator.createValue(1, 17, 1));
	trueParetoFront->push_back(networkCreator.createValue(2, 14, 1));
	trueParetoFront->push_back(networkCreator.createValue(4, 13, 1));
	trueParetoFront->push_back(networkCreator.createValue(5, 9, 1));
	trueParetoFront->push_back(networkCreator.createValue(6, 7, 1));
	trueParetoFront->push_back(networkCreator.createValue(7, 6, 1));
	trueParetoFront->push_back(networkCreator.createValue(9, 5, 1));
	trueParetoFront->push_back(networkCreator.createValue(11, 3, 1));
	trueParetoFront->push_back(networkCreator.createValue(12, 2, 1));
	QualityOperator qualityOperator = QualityOperator(trueParetoFront);

	vector<AbstractNetwork<MultiCriteriaValue>*>* population = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	population->push_back(networkCreator.createSimpleNetwork(2, 19, 1, 1));
	population->push_back(networkCreator.createSimpleNetwork(3, 16, 1, 2));
	population->push_back(networkCreator.createSimpleNetwork(5, 14, 1, 3));
	population->push_back(networkCreator.createSimpleNetwork(6, 13, 1, 4));
	population->push_back(networkCreator.createSimpleNetwork(7, 9, 1, 5));
	population->push_back(networkCreator.createSimpleNetwork(10, 7, 1, 6));
	population->push_back(networkCreator.createSimpleNetwork(13, 5, 1, 7));
	population->push_back(networkCreator.createSimpleNetwork(16, 3, 1, 8));
	population->push_back(networkCreator.createSimpleNetwork(50, 50, 2, 9));
	population->push_back(networkCreator.createSimpleNetwork(60, 60, 2, 10));

	vector<QualityEnum> qualityTypes;
	qualityTypes.push_back(QualityEnum::IGD);

	//when
	std::string result = qualityOperator.getQualites(population, qualityTypes);

	//then
	std::string expected = "2.3721322508096470293378504879985464123545604671002404381634;2.3721322508096470293378504879985464123545604671002404381634";
	EXPECT_EQ(result, expected);

	//clean
	for (vector<AbstractNetwork<MultiCriteriaValue>*>::iterator it = population->begin(); it != population->end(); ++it)
	{
		delete (*it);
	}

	delete population;
}
