#pragma once
#include "pch.h"
#include "../LUOptimization/IRandomNumberGenerator.h"

using namespace ::testing;

class RandomNumberGeneratorMock : public IRandomNumberGenerator
{
	public:
		using IRandomNumberGeneratorPointer = IRandomNumberGenerator*;
		MOCK_METHOD(int, getInt, (int, int), (override));
		MOCK_METHOD(unsigned long long int, getUnsignedLongLongInt, (unsigned long long int, unsigned long long int), (override));
		MOCK_METHOD(IRandomNumberGeneratorPointer, copy, (), (override));
};
