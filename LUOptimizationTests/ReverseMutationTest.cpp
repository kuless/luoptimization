#include "pch.h"
#include "../LUOptimization/ReverseMutation.h"
#include "RandomNumberGeneratorMock.h"

TEST(ReverseMutationTest, shouldReversLine)
{
	//given
	RandomNumberGeneratorMock* randomMock = new RandomNumberGeneratorMock();
	ReverseMutation* reverseMutation = new ReverseMutation(randomMock);
	Station* station1 = new Station("1", "name", 1.0, 1.0);
	Station* station2 = new Station("1", "name", 1.0, 1.0);
	Station* station3 = new Station("1", "name", 1.0, 1.0);
	std::vector<Station*>* stops = new std::vector<Station*>();
	stops->push_back(station1);
	stops->push_back(station2);
	stops->push_back(station3);
	std::vector<int> timetable;
	timetable.push_back(5);
	Line* line = new Line(1, "name", stops, timetable);

	//when
	reverseMutation->mutation(line);

	//then
	EXPECT_EQ(station3, line->getStops()[0]);
	EXPECT_EQ(station2, line->getStops()[1]);
	EXPECT_EQ(station1, line->getStops()[2]);
	
	//clean
	delete station1;
	delete station2;
	delete station3;
	delete line;
	delete reverseMutation;
}
