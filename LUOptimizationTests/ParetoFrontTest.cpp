#include "pch.h"
#include "../LUOptimization/ParetoFront.h"
#include "../LUOptimization/MultiCriteriaNetwork.h"

using namespace std;

pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>* createParetoFrontElement(int paretoFrontNumber, long long int distance)
{
	MultiCriteriaNetwork* network = new MultiCriteriaNetwork(new vector<Line*>());
	MultiCriteriaValue* value = new MultiCriteriaValue();
	value->setParetoFront(paretoFrontNumber);
	value->setDistance(distance);
	network->setValue(value);
	return new pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>(network, new FrontDomination());
}

TEST(ParetoFrontTest, sortTest)
{
	//given
	ParetoFront front = ParetoFront();
	front.addNetwork(createParetoFrontElement(2, 1));
	front.addNetwork(createParetoFrontElement(1, 4));
	front.addNetwork(createParetoFrontElement(2, 5));
	front.addNetwork(createParetoFrontElement(3, 6));
	front.addNetwork(createParetoFrontElement(1, 1));
	front.addNetwork(createParetoFrontElement(2, 6));
	front.addNetwork(createParetoFrontElement(3, 5));
	front.addNetwork(createParetoFrontElement(1, 4));
	front.addNetwork(createParetoFrontElement(3, 6));
	front.addNetwork(createParetoFrontElement(2, 5));
	front.addNetwork(createParetoFrontElement(3, 5));
	front.addNetwork(createParetoFrontElement(2, 6));
	front.addNetwork(createParetoFrontElement(2, 2));
	front.addNetwork(createParetoFrontElement(3, 5));
	front.addNetwork(createParetoFrontElement(3, 1));
	front.addNetwork(createParetoFrontElement(2, 5));
	front.addNetwork(createParetoFrontElement(3, 2));
	front.addNetwork(createParetoFrontElement(1, 3));
	front.addNetwork(createParetoFrontElement(1, 2));
	
	//when
	front.sortFront();
	
	//then
	ParetoFront expectedFront = ParetoFront();
	expectedFront.addNetwork(createParetoFrontElement(1, 4));
	expectedFront.addNetwork(createParetoFrontElement(1, 4));
	expectedFront.addNetwork(createParetoFrontElement(1, 3));
	expectedFront.addNetwork(createParetoFrontElement(1, 2));
	expectedFront.addNetwork(createParetoFrontElement(1, 1));
	expectedFront.addNetwork(createParetoFrontElement(2, 6));
	expectedFront.addNetwork(createParetoFrontElement(2, 6));
	expectedFront.addNetwork(createParetoFrontElement(2, 5));
	expectedFront.addNetwork(createParetoFrontElement(2, 5));
	expectedFront.addNetwork(createParetoFrontElement(2, 5));
	expectedFront.addNetwork(createParetoFrontElement(2, 2));
	expectedFront.addNetwork(createParetoFrontElement(2, 1));
	expectedFront.addNetwork(createParetoFrontElement(3, 6));
	expectedFront.addNetwork(createParetoFrontElement(3, 6));
	expectedFront.addNetwork(createParetoFrontElement(3, 5));
	expectedFront.addNetwork(createParetoFrontElement(3, 5));
	expectedFront.addNetwork(createParetoFrontElement(3, 5));
	expectedFront.addNetwork(createParetoFrontElement(3, 2));
	expectedFront.addNetwork(createParetoFrontElement(3, 1));
	
	EXPECT_TRUE(expectedFront._Equal(front));

	//clean
	for (std::vector<std::pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>::iterator it = front.begin(); it != front.end(); ++it)
	{
		delete (*it)->first;
	}
}
