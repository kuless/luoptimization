#include "pch.h"
#include "../LUOptimization/HyperVolume.h"
#include "../LUOptimization/MultiCriteriaNetwork.h"
#include "TestNetworkCreator.h"

using namespace std;

TEST(HyperVolumeTest, getQualityOfFront)
{
	//given
	TestNetworkCreator networkCreator;
	vector<AbstractNetwork<MultiCriteriaValue>*>* population = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	population->push_back(networkCreator.createSimpleNetwork(1, 9, 1));
	population->push_back(networkCreator.createSimpleNetwork(2, 5, 1));
	population->push_back(networkCreator.createSimpleNetwork(3, 4, 1));
	population->push_back(networkCreator.createSimpleNetwork(4, 3, 1));
	population->push_back(networkCreator.createSimpleNetwork(7, 1, 1));
	population->push_back(networkCreator.createSimpleNetwork(5, 6, 2));
	population->push_back(networkCreator.createSimpleNetwork(6, 5, 2));
	HyperVolume hyperVolume;

	//when
	QualityValue* result = hyperVolume.getQuality(population);

	//then
	std::string expected = "340282366920938463389587631136930004975";
	EXPECT_EQ(result->getQuality(), expected);

	//clean
	for (vector<AbstractNetwork<MultiCriteriaValue>*>::iterator it = population->begin(); it != population->end(); ++it)
	{
		delete (*it);
	}

	delete population;
}

TEST(HyperVolumeTest, getQualityOfOneElement)
{
	//given
	TestNetworkCreator networkCreator;
	vector<AbstractNetwork<MultiCriteriaValue>*>* population = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	population->push_back(networkCreator.createSimpleNetwork(1, 9, 1));
	HyperVolume hyperVolume;

	//when
	QualityValue* result = hyperVolume.getQuality(population);

	//then
	std::string expected = "340282366920938463242013678547253592084";
	EXPECT_EQ(result->getQuality(), expected);

	//clean
	for (vector<AbstractNetwork<MultiCriteriaValue>*>::iterator it = population->begin(); it != population->end(); ++it)
	{
		delete (*it);
	}

	delete population;
}
