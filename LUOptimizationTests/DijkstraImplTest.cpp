#include "pch.h"
#include "../LUOptimization/DijkstraImpl.h"
#include "TestNetworkCreator.h"

using namespace std;

TEST(DijkstraImplTest, findShortestPath)
{
	//given
	TestNetworkCreator test;
	DijkstraImpl* dijkstraImpl = new DijkstraImpl(test.getStationsVector());

	//when
	vector<Station*>* result = dijkstraImpl->dijkstra(test.getStationsVector()->at(13), test.getStationsVector()->at(20));

	//then
	EXPECT_EQ(5, result->size());

	EXPECT_EQ(test.getStationsVector()->at(13), result->at(0));
	EXPECT_EQ(test.getStationsVector()->at(4), result->at(1));
	EXPECT_EQ(test.getStationsVector()->at(12), result->at(2));
	EXPECT_EQ(test.getStationsVector()->at(19), result->at(3));
	EXPECT_EQ(test.getStationsVector()->at(20), result->at(4));

	//clean
	delete dijkstraImpl;
}
