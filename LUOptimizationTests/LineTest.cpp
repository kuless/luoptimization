#include "pch.h"
#include "../LUOptimization/Line.h"

using namespace std;

TEST(LineTest, addStopTest)
{
	//given
	Line line = Line();
	Station* firstStation = new Station("1", "Name", 12, 12);
	int firstJourneyTime = 10;
	Station* secondStation = new Station("2", "Name", 12, 12);
	int secondJourneyTime = 10;

	//when
	line.addStop(firstStation, firstJourneyTime);
	line.addStop(secondStation, secondJourneyTime);

	//then
	int exptedNumberOfStops = 2;
	EXPECT_EQ(exptedNumberOfStops, line.getStops().size());
	EXPECT_EQ(exptedNumberOfStops, line.getJourneyTime().size());

	EXPECT_EQ(firstStation, line.getStops()[0]);
	EXPECT_EQ(secondStation, line.getStops()[1]);

	EXPECT_EQ(firstJourneyTime, line.getJourneyTime()[0]);
	EXPECT_EQ(secondJourneyTime, line.getJourneyTime()[1]);

	//clean
	delete firstStation;
	delete secondStation;
}

TEST(LineTest, deleteLastStopTest)
{
	//given
	Line line = Line();
	Station* firstStation = new Station("1", "Name", 12, 12);
	int firstJourneyTime = 10;
	Station* secondStation = new Station("2", "Name", 12, 12);
	int secondJourneyTime = 10;

	//when
	line.addStop(firstStation, firstJourneyTime);
	line.addStop(secondStation, secondJourneyTime);
	line.deleteLastStop();

	//then
	int exptedNumberOfStops = 1;
	EXPECT_EQ(exptedNumberOfStops, line.getStops().size());
	EXPECT_EQ(exptedNumberOfStops, line.getJourneyTime().size());

	EXPECT_EQ(firstStation, line.getStops()[0]);

	EXPECT_EQ(firstJourneyTime, line.getJourneyTime()[0]);

	//clean
	delete firstStation;
	delete secondStation;
}

TEST(LineTest, deleteLastStopListEmptyTest)
{
	//given
	Line line = Line();

	//when
	line.deleteLastStop();

	//then
	int exptedNumberOfStops = 0;
	EXPECT_EQ(exptedNumberOfStops, line.getStops().size());
	EXPECT_EQ(exptedNumberOfStops, line.getJourneyTime().size());
}

TEST(LineTest, cointainsStopTrueTest)
{
	//given
	vector<Station*>* stops = new vector<Station*>();
	stops->push_back(new Station("1", "A", 1, 1));
	stops->push_back(new Station("2", "B", 1, 1));
	stops->push_back(new Station("3", "C", 1, 1));
	Line line = Line(1, "", stops);
	Station* station = new Station("2", "B", 1, 1);

	//when
	bool result = line.cointainsStop(station);

	//then
	EXPECT_TRUE(result);

	//clean
	for (int i = 0; stops->size() > i; i++)
	{
		delete (*stops)[i];
	}
	delete station;
}

TEST(LineTest, cointainsStopFalseTest)
{
	//given
	vector<Station*>* stops = new vector<Station*>();
	stops->push_back(new Station("1", "A", 1, 1));
	stops->push_back(new Station("2", "B", 1, 1));
	stops->push_back(new Station("3", "C", 1, 1));
	Line line = Line(1, "", stops);
	Station* station = new Station("8", "B", 1, 1);

	//when
	bool result = line.cointainsStop(station);

	//then
	EXPECT_FALSE(result);

	//clean
	for (int i = 0; stops->size() > i; i++)
	{
		delete (*stops)[i];
	}
	delete station;
}

TEST(LineTest, containsDuplicatesTrueTest)
{
	//given
	vector<Station*>* stops = new vector<Station*>();
	Station* station1 = new Station("1", "A", 1, 1);
	Station* station2 = new Station("2", "B", 1, 1);
	Station* station3 = new Station("3", "C", 1, 1);
	stops->push_back(station1);
	stops->push_back(station3);
	stops->push_back(station2);
	stops->push_back(station3);
	Line line = Line(1, "", stops);

	//when
	bool result = line.containsDuplicates();

	//then
	EXPECT_TRUE(result);

	//clean
	delete station1;
	delete station2;
	delete station3;
}

TEST(LineTest, containsDuplicatesFalseTest)
{
	//given
	vector<Station*>* stops = new vector<Station*>();
	Station* station1 = new Station("1", "A", 1, 1);
	Station* station2 = new Station("2", "B", 1, 1);
	Station* station3 = new Station("3", "C", 1, 1);
	stops->push_back(station1);
	stops->push_back(station2);
	stops->push_back(station3);
	Line line = Line(1, "", stops);

	//when
	bool result = line.containsDuplicates();

	//then
	EXPECT_FALSE(result);

	//clean
	delete station1;
	delete station2;
	delete station3;
}

TEST(LineTest, updateTimetableTest)
{
	//given
	vector<Station*>* stops = new vector<Station*>();
	Station* station1 = new Station("1", "A", 1, 1);
	Station* station2 = new Station("2", "B", 1, 1);
	Station* station3 = new Station("3", "C", 1, 1);
	Station* station4 = new Station("4", "D", 1, 1);
	Station* station5 = new Station("5", "E", 1, 1);

	station1->addConnection(new Connection(10, 1.0, station2));
	station2->addConnection(new Connection(10, 1.0, station3));
	station3->addConnection(new Connection(10, 1.0, station4));
	station4->addConnection(new Connection(10, 1.0, station5));

	stops->push_back(station1);
	stops->push_back(station2);
	stops->push_back(station3);
	stops->push_back(station4);
	stops->push_back(station5);
	Line line = Line(1, "", stops);

	//when
	line.updateJourneyTimeAndLenght();

	//then
	EXPECT_EQ(4, line.getJourneyTime().size());

	for (int i = 0; i < line.getJourneyTime().size(); i++)
	{
		EXPECT_EQ(10, line.getJourneyTime()[i]);
	}

	//clean
	delete station1;
	delete station2;
	delete station3;
	delete station4;
	delete station5;
}

TEST(LineTest, updateTimetableGraphErrorTest)
{
	//given
	vector<Station*>* stops = new vector<Station*>();
	Station* station1 = new Station("1", "A", 1, 1);
	Station* station2 = new Station("2", "B", 1, 1);
	Station* station3 = new Station("3", "C", 1, 1);
	Station* station4 = new Station("4", "D", 1, 1);
	Station* station5 = new Station("5", "E", 1, 1);

	station1->addConnection(new Connection(10, 1.0, station2));
	station2->addConnection(new Connection(10, 1.0, station3));
	station4->addConnection(new Connection(10, 1.0, station5));

	stops->push_back(station1);
	stops->push_back(station2);
	stops->push_back(station3);
	stops->push_back(station4);
	stops->push_back(station5);
	Line line = Line(1, "", stops);

	//when
	line.updateJourneyTimeAndLenght();

	//then
	EXPECT_EQ(4, line.getJourneyTime().size());
	EXPECT_EQ(10, line.getJourneyTime()[0]);
	EXPECT_EQ(10, line.getJourneyTime()[1]);
	EXPECT_EQ(1, line.getJourneyTime()[2]);
	EXPECT_EQ(10, line.getJourneyTime()[3]);

	//clean
	delete station1;
	delete station2;
	delete station3;
	delete station4;
	delete station5;
}

TEST(LineTest, containsDuplicatesAndReturnBeginAndEndOfDuplicates)
{
	//given
	vector<Station*>* stops = new vector<Station*>();
	Station* station1 = new Station("1", "A", 1, 1);
	Station* station2 = new Station("2", "B", 1, 1);
	Station* station3 = new Station("3", "C", 1, 1);
	Station* station4 = new Station("4", "D", 1, 1);
	Station* station5 = new Station("5", "E", 1, 1);
	Station* station6 = new Station("6", "F", 1, 1);
	Station* station7 = new Station("7", "G", 1, 1);

	stops->push_back(station1);
	stops->push_back(station2);
	stops->push_back(station3);
	stops->push_back(station4);
	stops->push_back(station3);
	stops->push_back(station2);
	stops->push_back(station3);
	stops->push_back(station6);
	stops->push_back(station7);
	Line line = Line(1, "", stops);

	//when
	pair<int, int> result = line.getBeginAndEndOfDuplicates();

	//then
	EXPECT_EQ(1, result.first);
	EXPECT_EQ(6, result.second);

	//clean
	delete station1;
	delete station2;
	delete station3;
	delete station4;
	delete station5;
	delete station6;
	delete station7;
}

TEST(LineTest, containsNoDuplicatesAndReturnBeginAndEndOfDuplicates)
{
	//given
	vector<Station*>* stops = new vector<Station*>();
	Station* station1 = new Station("1", "A", 1, 1);
	Station* station2 = new Station("2", "B", 1, 1);
	Station* station3 = new Station("3", "C", 1, 1);
	Station* station4 = new Station("4", "D", 1, 1);
	Station* station5 = new Station("5", "E", 1, 1);
	Station* station6 = new Station("6", "F", 1, 1);
	Station* station7 = new Station("7", "G", 1, 1);

	stops->push_back(station1);
	stops->push_back(station2);
	stops->push_back(station3);
	stops->push_back(station4);
	stops->push_back(station5);
	stops->push_back(station6);
	stops->push_back(station7);
	Line line = Line(1, "", stops);

	//when
	pair<int, int> result = line.getBeginAndEndOfDuplicates();

	//then
	EXPECT_EQ(INT_MAX, result.first);
	EXPECT_EQ(INT_MAX, result.second);

	//clean
	delete station1;
	delete station2;
	delete station3;
	delete station4;
	delete station5;
	delete station6;
	delete station7;
}
