#include "pch.h"
#include "../LUOptimization/ElitistArchive.h"
#include "../LUOptimization/MultiCriteriaNetwork.h"
#include "QualityMeasuresMock.h"
#include "TestNetworkCreator.h"

using namespace std;

vector<Line*>* getLines(TestNetworkCreator& networkCreator, int timetableStop)
{
	vector<Station*>* stops = new vector<Station*>();
	stops->push_back(networkCreator.getStationsVector()->at(0));
	vector<int> timetable;
	timetable.push_back(timetableStop);
	Line* line = new Line(1, "", stops, timetable);
	vector<Line*>* lines = new vector<Line*>();
	lines->push_back(line);
	return lines;
}

TEST(ElitistArchiveTest, addToArchiveOneGeneration)
{
	//given
	TestNetworkCreator networkCreator;
	vector<AbstractNetwork<MultiCriteriaValue>*>* firstGeneration = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	firstGeneration->push_back(networkCreator.createSimpleNetwork(1, 9, 1, 1));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(2, 5, 1, 2));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(3, 4, 1, 3));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(4, 3, 1, 4));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(7, 1, 1, 5));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(5, 6, 2, 6));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(6, 5, 2, 7));
	QualityMeasuresMock* mock = new QualityMeasuresMock();
	ElitistArchive elitistArchive = ElitistArchive(mock);

	//when
	elitistArchive.addToArchive(firstGeneration);

	//then
	vector<AbstractNetwork<MultiCriteriaValue>*>* archive = elitistArchive.getArchive();
	int expectedArchiveSize = 5;

	EXPECT_EQ(archive->size(), expectedArchiveSize);
	EXPECT_TRUE(archive->at(0)->_Equal(firstGeneration->at(0)));
	EXPECT_TRUE(archive->at(1)->_Equal(firstGeneration->at(1)));
	EXPECT_TRUE(archive->at(2)->_Equal(firstGeneration->at(2)));
	EXPECT_TRUE(archive->at(3)->_Equal(firstGeneration->at(3)));
	EXPECT_TRUE(archive->at(4)->_Equal(firstGeneration->at(4)));

	//clean
	for (int i = 0; i < firstGeneration->size() ; i++)
	{
		delete firstGeneration->at(i);
	}

	delete firstGeneration;
}

TEST(ElitistArchiveTest, addToArchiveTwoGenerationsSameFronts)
{
	//given
	TestNetworkCreator networkCreator;
	vector<AbstractNetwork<MultiCriteriaValue>*>* firstGeneration = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	firstGeneration->push_back(networkCreator.createSimpleNetwork(1, 9, 1, 1));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(2, 5, 1, 2));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(3, 4, 1, 3));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(4, 3, 1, 4));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(7, 1, 1, 5));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(5, 6, 2, 6));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(6, 5, 2, 7));
	vector<AbstractNetwork<MultiCriteriaValue>*>* secondGeneration = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	secondGeneration->push_back(networkCreator.createSimpleNetwork(1, 9, 1, 1));
	secondGeneration->push_back(networkCreator.createSimpleNetwork(2, 5, 1, 2));
	secondGeneration->push_back(networkCreator.createSimpleNetwork(3, 4, 1, 3));
	secondGeneration->push_back(networkCreator.createSimpleNetwork(4, 3, 1, 4));
	secondGeneration->push_back(networkCreator.createSimpleNetwork(7, 1, 1, 5));
	secondGeneration->push_back(networkCreator.createSimpleNetwork(5, 6, 2, 6));
	secondGeneration->push_back(networkCreator.createSimpleNetwork(6, 5, 2, 7));
	QualityMeasuresMock* mock = new QualityMeasuresMock();
	ElitistArchive elitistArchive = ElitistArchive(mock);

	//when
	elitistArchive.addToArchive(firstGeneration);
	elitistArchive.addToArchive(secondGeneration);

	//then
	vector<AbstractNetwork<MultiCriteriaValue>*>* archive = elitistArchive.getArchive();
	int expectedArchiveSize = 5;

	EXPECT_EQ(archive->size(), expectedArchiveSize);
	EXPECT_TRUE(archive->at(0)->_Equal(firstGeneration->at(0)));
	EXPECT_TRUE(archive->at(1)->_Equal(firstGeneration->at(1)));
	EXPECT_TRUE(archive->at(2)->_Equal(firstGeneration->at(2)));
	EXPECT_TRUE(archive->at(3)->_Equal(firstGeneration->at(3)));
	EXPECT_TRUE(archive->at(4)->_Equal(firstGeneration->at(4)));

	//clean
	for (int i = 0; i < firstGeneration->size(); i++)
	{
		delete firstGeneration->at(i);
	}

	delete firstGeneration;
}

TEST(ElitistArchiveTest, addToArchiveTwoGenerationsDifferentFronts)
{
	//given
	TestNetworkCreator networkCreator;
	vector<AbstractNetwork<MultiCriteriaValue>*>* firstGeneration = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	firstGeneration->push_back(networkCreator.createSimpleNetwork(1, 9, 1, 1));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(2, 5, 1, 2));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(3, 4, 1, 3));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(4, 3, 1, 4));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(7, 1, 1, 5));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(5, 6, 2, 6));
	firstGeneration->push_back(networkCreator.createSimpleNetwork(6, 5, 2, 7));
	vector<AbstractNetwork<MultiCriteriaValue>*>* secondGeneration = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	secondGeneration->push_back(networkCreator.createSimpleNetwork(1, 4, 1, 8));
	secondGeneration->push_back(networkCreator.createSimpleNetwork(4, 3, 1, 4));
	secondGeneration->push_back(networkCreator.createSimpleNetwork(9, 1, 1, 9));
	secondGeneration->push_back(networkCreator.createSimpleNetwork(5, 6, 2, 6));
	secondGeneration->push_back(networkCreator.createSimpleNetwork(6, 5, 2, 7));
	QualityMeasuresMock* mock = new QualityMeasuresMock();
	ElitistArchive elitistArchive = ElitistArchive(mock);

	//when
	elitistArchive.addToArchive(firstGeneration);
	elitistArchive.addToArchive(secondGeneration);

	//then
	vector<AbstractNetwork<MultiCriteriaValue>*>* archive = elitistArchive.getArchive();
	int expectedArchiveSize = 3;

	EXPECT_EQ(archive->size(), expectedArchiveSize);
	EXPECT_TRUE(archive->at(0)->_Equal(secondGeneration->at(0)));
	EXPECT_TRUE(archive->at(1)->_Equal(firstGeneration->at(3)));
	EXPECT_TRUE(archive->at(2)->_Equal(firstGeneration->at(4)));

	//clean
	for (int i = 0; i < firstGeneration->size(); i++)
	{
		delete firstGeneration->at(i);
	}

	delete firstGeneration;
}
