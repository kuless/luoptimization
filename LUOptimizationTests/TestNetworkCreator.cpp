#include "pch.h"
#include "TestNetworkCreator.h"

using namespace std;

TestNetworkCreator::TestNetworkCreator()
{
	createStationsVector();
	createConnectionsBetweenStations();
	createNetwork(getLines());
	createPairToCrossovers(getLines());
}

TestNetworkCreator::~TestNetworkCreator()
{
	for (vector<Station*>::iterator it = stationsVector->begin(); it != stationsVector->end(); ++it)
	{
		delete (*it);
	}

	delete stationsVector;
	delete pairToCrossovers->first;
	delete pairToCrossovers->second;
	delete pairToCrossovers;
	delete network;
}

vector<Station*>* TestNetworkCreator::getStationsVector()
{
	return stationsVector;
}

AbstractNetwork<MultiCriteriaValue>* TestNetworkCreator::getNetwork()
{
	return network;
}

pair<AbstractNetwork<MultiCriteriaValue>*, AbstractNetwork<MultiCriteriaValue>*>* TestNetworkCreator::getPairToCrossovers()
{
	return pairToCrossovers;
}

AbstractNetwork<MultiCriteriaValue>* TestNetworkCreator::createSimpleNetwork(unsigned long long int passengersValue, unsigned long long int maintenanceValue, int paretoFront)
{
	return createSimpleNetwork(passengersValue, maintenanceValue, paretoFront, 1);
}

AbstractNetwork<MultiCriteriaValue>* TestNetworkCreator::createSimpleNetwork(unsigned long long int passengersValue, unsigned long long int maintenanceValue, int paretoFront, int timetableStop)
{
	MultiCriteriaNetwork* network = new MultiCriteriaNetwork(getLines(timetableStop));
	network->setValue(createValue(passengersValue, maintenanceValue, paretoFront));
	return network;
}

MultiCriteriaValue* TestNetworkCreator::createValue(unsigned long long int passengersValue, unsigned long long int maintenanceValue, int paretoFront)
{
	MultiCriteriaValue* value = new MultiCriteriaValue();
	value->setPassengersValue(passengersValue);
	value->setMaintenanceValue(maintenanceValue);
	value->setParetoFront(paretoFront);
	return value;
}

vector<Line*>* TestNetworkCreator::getLines(int timetableStop)
{
	vector<Station*>* stops = new vector<Station*>();
	stops->push_back(stationsVector->at(0));
	vector<int> timetable;
	timetable.push_back(timetableStop);
	Line* line = new Line(1, "", stops, timetable);
	vector<Line*>* lines = new vector<Line*>();
	lines->push_back(line);
	return lines;
}

void TestNetworkCreator::createStationsVector()
{
	stationsVector = new vector<Station*>();
	stationsVector->push_back(new Station("0", "name", 1.0, 1.0, 0));
	stationsVector->push_back(new Station("1", "name", 1.0, 1.0, 1));
	stationsVector->push_back(new Station("2", "name", 1.0, 1.0, 2));
	stationsVector->push_back(new Station("3", "name", 1.0, 1.0, 3));
	stationsVector->push_back(new Station("4", "name", 1.0, 1.0, 4));
	stationsVector->push_back(new Station("5", "name", 1.0, 1.0, 5));
	stationsVector->push_back(new Station("6", "name", 1.0, 1.0, 6));
	stationsVector->push_back(new Station("7", "name", 1.0, 1.0, 7));
	stationsVector->push_back(new Station("8", "name", 1.0, 1.0, 8));
	stationsVector->push_back(new Station("9", "name", 1.0, 1.0, 9));
	stationsVector->push_back(new Station("10", "name", 1.0, 1.0, 10));
	stationsVector->push_back(new Station("11", "name", 1.0, 1.0, 11));
	stationsVector->push_back(new Station("12", "name", 1.0, 1.0, 12));
	stationsVector->push_back(new Station("13", "name", 1.0, 1.0, 13));
	stationsVector->push_back(new Station("14", "name", 1.0, 1.0, 14));
	stationsVector->push_back(new Station("15", "name", 1.0, 1.0, 15));
	stationsVector->push_back(new Station("16", "name", 1.0, 1.0, 16));
	stationsVector->push_back(new Station("17", "name", 1.0, 1.0, 17));
	stationsVector->push_back(new Station("18", "name", 1.0, 1.0, 18));
	stationsVector->push_back(new Station("19", "name", 1.0, 1.0, 19));
	stationsVector->push_back(new Station("20", "name", 1.0, 1.0, 20));
	stationsVector->push_back(new Station("21", "name", 1.0, 1.0, 21));
	stationsVector->push_back(new Station("22", "name", 1.0, 1.0, 22));
	stationsVector->push_back(new Station("23", "name", 1.0, 1.0, 23));
	stationsVector->push_back(new Station("24", "name", 1.0, 1.0, 24));
	stationsVector->push_back(new Station("25", "name", 1.0, 1.0, 25));
	stationsVector->push_back(new Station("26", "name", 1.0, 1.0, 26));
	stationsVector->push_back(new Station("27", "name", 1.0, 1.0, 27));
	stationsVector->push_back(new Station("28", "name", 1.0, 1.0, 28));
	stationsVector->push_back(new Station("29", "name", 1.0, 1.0, 29));
	stationsVector->push_back(new Station("30", "name", 1.0, 1.0, 30));
}

void TestNetworkCreator::createConnectionsBetweenStations()
{
	stationsVector->at(0)->addConnection(new Connection(2, 2.5, stationsVector->at(8)));

	stationsVector->at(1)->addConnection(new Connection(1, 2.0, stationsVector->at(2)));

	stationsVector->at(2)->addConnection(new Connection(1, 2.0, stationsVector->at(1)));
	stationsVector->at(2)->addConnection(new Connection(1, 2.0, stationsVector->at(3)));

	stationsVector->at(3)->addConnection(new Connection(1, 2.0, stationsVector->at(2)));
	stationsVector->at(3)->addConnection(new Connection(2, 2.5, stationsVector->at(4)));

	stationsVector->at(4)->addConnection(new Connection(2, 2.5, stationsVector->at(3)));
	stationsVector->at(4)->addConnection(new Connection(2, 2.5, stationsVector->at(12)));
	stationsVector->at(4)->addConnection(new Connection(2, 2.5, stationsVector->at(13)));
	stationsVector->at(4)->addConnection(new Connection(2, 2.5, stationsVector->at(5)));

	stationsVector->at(5)->addConnection(new Connection(2, 2.5, stationsVector->at(4)));
	stationsVector->at(5)->addConnection(new Connection(1, 2.0, stationsVector->at(6)));

	stationsVector->at(6)->addConnection(new Connection(1, 2.0, stationsVector->at(5)));
	stationsVector->at(6)->addConnection(new Connection(2, 2.5, stationsVector->at(22)));
	stationsVector->at(6)->addConnection(new Connection(2, 2.5, stationsVector->at(7)));

	stationsVector->at(7)->addConnection(new Connection(2, 2.5, stationsVector->at(14)));
	stationsVector->at(7)->addConnection(new Connection(2, 2.5, stationsVector->at(6)));
	stationsVector->at(7)->addConnection(new Connection(2, 2.5, stationsVector->at(8)));
	stationsVector->at(7)->addConnection(new Connection(2, 2.5, stationsVector->at(15)));

	stationsVector->at(8)->addConnection(new Connection(2, 2.5, stationsVector->at(7)));
	stationsVector->at(8)->addConnection(new Connection(2, 2.5, stationsVector->at(0)));
	stationsVector->at(8)->addConnection(new Connection(2, 2.5, stationsVector->at(9)));

	stationsVector->at(9)->addConnection(new Connection(2, 2.5, stationsVector->at(8)));
	stationsVector->at(9)->addConnection(new Connection(2, 2.5, stationsVector->at(16)));
	stationsVector->at(9)->addConnection(new Connection(2, 2.5, stationsVector->at(17)));
	stationsVector->at(9)->addConnection(new Connection(2, 2.5, stationsVector->at(10)));

	stationsVector->at(10)->addConnection(new Connection(2, 2.5, stationsVector->at(9)));

	stationsVector->at(11)->addConnection(new Connection(1, 2.0, stationsVector->at(12)));

	stationsVector->at(12)->addConnection(new Connection(1, 2.0, stationsVector->at(11)));
	stationsVector->at(12)->addConnection(new Connection(1, 2.0, stationsVector->at(19)));
	stationsVector->at(12)->addConnection(new Connection(2, 2.5, stationsVector->at(4)));

	stationsVector->at(13)->addConnection(new Connection(2, 2.5, stationsVector->at(4)));
	stationsVector->at(13)->addConnection(new Connection(1, 2.0, stationsVector->at(14)));

	stationsVector->at(14)->addConnection(new Connection(1, 2.0, stationsVector->at(13)));
	stationsVector->at(14)->addConnection(new Connection(2, 2.5, stationsVector->at(7)));

	stationsVector->at(15)->addConnection(new Connection(2, 2.5, stationsVector->at(7)));
	stationsVector->at(15)->addConnection(new Connection(1, 2.0, stationsVector->at(16)));

	stationsVector->at(16)->addConnection(new Connection(1, 2.0, stationsVector->at(15)));
	stationsVector->at(16)->addConnection(new Connection(1, 2.0, stationsVector->at(26)));
	stationsVector->at(16)->addConnection(new Connection(2, 2.5, stationsVector->at(9)));

	stationsVector->at(17)->addConnection(new Connection(2, 2.5, stationsVector->at(9)));

	stationsVector->at(18)->addConnection(new Connection(1, 2.0, stationsVector->at(19)));

	stationsVector->at(19)->addConnection(new Connection(1, 2.0, stationsVector->at(18)));
	stationsVector->at(19)->addConnection(new Connection(1, 2.0, stationsVector->at(12)));
	stationsVector->at(19)->addConnection(new Connection(1, 2.0, stationsVector->at(20)));

	stationsVector->at(20)->addConnection(new Connection(1, 2.0, stationsVector->at(19)));
	stationsVector->at(20)->addConnection(new Connection(1, 2.0, stationsVector->at(21)));

	stationsVector->at(21)->addConnection(new Connection(1, 2.0, stationsVector->at(20)));
	stationsVector->at(21)->addConnection(new Connection(1, 2.0, stationsVector->at(22)));

	stationsVector->at(22)->addConnection(new Connection(1, 2.0, stationsVector->at(21)));
	stationsVector->at(22)->addConnection(new Connection(2, 2.5, stationsVector->at(6)));
	stationsVector->at(22)->addConnection(new Connection(1, 2.0, stationsVector->at(23)));

	stationsVector->at(23)->addConnection(new Connection(1, 2.0, stationsVector->at(22)));
	stationsVector->at(23)->addConnection(new Connection(1, 2.0, stationsVector->at(24)));

	stationsVector->at(24)->addConnection(new Connection(1, 2.0, stationsVector->at(23)));
	stationsVector->at(24)->addConnection(new Connection(1, 2.0, stationsVector->at(25)));

	stationsVector->at(25)->addConnection(new Connection(1, 2.0, stationsVector->at(24)));
	stationsVector->at(25)->addConnection(new Connection(1, 2.0, stationsVector->at(26)));

	stationsVector->at(26)->addConnection(new Connection(1, 2.0, stationsVector->at(25)));
	stationsVector->at(26)->addConnection(new Connection(1, 2.0, stationsVector->at(16)));
	stationsVector->at(26)->addConnection(new Connection(1, 2.0, stationsVector->at(27)));

	stationsVector->at(27)->addConnection(new Connection(1, 2.0, stationsVector->at(26)));
	stationsVector->at(27)->addConnection(new Connection(1, 2.0, stationsVector->at(28)));

	stationsVector->at(28)->addConnection(new Connection(1, 2.0, stationsVector->at(27)));
	stationsVector->at(28)->addConnection(new Connection(1, 2.0, stationsVector->at(29)));
	stationsVector->at(28)->addConnection(new Connection(1, 2.0, stationsVector->at(30)));

	stationsVector->at(29)->addConnection(new Connection(1, 2.0, stationsVector->at(28)));

	stationsVector->at(30)->addConnection(new Connection(1, 2.0, stationsVector->at(28)));
}

std::vector<Line*>* TestNetworkCreator::getLines()
{
	vector<Station*>* line1 = new vector<Station*>();
	line1->push_back(stationsVector->at(1));
	line1->push_back(stationsVector->at(2));
	line1->push_back(stationsVector->at(3));
	line1->push_back(stationsVector->at(4));
	line1->push_back(stationsVector->at(5));
	line1->push_back(stationsVector->at(6));
	line1->push_back(stationsVector->at(7));
	line1->push_back(stationsVector->at(8));
	line1->push_back(stationsVector->at(9));
	line1->push_back(stationsVector->at(10));
	vector<int> line1Timetable;
	line1Timetable.push_back(1);
	line1Timetable.push_back(2);
	line1Timetable.push_back(3);
	line1Timetable.push_back(4);
	line1Timetable.push_back(5);
	line1Timetable.push_back(6);
	line1Timetable.push_back(7);
	line1Timetable.push_back(8);
	line1Timetable.push_back(9);
	line1Timetable.push_back(10);

	vector<Station*>* line2 = new vector<Station*>();
	line2->push_back(stationsVector->at(11));
	line2->push_back(stationsVector->at(12));
	line2->push_back(stationsVector->at(4));
	line2->push_back(stationsVector->at(13));
	line2->push_back(stationsVector->at(14));
	line2->push_back(stationsVector->at(7));
	line2->push_back(stationsVector->at(15));
	line2->push_back(stationsVector->at(16));
	line2->push_back(stationsVector->at(9));
	line2->push_back(stationsVector->at(17));
	vector<int> line2Timetable;
	line2Timetable.push_back(11);
	line2Timetable.push_back(12);
	line2Timetable.push_back(13);
	line2Timetable.push_back(14);
	line2Timetable.push_back(15);
	line2Timetable.push_back(16);
	line2Timetable.push_back(17);
	line2Timetable.push_back(18);
	line2Timetable.push_back(19);
	line2Timetable.push_back(20);
	line2Timetable.push_back(30);
	line2Timetable.push_back(40);

	vector<Station*>* line3 = new vector<Station*>();
	line3->push_back(stationsVector->at(0));
	line3->push_back(stationsVector->at(8));
	line3->push_back(stationsVector->at(7));
	line3->push_back(stationsVector->at(6));
	line3->push_back(stationsVector->at(22));
	vector<int> line3Timetable;
	line3Timetable.push_back(200);
	line3Timetable.push_back(300);
	line3Timetable.push_back(400);

	vector<Station*>* line4 = new vector<Station*>();
	line4->push_back(stationsVector->at(18));
	line4->push_back(stationsVector->at(19));
	line4->push_back(stationsVector->at(20));
	line4->push_back(stationsVector->at(21));
	line4->push_back(stationsVector->at(22));
	vector<int> line4Timetable;
	line4Timetable.push_back(220);
	line4Timetable.push_back(330);
	line4Timetable.push_back(440);

	vector<Station*>* line5 = new vector<Station*>();
	line5->push_back(stationsVector->at(23));
	line5->push_back(stationsVector->at(24));
	line5->push_back(stationsVector->at(25));
	line5->push_back(stationsVector->at(26));
	vector<int> line5Timetable;
	line5Timetable.push_back(100);
	line5Timetable.push_back(300);
	line5Timetable.push_back(700);
	line5Timetable.push_back(900);
	line5Timetable.push_back(990);

	vector<Station*>* line6 = new vector<Station*>();
	line6->push_back(stationsVector->at(7));
	line6->push_back(stationsVector->at(14));
	line6->push_back(stationsVector->at(13));
	line6->push_back(stationsVector->at(4));
	line6->push_back(stationsVector->at(3));
	line6->push_back(stationsVector->at(2));
	vector<int> line6Timetable;
	line6Timetable.push_back(14);
	line6Timetable.push_back(15);
	line6Timetable.push_back(16);
	line6Timetable.push_back(17);
	line6Timetable.push_back(18);
	line6Timetable.push_back(100);
	line6Timetable.push_back(300);
	line6Timetable.push_back(700);
	line6Timetable.push_back(900);
	line6Timetable.push_back(990);

	vector<Line*>* lines = new vector<Line*>();
	lines->push_back(new Line(1, "", line1, line1Timetable));
	lines->push_back(new Line(2, "", line2, line2Timetable));
	lines->push_back(new Line(3, "", line3, line3Timetable));
	lines->push_back(new Line(4, "", line4, line4Timetable));
	lines->push_back(new Line(5, "", line5, line5Timetable));
	lines->push_back(new Line(6, "", line6, line6Timetable));

	return lines;
}

void TestNetworkCreator::createNetwork(vector<Line*>* lines)
{
	network = new MultiCriteriaNetwork(lines);
}

void TestNetworkCreator::createPairToCrossovers(vector<Line*>* lines)
{
	vector<Line*>* firstNetwork = new vector<Line*>();
	vector<Line*>* secondNetwork = new vector<Line*>();
	firstNetwork->push_back(lines->at(0));
	firstNetwork->push_back(lines->at(2));
	firstNetwork->push_back(lines->at(4));
	firstNetwork->push_back(lines->at(5));
	secondNetwork->push_back(lines->at(1));
	secondNetwork->push_back(lines->at(3));
	delete lines;
	pairToCrossovers = new pair<AbstractNetwork<MultiCriteriaValue>*, AbstractNetwork<MultiCriteriaValue>*>(new MultiCriteriaNetwork(firstNetwork), new MultiCriteriaNetwork(secondNetwork));
}
