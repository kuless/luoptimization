#include "pch.h"
#include "../LUOptimization/AddTravelsMutation.h"
#include "RandomNumberGeneratorMock.h"

using ::testing::Return;

TEST(AddTravelsMutationTest, test)
{
	//given
	RandomNumberGeneratorMock* randomMock = new RandomNumberGeneratorMock();
	AddTravelsMutation* addTravelsMutation = new AddTravelsMutation(randomMock);
	EXPECT_CALL(*randomMock, getInt(_, _))
		.WillOnce(Return(3))
		.WillOnce(Return(10))
		.WillOnce(Return(30))
		.WillOnce(Return(20));
	Station* station = new Station("1", "name", 1.0, 1.0);
	std::vector<Station*>* stops = new std::vector<Station*>();
	stops->push_back(station);
	std::vector<int> timetable;
	timetable.push_back(5);
	Line* line = new Line(1, "name", stops, timetable);

	//when
	addTravelsMutation->mutation(line);

	//then
	EXPECT_EQ(4, line->getTimetableInMinutes().size());
	EXPECT_EQ(5, line->getTimetableInMinutes()[0]);
	EXPECT_EQ(10, line->getTimetableInMinutes()[1]);
	EXPECT_EQ(20, line->getTimetableInMinutes()[2]);
	EXPECT_EQ(30, line->getTimetableInMinutes()[3]);

	//clean
	delete station;
	delete line;
	delete addTravelsMutation;
}
