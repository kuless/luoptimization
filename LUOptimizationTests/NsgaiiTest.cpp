#include "pch.h"
#include "../LUOptimization/Nsgaii.h"
#include "../LUOptimization/MultiCriteriaNetwork.h"
#include "TestNetworkCreator.h"

using namespace std;

void checkResult(AbstractNetwork<MultiCriteriaValue>* network, unsigned long long int expetedPassengersValue, unsigned long long int expetedMaintenanceValue, int expetedParetoFrontNumber, unsigned long long int expetedDistance)
{
	EXPECT_EQ(expetedPassengersValue, network->getValue()->getPassengersValue());
	EXPECT_EQ(expetedMaintenanceValue, network->getValue()->getMaintenanceValue());
	EXPECT_EQ(expetedParetoFrontNumber, network->getValue()->getParetoFront());
	EXPECT_EQ(expetedDistance, network->getValue()->getDistance());
}

TEST(NsgaiiTest, testFirstEvaluation)
{
	//given
	TestNetworkCreator creator;
	vector<AbstractNetwork<MultiCriteriaValue>*>* population = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	population->push_back(creator.createSimpleNetwork(1, 7, 0));
	population->push_back(creator.createSimpleNetwork(3, 7, 0));
	population->push_back(creator.createSimpleNetwork(5, 8, 0));
	population->push_back(creator.createSimpleNetwork(6, 7, 0));
	population->push_back(creator.createSimpleNetwork(2, 5, 0));
	population->push_back(creator.createSimpleNetwork(5, 5, 0));
	population->push_back(creator.createSimpleNetwork(7, 6, 0));
	population->push_back(creator.createSimpleNetwork(3, 4, 0));
	population->push_back(creator.createSimpleNetwork(4, 3, 0));
	population->push_back(creator.createSimpleNetwork(8, 4, 0));
	population->push_back(creator.createSimpleNetwork(7, 2, 0));
	population->push_back(creator.createSimpleNetwork(8, 1, 0));
	Nsgaii nsgaii;

	//when
	nsgaii.evaluate(population);

	//then
	int expetedPopulationSize = 12;
	EXPECT_EQ(expetedPopulationSize, population->size());
	checkResult(population->at(0), 1, 7, 1, ULLONG_MAX);
	checkResult(population->at(1), 2, 5, 1, 5);
	checkResult(population->at(2), 3, 4, 1, 4);
	checkResult(population->at(3), 4, 3, 1, 6);
	checkResult(population->at(4), 7, 2, 1, 6);
	checkResult(population->at(5), 8, 1, 1, ULLONG_MAX);
	checkResult(population->at(6), 3, 7, 2, ULLONG_MAX);
	checkResult(population->at(7), 5, 5, 2, 8);
	checkResult(population->at(8), 8, 4, 2, ULLONG_MAX);
	checkResult(population->at(9), 5, 8, 3, ULLONG_MAX);
	checkResult(population->at(10), 6, 7, 3, 4);
	checkResult(population->at(11), 7, 6, 3, ULLONG_MAX);

	//clean
	for (vector<AbstractNetwork<MultiCriteriaValue>*>::iterator it = population->begin(); it != population->end(); ++it)
	{
		delete (*it);
	}

	delete population;
}

TEST(NsgaiiTest, testNextEvaluation)
{
	//given
	TestNetworkCreator creator;
	vector<AbstractNetwork<MultiCriteriaValue>*>* oldPopulation = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	oldPopulation->push_back(creator.createSimpleNetwork(1, 9, 0));
	oldPopulation->push_back(creator.createSimpleNetwork(2, 9, 0));
	oldPopulation->push_back(creator.createSimpleNetwork(5, 8, 0));
	oldPopulation->push_back(creator.createSimpleNetwork(3, 7, 0));
	oldPopulation->push_back(creator.createSimpleNetwork(6, 7, 0));
	oldPopulation->push_back(creator.createSimpleNetwork(4, 6, 0));
	oldPopulation->push_back(creator.createSimpleNetwork(7, 6, 0));
	oldPopulation->push_back(creator.createSimpleNetwork(5, 5, 0));
	oldPopulation->push_back(creator.createSimpleNetwork(2, 3, 0));
	oldPopulation->push_back(creator.createSimpleNetwork(8, 3, 0));
	oldPopulation->push_back(creator.createSimpleNetwork(10, 2, 0));
	oldPopulation->push_back(creator.createSimpleNetwork(10, 1, 0));

	vector<AbstractNetwork<MultiCriteriaValue>*>* newPopulation = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	newPopulation->push_back(creator.createSimpleNetwork(1, 7, 0));
	newPopulation->push_back(creator.createSimpleNetwork(3, 7, 0));
	newPopulation->push_back(creator.createSimpleNetwork(5, 8, 0));
	newPopulation->push_back(creator.createSimpleNetwork(6, 7, 0));
	newPopulation->push_back(creator.createSimpleNetwork(2, 5, 0));
	newPopulation->push_back(creator.createSimpleNetwork(5, 5, 0));
	newPopulation->push_back(creator.createSimpleNetwork(7, 6, 0));
	newPopulation->push_back(creator.createSimpleNetwork(3, 4, 0));
	newPopulation->push_back(creator.createSimpleNetwork(4, 3, 0));
	newPopulation->push_back(creator.createSimpleNetwork(8, 4, 0));
	newPopulation->push_back(creator.createSimpleNetwork(7, 2, 0));
	newPopulation->push_back(creator.createSimpleNetwork(8, 1, 0));

	Nsgaii nsgaii;

	//when
	vector<AbstractNetwork<MultiCriteriaValue>*>* resultPopulation =  nsgaii.evaluate(oldPopulation, newPopulation);

	//then
	int expetedPopulationSize = 12;
	EXPECT_EQ(expetedPopulationSize, resultPopulation->size());
	checkResult(resultPopulation->at(0), 1, 7, 1, ULLONG_MAX);
	checkResult(resultPopulation->at(1), 2, 3, 1, 11);
	checkResult(resultPopulation->at(2), 7, 2, 1, 8);
	checkResult(resultPopulation->at(3), 8, 1, 1, ULLONG_MAX);
	checkResult(resultPopulation->at(4), 1, 9, 2, ULLONG_MAX);
	checkResult(resultPopulation->at(5), 2, 5, 2, 7);
	checkResult(resultPopulation->at(6), 3, 4, 2, 4);
	checkResult(resultPopulation->at(7), 4, 3, 2, 10);
	checkResult(resultPopulation->at(8), 10, 1, 2, ULLONG_MAX);
	checkResult(resultPopulation->at(9), 2, 9, 3, ULLONG_MAX);
	checkResult(resultPopulation->at(10), 10, 2, 3, ULLONG_MAX);
	checkResult(resultPopulation->at(11), 8, 3, 3, 8);

	//clean
	for (int i = 0; i < oldPopulation->size(); i++)
	{
		delete oldPopulation->at(i);
		delete newPopulation->at(i);
	}

	delete oldPopulation;
	delete newPopulation;
	delete resultPopulation;
}

TEST(NsgaiiTest, testNextEvaluationWithSimilarIndividual)
{
	//given
	TestNetworkCreator creator;
	vector<AbstractNetwork<MultiCriteriaValue>*>* oldPopulation = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	oldPopulation->push_back(creator.createSimpleNetwork(496206992, 10344000, 0));
	oldPopulation->push_back(creator.createSimpleNetwork(496206992, 10344000, 0));
	oldPopulation->push_back(creator.createSimpleNetwork(496206992, 10344000, 0));
	oldPopulation->push_back(creator.createSimpleNetwork(496206992, 10344000, 0));
	oldPopulation->push_back(creator.createSimpleNetwork(496206992, 10344000, 0));
	oldPopulation->push_back(creator.createSimpleNetwork(496206992, 10344000, 0));
	oldPopulation->push_back(creator.createSimpleNetwork(496206992, 10344000, 0));
	oldPopulation->push_back(creator.createSimpleNetwork(496206992, 10344000, 0));
	oldPopulation->push_back(creator.createSimpleNetwork(496206992, 10344000, 0));
	oldPopulation->push_back(creator.createSimpleNetwork(496206992, 10344000, 0));

	vector<AbstractNetwork<MultiCriteriaValue>*>* newPopulation = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	newPopulation->push_back(creator.createSimpleNetwork(496155742, 10344000, 0));
	newPopulation->push_back(creator.createSimpleNetwork(496155742, 10344000, 0));
	newPopulation->push_back(creator.createSimpleNetwork(496206992, 10344000, 0));
	newPopulation->push_back(creator.createSimpleNetwork(496206992, 10344000, 0));
	newPopulation->push_back(creator.createSimpleNetwork(496206992, 10344000, 0));
	newPopulation->push_back(creator.createSimpleNetwork(496206992, 10344000, 0));
	newPopulation->push_back(creator.createSimpleNetwork(496206992, 10344000, 0));
	newPopulation->push_back(creator.createSimpleNetwork(496206992, 10344000, 0));
	newPopulation->push_back(creator.createSimpleNetwork(496206992, 10344000, 0));
	newPopulation->push_back(creator.createSimpleNetwork(496206992, 10344000, 0));

	Nsgaii nsgaii;

	//when
	vector<AbstractNetwork<MultiCriteriaValue>*>* resultPopulation = nsgaii.evaluate(oldPopulation, newPopulation);

	//then
	int expetedPopulationSize = 10;
	EXPECT_EQ(expetedPopulationSize, resultPopulation->size());
	checkResult(resultPopulation->at(0), 496155742, 10344000, 1, 0);
	checkResult(resultPopulation->at(1), 496155742, 10344000, 1, 0);
	checkResult(resultPopulation->at(2), 496206992, 10344000, 2, ULLONG_MAX);
	checkResult(resultPopulation->at(3), 496206992, 10344000, 2, ULLONG_MAX);
	checkResult(resultPopulation->at(4), 496206992, 10344000, 2, 0);
	checkResult(resultPopulation->at(5), 496206992, 10344000, 2, 0);
	checkResult(resultPopulation->at(6), 496206992, 10344000, 2, 0);
	checkResult(resultPopulation->at(7), 496206992, 10344000, 2, 0);
	checkResult(resultPopulation->at(8), 496206992, 10344000, 2, 0);
	checkResult(resultPopulation->at(9), 496206992, 10344000, 2, 0);

	//clean
	for (int i = 0; i < oldPopulation->size(); i++)
	{
		delete oldPopulation->at(i);
		delete newPopulation->at(i);
	}

	delete oldPopulation;
	delete newPopulation;
	delete resultPopulation;
}
