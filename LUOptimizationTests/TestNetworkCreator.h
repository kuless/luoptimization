#pragma once
#include "pch.h"
#include "../LUOptimization/MultiCriteriaNetwork.h"

class TestNetworkCreator
{
	public:
		TestNetworkCreator();
		~TestNetworkCreator();
		std::vector<Station*>* getStationsVector();
		AbstractNetwork<MultiCriteriaValue>* getNetwork();
		std::pair<AbstractNetwork<MultiCriteriaValue>*, AbstractNetwork<MultiCriteriaValue>*>* getPairToCrossovers();
		AbstractNetwork<MultiCriteriaValue>* createSimpleNetwork(unsigned long long int passengersValue, unsigned long long int maintenanceValue, int paretoFront);
		AbstractNetwork<MultiCriteriaValue>* createSimpleNetwork(unsigned long long int passengersValue, unsigned long long int maintenanceValue, int paretoFront, int timetableStop);
		MultiCriteriaValue* createValue(unsigned long long int passengersValue, unsigned long long int maintenanceValue, int paretoFront);

	private:
		std::vector<Station*>* stationsVector;
		AbstractNetwork<MultiCriteriaValue>* network;
		std::pair<AbstractNetwork<MultiCriteriaValue>*, AbstractNetwork<MultiCriteriaValue>*>* pairToCrossovers;

		std::vector<Line*>* getLines(int timetableStop);
		void createStationsVector();
		void createConnectionsBetweenStations();
		std::vector<Line*>* getLines();
		void createNetwork(std::vector<Line*>* lines);
		void createPairToCrossovers(std::vector<Line*>* lines);
};
