#include "pch.h"
#include "../LUOptimization/Train.h"

using namespace std;

TEST(TrainTest, addPassangersAllPassengersFittedTest)
{
	//given
	Station* startStation = new Station("1", "Name", 12, 12);
	Station* endStation = new Station("2", "Name", 12, 12);
	std::vector<Station*>* stops = new std::vector<Station*>();
	stops->push_back(startStation);
	stops->push_back(endStation);
	Line* line = new Line();
	line->setStops(stops);
	Train* train = new Train(100, line, 10);
	Travel::TravelBuilder builder = Travel::TravelBuilder();
	int firstTravelPassengers = 50;
	builder.setNumberOfPassengers(firstTravelPassengers);
	builder.setStartHour(10);
	builder.setStartMinute(10);
	builder.setStartStation(startStation);
	builder.setEndStation(endStation);
	Travel* firstTravel = builder.build();
	int secondTravelPassengers = 30;
	builder.setNumberOfPassengers(secondTravelPassengers);
	Travel* secondTravel = builder.build();

	//when
	Travel* firstResult = train->addPassangers(firstTravel);
	Travel* secondResult = train->addPassangers(secondTravel);

	//then
	int expectedTravels = 2;
	EXPECT_EQ(nullptr, firstResult);
	EXPECT_EQ(nullptr, secondResult);
	EXPECT_EQ(firstTravelPassengers + secondTravelPassengers, train->getTotalPassengers());
	EXPECT_EQ(expectedTravels, train->getPassengers().size());
	EXPECT_EQ(firstTravel, train->getPassengers().front());
	EXPECT_EQ(secondTravel, train->getPassengers().back());

	//clean
	delete train;
	delete line;
	delete startStation;
	delete endStation;
}

TEST(TrainTest, addPassangersAllPassengersNotFittedTest)
{
	//given
	Station* startStation = new Station("1", "Name", 12, 12);
	Station* endStation = new Station("2", "Name", 12, 12);
	std::vector<Station*>* stops = new std::vector<Station*>();
	stops->push_back(startStation);
	stops->push_back(endStation);
	Line* line = new Line();
	line->setStops(stops);
	int capacity = 100;
	Train* train = new Train(capacity, line, 10);
	Travel::TravelBuilder builder = Travel::TravelBuilder();
	int firstTravelPassengers = 50;
	builder.setNumberOfPassengers(firstTravelPassengers);
	builder.setStartHour(10);
	builder.setStartMinute(10);
	builder.setStartStation(startStation);
	builder.setEndStation(endStation);
	Travel* firstTravel = builder.build();
	int secondTravelPassengers = 250;
	builder.setNumberOfPassengers(secondTravelPassengers);
	Travel* secondTravel = builder.build();

	//when
	Travel* firstResult = train->addPassangers(firstTravel);
	Travel* secondResult = train->addPassangers(secondTravel);

	//then
	int expectedTravels = 2;
	EXPECT_EQ(nullptr, firstResult);
	EXPECT_NE(nullptr, secondResult);
	EXPECT_EQ(capacity, train->getTotalPassengers());
	EXPECT_EQ(expectedTravels, train->getPassengers().size());
	EXPECT_EQ(firstTravel, train->getPassengers().front());
	EXPECT_EQ(secondTravel, train->getPassengers().back());

	//clean
	delete train;
	delete line;
	delete startStation;
	delete endStation;
}
