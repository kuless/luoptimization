#pragma once
#include "pch.h"
#include "../LUOptimization/IQualityMeasures.h"

using namespace ::testing;

class QualityMeasuresMock : public IQualityMeasures
{
	public:
		MOCK_METHOD((QualityValue*), getQuality, (std::vector<AbstractNetwork<MultiCriteriaValue>*>* population, QualityEnum qualityType), (override));
};
