#include "pch.h"
#include "../LUOptimization/Travel.h"
#include "../LUOptimization/Route.h"

using namespace std;

TEST(TravelTest, travelBuilderSuccessTest)
{
	//given
	Travel::TravelBuilder builder = Travel::TravelBuilder();
	Station* startStation = new Station("1", "Name", 12, 12);
	Station* endStation = new Station("2", "Name", 12, 12);

	//when
	builder.setNumberOfPassengers(500);
	builder.setStartHour(10);
	builder.setStartMinute(10);
	builder.setStartStation(startStation);
	builder.setEndStation(endStation);
	Travel* result = builder.build();

	//then
	int expectedNumberOfPassengers = 500;
	int expectedStartHour = 10;
	int expectedStartMinute = 10;
	EXPECT_EQ(expectedNumberOfPassengers, result->getNumberOfPassengers());
	EXPECT_EQ(expectedStartHour, result->getStartHour());
	EXPECT_EQ(expectedStartMinute, result->getStartMinute());
	EXPECT_EQ(startStation, result->getStartStation());
	EXPECT_EQ(endStation, result->getEndStation());

	//clean
	delete result;
	delete startStation;
	delete endStation;
}

TEST(TravelTest, travelBuilderFailedTest)
{
	//given
	Travel::TravelBuilder builder = Travel::TravelBuilder();
	Station* startStation = new Station("1", "Name", 12, 12);

	//when
	builder.setNumberOfPassengers(500);
	builder.setStartHour(10);
	builder.setStartMinute(10);
	builder.setStartStation(startStation);
	EXPECT_THROW(builder.build(), TravelException);

	//clean
	delete startStation;
}

TEST(TravelTest, splitPassengersSuccessTest)
{
	//given
	Travel::TravelBuilder builder = Travel::TravelBuilder();
	Station* startStation = new Station("1", "Name", 12, 12);
	Station* endStation = new Station("2", "Name", 12, 12);
	builder.setNumberOfPassengers(500);
	builder.setStartHour(10);
	builder.setStartMinute(10);
	builder.setStartStation(startStation);
	builder.setEndStation(endStation);
	Travel* orginal = builder.build();
	
	int split = 200;

	//when
	Travel* result = orginal->splitPassengers(split);

	//then
	int expectedOrginal = 300;
	int expectedResult = 200;

	EXPECT_EQ(expectedOrginal, orginal->getNumberOfPassengers());
	EXPECT_EQ(expectedResult, result->getNumberOfPassengers());

	//clean
	delete startStation;
	delete endStation;
	delete orginal;
	delete result;
}

TEST(TravelTest, splitPassengersFaildTest)
{
	//given
	Travel::TravelBuilder builder = Travel::TravelBuilder();
	Station* startStation = new Station("1", "Name", 12, 12);
	Station* endStation = new Station("2", "Name", 12, 12);
	builder.setNumberOfPassengers(100);
	builder.setStartHour(10);
	builder.setStartMinute(10);
	builder.setStartStation(startStation);
	builder.setEndStation(endStation);
	Travel* orginal = builder.build();
	
	int split = 200;

	//when
	EXPECT_THROW(orginal->splitPassengers(split), TravelException);

	//clean
	delete startStation;
	delete endStation;
	delete orginal;
}
