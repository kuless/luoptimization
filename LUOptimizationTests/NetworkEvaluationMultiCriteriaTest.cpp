#include "pch.h"
#include "../LUOptimization/Travel.h"
#include "../LUOptimization/MultiCriteriaNetwork.h"
#include "../LUOptimization/Station.h"
#include "../LUOptimization/NetworkEvaluationMultiCriteria.h"
#include "TestNetworkCreator.h"

TEST(NetworkEvaluationMultiCriteriaTest, maintenanceOfStationsAndTracksTest)
{
	//given
	std::list<Travel*>* travels = new std::list<Travel*>();
	TestNetworkCreator networkCreator = TestNetworkCreator();
	MultiCriteriaNetwork* network = dynamic_cast<MultiCriteriaNetwork*>(networkCreator.getNetwork());
	std::vector<Station*>* stationsVector = networkCreator.getStationsVector();

	std::vector<std::pair<int, int>> timetable0;
	network->getLines()->at(0)->setTimetable(timetable0);

	std::vector<std::pair<int, int>> timetable1;
	network->getLines()->at(1)->setTimetable(timetable1);

	std::vector<std::pair<int, int>> timetable2;
	network->getLines()->at(2)->setTimetable(timetable2);

	std::vector<std::pair<int, int>> timetable3;
	network->getLines()->at(3)->setTimetable(timetable3);

	std::vector<std::pair<int, int>> timetable4;
	network->getLines()->at(4)->setTimetable(timetable4);

	std::vector<std::pair<int, int>> timetable5;
	network->getLines()->at(5)->setTimetable(timetable5);

	//when
	NetworkEvaluationMultiCriteria networkEvaluation = NetworkEvaluationMultiCriteria(travels, network);
	MultiCriteriaValue* result = networkEvaluation.evaluate();

	//then
	unsigned long long int expectedMaintenanceValue = 3627362576;
	unsigned long long int expectedPassengersValue = 0;

	EXPECT_EQ(expectedMaintenanceValue, result->getMaintenanceValue());
	EXPECT_EQ(expectedPassengersValue, result->getPassengersValue());

	//clean
}

TEST(NetworkEvaluationMultiCriteriaTest, maintenanceTest)
{
	//given
	std::list<Travel*>* travels = new std::list<Travel*>();
	TestNetworkCreator networkCreator = TestNetworkCreator();
	MultiCriteriaNetwork* network = dynamic_cast<MultiCriteriaNetwork*>(networkCreator.getNetwork());
	std::vector<Station*>* stationsVector = networkCreator.getStationsVector();

	std::vector<std::pair<int, int>> timetable0;
	timetable0.push_back(std::pair<int, int>(1, 1));
	network->getLines()->at(0)->setTimetable(timetable0);

	std::vector<std::pair<int, int>> timetable1;
	timetable1.push_back(std::pair<int, int>(1, 1));
	network->getLines()->at(1)->setTimetable(timetable1);

	std::vector<std::pair<int, int>> timetable2;
	timetable2.push_back(std::pair<int, int>(1, 1));
	network->getLines()->at(2)->setTimetable(timetable2);

	std::vector<std::pair<int, int>> timetable3;
	timetable3.push_back(std::pair<int, int>(1, 1));
	network->getLines()->at(3)->setTimetable(timetable3);

	std::vector<std::pair<int, int>> timetable4;
	timetable4.push_back(std::pair<int, int>(1, 1));
	network->getLines()->at(4)->setTimetable(timetable4);

	std::vector<std::pair<int, int>> timetable5;
	timetable5.push_back(std::pair<int, int>(1, 1));
	network->getLines()->at(5)->setTimetable(timetable5);

	//when
	NetworkEvaluationMultiCriteria networkEvaluation = NetworkEvaluationMultiCriteria(travels, network);
	MultiCriteriaValue* result = networkEvaluation.evaluate();

	//then
	unsigned long long int expectedMaintenanceValue = 3764719407;
	unsigned long long int expectedPassengersValue = 0;

	EXPECT_EQ(expectedMaintenanceValue, result->getMaintenanceValue());
	EXPECT_EQ(expectedPassengersValue, result->getPassengersValue());

	//clean
}

TEST(NetworkEvaluationMultiCriteriaTest, trainDrivingTimeTest)
{
	//given
	std::list<Travel*>* travels = new std::list<Travel*>();
	TestNetworkCreator networkCreator = TestNetworkCreator();
	MultiCriteriaNetwork* network = dynamic_cast<MultiCriteriaNetwork*>(networkCreator.getNetwork());
	std::vector<Station*>* stationsVector = networkCreator.getStationsVector();
	Travel::TravelBuilder* travelBuilder = new Travel::TravelBuilder();

	travelBuilder->setNumberOfPassengers(3);
	travelBuilder->setStartHour(10);
	travelBuilder->setStartMinute(24);
	travelBuilder->setStartStation(stationsVector->at(2));
	travelBuilder->setEndStation(stationsVector->at(13));
	Travel* travel0 = travelBuilder->build();
	std::list<Route*>* routes0 = new std::list<Route*>();
	routes0->push_back(new Route(stationsVector->at(13), network->getLines()->at(5), false));
	travel0->setRoutes(routes0);
	travels->push_back(travel0);

	std::vector<std::pair<int, int>> timetable0;
	timetable0.push_back(std::pair<int, int>(0, 0));
	timetable0.push_back(std::pair<int, int>(9, 59));
	timetable0.push_back(std::pair<int, int>(10, 10));
	timetable0.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(0)->setTimetable(timetable0);

	std::vector<std::pair<int, int>> timetable1;
	timetable1.push_back(std::pair<int, int>(9, 48));
	timetable1.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(1)->setTimetable(timetable1);

	std::vector<std::pair<int, int>> timetable2;
	timetable2.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(2)->setTimetable(timetable2);

	std::vector<std::pair<int, int>> timetable3;
	timetable3.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(3)->setTimetable(timetable3);

	std::vector<std::pair<int, int>> timetable4;
	timetable4.push_back(std::pair<int, int>(23, 0));
	network->getLines()->at(4)->setTimetable(timetable4);

	std::vector<std::pair<int, int>> timetable5;
	timetable5.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(5)->setTimetable(timetable5);

	//when
	NetworkEvaluationMultiCriteria networkEvaluation = NetworkEvaluationMultiCriteria(travels, network);
	MultiCriteriaValue* result = networkEvaluation.evaluate();

	//then
	unsigned long long int expectedMaintenanceValue = 3813486059;
	unsigned long long int expectedPassengersValue = 15;

	EXPECT_EQ(expectedMaintenanceValue, result->getMaintenanceValue());
	EXPECT_EQ(expectedPassengersValue, result->getPassengersValue());

	//clean
	delete travelBuilder;
}

TEST(NetworkEvaluationMultiCriteriaTest, trainDrivingAndWaitingTimeTest)
{
	//given
	std::list<Travel*>* travels = new std::list<Travel*>();
	TestNetworkCreator networkCreator = TestNetworkCreator();
	MultiCriteriaNetwork* network = dynamic_cast<MultiCriteriaNetwork*>(networkCreator.getNetwork());
	std::vector<Station*>* stationsVector = networkCreator.getStationsVector();
	Travel::TravelBuilder* travelBuilder = new Travel::TravelBuilder();

	travelBuilder->setNumberOfPassengers(3);
	travelBuilder->setStartHour(10);
	travelBuilder->setStartMinute(14);
	travelBuilder->setStartStation(stationsVector->at(2));
	travelBuilder->setEndStation(stationsVector->at(13));
	Travel* travel0 = travelBuilder->build();
	std::list<Route*>* routes0 = new std::list<Route*>();
	routes0->push_back(new Route(stationsVector->at(13), network->getLines()->at(5), false));
	travel0->setRoutes(routes0);
	travels->push_back(travel0);

	std::vector<std::pair<int, int>> timetable0;
	timetable0.push_back(std::pair<int, int>(0, 0));
	timetable0.push_back(std::pair<int, int>(9, 59));
	timetable0.push_back(std::pair<int, int>(10, 10));
	timetable0.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(0)->setTimetable(timetable0);

	std::vector<std::pair<int, int>> timetable1;
	timetable1.push_back(std::pair<int, int>(9, 48));
	timetable1.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(1)->setTimetable(timetable1);

	std::vector<std::pair<int, int>> timetable2;
	timetable2.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(2)->setTimetable(timetable2);

	std::vector<std::pair<int, int>> timetable3;
	timetable3.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(3)->setTimetable(timetable3);

	std::vector<std::pair<int, int>> timetable4;
	timetable4.push_back(std::pair<int, int>(23, 0));
	network->getLines()->at(4)->setTimetable(timetable4);

	std::vector<std::pair<int, int>> timetable5;
	timetable5.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(5)->setTimetable(timetable5);

	//when
	NetworkEvaluationMultiCriteria networkEvaluation = NetworkEvaluationMultiCriteria(travels, network);
	MultiCriteriaValue* result = networkEvaluation.evaluate();

	//then
	unsigned long long int expectedMaintenanceValue = 3813486059;
	unsigned long long int expectedPassengersValue = 45;

	EXPECT_EQ(expectedMaintenanceValue, result->getMaintenanceValue());
	EXPECT_EQ(expectedPassengersValue, result->getPassengersValue());

	//clean
	delete travelBuilder;
}

TEST(NetworkEvaluationMultiCriteriaTest, passengersWithNoRouteTest)
{
	//given
	std::list<Travel*>* travels = new std::list<Travel*>();
	TestNetworkCreator networkCreator = TestNetworkCreator();
	MultiCriteriaNetwork* network = dynamic_cast<MultiCriteriaNetwork*>(networkCreator.getNetwork());
	std::vector<Station*>* stationsVector = networkCreator.getStationsVector();
	Travel::TravelBuilder* travelBuilder = new Travel::TravelBuilder();

	travelBuilder->setNumberOfPassengers(3);
	travelBuilder->setStartHour(10);
	travelBuilder->setStartMinute(14);
	travelBuilder->setStartStation(stationsVector->at(2));
	travelBuilder->setEndStation(stationsVector->at(13));
	Travel* travel0 = travelBuilder->build();
	std::list<Route*>* routes0 = new std::list<Route*>();
	travel0->setRoutes(routes0);
	travels->push_back(travel0);

	std::vector<std::pair<int, int>> timetable0;
	timetable0.push_back(std::pair<int, int>(0, 0));
	timetable0.push_back(std::pair<int, int>(9, 59));
	timetable0.push_back(std::pair<int, int>(10, 10));
	timetable0.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(0)->setTimetable(timetable0);

	std::vector<std::pair<int, int>> timetable1;
	timetable1.push_back(std::pair<int, int>(9, 48));
	timetable1.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(1)->setTimetable(timetable1);

	std::vector<std::pair<int, int>> timetable2;
	timetable2.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(2)->setTimetable(timetable2);

	std::vector<std::pair<int, int>> timetable3;
	timetable3.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(3)->setTimetable(timetable3);

	std::vector<std::pair<int, int>> timetable4;
	timetable4.push_back(std::pair<int, int>(23, 0));
	network->getLines()->at(4)->setTimetable(timetable4);

	std::vector<std::pair<int, int>> timetable5;
	timetable5.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(5)->setTimetable(timetable5);

	//when
	NetworkEvaluationMultiCriteria networkEvaluation = NetworkEvaluationMultiCriteria(travels, network);
	MultiCriteriaValue* result = networkEvaluation.evaluate();

	//then
	unsigned long long int expectedMaintenanceValue = 3813486059;
	unsigned long long int expectedPassengersValue = 3000;

	EXPECT_EQ(expectedMaintenanceValue, result->getMaintenanceValue());
	EXPECT_EQ(expectedPassengersValue, result->getPassengersValue());

	//clean
	delete travelBuilder;
}

TEST(NetworkEvaluationMultiCriteriaTest, passengersLeftOnPlatormTest)
{
	//given
	std::list<Travel*>* travels = new std::list<Travel*>();
	TestNetworkCreator networkCreator = TestNetworkCreator();
	MultiCriteriaNetwork* network = dynamic_cast<MultiCriteriaNetwork*>(networkCreator.getNetwork());
	std::vector<Station*>* stationsVector = networkCreator.getStationsVector();
	Travel::TravelBuilder* travelBuilder = new Travel::TravelBuilder();

	travelBuilder->setNumberOfPassengers(3);
	travelBuilder->setStartHour(10);
	travelBuilder->setStartMinute(59);
	travelBuilder->setStartStation(stationsVector->at(2));
	travelBuilder->setEndStation(stationsVector->at(13));
	Travel* travel0 = travelBuilder->build();
	std::list<Route*>* routes0 = new std::list<Route*>();
	routes0->push_back(new Route(stationsVector->at(13), network->getLines()->at(5), false));
	travel0->setRoutes(routes0);
	travels->push_back(travel0);

	std::vector<std::pair<int, int>> timetable0;
	timetable0.push_back(std::pair<int, int>(0, 0));
	timetable0.push_back(std::pair<int, int>(9, 59));
	timetable0.push_back(std::pair<int, int>(10, 10));
	timetable0.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(0)->setTimetable(timetable0);

	std::vector<std::pair<int, int>> timetable1;
	timetable1.push_back(std::pair<int, int>(9, 48));
	timetable1.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(1)->setTimetable(timetable1);

	std::vector<std::pair<int, int>> timetable2;
	timetable2.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(2)->setTimetable(timetable2);

	std::vector<std::pair<int, int>> timetable3;
	timetable3.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(3)->setTimetable(timetable3);

	std::vector<std::pair<int, int>> timetable4;
	timetable4.push_back(std::pair<int, int>(23, 0));
	network->getLines()->at(4)->setTimetable(timetable4);

	std::vector<std::pair<int, int>> timetable5;
	timetable5.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(5)->setTimetable(timetable5);

	//when
	NetworkEvaluationMultiCriteria networkEvaluation = NetworkEvaluationMultiCriteria(travels, network);
	MultiCriteriaValue* result = networkEvaluation.evaluate();

	//then
	unsigned long long int expectedMaintenanceValue = 3813486059;
	unsigned long long int expectedPassengersValue = 5340;

	EXPECT_EQ(expectedMaintenanceValue, result->getMaintenanceValue());
	EXPECT_EQ(expectedPassengersValue, result->getPassengersValue());

	//clean
	delete travelBuilder;
}

TEST(NetworkEvaluationMultiCriteriaTest, evaluationTest)
{
	//given
	std::list<Travel*>* travels = new std::list<Travel*>();
	TestNetworkCreator networkCreator = TestNetworkCreator();
	MultiCriteriaNetwork* network = dynamic_cast<MultiCriteriaNetwork*>(networkCreator.getNetwork());
	std::vector<Station*>* stationsVector = networkCreator.getStationsVector();
	Travel::TravelBuilder* travelBuilder = new Travel::TravelBuilder();

	travelBuilder->setNumberOfPassengers(2);
	travelBuilder->setStartHour(8);
	travelBuilder->setStartMinute(15);
	travelBuilder->setStartStation(stationsVector->at(27));
	travelBuilder->setEndStation(stationsVector->at(10));
	Travel* travel0 = travelBuilder->build();
	std::list<Route*>* routes0 = new std::list<Route*>();
	travel0->setRoutes(routes0);
	travels->push_back(travel0);

	travelBuilder->setNumberOfPassengers(2);
	travelBuilder->setStartHour(10);
	travelBuilder->setStartMinute(0);
	travelBuilder->setStartStation(stationsVector->at(1));
	travelBuilder->setEndStation(stationsVector->at(11));
	Travel* travel1 = travelBuilder->build();
	std::list<Route*>* routes1 = new std::list<Route*>();
	routes1->push_back(new Route(stationsVector->at(4), network->getLines()->at(0), true));
	routes1->push_back(new Route(stationsVector->at(11), network->getLines()->at(1), false));
	travel1->setRoutes(routes1);
	travels->push_back(travel1);

	travelBuilder->setNumberOfPassengers(5);
	travelBuilder->setStartMinute(10);
	Travel* travel2 = travelBuilder->build();
	std::list<Route*>* routes2 = new std::list<Route*>();
	routes2->push_back(new Route(stationsVector->at(4), network->getLines()->at(0), true));
	routes2->push_back(new Route(stationsVector->at(11), network->getLines()->at(1), false));
	travel2->setRoutes(routes2);
	travels->push_back(travel2);

	travelBuilder->setNumberOfPassengers(3);
	travelBuilder->setEndStation(stationsVector->at(13));
	Travel* travel3 = travelBuilder->build();
	std::list<Route*>* routes3 = new std::list<Route*>();
	routes3->push_back(new Route(stationsVector->at(4), network->getLines()->at(0), true));
	routes3->push_back(new Route(stationsVector->at(13), network->getLines()->at(1), true));
	travel3->setRoutes(routes3);
	travels->push_back(travel3);

	travelBuilder->setNumberOfPassengers(2);
	travelBuilder->setStartHour(20);
	travelBuilder->setStartMinute(0);
	travelBuilder->setStartStation(stationsVector->at(2));
	travelBuilder->setEndStation(stationsVector->at(13));
	Travel* travel4 = travelBuilder->build();
	std::list<Route*>* routes4 = new std::list<Route*>();
	routes4->push_back(new Route(stationsVector->at(13), network->getLines()->at(5), false));
	travel4->setRoutes(routes4);
	travels->push_back(travel4);

	std::vector<std::pair<int, int>> timetable0;
	timetable0.push_back(std::pair<int, int>(0, 0));
	timetable0.push_back(std::pair<int, int>(9, 59));
	timetable0.push_back(std::pair<int, int>(10, 10));
	timetable0.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(0)->setTimetable(timetable0);

	std::vector<std::pair<int, int>> timetable1;
	timetable1.push_back(std::pair<int, int>(9, 48));
	timetable1.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(1)->setTimetable(timetable1);

	std::vector<std::pair<int, int>> timetable2;
	timetable2.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(2)->setTimetable(timetable2);

	std::vector<std::pair<int, int>> timetable3;
	timetable3.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(3)->setTimetable(timetable3);

	std::vector<std::pair<int, int>> timetable4;
	timetable4.push_back(std::pair<int, int>(23, 0));
	network->getLines()->at(4)->setTimetable(timetable4);

	std::vector<std::pair<int, int>> timetable5;
	timetable5.push_back(std::pair<int, int>(10, 16));
	network->getLines()->at(5)->setTimetable(timetable5);

	//when
	NetworkEvaluationMultiCriteria networkEvaluation = NetworkEvaluationMultiCriteria(travels, network);
	MultiCriteriaValue* result = networkEvaluation.evaluate();

	//then
	unsigned long long int expectedMaintenanceValue = 3813486059;
	unsigned long long int expectedPassengersValue = 4637;

	EXPECT_EQ(expectedMaintenanceValue, result->getMaintenanceValue());
	EXPECT_EQ(expectedPassengersValue, result->getPassengersValue());

	//clean
	delete travelBuilder;
}
