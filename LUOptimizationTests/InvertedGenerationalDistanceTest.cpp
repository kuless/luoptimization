#include "pch.h"
#include "../LUOptimization/InvertedGenerationalDistance.h"
#include "../LUOptimization/MultiCriteriaNetwork.h"
#include "TestNetworkCreator.h"

using namespace std;

TEST(InvertedGenerationalDistanceTest, getQualityOfForntSmallerThenTpf)
{
	//given
	TestNetworkCreator networkCreator;
	vector<MultiCriteriaValue*>* trueParetoFront = new vector<MultiCriteriaValue*>();
	trueParetoFront->push_back(networkCreator.createValue(1, 17, 1));
	trueParetoFront->push_back(networkCreator.createValue(2, 14, 1));
	trueParetoFront->push_back(networkCreator.createValue(4, 13, 1));
	trueParetoFront->push_back(networkCreator.createValue(5, 9, 1));
	trueParetoFront->push_back(networkCreator.createValue(6, 7, 1));
	trueParetoFront->push_back(networkCreator.createValue(7, 6, 1));
	trueParetoFront->push_back(networkCreator.createValue(9, 5, 1));
	trueParetoFront->push_back(networkCreator.createValue(11, 3, 1));
	trueParetoFront->push_back(networkCreator.createValue(12, 2, 1));
	InvertedGenerationalDistance igd = InvertedGenerationalDistance(trueParetoFront);

	vector<AbstractNetwork<MultiCriteriaValue>*>* population = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	population->push_back(networkCreator.createSimpleNetwork(2, 19, 1));
	population->push_back(networkCreator.createSimpleNetwork(3, 16, 1));
	population->push_back(networkCreator.createSimpleNetwork(5, 14, 1));
	population->push_back(networkCreator.createSimpleNetwork(6, 13, 1));
	population->push_back(networkCreator.createSimpleNetwork(7, 9, 1));
	population->push_back(networkCreator.createSimpleNetwork(10, 7, 1));
	population->push_back(networkCreator.createSimpleNetwork(13, 5, 1));
	population->push_back(networkCreator.createSimpleNetwork(16, 3, 1));
	population->push_back(networkCreator.createSimpleNetwork(50, 50, 2));
	population->push_back(networkCreator.createSimpleNetwork(60, 60, 2));

	//when
	QualityValue* result = igd.getQuality(population);

	//then
	std::string expected = "2.37213225";
	std::string stringResult = result->getQuality();
	stringResult.resize(10);
	EXPECT_EQ(stringResult, expected);

	//clean
	for (vector<AbstractNetwork<MultiCriteriaValue>*>::iterator it = population->begin(); it != population->end(); ++it)
	{
		delete (*it);
	}

	delete population;
}


TEST(InvertedGenerationalDistanceTest, getQualityOfForntLargerThenTpf)
{
	//given
	TestNetworkCreator networkCreator;
	vector<MultiCriteriaValue*>* trueParetoFront = new vector<MultiCriteriaValue*>();
	trueParetoFront->push_back(networkCreator.createValue(1, 17, 1));
	trueParetoFront->push_back(networkCreator.createValue(2, 14, 1));
	trueParetoFront->push_back(networkCreator.createValue(4, 13, 1));
	trueParetoFront->push_back(networkCreator.createValue(5, 9, 1));
	trueParetoFront->push_back(networkCreator.createValue(6, 7, 1));
	trueParetoFront->push_back(networkCreator.createValue(7, 6, 1));
	trueParetoFront->push_back(networkCreator.createValue(9, 5, 1));
	trueParetoFront->push_back(networkCreator.createValue(11, 3, 1));
	trueParetoFront->push_back(networkCreator.createValue(12, 2, 1));
	InvertedGenerationalDistance igd = InvertedGenerationalDistance(trueParetoFront);

	vector<AbstractNetwork<MultiCriteriaValue>*>* population = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	population->push_back(networkCreator.createSimpleNetwork(2, 19, 1));
	population->push_back(networkCreator.createSimpleNetwork(3, 16, 1));
	population->push_back(networkCreator.createSimpleNetwork(5, 14, 1));
	population->push_back(networkCreator.createSimpleNetwork(6, 13, 1));
	population->push_back(networkCreator.createSimpleNetwork(7, 9, 1));
	population->push_back(networkCreator.createSimpleNetwork(10, 7, 1));
	population->push_back(networkCreator.createSimpleNetwork(13, 5, 1));
	population->push_back(networkCreator.createSimpleNetwork(16, 3, 1));
	population->push_back(networkCreator.createSimpleNetwork(17, 2, 1));
	population->push_back(networkCreator.createSimpleNetwork(18, 1, 1));
	population->push_back(networkCreator.createSimpleNetwork(10, 7, 2));
	population->push_back(networkCreator.createSimpleNetwork(6, 5, 2));

	//when
	QualityValue* result = igd.getQuality(population);

	//then
	std::string expected = "2.37213225";
	std::string stringResult = result->getQuality();
	stringResult.resize(10);
	EXPECT_EQ(stringResult, expected);

	//clean
	for (vector<AbstractNetwork<MultiCriteriaValue>*>::iterator it = population->begin(); it != population->end(); ++it)
	{
		delete (*it);
	}

	delete population;
	delete result;
}

TEST(InvertedGenerationalDistanceTest, getQualityOfSameSizeLikeTpf)
{
	//given
	TestNetworkCreator networkCreator;
	vector<MultiCriteriaValue*>* trueParetoFront = new vector<MultiCriteriaValue*>();
	trueParetoFront->push_back(networkCreator.createValue(1, 17, 1));
	trueParetoFront->push_back(networkCreator.createValue(2, 14, 1));
	trueParetoFront->push_back(networkCreator.createValue(4, 13, 1));
	trueParetoFront->push_back(networkCreator.createValue(5, 9, 1));
	trueParetoFront->push_back(networkCreator.createValue(6, 7, 1));
	trueParetoFront->push_back(networkCreator.createValue(7, 6, 1));
	trueParetoFront->push_back(networkCreator.createValue(9, 5, 1));
	trueParetoFront->push_back(networkCreator.createValue(11, 3, 1));
	trueParetoFront->push_back(networkCreator.createValue(12, 2, 1));
	InvertedGenerationalDistance igd = InvertedGenerationalDistance(trueParetoFront);

	vector<AbstractNetwork<MultiCriteriaValue>*>* population = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	population->push_back(networkCreator.createSimpleNetwork(1, 17, 1));
	population->push_back(networkCreator.createSimpleNetwork(3, 16, 1));
	population->push_back(networkCreator.createSimpleNetwork(5, 14, 1));
	population->push_back(networkCreator.createSimpleNetwork(6, 13, 1));
	population->push_back(networkCreator.createSimpleNetwork(7, 9, 1));
	population->push_back(networkCreator.createSimpleNetwork(10, 7, 1));
	population->push_back(networkCreator.createSimpleNetwork(13, 5, 1));
	population->push_back(networkCreator.createSimpleNetwork(16, 3, 1));
	population->push_back(networkCreator.createSimpleNetwork(17, 2, 1));
	population->push_back(networkCreator.createSimpleNetwork(10, 7, 2));
	population->push_back(networkCreator.createSimpleNetwork(6, 5, 2));

	//when
	QualityValue* result = igd.getQuality(population);

	//then
	std::string expected = "2.12368025";
	std::string stringResult = result->getQuality();
	stringResult.resize(10);
	EXPECT_EQ(stringResult, expected);

	//clean
	for (vector<AbstractNetwork<MultiCriteriaValue>*>::iterator it = population->begin(); it != population->end(); ++it)
	{
		delete (*it);
	}

	delete population;
	delete result;
}
