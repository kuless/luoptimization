#include "pch.h"
#include "../LUOptimization/LineMutation.h"
#include "../LUOptimization/QuasiShortestPath.h"
#include "../LUOptimization/DijkstraImpl.h"
#include "RandomNumberGeneratorMock.h"
#include "DijkstraMock.h"
#include "TestNetworkCreator.h"

using ::testing::Return;

TEST(LineMutationTest, canMutateLine)
{
	//given
	TestNetworkCreator test;
	RandomNumberGeneratorMock* randomMock = new RandomNumberGeneratorMock();
	EXPECT_CALL(*randomMock, getInt(_, _))
		.WillOnce(Return(0));
	std::vector<Station*>* dijkstraResult = new std::vector<Station*>();
	dijkstraResult->push_back(test.getStationsVector()->at(4));
	dijkstraResult->push_back(test.getStationsVector()->at(13));
	dijkstraResult->push_back(test.getStationsVector()->at(14));
	dijkstraResult->push_back(test.getStationsVector()->at(7));
	dijkstraResult->push_back(test.getStationsVector()->at(6));
	DijkstraMock* dijkstraMock = new DijkstraMock();
	EXPECT_CALL(*dijkstraMock, dijkstra(test.getStationsVector()->at(4), test.getStationsVector()->at(6)))
		.WillOnce(Return(dijkstraResult));
	LineMutation* lineMutation = new LineMutation(randomMock, dijkstraMock);

	std::vector<Station*>* stops = new std::vector<Station*>();
	stops->push_back(test.getStationsVector()->at(1));
	stops->push_back(test.getStationsVector()->at(2));
	stops->push_back(test.getStationsVector()->at(3));
	stops->push_back(test.getStationsVector()->at(4));
	stops->push_back(test.getStationsVector()->at(5));
	stops->push_back(test.getStationsVector()->at(6));
	std::vector<int> timetable;
	timetable.push_back(5);
	Line* line = new Line(1, "name", stops, timetable);

	//when
	lineMutation->mutation(line);

	//then
	EXPECT_EQ(8, line->getStops().size());
	EXPECT_EQ(test.getStationsVector()->at(1), line->getStops()[0]);
	EXPECT_EQ(test.getStationsVector()->at(2), line->getStops()[1]);
	EXPECT_EQ(test.getStationsVector()->at(3), line->getStops()[2]);
	EXPECT_EQ(test.getStationsVector()->at(4), line->getStops()[3]);
	EXPECT_EQ(test.getStationsVector()->at(13), line->getStops()[4]);
	EXPECT_EQ(test.getStationsVector()->at(14), line->getStops()[5]);
	EXPECT_EQ(test.getStationsVector()->at(7), line->getStops()[6]);
	EXPECT_EQ(test.getStationsVector()->at(6), line->getStops()[7]);

	//clean
	delete line;
	delete lineMutation;
	delete dijkstraMock;
}

TEST(LineMutationTest, cannotMutateLineOneJunction)
{
	//given
	TestNetworkCreator test;
	RandomNumberGeneratorMock* randomMock = new RandomNumberGeneratorMock();
	DijkstraMock* dijkstraMock = new DijkstraMock();
	LineMutation* lineMutation = new LineMutation(randomMock, dijkstraMock);

	std::vector<Station*>* stops = new std::vector<Station*>();
	stops->push_back(test.getStationsVector()->at(1));
	stops->push_back(test.getStationsVector()->at(2));
	stops->push_back(test.getStationsVector()->at(3));
	stops->push_back(test.getStationsVector()->at(4));
	stops->push_back(test.getStationsVector()->at(5));
	std::vector<int> timetable;
	timetable.push_back(5);
	Line* line = new Line(1, "name", stops, timetable);

	//when
	lineMutation->mutation(line);

	//then
	EXPECT_EQ(5, line->getStops().size());
	EXPECT_EQ(test.getStationsVector()->at(1), line->getStops()[0]);
	EXPECT_EQ(test.getStationsVector()->at(2), line->getStops()[1]);
	EXPECT_EQ(test.getStationsVector()->at(3), line->getStops()[2]);
	EXPECT_EQ(test.getStationsVector()->at(4), line->getStops()[3]);
	EXPECT_EQ(test.getStationsVector()->at(5), line->getStops()[4]);

	//clean
	delete line;
	delete lineMutation;
	delete dijkstraMock;
}

TEST(LineMutationTest, cannotMutateLineNoRoutes)
{
	//given
	TestNetworkCreator test;
	RandomNumberGeneratorMock* randomMock = new RandomNumberGeneratorMock();
	EXPECT_CALL(*randomMock, getInt(_, _))
		.WillOnce(Return(0));
	std::vector<Station*>* dijkstraResult = new std::vector<Station*>();
	DijkstraMock* dijkstraMock = new DijkstraMock();
	EXPECT_CALL(*dijkstraMock, dijkstra(test.getStationsVector()->at(26), test.getStationsVector()->at(28)))
		.WillOnce(Return(dijkstraResult));
	LineMutation* lineMutation = new LineMutation(randomMock, dijkstraMock);

	std::vector<Station*>* stops = new std::vector<Station*>();
	stops->push_back(test.getStationsVector()->at(25));
	stops->push_back(test.getStationsVector()->at(26));
	stops->push_back(test.getStationsVector()->at(27));
	stops->push_back(test.getStationsVector()->at(28));
	stops->push_back(test.getStationsVector()->at(29));
	std::vector<int> timetable;
	timetable.push_back(5);
	Line* line = new Line(1, "name", stops, timetable);

	//when
	lineMutation->mutation(line);

	//then
	EXPECT_EQ(5, line->getStops().size());
	EXPECT_EQ(test.getStationsVector()->at(25), line->getStops()[0]);
	EXPECT_EQ(test.getStationsVector()->at(26), line->getStops()[1]);
	EXPECT_EQ(test.getStationsVector()->at(27), line->getStops()[2]);
	EXPECT_EQ(test.getStationsVector()->at(28), line->getStops()[3]);
	EXPECT_EQ(test.getStationsVector()->at(29), line->getStops()[4]);

	//clean
	delete line;
	delete lineMutation;
	delete dijkstraMock;
}

TEST(LineMutationTest, cannotMutateLineContainsDuplicates)
{
	//given
	TestNetworkCreator test;
	RandomNumberGeneratorMock* randomMock = new RandomNumberGeneratorMock();
	EXPECT_CALL(*randomMock, getInt(_, _))
		.WillOnce(Return(0));
	std::vector<Station*>* dijkstraResult = new std::vector<Station*>();
	dijkstraResult->push_back(test.getStationsVector()->at(4));
	dijkstraResult->push_back(test.getStationsVector()->at(5));
	dijkstraResult->push_back(test.getStationsVector()->at(6));
	dijkstraResult->push_back(test.getStationsVector()->at(22));
	dijkstraResult->push_back(test.getStationsVector()->at(21));
	dijkstraResult->push_back(test.getStationsVector()->at(20));
	dijkstraResult->push_back(test.getStationsVector()->at(19));
	DijkstraMock* dijkstraMock = new DijkstraMock();
	EXPECT_CALL(*dijkstraMock, dijkstra(test.getStationsVector()->at(12), test.getStationsVector()->at(19)))
		.WillOnce(Return(dijkstraResult));
	LineMutation* lineMutation = new LineMutation(randomMock, dijkstraMock);

	std::vector<Station*>* stops = new std::vector<Station*>();
	stops->push_back(test.getStationsVector()->at(11));
	stops->push_back(test.getStationsVector()->at(12));
	stops->push_back(test.getStationsVector()->at(19));
	stops->push_back(test.getStationsVector()->at(20));
	stops->push_back(test.getStationsVector()->at(21));
	stops->push_back(test.getStationsVector()->at(22));
	stops->push_back(test.getStationsVector()->at(6));
	stops->push_back(test.getStationsVector()->at(5));
	stops->push_back(test.getStationsVector()->at(4));
	stops->push_back(test.getStationsVector()->at(13));
	stops->push_back(test.getStationsVector()->at(14));
	std::vector<int> timetable;
	timetable.push_back(5);
	Line* line = new Line(1, "name", stops, timetable);

	//when
	lineMutation->mutation(line);

	//then
	EXPECT_EQ(11, line->getStops().size());
	EXPECT_EQ(test.getStationsVector()->at(11), line->getStops()[0]);
	EXPECT_EQ(test.getStationsVector()->at(12), line->getStops()[1]);
	EXPECT_EQ(test.getStationsVector()->at(19), line->getStops()[2]);
	EXPECT_EQ(test.getStationsVector()->at(20), line->getStops()[3]);
	EXPECT_EQ(test.getStationsVector()->at(21), line->getStops()[4]);
	EXPECT_EQ(test.getStationsVector()->at(22), line->getStops()[5]);
	EXPECT_EQ(test.getStationsVector()->at(6), line->getStops()[6]);
	EXPECT_EQ(test.getStationsVector()->at(5), line->getStops()[7]);
	EXPECT_EQ(test.getStationsVector()->at(4), line->getStops()[8]);
	EXPECT_EQ(test.getStationsVector()->at(13), line->getStops()[9]);
	EXPECT_EQ(test.getStationsVector()->at(14), line->getStops()[10]);

	//clean
	delete line;
	delete lineMutation;
	delete dijkstraMock;
}

TEST(LineMutationTest, canMutateLineIntegrationTest)
{
	//given
	TestNetworkCreator test;
	RandomNumberGeneratorMock* randomMock = new RandomNumberGeneratorMock();
	EXPECT_CALL(*randomMock, getInt(_, _))
		.WillOnce(Return(0));
	DijkstraImpl* dijkstraImpl = new DijkstraImpl(test.getStationsVector());
	QuasiShortestPath* quasiShortestPath = new QuasiShortestPath(test.getStationsVector(), dijkstraImpl);
	LineMutation* lineMutation = new LineMutation(randomMock, quasiShortestPath);

	std::vector<Station*>* stops = new std::vector<Station*>();
	stops->push_back(test.getStationsVector()->at(1));
	stops->push_back(test.getStationsVector()->at(2));
	stops->push_back(test.getStationsVector()->at(3));
	stops->push_back(test.getStationsVector()->at(4));
	stops->push_back(test.getStationsVector()->at(5));
	stops->push_back(test.getStationsVector()->at(6));
	std::vector<int> timetable;
	timetable.push_back(5);
	Line* line = new Line(1, "name", stops, timetable);

	//when
	lineMutation->mutation(line);

	//then
	EXPECT_EQ(8, line->getStops().size());
	EXPECT_EQ(test.getStationsVector()->at(1), line->getStops()[0]);
	EXPECT_EQ(test.getStationsVector()->at(2), line->getStops()[1]);
	EXPECT_EQ(test.getStationsVector()->at(3), line->getStops()[2]);
	EXPECT_EQ(test.getStationsVector()->at(4), line->getStops()[3]);
	EXPECT_EQ(test.getStationsVector()->at(13), line->getStops()[4]);
	EXPECT_EQ(test.getStationsVector()->at(14), line->getStops()[5]);
	EXPECT_EQ(test.getStationsVector()->at(7), line->getStops()[6]);
	EXPECT_EQ(test.getStationsVector()->at(6), line->getStops()[7]);

	//clean
	delete line;
	delete lineMutation;
	delete dijkstraImpl;
	delete quasiShortestPath;
}

TEST(LineMutationTest, cannotMutateLineOneJunctionIntegrationTest)
{
	//given
	TestNetworkCreator test;
	RandomNumberGeneratorMock* randomMock = new RandomNumberGeneratorMock();
	DijkstraImpl* dijkstraImpl = new DijkstraImpl(test.getStationsVector());
	QuasiShortestPath* quasiShortestPath = new QuasiShortestPath(test.getStationsVector(), dijkstraImpl);
	LineMutation* lineMutation = new LineMutation(randomMock, quasiShortestPath);

	std::vector<Station*>* stops = new std::vector<Station*>();
	stops->push_back(test.getStationsVector()->at(1));
	stops->push_back(test.getStationsVector()->at(2));
	stops->push_back(test.getStationsVector()->at(3));
	stops->push_back(test.getStationsVector()->at(4));
	stops->push_back(test.getStationsVector()->at(5));
	std::vector<int> timetable;
	timetable.push_back(5);
	Line* line = new Line(1, "name", stops, timetable);

	//when
	lineMutation->mutation(line);

	//then
	EXPECT_EQ(5, line->getStops().size());
	EXPECT_EQ(test.getStationsVector()->at(1), line->getStops()[0]);
	EXPECT_EQ(test.getStationsVector()->at(2), line->getStops()[1]);
	EXPECT_EQ(test.getStationsVector()->at(3), line->getStops()[2]);
	EXPECT_EQ(test.getStationsVector()->at(4), line->getStops()[3]);
	EXPECT_EQ(test.getStationsVector()->at(5), line->getStops()[4]);

	//clean
	delete line;
	delete lineMutation;
	delete dijkstraImpl;
	delete quasiShortestPath;
}

TEST(LineMutationTest, cannotMutateLineNoRoutesIntegrationTest)
{
	//given
	TestNetworkCreator test;
	RandomNumberGeneratorMock* randomMock = new RandomNumberGeneratorMock();
	DijkstraImpl* dijkstraImpl = new DijkstraImpl(test.getStationsVector());
	QuasiShortestPath* quasiShortestPath = new QuasiShortestPath(test.getStationsVector(), dijkstraImpl);
	EXPECT_CALL(*randomMock, getInt(_, _))
		.WillOnce(Return(0));
	LineMutation* lineMutation = new LineMutation(randomMock, quasiShortestPath);

	std::vector<Station*>* stops = new std::vector<Station*>();
	stops->push_back(test.getStationsVector()->at(25));
	stops->push_back(test.getStationsVector()->at(26));
	stops->push_back(test.getStationsVector()->at(27));
	stops->push_back(test.getStationsVector()->at(28));
	stops->push_back(test.getStationsVector()->at(29));
	std::vector<int> timetable;
	timetable.push_back(5);
	Line* line = new Line(1, "name", stops, timetable);

	//when
	lineMutation->mutation(line);

	//then
	EXPECT_EQ(5, line->getStops().size());
	EXPECT_EQ(test.getStationsVector()->at(25), line->getStops()[0]);
	EXPECT_EQ(test.getStationsVector()->at(26), line->getStops()[1]);
	EXPECT_EQ(test.getStationsVector()->at(27), line->getStops()[2]);
	EXPECT_EQ(test.getStationsVector()->at(28), line->getStops()[3]);
	EXPECT_EQ(test.getStationsVector()->at(29), line->getStops()[4]);

	//clean
	delete line;
	delete lineMutation;
	delete dijkstraImpl;
	delete quasiShortestPath;
}

TEST(LineMutationTest, cannotMutateLineContainsDuplicatesIntegrationTest)
{
	//given
	TestNetworkCreator test;
	RandomNumberGeneratorMock* randomMock = new RandomNumberGeneratorMock();
	EXPECT_CALL(*randomMock, getInt(_, _))
		.WillOnce(Return(0));
	DijkstraImpl* dijkstraImpl = new DijkstraImpl(test.getStationsVector());
	QuasiShortestPath* quasiShortestPath = new QuasiShortestPath(test.getStationsVector(), dijkstraImpl);
	LineMutation* lineMutation = new LineMutation(randomMock, quasiShortestPath);

	std::vector<Station*>* stops = new std::vector<Station*>();
	stops->push_back(test.getStationsVector()->at(11));
	stops->push_back(test.getStationsVector()->at(12));
	stops->push_back(test.getStationsVector()->at(19));
	stops->push_back(test.getStationsVector()->at(20));
	stops->push_back(test.getStationsVector()->at(21));
	stops->push_back(test.getStationsVector()->at(22));
	stops->push_back(test.getStationsVector()->at(6));
	stops->push_back(test.getStationsVector()->at(5));
	stops->push_back(test.getStationsVector()->at(4));
	stops->push_back(test.getStationsVector()->at(13));
	stops->push_back(test.getStationsVector()->at(14));
	std::vector<int> timetable;
	timetable.push_back(5);
	Line* line = new Line(1, "name", stops, timetable);

	//when
	lineMutation->mutation(line);

	//then
	EXPECT_EQ(11, line->getStops().size());
	EXPECT_EQ(test.getStationsVector()->at(11), line->getStops()[0]);
	EXPECT_EQ(test.getStationsVector()->at(12), line->getStops()[1]);
	EXPECT_EQ(test.getStationsVector()->at(19), line->getStops()[2]);
	EXPECT_EQ(test.getStationsVector()->at(20), line->getStops()[3]);
	EXPECT_EQ(test.getStationsVector()->at(21), line->getStops()[4]);
	EXPECT_EQ(test.getStationsVector()->at(22), line->getStops()[5]);
	EXPECT_EQ(test.getStationsVector()->at(6), line->getStops()[6]);
	EXPECT_EQ(test.getStationsVector()->at(5), line->getStops()[7]);
	EXPECT_EQ(test.getStationsVector()->at(4), line->getStops()[8]);
	EXPECT_EQ(test.getStationsVector()->at(13), line->getStops()[9]);
	EXPECT_EQ(test.getStationsVector()->at(14), line->getStops()[10]);

	//clean
	delete line;
	delete lineMutation;
	delete dijkstraImpl;
	delete quasiShortestPath;
}
