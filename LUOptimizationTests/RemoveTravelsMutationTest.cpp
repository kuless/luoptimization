#include "pch.h"
#include "../LUOptimization/RemoveTravelsMutation.h"
#include "RandomNumberGeneratorMock.h"

using ::testing::Return;

TEST(RemoveTravelsMutationTest, test)
{
	//given
	RandomNumberGeneratorMock* randomMock = new RandomNumberGeneratorMock();
	RemoveTravelsMutation* removeTravelsMutation = new RemoveTravelsMutation(randomMock);
	EXPECT_CALL(*randomMock, getInt(_, _))
		.WillOnce(Return(2))
		.WillOnce(Return(2))
		.WillOnce(Return(1));
	Station* station = new Station("1", "name", 1.0, 1.0);
	std::vector<Station*>* stops = new std::vector<Station*>();
	stops->push_back(station);
	std::vector<int> timetable;
	timetable.push_back(15);
	timetable.push_back(25);
	timetable.push_back(35);
	timetable.push_back(45);
	Line* line = new Line(1, "name", stops, timetable);

	//when
	removeTravelsMutation->mutation(line);

	//then
	EXPECT_EQ(2, line->getTimetableInMinutes().size());
	EXPECT_EQ(15, line->getTimetableInMinutes()[0]);
	EXPECT_EQ(45, line->getTimetableInMinutes()[1]);

	//clean
	delete station;
	delete line;
	delete removeTravelsMutation;
}
