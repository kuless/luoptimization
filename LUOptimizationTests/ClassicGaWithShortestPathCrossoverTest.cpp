#include "pch.h"
#include "../LUOptimization/ClassicGaWithShortestPathCrossover.h"
#include "../LUOptimization/DijkstraImpl.h"
#include "RandomNumberGeneratorMock.h"
#include "DijkstraMock.h"
#include "TestNetworkCreator.h"

TEST(ClassicGaWithShortestPathCrossoverTest, testCrossoverWithCommonJuntionsAndMinLines)
{
	//given
	RandomNumberGeneratorMock* randomMock = new RandomNumberGeneratorMock();
	DijkstraMock* dijkstraMock = new DijkstraMock();
	ClassicGaWithShortestPathCrossover<MultiCriteriaValue> crossoverOperator = ClassicGaWithShortestPathCrossover<MultiCriteriaValue>(randomMock, dijkstraMock);
	TestNetworkCreator test;
	EXPECT_CALL(*randomMock, getInt(_, _))
		.WillOnce(Return(1))
		.WillOnce(Return(3))
		.WillOnce(Return(3))
		.WillOnce(Return(1))
		.WillOnce(Return(3))
		.WillOnce(Return(3));

	std::vector<Station*>* dijkstraFirstResult = new std::vector<Station*>();
	dijkstraFirstResult->push_back(test.getStationsVector()->at(1));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(2));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(3));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(4));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(12));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(19));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(20));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(21));
	std::vector<Station*>* dijkstraSecondResult = new std::vector<Station*>();
	dijkstraSecondResult->push_back(test.getStationsVector()->at(20));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(19));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(12));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(4));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(3));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(2));
	std::vector<Station*>* dijkstraThirdResult = new std::vector<Station*>();
	dijkstraThirdResult->push_back(test.getStationsVector()->at(19));
	dijkstraThirdResult->push_back(test.getStationsVector()->at(12));
	dijkstraThirdResult->push_back(test.getStationsVector()->at(4));
	std::vector<Station*>* dijkstraForthResult = new std::vector<Station*>();
	dijkstraForthResult->push_back(test.getStationsVector()->at(0));
	dijkstraForthResult->push_back(test.getStationsVector()->at(8));
	dijkstraForthResult->push_back(test.getStationsVector()->at(7));
	dijkstraForthResult->push_back(test.getStationsVector()->at(14));
	dijkstraForthResult->push_back(test.getStationsVector()->at(13));
	std::vector<Station*>* dijkstraFifthResult = new std::vector<Station*>();
	dijkstraFifthResult->push_back(test.getStationsVector()->at(4));
	dijkstraFifthResult->push_back(test.getStationsVector()->at(13));
	dijkstraFifthResult->push_back(test.getStationsVector()->at(14));
	dijkstraFifthResult->push_back(test.getStationsVector()->at(7));
	dijkstraFifthResult->push_back(test.getStationsVector()->at(8));
	std::vector<Station*>* dijkstraSixthResult = new std::vector<Station*>();
	dijkstraSixthResult->push_back(test.getStationsVector()->at(7));
	std::vector<Station*>* dijkstraSeventhResult = new std::vector<Station*>();
	dijkstraSeventhResult->push_back(test.getStationsVector()->at(7));
	EXPECT_CALL(*dijkstraMock, dijkstra(_, _))
		.WillOnce(Return(dijkstraFirstResult))
		.WillOnce(Return(dijkstraSecondResult))
		.WillOnce(Return(dijkstraThirdResult))
		.WillOnce(Return(dijkstraForthResult))
		.WillOnce(Return(dijkstraFifthResult))
		.WillOnce(Return(dijkstraSixthResult))
		.WillOnce(Return(dijkstraSeventhResult));

	//when
	std::pair<AbstractNetwork<MultiCriteriaValue>*, AbstractNetwork<MultiCriteriaValue>*>* result = crossoverOperator.crossover(test.getPairToCrossovers());

	//then
	std::vector<Station*>* stations = test.getStationsVector();

	EXPECT_EQ(2, result->first->getLines()->size());
	EXPECT_EQ(9, result->first->getLines()->at(0)->getStops().size());
	EXPECT_EQ(stations->at(1), result->first->getLines()->at(0)->getStops().at(0));
	EXPECT_EQ(stations->at(2), result->first->getLines()->at(0)->getStops().at(1));
	EXPECT_EQ(stations->at(3), result->first->getLines()->at(0)->getStops().at(2));
	EXPECT_EQ(stations->at(4), result->first->getLines()->at(0)->getStops().at(3));
	EXPECT_EQ(stations->at(12), result->first->getLines()->at(0)->getStops().at(4));
	EXPECT_EQ(stations->at(19), result->first->getLines()->at(0)->getStops().at(5));
	EXPECT_EQ(stations->at(20), result->first->getLines()->at(0)->getStops().at(6));
	EXPECT_EQ(stations->at(21), result->first->getLines()->at(0)->getStops().at(7));
	EXPECT_EQ(stations->at(22), result->first->getLines()->at(0)->getStops().at(8));
	EXPECT_EQ(7, result->first->getLines()->at(1)->getStops().size());
	EXPECT_EQ(stations->at(0), result->first->getLines()->at(1)->getStops().at(0));
	EXPECT_EQ(stations->at(8), result->first->getLines()->at(1)->getStops().at(1));
	EXPECT_EQ(stations->at(7), result->first->getLines()->at(1)->getStops().at(2));
	EXPECT_EQ(stations->at(15), result->first->getLines()->at(1)->getStops().at(3));
	EXPECT_EQ(stations->at(16), result->first->getLines()->at(1)->getStops().at(4));
	EXPECT_EQ(stations->at(9), result->first->getLines()->at(1)->getStops().at(5));
	EXPECT_EQ(stations->at(17), result->first->getLines()->at(1)->getStops().at(6));

	EXPECT_EQ(4, result->second->getLines()->size());
	EXPECT_EQ(10, result->second->getLines()->at(0)->getStops().size());
	EXPECT_EQ(stations->at(18), result->second->getLines()->at(0)->getStops().at(0));
	EXPECT_EQ(stations->at(19), result->second->getLines()->at(0)->getStops().at(1));
	EXPECT_EQ(stations->at(12), result->second->getLines()->at(0)->getStops().at(2));
	EXPECT_EQ(stations->at(4), result->second->getLines()->at(0)->getStops().at(3));
	EXPECT_EQ(stations->at(5), result->second->getLines()->at(0)->getStops().at(4));
	EXPECT_EQ(stations->at(6), result->second->getLines()->at(0)->getStops().at(5));
	EXPECT_EQ(stations->at(7), result->second->getLines()->at(0)->getStops().at(6));
	EXPECT_EQ(stations->at(8), result->second->getLines()->at(0)->getStops().at(7));
	EXPECT_EQ(stations->at(9), result->second->getLines()->at(0)->getStops().at(8));
	EXPECT_EQ(stations->at(10), result->second->getLines()->at(0)->getStops().at(9));
	EXPECT_EQ(8, result->second->getLines()->at(1)->getStops().size());
	EXPECT_EQ(stations->at(11), result->second->getLines()->at(1)->getStops().at(0));
	EXPECT_EQ(stations->at(12), result->second->getLines()->at(1)->getStops().at(1));
	EXPECT_EQ(stations->at(4), result->second->getLines()->at(1)->getStops().at(2));
	EXPECT_EQ(stations->at(13), result->second->getLines()->at(1)->getStops().at(3));
	EXPECT_EQ(stations->at(14), result->second->getLines()->at(1)->getStops().at(4));
	EXPECT_EQ(stations->at(7), result->second->getLines()->at(1)->getStops().at(5));
	EXPECT_EQ(stations->at(6), result->second->getLines()->at(1)->getStops().at(6));
	EXPECT_EQ(stations->at(22), result->second->getLines()->at(1)->getStops().at(7));
	EXPECT_EQ(4, result->second->getLines()->at(2)->getStops().size());
	EXPECT_EQ(stations->at(23), result->second->getLines()->at(2)->getStops().at(0));
	EXPECT_EQ(stations->at(24), result->second->getLines()->at(2)->getStops().at(1));
	EXPECT_EQ(stations->at(25), result->second->getLines()->at(2)->getStops().at(2));
	EXPECT_EQ(stations->at(26), result->second->getLines()->at(2)->getStops().at(3));
	EXPECT_EQ(6, result->second->getLines()->at(3)->getStops().size());
	EXPECT_EQ(stations->at(7), result->second->getLines()->at(3)->getStops().at(0));
	EXPECT_EQ(stations->at(14), result->second->getLines()->at(3)->getStops().at(1));
	EXPECT_EQ(stations->at(13), result->second->getLines()->at(3)->getStops().at(2));
	EXPECT_EQ(stations->at(4), result->second->getLines()->at(3)->getStops().at(3));
	EXPECT_EQ(stations->at(3), result->second->getLines()->at(3)->getStops().at(4));
	EXPECT_EQ(stations->at(2), result->second->getLines()->at(3)->getStops().at(5));

	//clean
	delete dijkstraMock;
}

TEST(ClassicGaWithShortestPathCrossoverTest, testCrossoverWithCommonJuntionsAndMinLinesIntegrationTest)
{
	//given
	RandomNumberGeneratorMock* randomMock = new RandomNumberGeneratorMock();
	TestNetworkCreator test;
	DijkstraImpl* dijkstraImpl = new DijkstraImpl(test.getStationsVector());
	ClassicGaWithShortestPathCrossover<MultiCriteriaValue> crossoverOperator = ClassicGaWithShortestPathCrossover<MultiCriteriaValue>(randomMock, dijkstraImpl);
	EXPECT_CALL(*randomMock, getInt(_, _))
		.WillOnce(Return(1))
		.WillOnce(Return(3))
		.WillOnce(Return(3))
		.WillOnce(Return(1))
		.WillOnce(Return(3))
		.WillOnce(Return(3));

	//when
	std::pair<AbstractNetwork<MultiCriteriaValue>*, AbstractNetwork<MultiCriteriaValue>*>* result = crossoverOperator.crossover(test.getPairToCrossovers());

	//then
	std::vector<Station*>* stations = test.getStationsVector();

	EXPECT_EQ(2, result->first->getLines()->size());
	EXPECT_EQ(9, result->first->getLines()->at(0)->getStops().size());
	EXPECT_EQ(stations->at(1), result->first->getLines()->at(0)->getStops().at(0));
	EXPECT_EQ(stations->at(2), result->first->getLines()->at(0)->getStops().at(1));
	EXPECT_EQ(stations->at(3), result->first->getLines()->at(0)->getStops().at(2));
	EXPECT_EQ(stations->at(4), result->first->getLines()->at(0)->getStops().at(3));
	EXPECT_EQ(stations->at(12), result->first->getLines()->at(0)->getStops().at(4));
	EXPECT_EQ(stations->at(19), result->first->getLines()->at(0)->getStops().at(5));
	EXPECT_EQ(stations->at(20), result->first->getLines()->at(0)->getStops().at(6));
	EXPECT_EQ(stations->at(21), result->first->getLines()->at(0)->getStops().at(7));
	EXPECT_EQ(stations->at(22), result->first->getLines()->at(0)->getStops().at(8));
	EXPECT_EQ(7, result->first->getLines()->at(1)->getStops().size());
	EXPECT_EQ(stations->at(0), result->first->getLines()->at(1)->getStops().at(0));
	EXPECT_EQ(stations->at(8), result->first->getLines()->at(1)->getStops().at(1));
	EXPECT_EQ(stations->at(7), result->first->getLines()->at(1)->getStops().at(2));
	EXPECT_EQ(stations->at(15), result->first->getLines()->at(1)->getStops().at(3));
	EXPECT_EQ(stations->at(16), result->first->getLines()->at(1)->getStops().at(4));
	EXPECT_EQ(stations->at(9), result->first->getLines()->at(1)->getStops().at(5));
	EXPECT_EQ(stations->at(17), result->first->getLines()->at(1)->getStops().at(6));

	EXPECT_EQ(4, result->second->getLines()->size());
	EXPECT_EQ(10, result->second->getLines()->at(0)->getStops().size());
	EXPECT_EQ(stations->at(18), result->second->getLines()->at(0)->getStops().at(0));
	EXPECT_EQ(stations->at(19), result->second->getLines()->at(0)->getStops().at(1));
	EXPECT_EQ(stations->at(12), result->second->getLines()->at(0)->getStops().at(2));
	EXPECT_EQ(stations->at(4), result->second->getLines()->at(0)->getStops().at(3));
	EXPECT_EQ(stations->at(5), result->second->getLines()->at(0)->getStops().at(4));
	EXPECT_EQ(stations->at(6), result->second->getLines()->at(0)->getStops().at(5));
	EXPECT_EQ(stations->at(7), result->second->getLines()->at(0)->getStops().at(6));
	EXPECT_EQ(stations->at(8), result->second->getLines()->at(0)->getStops().at(7));
	EXPECT_EQ(stations->at(9), result->second->getLines()->at(0)->getStops().at(8));
	EXPECT_EQ(stations->at(10), result->second->getLines()->at(0)->getStops().at(9));
	EXPECT_EQ(8, result->second->getLines()->at(1)->getStops().size());
	EXPECT_EQ(stations->at(11), result->second->getLines()->at(1)->getStops().at(0));
	EXPECT_EQ(stations->at(12), result->second->getLines()->at(1)->getStops().at(1));
	EXPECT_EQ(stations->at(4), result->second->getLines()->at(1)->getStops().at(2));
	EXPECT_EQ(stations->at(13), result->second->getLines()->at(1)->getStops().at(3));
	EXPECT_EQ(stations->at(14), result->second->getLines()->at(1)->getStops().at(4));
	EXPECT_EQ(stations->at(7), result->second->getLines()->at(1)->getStops().at(5));
	EXPECT_EQ(stations->at(6), result->second->getLines()->at(1)->getStops().at(6));
	EXPECT_EQ(stations->at(22), result->second->getLines()->at(1)->getStops().at(7));
	EXPECT_EQ(4, result->second->getLines()->at(2)->getStops().size());
	EXPECT_EQ(stations->at(23), result->second->getLines()->at(2)->getStops().at(0));
	EXPECT_EQ(stations->at(24), result->second->getLines()->at(2)->getStops().at(1));
	EXPECT_EQ(stations->at(25), result->second->getLines()->at(2)->getStops().at(2));
	EXPECT_EQ(stations->at(26), result->second->getLines()->at(2)->getStops().at(3));
	EXPECT_EQ(6, result->second->getLines()->at(3)->getStops().size());
	EXPECT_EQ(stations->at(7), result->second->getLines()->at(3)->getStops().at(0));
	EXPECT_EQ(stations->at(14), result->second->getLines()->at(3)->getStops().at(1));
	EXPECT_EQ(stations->at(13), result->second->getLines()->at(3)->getStops().at(2));
	EXPECT_EQ(stations->at(4), result->second->getLines()->at(3)->getStops().at(3));
	EXPECT_EQ(stations->at(3), result->second->getLines()->at(3)->getStops().at(4));
	EXPECT_EQ(stations->at(2), result->second->getLines()->at(3)->getStops().at(5));

	//clean
}
