#include "pch.h"
#include "../LUOptimization/ShortDistancePopulationGenerator.h"
#include "../LUOptimization/MultiCriteriaNetworkBuilder.h"
#include "RandomNumberGeneratorMock.h"
#include "DijkstraMock.h"
#include "TestNetworkCreator.h"

TEST(ShortDistancePopulationGeneratorTest, createNetworkThatHasReachedCoveredThreshold)
{
	//given
	RandomNumberGeneratorMock* randomMock = new RandomNumberGeneratorMock();
	DijkstraMock* dijkstraMock = new DijkstraMock();
	MultiCriteriaNetworkBuilder* builder = new MultiCriteriaNetworkBuilder();
	ShortDistancePopulationGenerator<MultiCriteriaValue>* generator = new ShortDistancePopulationGenerator<MultiCriteriaValue>(randomMock, builder, dijkstraMock);
	TestNetworkCreator test;
	std::vector<Travel*> travels;
	PopulationGeneratorInputParameters* inputParameters = new PopulationGeneratorInputParameters();
	inputParameters->setPopulationSize(1);
	inputParameters->setNumberOfLines(3);
	inputParameters->setPercentOfCoveredStations(60);

	EXPECT_CALL(*randomMock, getInt(_, _))
		.WillOnce(Return(1))
		.WillOnce(Return(10))
		.WillOnce(Return(1))
		.WillOnce(Return(1))
		.WillOnce(Return(10)) //first line

		.WillOnce(Return(11))
		.WillOnce(Return(17))
		.WillOnce(Return(1))
		.WillOnce(Return(1))
		.WillOnce(Return(10)) //second line

		.WillOnce(Return(18))
		.WillOnce(Return(25))
		.WillOnce(Return(1))
		.WillOnce(Return(1))
		.WillOnce(Return(10)); //third line

	std::vector<Station*>* dijkstraFirstResult = new std::vector<Station*>();
	dijkstraFirstResult->push_back(test.getStationsVector()->at(1));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(2));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(3));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(4));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(5));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(6));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(7));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(8));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(9));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(10));

	std::vector<Station*>* dijkstraSecondResult = new std::vector<Station*>();
	dijkstraSecondResult->push_back(test.getStationsVector()->at(11));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(12));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(4));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(13));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(14));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(7));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(15));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(16));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(9));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(17));

	std::vector<Station*>* dijkstraThirdResult = new std::vector<Station*>();
	dijkstraThirdResult->push_back(test.getStationsVector()->at(18));
	dijkstraThirdResult->push_back(test.getStationsVector()->at(19));
	dijkstraThirdResult->push_back(test.getStationsVector()->at(20));
	dijkstraThirdResult->push_back(test.getStationsVector()->at(21));
	dijkstraThirdResult->push_back(test.getStationsVector()->at(22));
	dijkstraThirdResult->push_back(test.getStationsVector()->at(23));
	dijkstraThirdResult->push_back(test.getStationsVector()->at(24));
	dijkstraThirdResult->push_back(test.getStationsVector()->at(25));

	EXPECT_CALL(*dijkstraMock, dijkstra(_, _))
		.WillOnce(Return(dijkstraFirstResult))
		.WillOnce(Return(dijkstraSecondResult))
		.WillOnce(Return(dijkstraThirdResult));

	//when
	std::vector<AbstractNetwork<MultiCriteriaValue>*>* result = generator->generatePopulation(*test.getStationsVector(), travels, inputParameters);

	//then
	std::vector<Station*>* stations = test.getStationsVector();

	EXPECT_EQ(1, result->size());
	EXPECT_EQ(3, result->at(0)->getLines()->size());

	EXPECT_EQ(10, result->at(0)->getLines()->at(0)->getStops().size());
	EXPECT_EQ(stations->at(1), result->at(0)->getLines()->at(0)->getStops()[0]);
	EXPECT_EQ(stations->at(2), result->at(0)->getLines()->at(0)->getStops()[1]);
	EXPECT_EQ(stations->at(3), result->at(0)->getLines()->at(0)->getStops()[2]);
	EXPECT_EQ(stations->at(4), result->at(0)->getLines()->at(0)->getStops()[3]);
	EXPECT_EQ(stations->at(5), result->at(0)->getLines()->at(0)->getStops()[4]);
	EXPECT_EQ(stations->at(6), result->at(0)->getLines()->at(0)->getStops()[5]);
	EXPECT_EQ(stations->at(7), result->at(0)->getLines()->at(0)->getStops()[6]);
	EXPECT_EQ(stations->at(8), result->at(0)->getLines()->at(0)->getStops()[7]);
	EXPECT_EQ(stations->at(9), result->at(0)->getLines()->at(0)->getStops()[8]);
	EXPECT_EQ(stations->at(10), result->at(0)->getLines()->at(0)->getStops()[9]);

	EXPECT_EQ(10, result->at(0)->getLines()->at(1)->getStops().size());
	EXPECT_EQ(stations->at(11), result->at(0)->getLines()->at(1)->getStops()[0]);
	EXPECT_EQ(stations->at(12), result->at(0)->getLines()->at(1)->getStops()[1]);
	EXPECT_EQ(stations->at(4), result->at(0)->getLines()->at(1)->getStops()[2]);
	EXPECT_EQ(stations->at(13), result->at(0)->getLines()->at(1)->getStops()[3]);
	EXPECT_EQ(stations->at(14), result->at(0)->getLines()->at(1)->getStops()[4]);
	EXPECT_EQ(stations->at(7), result->at(0)->getLines()->at(1)->getStops()[5]);
	EXPECT_EQ(stations->at(15), result->at(0)->getLines()->at(1)->getStops()[6]);
	EXPECT_EQ(stations->at(16), result->at(0)->getLines()->at(1)->getStops()[7]);
	EXPECT_EQ(stations->at(9), result->at(0)->getLines()->at(1)->getStops()[8]);
	EXPECT_EQ(stations->at(17), result->at(0)->getLines()->at(1)->getStops()[9]);

	EXPECT_EQ(8, result->at(0)->getLines()->at(2)->getStops().size());
	EXPECT_EQ(stations->at(18), result->at(0)->getLines()->at(2)->getStops()[0]);
	EXPECT_EQ(stations->at(19), result->at(0)->getLines()->at(2)->getStops()[1]);
	EXPECT_EQ(stations->at(20), result->at(0)->getLines()->at(2)->getStops()[2]);
	EXPECT_EQ(stations->at(21), result->at(0)->getLines()->at(2)->getStops()[3]);
	EXPECT_EQ(stations->at(22), result->at(0)->getLines()->at(2)->getStops()[4]);
	EXPECT_EQ(stations->at(23), result->at(0)->getLines()->at(2)->getStops()[5]);
	EXPECT_EQ(stations->at(24), result->at(0)->getLines()->at(2)->getStops()[6]);
	EXPECT_EQ(stations->at(25), result->at(0)->getLines()->at(2)->getStops()[7]);

	//clean
	delete generator;
	delete dijkstraMock;
	delete inputParameters;
	delete dijkstraFirstResult;
	delete dijkstraSecondResult;
	delete dijkstraThirdResult;
}

TEST(ShortDistancePopulationGeneratorTest, createNetworkThatHasNotReachedCoveredThreshold)
{
	//given
	RandomNumberGeneratorMock* randomMock = new RandomNumberGeneratorMock();
	DijkstraMock* dijkstraMock = new DijkstraMock();
	MultiCriteriaNetworkBuilder* builder = new MultiCriteriaNetworkBuilder();
	ShortDistancePopulationGenerator<MultiCriteriaValue>* generator = new ShortDistancePopulationGenerator<MultiCriteriaValue>(randomMock, builder, dijkstraMock);
	TestNetworkCreator test;
	std::vector<Travel*> travels;
	PopulationGeneratorInputParameters* inputParameters = new PopulationGeneratorInputParameters();
	inputParameters->setPopulationSize(1);
	inputParameters->setNumberOfLines(3);
	inputParameters->setPercentOfCoveredStations(60);

	EXPECT_CALL(*randomMock, getInt(_, _))
		.WillOnce(Return(1))
		.WillOnce(Return(10))
		.WillOnce(Return(1))
		.WillOnce(Return(1))
		.WillOnce(Return(10)) //first line

		.WillOnce(Return(1))
		.WillOnce(Return(10))
		.WillOnce(Return(1))
		.WillOnce(Return(1))
		.WillOnce(Return(10)) //second line

		.WillOnce(Return(12))
		.WillOnce(Return(15))
		.WillOnce(Return(1))
		.WillOnce(Return(1))
		.WillOnce(Return(10)) //third line

		.WillOnce(Return(18))
		.WillOnce(Return(30))
		.WillOnce(Return(1))
		.WillOnce(Return(1))
		.WillOnce(Return(10)) //fourth line

		.WillOnce(Return(8))
		.WillOnce(Return(22))
		.WillOnce(Return(1))
		.WillOnce(Return(1))
		.WillOnce(Return(10)); //fifth line

	std::vector<Station*>* dijkstraFirstResult = new std::vector<Station*>();
	dijkstraFirstResult->push_back(test.getStationsVector()->at(1));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(2));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(3));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(4));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(5));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(6));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(7));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(8));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(9));
	dijkstraFirstResult->push_back(test.getStationsVector()->at(10));

	std::vector<Station*>* dijkstraSecondResult = new std::vector<Station*>();
	dijkstraSecondResult->push_back(test.getStationsVector()->at(1));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(2));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(3));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(4));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(5));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(6));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(7));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(8));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(9));
	dijkstraSecondResult->push_back(test.getStationsVector()->at(10));

	std::vector<Station*>* dijkstraThirdResult = new std::vector<Station*>();
	dijkstraThirdResult->push_back(test.getStationsVector()->at(12));
	dijkstraThirdResult->push_back(test.getStationsVector()->at(4));
	dijkstraThirdResult->push_back(test.getStationsVector()->at(13));
	dijkstraThirdResult->push_back(test.getStationsVector()->at(14));
	dijkstraThirdResult->push_back(test.getStationsVector()->at(7));
	dijkstraThirdResult->push_back(test.getStationsVector()->at(15));

	std::vector<Station*>* dijkstraFourthResult = new std::vector<Station*>();
	dijkstraFourthResult->push_back(test.getStationsVector()->at(18));
	dijkstraFourthResult->push_back(test.getStationsVector()->at(19));
	dijkstraFourthResult->push_back(test.getStationsVector()->at(20));
	dijkstraFourthResult->push_back(test.getStationsVector()->at(21));
	dijkstraFourthResult->push_back(test.getStationsVector()->at(22));
	dijkstraFourthResult->push_back(test.getStationsVector()->at(23));
	dijkstraFourthResult->push_back(test.getStationsVector()->at(24));
	dijkstraFourthResult->push_back(test.getStationsVector()->at(25));
	dijkstraFourthResult->push_back(test.getStationsVector()->at(26));
	dijkstraFourthResult->push_back(test.getStationsVector()->at(27));
	dijkstraFourthResult->push_back(test.getStationsVector()->at(28));
	dijkstraFourthResult->push_back(test.getStationsVector()->at(30));

	std::vector<Station*>* dijkstraFifthResult = new std::vector<Station*>();
	dijkstraFifthResult->push_back(test.getStationsVector()->at(8));
	dijkstraFifthResult->push_back(test.getStationsVector()->at(7));
	dijkstraFifthResult->push_back(test.getStationsVector()->at(6));
	dijkstraFifthResult->push_back(test.getStationsVector()->at(22));

	EXPECT_CALL(*dijkstraMock, dijkstra(_, _))
		.WillOnce(Return(dijkstraFirstResult))
		.WillOnce(Return(dijkstraSecondResult))
		.WillOnce(Return(dijkstraThirdResult))
		.WillOnce(Return(dijkstraFourthResult))
		.WillOnce(Return(dijkstraFifthResult));

	//when
	std::vector<AbstractNetwork<MultiCriteriaValue>*>* result = generator->generatePopulation(*test.getStationsVector(), travels, inputParameters);

	//then
	std::vector<Station*>* stations = test.getStationsVector();

	EXPECT_EQ(1, result->size());
	EXPECT_EQ(4, result->at(0)->getLines()->size());

	EXPECT_EQ(10, result->at(0)->getLines()->at(0)->getStops().size());
	EXPECT_EQ(stations->at(1), result->at(0)->getLines()->at(0)->getStops()[0]);
	EXPECT_EQ(stations->at(2), result->at(0)->getLines()->at(0)->getStops()[1]);
	EXPECT_EQ(stations->at(3), result->at(0)->getLines()->at(0)->getStops()[2]);
	EXPECT_EQ(stations->at(4), result->at(0)->getLines()->at(0)->getStops()[3]);
	EXPECT_EQ(stations->at(5), result->at(0)->getLines()->at(0)->getStops()[4]);
	EXPECT_EQ(stations->at(6), result->at(0)->getLines()->at(0)->getStops()[5]);
	EXPECT_EQ(stations->at(7), result->at(0)->getLines()->at(0)->getStops()[6]);
	EXPECT_EQ(stations->at(8), result->at(0)->getLines()->at(0)->getStops()[7]);
	EXPECT_EQ(stations->at(9), result->at(0)->getLines()->at(0)->getStops()[8]);
	EXPECT_EQ(stations->at(10), result->at(0)->getLines()->at(0)->getStops()[9]);

	EXPECT_EQ(10, result->at(0)->getLines()->at(1)->getStops().size());
	EXPECT_EQ(stations->at(1), result->at(0)->getLines()->at(1)->getStops()[0]);
	EXPECT_EQ(stations->at(2), result->at(0)->getLines()->at(1)->getStops()[1]);
	EXPECT_EQ(stations->at(3), result->at(0)->getLines()->at(1)->getStops()[2]);
	EXPECT_EQ(stations->at(4), result->at(0)->getLines()->at(1)->getStops()[3]);
	EXPECT_EQ(stations->at(5), result->at(0)->getLines()->at(1)->getStops()[4]);
	EXPECT_EQ(stations->at(6), result->at(0)->getLines()->at(1)->getStops()[5]);
	EXPECT_EQ(stations->at(7), result->at(0)->getLines()->at(1)->getStops()[6]);
	EXPECT_EQ(stations->at(8), result->at(0)->getLines()->at(1)->getStops()[7]);
	EXPECT_EQ(stations->at(9), result->at(0)->getLines()->at(1)->getStops()[8]);
	EXPECT_EQ(stations->at(10), result->at(0)->getLines()->at(1)->getStops()[9]);

	EXPECT_EQ(12, result->at(0)->getLines()->at(2)->getStops().size());
	EXPECT_EQ(stations->at(18), result->at(0)->getLines()->at(2)->getStops()[0]);
	EXPECT_EQ(stations->at(19), result->at(0)->getLines()->at(2)->getStops()[1]);
	EXPECT_EQ(stations->at(20), result->at(0)->getLines()->at(2)->getStops()[2]);
	EXPECT_EQ(stations->at(21), result->at(0)->getLines()->at(2)->getStops()[3]);
	EXPECT_EQ(stations->at(22), result->at(0)->getLines()->at(2)->getStops()[4]);
	EXPECT_EQ(stations->at(23), result->at(0)->getLines()->at(2)->getStops()[5]);
	EXPECT_EQ(stations->at(24), result->at(0)->getLines()->at(2)->getStops()[6]);
	EXPECT_EQ(stations->at(25), result->at(0)->getLines()->at(2)->getStops()[7]);
	EXPECT_EQ(stations->at(26), result->at(0)->getLines()->at(2)->getStops()[8]);
	EXPECT_EQ(stations->at(27), result->at(0)->getLines()->at(2)->getStops()[9]);
	EXPECT_EQ(stations->at(28), result->at(0)->getLines()->at(2)->getStops()[10]);
	EXPECT_EQ(stations->at(30), result->at(0)->getLines()->at(2)->getStops()[11]);

	EXPECT_EQ(4, result->at(0)->getLines()->at(3)->getStops().size());
	EXPECT_EQ(stations->at(8), result->at(0)->getLines()->at(3)->getStops()[0]);
	EXPECT_EQ(stations->at(7), result->at(0)->getLines()->at(3)->getStops()[1]);
	EXPECT_EQ(stations->at(6), result->at(0)->getLines()->at(3)->getStops()[2]);
	EXPECT_EQ(stations->at(22), result->at(0)->getLines()->at(3)->getStops()[3]);

	//clean
	delete generator;
	delete dijkstraMock;
	delete inputParameters;
	delete dijkstraFirstResult;
	delete dijkstraSecondResult;
	delete dijkstraFourthResult;
	delete dijkstraFifthResult;
}
