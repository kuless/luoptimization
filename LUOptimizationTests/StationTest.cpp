#include "pch.h"
#include "../LUOptimization/Station.h"

using namespace std;

TEST(StationTest, containsConnectionFalseTest)
{
	//given
	Station station = Station("1", "Name", 12, 12);

	//when
	Station contains = Station("2", "Name", 12, 12);
	bool result = station.containsConnection(contains);

	//then
	EXPECT_FALSE(result);
}

TEST(StationTest, containsConnectionTrueTest)
{
	//given
	Station* destination = new Station("2", "Name", 12, 12);
	vector<Connection*> connections;
	int time = 5;
	double distance = 1.0;
	connections.push_back(new Connection(time, distance, destination));
	Station station = Station("1", "Name", 12, 12, connections);

	//when
	Station contains = Station("2", "Name", 12, 12);
	bool result = station.containsConnection(contains);

	//then
	EXPECT_TRUE(result);

	//clean
	delete destination;
}

TEST(StationTest, addConnectionSuccessTest)
{
	//given
	Station* destination = new Station("2", "Name", 12, 12);
	vector<Connection*> connections;
	int time = 5;
	double distance = 1.0;
	connections.push_back(new Connection(time, distance, destination));
	Station station = Station("1", "Name", 12, 12, connections);

	//when
	Station* newStation = new Station("3", "Name", 12, 12);
	int newTime = 1;
	double newDistance = 2.0;
	Station* result = station.addConnection(new Connection(newTime, newDistance, newStation));

	//then
	EXPECT_EQ(newStation, result);

	//clean
	delete destination;
	delete newStation;
}

TEST(StationTest, addConnectionFailedTest)
{
	//given
	Station* destination = new Station("2", "Name", 12, 12);
	vector<Connection*> connections;
	int time = 5;
	double distance = 1.0;
	connections.push_back(new Connection(time, distance, destination));
	Station station = Station("1", "Name", 12, 12, connections);

	//when
	Station* newStation = new Station("2", "Name", 12, 12);
	int newTime = 1;
	double newDistance = 2.0;
	Station* result = station.addConnection(new Connection(newTime, newDistance, newStation));

	//then
	EXPECT_EQ(NULL, result);

	//clean
	delete destination;
	delete newStation;
}

TEST(StationTest, getJourneyTimeSuccessTest)
{
	//given
	Station* destination = new Station("2", "Name", 12, 12);
	vector<Connection*> connections;
	int time = 5;
	double distance = 1.0;
	connections.push_back(new Connection(time, distance, destination));
	Station station = Station("1", "Name", 12, 12, connections);

	//when
	string stationId = "2";
	int result = station.getJourneyTime(stationId);

	//then
	int expected = 5;
	EXPECT_EQ(expected, result);

	//clean
	delete destination;
}

TEST(StationTest, getJourneyTimeFailedTest)
{
	//given
	Station* destination = new Station("2", "NewName", 12, 12);
	vector<Connection*> connections;
	int time = 5;
	double distance = 1.0;
	connections.push_back(new Connection(time, distance, destination));
	Station station = Station("1", "Name", 12, 12, connections);

	//when
	string stationId = "not existing name";
	int result = station.getJourneyTime(stationId);

	//then
	int expected = -1;
	EXPECT_EQ(expected, result);

	//clean
	delete destination;
}
