#include "pch.h"
#include "../LUOptimization/QuasiShortestPath.h"
#include "DijkstraMock.h"
#include "TestNetworkCreator.h"

using namespace std;

TEST(QuasiShortestPathTest, returnPath)
{
	//given
	TestNetworkCreator test;
	DijkstraMock* dijkstraMock = new DijkstraMock();
	std::vector<Station*>* dijkstraResult = new std::vector<Station*>();
	dijkstraResult->push_back(test.getStationsVector()->at(12));
	dijkstraResult->push_back(test.getStationsVector()->at(4));
	dijkstraResult->push_back(test.getStationsVector()->at(5));
	dijkstraResult->push_back(test.getStationsVector()->at(6));
	EXPECT_CALL(*dijkstraMock, dijkstra(test.getStationsVector()->at(12), test.getStationsVector()->at(6)))
		.WillOnce(Return(dijkstraResult));
	QuasiShortestPath* quasiShortestPath = new QuasiShortestPath(test.getStationsVector(), dijkstraMock);

	//when
	vector<Station*>* result = quasiShortestPath->dijkstra(test.getStationsVector()->at(12), test.getStationsVector()->at(6));

	//then
	EXPECT_EQ(6, result->size());

	EXPECT_EQ(test.getStationsVector()->at(12), result->at(0));
	EXPECT_EQ(test.getStationsVector()->at(19), result->at(1));
	EXPECT_EQ(test.getStationsVector()->at(20), result->at(2));
	EXPECT_EQ(test.getStationsVector()->at(21), result->at(3));
	EXPECT_EQ(test.getStationsVector()->at(22), result->at(4));
	EXPECT_EQ(test.getStationsVector()->at(6), result->at(5));

	//clean
	delete quasiShortestPath;
	delete dijkstraMock;
}

TEST(QuasiShortestPathTest, returnSecondPathFromCache)
{
	//given
	TestNetworkCreator test;
	DijkstraMock* dijkstraMock = new DijkstraMock();
	std::vector<Station*>* dijkstraResult = new std::vector<Station*>();
	dijkstraResult->push_back(test.getStationsVector()->at(12));
	dijkstraResult->push_back(test.getStationsVector()->at(4));
	dijkstraResult->push_back(test.getStationsVector()->at(5));
	dijkstraResult->push_back(test.getStationsVector()->at(6));
	EXPECT_CALL(*dijkstraMock, dijkstra(test.getStationsVector()->at(12), test.getStationsVector()->at(6)))
		.WillOnce(Return(dijkstraResult));
	QuasiShortestPath* quasiShortestPath = new QuasiShortestPath(test.getStationsVector(), dijkstraMock);

	//when
	quasiShortestPath->dijkstra(test.getStationsVector()->at(12), test.getStationsVector()->at(6));
	vector<Station*>* result = quasiShortestPath->dijkstra(test.getStationsVector()->at(12), test.getStationsVector()->at(5));

	//then
	EXPECT_EQ(7, result->size());

	EXPECT_EQ(test.getStationsVector()->at(12), result->at(0));
	EXPECT_EQ(test.getStationsVector()->at(19), result->at(1));
	EXPECT_EQ(test.getStationsVector()->at(20), result->at(2));
	EXPECT_EQ(test.getStationsVector()->at(21), result->at(3));
	EXPECT_EQ(test.getStationsVector()->at(22), result->at(4));
	EXPECT_EQ(test.getStationsVector()->at(6), result->at(5));
	EXPECT_EQ(test.getStationsVector()->at(5), result->at(6));

	//clean
	delete quasiShortestPath;
	delete dijkstraMock;
}

TEST(QuasiShortestPathTest, returnEmptyLineNoRoute)
{
	//given
	TestNetworkCreator test;
	DijkstraMock* dijkstraMock = new DijkstraMock();
	std::vector<Station*>* dijkstraResult = new std::vector<Station*>();
	dijkstraResult->push_back(test.getStationsVector()->at(1));
	dijkstraResult->push_back(test.getStationsVector()->at(2));
	dijkstraResult->push_back(test.getStationsVector()->at(3));
	dijkstraResult->push_back(test.getStationsVector()->at(4));
	dijkstraResult->push_back(test.getStationsVector()->at(5));
	dijkstraResult->push_back(test.getStationsVector()->at(6));
	dijkstraResult->push_back(test.getStationsVector()->at(7));
	dijkstraResult->push_back(test.getStationsVector()->at(8));
	dijkstraResult->push_back(test.getStationsVector()->at(9));
	dijkstraResult->push_back(test.getStationsVector()->at(10));
	EXPECT_CALL(*dijkstraMock, dijkstra(test.getStationsVector()->at(1), test.getStationsVector()->at(10)))
		.WillOnce(Return(dijkstraResult));
	QuasiShortestPath* quasiShortestPath = new QuasiShortestPath(test.getStationsVector(), dijkstraMock);

	//when
	vector<Station*>* result = quasiShortestPath->dijkstra(test.getStationsVector()->at(1), test.getStationsVector()->at(10));

	//then
	EXPECT_EQ(0, result->size());

	//clean
	delete quasiShortestPath;
	delete dijkstraMock;
}

TEST(QuasiShortestPathTest, returnEmptyLineToShortPath)
{
	//given
	TestNetworkCreator test;
	DijkstraMock* dijkstraMock = new DijkstraMock();
	std::vector<Station*>* dijkstraResult = new std::vector<Station*>();
	dijkstraResult->push_back(test.getStationsVector()->at(1));
	EXPECT_CALL(*dijkstraMock, dijkstra(test.getStationsVector()->at(1), test.getStationsVector()->at(1)))
		.WillOnce(Return(dijkstraResult));
	QuasiShortestPath* quasiShortestPath = new QuasiShortestPath(test.getStationsVector(), dijkstraMock);

	//when
	vector<Station*>* result = quasiShortestPath->dijkstra(test.getStationsVector()->at(1), test.getStationsVector()->at(1));

	//then
	EXPECT_EQ(0, result->size());

	//clean
	delete quasiShortestPath;
	delete dijkstraMock;
}
