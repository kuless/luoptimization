#include "pch.h"
#include "../LUOptimization/FrequencyMutation.h"
#include "RandomNumberGeneratorMock.h"

using ::testing::Return;

TEST(FrequencyMutationTest, shouldAddFiveMinutes)
{
	//given
	RandomNumberGeneratorMock* randomMock = new RandomNumberGeneratorMock();
	FrequencyMutation* frequencyMutation = new FrequencyMutation(randomMock);
	EXPECT_CALL(*randomMock, getInt(_, _))
		.Times(1)
		.WillOnce(Return(10));
	Station* station = new Station("1", "name", 1.0, 1.0);
	std::vector<Station*>* stops = new std::vector<Station*>();
	stops->push_back(station);
	std::vector<int> timetable;
	timetable.push_back(5);
	Line* line = new Line(1, "name", stops, timetable);

	//when
	frequencyMutation->mutation(line);

	//then
	EXPECT_EQ(10, line->getTimetableInMinutes()[0]);

	//clean
	delete station;
	delete line;
	delete frequencyMutation;
}

TEST(FrequencyMutationTest, doesNotExceedTheNextDay)
{
	//given
	RandomNumberGeneratorMock* randomMock = new RandomNumberGeneratorMock();
	FrequencyMutation* frequencyMutation = new FrequencyMutation(randomMock);
	EXPECT_CALL(*randomMock, getInt(_, _))
		.Times(1)
		.WillOnce(Return(10));
	Station* station = new Station("1", "name", 1.0, 1.0);
	std::vector<Station*>* stops = new std::vector<Station*>();
	stops->push_back(station);
	std::vector<int> timetable;
	timetable.push_back(1439);
	Line* line = new Line(1, "name", stops, timetable);

	//when
	frequencyMutation->mutation(line);

	//then
	EXPECT_EQ(0, line->getTimetableInMinutes().size());

	//clean
	delete station;
	delete line;
	delete frequencyMutation;
}

TEST(FrequencyMutationTest, doesNotExceedThePreviousDay)
{
	//given
	RandomNumberGeneratorMock* randomMock = new RandomNumberGeneratorMock();
	FrequencyMutation* frequencyMutation = new FrequencyMutation(randomMock);
	EXPECT_CALL(*randomMock, getInt(_, _))
		.Times(1)
		.WillOnce(Return(0));
	Station* station = new Station("1", "name", 1.0, 1.0);
	std::vector<Station*>* stops = new std::vector<Station*>();
	stops->push_back(station);
	std::vector<int> timetable;
	timetable.push_back(3);
	Line* line = new Line(1, "name", stops, timetable);

	//when
	frequencyMutation->mutation(line);

	//then
	EXPECT_EQ(0, line->getTimetableInMinutes().size());

	//clean
	delete station;
	delete line;
	delete frequencyMutation;
}
