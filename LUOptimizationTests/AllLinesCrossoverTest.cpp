#include "pch.h"
#include "../LUOptimization/AllLinesCrossover.h"
#include "RandomNumberGeneratorMock.h"
#include "TestNetworkCreator.h"

TEST(AllLinesCrossoverTest, testCrossoverWithCommonJuntionsAndMinLines)
{
	//given
	RandomNumberGeneratorMock* mock = new RandomNumberGeneratorMock();
	AllLinesCrossover<MultiCriteriaValue> crossoverOperator = AllLinesCrossover<MultiCriteriaValue>(mock);
	TestNetworkCreator test;
	EXPECT_CALL(*mock, getInt(_, _))
		.WillOnce(Return(0)) //getRandSize
		.WillOnce(Return(1)) //numberOfLinesMuteted
		.WillOnce(Return(0)) //firstGenLine
		.WillOnce(Return(0)) //secondGenLine
		.WillOnce(Return(5)) //crossoverTimetable
		.WillOnce(Return(1)) //crossoverStation
		.WillOnce(Return(1)) //firstGenLine
		.WillOnce(Return(1)); //secondGenLine

	//when
	std::pair<AbstractNetwork<MultiCriteriaValue>*, AbstractNetwork<MultiCriteriaValue>*>* result = crossoverOperator.crossover(test.getPairToCrossovers());

	//then
	std::vector<Station*>* stations = test.getStationsVector();

	EXPECT_EQ(2, result->first->getLines()->size());
	EXPECT_EQ(11, result->first->getLines()->at(0)->getStops().size());
	EXPECT_EQ(stations->at(1), result->first->getLines()->at(0)->getStops().at(0));
	EXPECT_EQ(stations->at(6), result->first->getLines()->at(0)->getStops().at(5));
	EXPECT_EQ(stations->at(7), result->first->getLines()->at(0)->getStops().at(6));
	EXPECT_EQ(stations->at(15), result->first->getLines()->at(0)->getStops().at(7));
	EXPECT_EQ(stations->at(17), result->first->getLines()->at(0)->getStops().at(10));
	EXPECT_EQ(12, result->first->getLines()->at(0)->getTimetableInMinutes().size());
	EXPECT_EQ(1, result->first->getLines()->at(0)->getTimetableInMinutes().at(0));
	EXPECT_EQ(4, result->first->getLines()->at(0)->getTimetableInMinutes().at(3));
	EXPECT_EQ(5, result->first->getLines()->at(0)->getTimetableInMinutes().at(4));
	EXPECT_EQ(16, result->first->getLines()->at(0)->getTimetableInMinutes().at(5));
	EXPECT_EQ(40, result->first->getLines()->at(0)->getTimetableInMinutes().at(11));
	EXPECT_EQ(5, result->first->getLines()->at(1)->getStops().size());
	EXPECT_EQ(stations->at(0), result->first->getLines()->at(1)->getStops().at(0));
	EXPECT_EQ(stations->at(22), result->first->getLines()->at(1)->getStops().at(4));

	EXPECT_EQ(2, result->second->getLines()->size());
	EXPECT_EQ(9, result->second->getLines()->at(0)->getStops().size());
	EXPECT_EQ(stations->at(11), result->second->getLines()->at(0)->getStops().at(0));
	EXPECT_EQ(stations->at(14), result->second->getLines()->at(0)->getStops().at(4));
	EXPECT_EQ(stations->at(7), result->second->getLines()->at(0)->getStops().at(5));
	EXPECT_EQ(stations->at(8), result->second->getLines()->at(0)->getStops().at(6));
	EXPECT_EQ(stations->at(10), result->second->getLines()->at(0)->getStops().at(8));
	EXPECT_EQ(10, result->second->getLines()->at(0)->getTimetableInMinutes().size());
	EXPECT_EQ(11, result->second->getLines()->at(0)->getTimetableInMinutes().at(0));
	EXPECT_EQ(14, result->second->getLines()->at(0)->getTimetableInMinutes().at(3));
	EXPECT_EQ(15, result->second->getLines()->at(0)->getTimetableInMinutes().at(4));
	EXPECT_EQ(6, result->second->getLines()->at(0)->getTimetableInMinutes().at(5));
	EXPECT_EQ(10, result->second->getLines()->at(0)->getTimetableInMinutes().at(9));
	EXPECT_EQ(5, result->second->getLines()->at(1)->getStops().size());
	EXPECT_EQ(stations->at(18), result->second->getLines()->at(1)->getStops().at(0));
	EXPECT_EQ(stations->at(22), result->second->getLines()->at(1)->getStops().at(4));

	//clean
}

TEST(AllLinesCrossoverTest, testCrossoverWithCommonJuntionsAndMaxLines)
{
	//given
	RandomNumberGeneratorMock* mock = new RandomNumberGeneratorMock();
	AllLinesCrossover<MultiCriteriaValue> crossoverOperator = AllLinesCrossover<MultiCriteriaValue>(mock);
	TestNetworkCreator test;
	EXPECT_CALL(*mock, getInt(_, _))
		.WillOnce(Return(2)) //getRandSize
		.WillOnce(Return(1)) //numberOfLinesMuteted
		.WillOnce(Return(0)) //firstGenLine
		.WillOnce(Return(0)) //secondGenLine
		.WillOnce(Return(5)) //crossoverTimetable
		.WillOnce(Return(1)) //crossoverStation
		.WillOnce(Return(1)) //firstGenLine
		.WillOnce(Return(1)) //secondGenLine
		.WillOnce(Return(3)) //firstGenLine
		.WillOnce(Return(0)) //secondGenLine
		.WillOnce(Return(3)) //firstGenLine
		.WillOnce(Return(0)); //secondGenLine

	//when
	std::pair<AbstractNetwork<MultiCriteriaValue>*, AbstractNetwork<MultiCriteriaValue>*>* result = crossoverOperator.crossover(test.getPairToCrossovers());

	//then
	std::vector<Station*>* stations = test.getStationsVector();

	EXPECT_EQ(4, result->first->getLines()->size());
	EXPECT_EQ(11, result->first->getLines()->at(0)->getStops().size());
	EXPECT_EQ(stations->at(1), result->first->getLines()->at(0)->getStops().at(0));
	EXPECT_EQ(stations->at(6), result->first->getLines()->at(0)->getStops().at(5));
	EXPECT_EQ(stations->at(7), result->first->getLines()->at(0)->getStops().at(6));
	EXPECT_EQ(stations->at(15), result->first->getLines()->at(0)->getStops().at(7));
	EXPECT_EQ(stations->at(17), result->first->getLines()->at(0)->getStops().at(10));
	EXPECT_EQ(12, result->first->getLines()->at(0)->getTimetableInMinutes().size());
	EXPECT_EQ(1, result->first->getLines()->at(0)->getTimetableInMinutes().at(0));
	EXPECT_EQ(4, result->first->getLines()->at(0)->getTimetableInMinutes().at(3));
	EXPECT_EQ(5, result->first->getLines()->at(0)->getTimetableInMinutes().at(4));
	EXPECT_EQ(16, result->first->getLines()->at(0)->getTimetableInMinutes().at(5));
	EXPECT_EQ(40, result->first->getLines()->at(0)->getTimetableInMinutes().at(11));
	EXPECT_EQ(5, result->first->getLines()->at(1)->getStops().size());
	EXPECT_EQ(stations->at(0), result->first->getLines()->at(1)->getStops().at(0));
	EXPECT_EQ(stations->at(22), result->first->getLines()->at(1)->getStops().at(4));
	EXPECT_EQ(10, result->first->getLines()->at(2)->getStops().size());
	EXPECT_EQ(stations->at(11), result->first->getLines()->at(2)->getStops().at(0));
	EXPECT_EQ(stations->at(17), result->first->getLines()->at(2)->getStops().at(9));
	EXPECT_EQ(6, result->first->getLines()->at(3)->getStops().size());
	EXPECT_EQ(stations->at(7), result->first->getLines()->at(3)->getStops().at(0));
	EXPECT_EQ(stations->at(2), result->first->getLines()->at(3)->getStops().at(5));

	EXPECT_EQ(4, result->second->getLines()->size());
	EXPECT_EQ(9, result->second->getLines()->at(0)->getStops().size());
	EXPECT_EQ(stations->at(11), result->second->getLines()->at(0)->getStops().at(0));
	EXPECT_EQ(stations->at(14), result->second->getLines()->at(0)->getStops().at(4));
	EXPECT_EQ(stations->at(7), result->second->getLines()->at(0)->getStops().at(5));
	EXPECT_EQ(stations->at(8), result->second->getLines()->at(0)->getStops().at(6));
	EXPECT_EQ(stations->at(10), result->second->getLines()->at(0)->getStops().at(8));
	EXPECT_EQ(10, result->second->getLines()->at(0)->getTimetableInMinutes().size());
	EXPECT_EQ(11, result->second->getLines()->at(0)->getTimetableInMinutes().at(0));
	EXPECT_EQ(14, result->second->getLines()->at(0)->getTimetableInMinutes().at(3));
	EXPECT_EQ(15, result->second->getLines()->at(0)->getTimetableInMinutes().at(4));
	EXPECT_EQ(6, result->second->getLines()->at(0)->getTimetableInMinutes().at(5));
	EXPECT_EQ(10, result->second->getLines()->at(0)->getTimetableInMinutes().at(9));
	EXPECT_EQ(5, result->second->getLines()->at(1)->getStops().size());
	EXPECT_EQ(stations->at(18), result->second->getLines()->at(1)->getStops().at(0));
	EXPECT_EQ(stations->at(22), result->second->getLines()->at(1)->getStops().at(4));
	EXPECT_EQ(6, result->second->getLines()->at(2)->getStops().size());
	EXPECT_EQ(stations->at(7), result->second->getLines()->at(2)->getStops().at(0));
	EXPECT_EQ(stations->at(2), result->second->getLines()->at(2)->getStops().at(5));
	EXPECT_EQ(10, result->second->getLines()->at(3)->getStops().size());
	EXPECT_EQ(stations->at(11), result->second->getLines()->at(3)->getStops().at(0));
	EXPECT_EQ(stations->at(17), result->second->getLines()->at(3)->getStops().at(9));

	//clean
}

TEST(AllLinesCrossoverTest, testCrossoverWithNoCommonJuntionsAndMinLines)
{
	//given
	RandomNumberGeneratorMock* mock = new RandomNumberGeneratorMock();
	AllLinesCrossover<MultiCriteriaValue> crossoverOperator = AllLinesCrossover<MultiCriteriaValue>(mock);
	TestNetworkCreator test;
	EXPECT_CALL(*mock, getInt(_, _))
		.WillOnce(Return(0)) //getRandSize
		.WillOnce(Return(1)) //numberOfLinesMuteted
		.WillOnce(Return(2)) //firstGenLine
		.WillOnce(Return(0)) //secondGenLine
		.WillOnce(Return(2)) //crossoverTimetable
		.WillOnce(Return(1)) //firstGenLine
		.WillOnce(Return(1)); //secondGenLine

	//when
	std::pair<AbstractNetwork<MultiCriteriaValue>*, AbstractNetwork<MultiCriteriaValue>*>* result = crossoverOperator.crossover(test.getPairToCrossovers());

	//then
	std::vector<Station*>* stations = test.getStationsVector();

	EXPECT_EQ(2, result->first->getLines()->size());
	EXPECT_EQ(4, result->first->getLines()->at(0)->getStops().size());
	EXPECT_EQ(stations->at(23), result->first->getLines()->at(0)->getStops().at(0));
	EXPECT_EQ(stations->at(26), result->first->getLines()->at(0)->getStops().at(3));
	EXPECT_EQ(12, result->first->getLines()->at(0)->getTimetableInMinutes().size());
	EXPECT_EQ(100, result->first->getLines()->at(0)->getTimetableInMinutes().at(0));
	EXPECT_EQ(300, result->first->getLines()->at(0)->getTimetableInMinutes().at(1));
	EXPECT_EQ(13, result->first->getLines()->at(0)->getTimetableInMinutes().at(2));
	EXPECT_EQ(14, result->first->getLines()->at(0)->getTimetableInMinutes().at(3));
	EXPECT_EQ(40, result->first->getLines()->at(0)->getTimetableInMinutes().at(11));
	EXPECT_EQ(5, result->first->getLines()->at(1)->getStops().size());
	EXPECT_EQ(stations->at(0), result->first->getLines()->at(1)->getStops().at(0));
	EXPECT_EQ(stations->at(22), result->first->getLines()->at(1)->getStops().at(4));

	EXPECT_EQ(2, result->second->getLines()->size());
	EXPECT_EQ(10, result->second->getLines()->at(0)->getStops().size());
	EXPECT_EQ(stations->at(11), result->second->getLines()->at(0)->getStops().at(0));
	EXPECT_EQ(stations->at(17), result->second->getLines()->at(0)->getStops().at(9));
	EXPECT_EQ(5, result->second->getLines()->at(0)->getTimetableInMinutes().size());
	EXPECT_EQ(11, result->second->getLines()->at(0)->getTimetableInMinutes().at(0));
	EXPECT_EQ(12, result->second->getLines()->at(0)->getTimetableInMinutes().at(1));
	EXPECT_EQ(700, result->second->getLines()->at(0)->getTimetableInMinutes().at(2));
	EXPECT_EQ(900, result->second->getLines()->at(0)->getTimetableInMinutes().at(3));
	EXPECT_EQ(990, result->second->getLines()->at(0)->getTimetableInMinutes().at(4));
	EXPECT_EQ(5, result->second->getLines()->at(1)->getStops().size());
	EXPECT_EQ(stations->at(18), result->second->getLines()->at(1)->getStops().at(0));
	EXPECT_EQ(stations->at(22), result->second->getLines()->at(1)->getStops().at(4));

	//clean
}

TEST(AllLinesCrossoverTest, testCrossoverAndLineContainsDuplicates)
{
	//given
	RandomNumberGeneratorMock* mock = new RandomNumberGeneratorMock();
	AllLinesCrossover<MultiCriteriaValue> crossoverOperator = AllLinesCrossover<MultiCriteriaValue>(mock);
	TestNetworkCreator test;
	EXPECT_CALL(*mock, getInt(_, _))
		.WillOnce(Return(0)) //getRandSize
		.WillOnce(Return(1)) //numberOfLinesMuteted
		.WillOnce(Return(3)) //firstGenLine
		.WillOnce(Return(0)) //secondGenLine
		.WillOnce(Return(5)) //crossoverTimetable
		.WillOnce(Return(1)) //crossoverStation
		.WillOnce(Return(1)) //firstGenLine
		.WillOnce(Return(1)); //secondGenLine

	//when
	std::pair<AbstractNetwork<MultiCriteriaValue>*, AbstractNetwork<MultiCriteriaValue>*>* result = crossoverOperator.crossover(test.getPairToCrossovers());

	//then
	std::vector<Station*>* stations = test.getStationsVector();

	EXPECT_EQ(2, result->first->getLines()->size());
	EXPECT_EQ(6, result->first->getLines()->at(0)->getStops().size());
	EXPECT_EQ(stations->at(7), result->first->getLines()->at(0)->getStops().at(0));
	EXPECT_EQ(stations->at(13), result->first->getLines()->at(0)->getStops().at(2));
	EXPECT_EQ(stations->at(4), result->first->getLines()->at(0)->getStops().at(3));
	EXPECT_EQ(stations->at(3), result->first->getLines()->at(0)->getStops().at(4));
	EXPECT_EQ(stations->at(2), result->first->getLines()->at(0)->getStops().at(5));
	EXPECT_EQ(12, result->first->getLines()->at(0)->getTimetableInMinutes().size());
	EXPECT_EQ(14, result->first->getLines()->at(0)->getTimetableInMinutes().at(0));
	EXPECT_EQ(17, result->first->getLines()->at(0)->getTimetableInMinutes().at(3));
	EXPECT_EQ(18, result->first->getLines()->at(0)->getTimetableInMinutes().at(4));
	EXPECT_EQ(16, result->first->getLines()->at(0)->getTimetableInMinutes().at(5));
	EXPECT_EQ(40, result->first->getLines()->at(0)->getTimetableInMinutes().at(11));
	EXPECT_EQ(5, result->first->getLines()->at(1)->getStops().size());
	EXPECT_EQ(stations->at(0), result->first->getLines()->at(1)->getStops().at(0));
	EXPECT_EQ(stations->at(22), result->first->getLines()->at(1)->getStops().at(4));

	EXPECT_EQ(2, result->second->getLines()->size());
	EXPECT_EQ(10, result->second->getLines()->at(0)->getStops().size());
	EXPECT_EQ(stations->at(11), result->second->getLines()->at(0)->getStops().at(0));
	EXPECT_EQ(stations->at(12), result->second->getLines()->at(0)->getStops().at(1));
	EXPECT_EQ(stations->at(4), result->second->getLines()->at(0)->getStops().at(2));
	EXPECT_EQ(stations->at(13), result->second->getLines()->at(0)->getStops().at(3));
	EXPECT_EQ(stations->at(17), result->second->getLines()->at(0)->getStops().at(9));
	EXPECT_EQ(10, result->second->getLines()->at(0)->getTimetableInMinutes().size());
	EXPECT_EQ(11, result->second->getLines()->at(0)->getTimetableInMinutes().at(0));
	EXPECT_EQ(14, result->second->getLines()->at(0)->getTimetableInMinutes().at(3));
	EXPECT_EQ(15, result->second->getLines()->at(0)->getTimetableInMinutes().at(4));
	EXPECT_EQ(100, result->second->getLines()->at(0)->getTimetableInMinutes().at(5));
	EXPECT_EQ(990, result->second->getLines()->at(0)->getTimetableInMinutes().at(9));
	EXPECT_EQ(5, result->second->getLines()->at(1)->getStops().size());
	EXPECT_EQ(stations->at(18), result->second->getLines()->at(1)->getStops().at(0));
	EXPECT_EQ(stations->at(22), result->second->getLines()->at(1)->getStops().at(4));

	//clean
}
