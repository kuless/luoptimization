#include "pch.h"
#include "../LUOptimization/WholeChromosomeCrossover.h"
#include "RandomNumberGeneratorMock.h"
#include "TestNetworkCreator.h"

TEST(WholeChromosomeCrossoverTest, testCrossover)
{
	//given
	RandomNumberGeneratorMock* mock = new RandomNumberGeneratorMock();
	WholeChromosomeCrossover<MultiCriteriaValue> crossoverOperator = WholeChromosomeCrossover<MultiCriteriaValue>(mock);
	TestNetworkCreator test;
	EXPECT_CALL(*mock, getInt(_, _))
		.WillOnce(Return(1));

	//when
	std::pair<AbstractNetwork<MultiCriteriaValue>*, AbstractNetwork<MultiCriteriaValue>*>* result = crossoverOperator.crossover(test.getPairToCrossovers());

	//then
	std::vector<Station*>* stations = test.getStationsVector();

	EXPECT_EQ(2, result->first->getLines()->size());
	EXPECT_EQ(10, result->first->getLines()->at(0)->getStops().size());
	EXPECT_EQ(stations->at(1), result->first->getLines()->at(0)->getStops().at(0));
	EXPECT_EQ(stations->at(10), result->first->getLines()->at(0)->getStops().at(9));
	EXPECT_EQ(5, result->first->getLines()->at(1)->getStops().size());
	EXPECT_EQ(stations->at(18), result->first->getLines()->at(1)->getStops().at(0));
	EXPECT_EQ(stations->at(22), result->first->getLines()->at(1)->getStops().at(4));

	EXPECT_EQ(4, result->second->getLines()->size());
	EXPECT_EQ(10, result->second->getLines()->at(0)->getStops().size());
	EXPECT_EQ(stations->at(11), result->second->getLines()->at(0)->getStops().at(0));
	EXPECT_EQ(stations->at(17), result->second->getLines()->at(0)->getStops().at(9));
	EXPECT_EQ(5, result->second->getLines()->at(1)->getStops().size());
	EXPECT_EQ(stations->at(0), result->second->getLines()->at(1)->getStops().at(0));
	EXPECT_EQ(stations->at(22), result->second->getLines()->at(1)->getStops().at(4));
	EXPECT_EQ(4, result->second->getLines()->at(2)->getStops().size());
	EXPECT_EQ(stations->at(23), result->second->getLines()->at(2)->getStops().at(0));
	EXPECT_EQ(stations->at(26), result->second->getLines()->at(2)->getStops().at(3));
	EXPECT_EQ(6, result->second->getLines()->at(3)->getStops().size());
	EXPECT_EQ(stations->at(7), result->second->getLines()->at(3)->getStops().at(0));
	EXPECT_EQ(stations->at(2), result->second->getLines()->at(3)->getStops().at(5));

	//clean
}
