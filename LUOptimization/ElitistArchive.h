#pragma once
#include <vector>
#include <unordered_set>
#include "AbstractNetwork.h"
#include "IQualityMeasures.h"
#include "QualityEnum.h"
#include "MultiCriteriaNetwork.h"
#include "MultiCriteriaValue.h"
#include "Nsgaii.h"
#include "QualityValue.h"
#include "Constants.h"

class ElitistArchive
{
	public:
		ElitistArchive(IQualityMeasures* qualityMeasure);
		~ElitistArchive();
		QualityValue* getQuality(QualityEnum qualityType);
		void addToArchive(std::vector<AbstractNetwork<MultiCriteriaValue>*>* population);
		std::vector<AbstractNetwork<MultiCriteriaValue>*>* getArchive();
		
	private:
		IQualityMeasures* qualityMeasure;
		Nsgaii* nsgaii;
		std::vector<AbstractNetwork<MultiCriteriaValue>*>* archive;
		std::unordered_set<AbstractNetwork<MultiCriteriaValue>*, NetworkHash<MultiCriteriaValue>, NetworkEqual<MultiCriteriaValue>>* fullArchive;
};
