#include "DijkstraValueRoute.h"

DijkstraValueRoute::DijkstraValueRoute(int value, Station* station, Station* previosStation, int stationNumber,
	Line* line, Line* previosStationLine, bool forwardMovement):DijkstraValue(value, station, previosStation)
{
	this->stationNumber = stationNumber;
	this->line = line;
	this->previosStationLine = previosStationLine;
	this->forwardMovement = forwardMovement;
}

DijkstraValueRoute::~DijkstraValueRoute()
{
}

Line* DijkstraValueRoute::getLine()
{
	return line;
}

Line* DijkstraValueRoute::getPreviosStationLine()
{
	return previosStationLine;
}

bool DijkstraValueRoute::getForwardMovement()
{
	return forwardMovement;
}

int DijkstraValueRoute::getStationNumber()
{
	return stationNumber;
}

void DijkstraValueRoute::setPreviosStationLine(Line* previosStationLine)
{
	this->previosStationLine = previosStationLine;
}

void DijkstraValueRoute::setForwardMovement(bool forwardMovement)
{
	this->forwardMovement = forwardMovement;
}
