#pragma once
#include "AbstractNetwork.h"
#include "SingleCriteriaValue.h"

class SingleCriteriaNetwork : public AbstractNetwork<SingleCriteriaValue>
{
	public:
		SingleCriteriaNetwork(std::vector<Line*>* lines);
		SingleCriteriaNetwork(const AbstractNetwork<SingleCriteriaValue>& network);
		~SingleCriteriaNetwork();

		unsigned long long int getSelectionValue() override;

		AbstractNetwork<SingleCriteriaValue>* clone() override;
		AbstractNetwork<SingleCriteriaValue>* callChildConstructor(std::vector<Line*>* lines) override;
		AbstractNetwork<SingleCriteriaValue>* callChildConstructor(const AbstractNetwork<SingleCriteriaValue>& network) override;
};
