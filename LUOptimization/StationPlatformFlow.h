#pragma once
#include <string>
#include <vector>

class StationPlatformFlow 
{
	public:
		StationPlatformFlow(int* NLCCode, std::string* name);
		~StationPlatformFlow();
		int* getNLCCode();
		std::string* getName();
		std::vector<int*>* getFlow();
		void setNLCCode(int& NLCCode);
		void setName(std::string& name);
		void setFlow(std::vector<int*>& flow);

	private:
		int* NLCCode;
		std::string* name;
		std::vector<int*>* flow;
};