#include "AddTravelsMutation.h"

using namespace std;

AddTravelsMutation::AddTravelsMutation(IRandomNumberGenerator* random): AbstractMutationOperator(random)
{
}

AddTravelsMutation::~AddTravelsMutation()
{
}

void AddTravelsMutation::mutation(Line* lineForMutation)
{
	int travelsToAdd = random->getInt(0, MAX_REMOVE_ADD_FREQENCE_MUTATION);
	vector<int> timetable = lineForMutation->getTimetableInMinutes();

	for (int i = 0; i < travelsToAdd; i++)
	{
		int travelStart = random->getInt(0, DAY_IN_MINUTES);
		timetable.push_back(travelStart);
	}

	sort(timetable.begin(), timetable.end());
	lineForMutation->setTimetable(timetable);
}

MutationTechniqueEnum AddTravelsMutation::getType()
{
	return MutationTechniqueEnum::ADD_TRAVELS;
}
