#include "PopulationGeneratorInputParameters.h"

PopulationGeneratorInputParameters::PopulationGeneratorInputParameters()
{
}

PopulationGeneratorInputParameters::PopulationGeneratorInputParameters(PopulationGeneratorInputParameters& populationGeneratorInputParameters)
{
	populationSize = populationGeneratorInputParameters.getPopulationSize();
	numberOfLines = populationGeneratorInputParameters.getNumberOfLines();
	percentOfCoveredStations = populationGeneratorInputParameters.getPercentOfCoveredStations();
	populationGenerationTechnique = populationGeneratorInputParameters.getPopulationGenerationTechnique();

}

PopulationGeneratorInputParameters::~PopulationGeneratorInputParameters()
{
}

void PopulationGeneratorInputParameters::setPopulationSize(int populationSize)
{
	this->populationSize = populationSize;
}

void PopulationGeneratorInputParameters::setNumberOfLines(int numberOfLines)
{
	this->numberOfLines = numberOfLines;
}

void PopulationGeneratorInputParameters::setPercentOfCoveredStations(int percentOfCoveredStations)
{
	this->percentOfCoveredStations = percentOfCoveredStations;
}

void PopulationGeneratorInputParameters::setPopulationGenerationTechnique(std::string populationGenerationTechnique)
{
	this->populationGenerationTechnique = PopulationGenerationTechnique::getValue(populationGenerationTechnique);
}

int PopulationGeneratorInputParameters::getPopulationSize()
{
	return populationSize;
}

int PopulationGeneratorInputParameters::getNumberOfLines()
{
	return numberOfLines;
}

int PopulationGeneratorInputParameters::getPercentOfCoveredStations()
{
	return percentOfCoveredStations;
}

PopulationGenerationTechniqueEnum PopulationGeneratorInputParameters::getPopulationGenerationTechnique()
{
	return populationGenerationTechnique;
}
