#include "InitInputParameters.h"

using namespace std;

InitInputParameters::InitInputParameters()
{
}

InitInputParameters::~InitInputParameters()
{
}

void InitInputParameters::setOutputFile(string outputFile)
{
	this->outputFile = outputFile;
}

void InitInputParameters::setCrossoverProbability(int crossoverProbability)
{
	this->crossoverProbability = crossoverProbability;
}

void InitInputParameters::setMutationProbability(int mutationProbability)
{
	this->mutationProbability = mutationProbability;
}

void InitInputParameters::setSelectionsSize(int selectionsSize)
{
	this->selectionsSize = selectionsSize;
}

void InitInputParameters::setRouletteSelection(bool rouletteSelection)
{
	this->rouletteSelection = rouletteSelection;
}

void InitInputParameters::setMutationTechnique(string mutationTechnique)
{
	this->mutationTechnique = MutationTechnique::getValue(mutationTechnique);
}

void InitInputParameters::setCrossoverTechnique(string crossoverTechnique)
{
	this->crossoverTechnique = CrossoverTechnique::getValue(crossoverTechnique);
}

void InitInputParameters::setSolutionType(string solutionType)
{
	this->solutionType = SolutionType::getValue(solutionType);
}

void InitInputParameters::setQualityTypes(string qualityTypes)
{
	vector<string> lineSplited;
	boost::split(lineSplited, qualityTypes, boost::algorithm::is_any_of(COMMA_SEPERATOR));

	for (int i = 0; i < lineSplited.size(); i++)
	{
		this->qualityTypes.push_back(Quality::getValue(lineSplited[i]));
	}
}

void InitInputParameters::setSaveValuesForTpf(bool saveValuesForTpf)
{
	this->saveValuesForTpf = saveValuesForTpf;
}

string InitInputParameters::getOutputFile()
{
	return outputFile;
}

int InitInputParameters::getCrossoverProbability()
{
	return crossoverProbability;
}

int InitInputParameters::getMutationProbability()
{
	return mutationProbability;
}

int InitInputParameters::getSelectionsSize()
{
	return selectionsSize;
}

bool InitInputParameters::getRouletteSelection()
{
	return rouletteSelection;
}

MutationTechniqueEnum InitInputParameters::getMutationTechnique()
{
	return mutationTechnique;
}

CrossoverTechniqueEnum InitInputParameters::getCrossoverTechnique()
{
	return crossoverTechnique;
}

SolutionTypeEnum InitInputParameters::getSolutionType()
{
	return solutionType;
}

vector<QualityEnum> InitInputParameters::getQualityTypes()
{
	return qualityTypes;
}

bool InitInputParameters::getSaveValuesForTpf()
{
	return saveValuesForTpf;
}
