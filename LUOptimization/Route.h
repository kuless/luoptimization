#pragma once
#include "Station.h"
#include "Line.h"

class Route
{
	public:
		Route(Station* endStation, Line* line, bool forwardMovement);
		Route(Route& route);
		~Route();

		Station* getEndStation();
		Line* getLine();
		bool isForwardMovement();

	private:
		Station* endStation;
		Line* line;
		bool forwardMovement;
};
