#pragma once
#include <list>
#include <iterator>
#include <algorithm>
#include "Line.h"
#include "Station.h"
#include "Travel.h"
#include "TravelException.h"
#include "Constants.h"

class Train
{
	public:
		Train(int capacity, Line* line, int departure);
		~Train();

		bool operator<(const Train& train) const;

		Travel* addPassangers(Travel* passangers);
		bool isFull();
		std::pair<int, int> update();
		std::list<Travel*> getPassengersExitingTrain();
		
		int getDeparture();
		Line* getLine();
		Station* getCurrentStation();
		int getState();
		bool isForwardMovment();
		int getTotalPassengers();
		int getCurrentTravelTime();
		std::list<Travel*> getPassengers();

	private:
		int totalPassengers;
		std::list<Travel*> passengers;
		int capacity;
		Line* line;
		Station* currentStation;
		int state;
		int currentTravelTime;
		int departure;
		bool forwardMovment;

		void handleForwardMovement();
		void handleBackwardMovement();
};
