#include "IRandomNumberGenerator.h"

using namespace std;

IRandomNumberGenerator::IRandomNumberGenerator()
{
}

IRandomNumberGenerator::~IRandomNumberGenerator()
{
}

int IRandomNumberGenerator::getInt(int lowerLimit, int upperLimit)
{
	throw NotImplementedException("getInt not implemented");
}

unsigned long long int IRandomNumberGenerator::getUnsignedLongLongInt(unsigned long long int lowerLimit, unsigned long long int upperLimit)
{
	throw NotImplementedException("getUnsignedLongLongInt not implemented");
}

boost::multiprecision::uint128_t IRandomNumberGenerator::getUint128(boost::multiprecision::uint128_t lowerLimit, boost::multiprecision::uint128_t upperLimit)
{
	throw NotImplementedException("getUint128 not implemented");
}

IRandomNumberGenerator* IRandomNumberGenerator::copy()
{
	throw NotImplementedException("copy not implemented");
}
