#pragma once
#include <vector>
#include <list>
#include "Station.h"
#include "Line.h"
#include "Route.h"
#include "TravelException.h"
#include "Constants.h"

class Travel
{
	public:
		Travel(Travel& travel);
		~Travel();

		class TravelBuilder
		{
			public:
				TravelBuilder();
				~TravelBuilder();

				Travel* build();
				void setNumberOfPassengers(int numberOfPassengers);
				void setStartHour(int hour);
				void setStartMinute(int minute);
				void setStartStation(Station* startStation);
				void setEndStation(Station* endStation);

			private:
				int numberOfPassengers;
				int startHour;
				int startMinute;
				Station* startStation;
				Station* endStation;
		};

		bool operator<(const Travel& travel) const;

		int getNumberOfPassengers();
		int getStartHour();
		int getStartMinute();
		int getStartInMinutes();
		Station* getStartStation();
		Station* getEndStation();
		std::list<Route*>* getRoutes();

		void updateAfterComplitionPartOfTheJourney();
		void setRoutes(std::list<Route*>* routes);

		Travel* splitPassengers(int reducePassengersInCurrentObj);

	private:
		int numberOfPassengers;
		int startHour;
		int startMinute;
		int startInMinutes;
		Station* startStation;
		Station* endStation;
		std::list<Route*>* routes;

		Travel(int numberOfPassengers, int startHour, int startMinute, Station* startStation, Station* endStation);
};