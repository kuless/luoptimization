#pragma once
#include "AbstractQualityMeasure.h"
#include <boost/multiprecision/cpp_int.hpp>

class HyperVolume: public AbstractQualityMeasure
{
	public:
		HyperVolume();
		~HyperVolume();
		QualityValue* getQuality(std::vector<AbstractNetwork<MultiCriteriaValue>*>* population) override;
		QualityEnum getType() override;
};
