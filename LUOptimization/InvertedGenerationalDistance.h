#pragma once
#pragma warning(disable:4146)
#include "AbstractQualityMeasure.h"
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/gmp.hpp>

class InvertedGenerationalDistance: public AbstractQualityMeasure
{
	public:
		InvertedGenerationalDistance(std::vector<MultiCriteriaValue*>* trueParetoFornt);
		~InvertedGenerationalDistance();
		QualityValue* getQuality(std::vector<AbstractNetwork<MultiCriteriaValue>*>* population) override;
		QualityEnum getType() override;

	private:
		boost::multiprecision::mpf_float getDistance(MultiCriteriaValue* trueParetoFrontValue, MultiCriteriaValue* populationValue);

		std::vector<MultiCriteriaValue*>* trueParetoFornt;
};
