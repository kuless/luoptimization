#include "DataLoader.h"
#pragma warning(disable : 4996)

using namespace std;

DataLoader::DataLoader()
{
	stationsVector = new vector<Station*>();
}

DataLoader::~DataLoader()
{
}

string* DataLoader::getLineDetailsFromAPI(string lineName)
{
	//stringstream ss;
	//ss << TFL_BASE_URL << LINE_URL << lineName << LINE_DETAILS_URL << APP_ID_AND_KEY;
	//cpr::Response httpResponse = cpr::Get(cpr::Url{ ss.str() });
	//string* result = new string(httpResponse.text);
	//return result;
	return new string();
}

void DataLoader::addStationsToVectorFromAPI(string& rootJSON)
{
	Json::Value root = readJSON(rootJSON);

	root = root[JSON_STOP_SEQUENCES];

	for (int i = 0; i < root.size(); i++)
	{
		Json::Value stopPoints = root[i][JSON_STOP_POINT];

		for (int n = 0; n < stopPoints.size(); n++)
		{
			Station* station = new Station(stopPoints[n][JSON_ID].asString(), stopPoints[n][JSON_NAME].asString(),
				stopPoints[n][JSON_LAT].asDouble(), stopPoints[n][JSON_LAT].asDouble());
			bool vectorContainsStation = false;

			for (int k = 0; k < stationsVector->size(); k++)
			{
				if (!vectorContainsStation && (*stationsVector)[k]->_Equal(*station))
				{
					vectorContainsStation = true;
				}
			}

			if (vectorContainsStation)
			{
				delete station;
			}
			else
			{
				stationsVector->push_back(station);
			}
		}
	}
}

void DataLoader::addLinesToVectorFromAPI(string& rootJSON)
{
	//Json::Value fullRoot = readJSON(rootJSON);

	//Json::Value root;
	//root = fullRoot[JSON_ROUTES_ORDER];

	//for (int i = 0; i < root.size(); i++)
	//{
	//	vector<Station*>* lineStops = new vector<Station*>();
	//	vector<int> journeyTime;
	//	string naptanId = root[i][JSON_NAPTAN_ID][0].asString();
	//	lineStops->push_back(findStationById(naptanId));

	//	for (int k = 1; k < root[i][JSON_NAPTAN_ID].size(); k++)
	//	{
	//		string naptanId = root[i][JSON_NAPTAN_ID][k].asString();
	//		Station* station = findStationById(naptanId);
	//		lineStops->push_back(station);
	//		int journeyTimeBetweenStations = getJourneyTimeBetweenStationsFromAPI(fullRoot[JSON_LINE_ID].asString(),
	//			(*lineStops)[k - 1]->getId(), station->getId());

	//		if (!(*lineStops)[k - 1]->containsConnection(*station))
	//		{
	//			(*lineStops)[k - 1]->addConnection(new Connection(journeyTimeBetweenStations, 0.0, station));
	//		}

	//		journeyTime.push_back(journeyTimeBetweenStations);
	//	}

	//	vector<pair<int, int>> timetable = getTimetableBetweenStationsFromAPI(fullRoot[JSON_LINE_ID].asString(),
	//		(*lineStops)[0]->getId(), (*lineStops)[lineStops->size() - 1]->getId());

	//	lineVector.push_back(new Line(lineVector.size() + 1, fullRoot[JSON_LINE_ID].asString(), lineStops, journeyTime, timetable));
	//}
}


void DataLoader::addStationsToVectorFromFile(string fileName)
{
	ifstream file(fileName);
	string line;
	getline(file, line);

	while (getline(file, line))
	{
		vector<string> lineSplited;
		boost::split(lineSplited, line, boost::algorithm::is_any_of(SEPERATOR));
		stationsVector->push_back(new Station(lineSplited[STATION_ID_IN_FILE], lineSplited[STATION_NAME_IN_FILE],
			stod(lineSplited[STATION_LAT_IN_FILE]), stod(lineSplited[STATION_LON_IN_FILE]), stoi(lineSplited[STATION_NLC_CODE])));
	}
}

void DataLoader::addConnectionsToStationsFormFile(string fileName)
{
	ifstream file(fileName);
	string line;
	Station* station = new Station("", "", 0, 0);
	Station* instantToHideError = station;
	delete instantToHideError;
	getline(file, line);

	while (getline(file, line))
	{
		vector<string> lineSplited;
		boost::split(lineSplited, line, boost::algorithm::is_any_of(SEPERATOR));
		
		if (lineSplited[STATION_ID_IN_FILE].size() > 0)
		{
			station = findStationById(lineSplited[STATION_ID_IN_FILE]);
		}
		else
		{
			station->addConnection(new Connection(
				stoi(lineSplited[STATION_CONNECTION_JOURNEY_TIME_IN_FILE]),
				stod(lineSplited[STATION_CONNECTION_DISTANCE_IN_FILE]),
				findStationById(lineSplited[STATION_CONNECTION_ID_IN_FILE])
			));
		}
	}
}

void DataLoader::addLineToVectorFromFile(string fileName)
{
	ifstream file(fileName);
	string textLine;
	int lastStopNumber = 0;
	getline(file, textLine);

	while (getline(file, textLine))
	{
		vector<string> lineSplited;
		boost::split(lineSplited, textLine, boost::algorithm::is_any_of(SEPERATOR));
		int id = stoi(lineSplited[LINE_ID_IN_FILE]);
		Line* line = findLineById(id);
		Station* station = findStationById(lineSplited[LINE_STOP_IN_FILE]);

		if (line == nullptr)
		{
			lastStopNumber = 0;
			vector<Station*>* stops = new vector<Station*>();
			stops->push_back(station);
			vector<string> direction;
			direction.push_back(lineSplited[LINE_DIRECTION_IN_FILE]);
			lineVector.push_back(new Line(id, lineSplited[LINE_NAME_IN_FILE], stops));
		}
		else
		{
			line->addStop(station,
				getJourneyTimeBetweenStationsFormSavedData(line->getStops()[lastStopNumber], station->getId()));
			lastStopNumber++;
		}
	}
}

void DataLoader::addTimetableToVectorFromFile(string fileName)
{
	ifstream file(fileName);
	string textLine;
	int lineId = 1;
	vector<pair<int, int>> timetable;
	getline(file, textLine);

	while (getline(file, textLine))
	{
		vector<string> lineSplited;
		boost::split(lineSplited, textLine, boost::algorithm::is_any_of(SEPERATOR));
		
		if (lineId != stoi(lineSplited[LINE_ID_IN_FILE]))
		{
			Line* line = findLineById(lineId);
			line->setTimetable(timetable);
			lineId = stoi(lineSplited[LINE_ID_IN_FILE]);
			timetable = vector<pair<int, int>>();
		}

		timetable.push_back(pair<int, int>(stoi(lineSplited[TIMETABLE_HOUR_IN_FILE]), stoi(lineSplited[TIMETABLE_MINUTE_IN_FILE])));
	}

	Line* line = findLineById(lineId);
	line->setTimetable(timetable);
}

void DataLoader::addTravelsToVectorFormFile(string fileName)
{
	ifstream file(fileName);
	string textLine;
	getline(file, textLine);

	while (getline(file, textLine))
	{

	}
}

void DataLoader::addConnectionsToStationsFormSmallFile(string fileName)
{
	ifstream file(fileName);
	string line;
	Station* station = new Station("", "", 0, 0);
	Station* instantToHideError = station;
	delete instantToHideError;
	bool stationExist = false;
	getline(file, line);

	while (getline(file, line))
	{
		vector<string> lineSplited;
		boost::split(lineSplited, line, boost::algorithm::is_any_of(SEPERATOR));

		if (lineSplited[STATION_ID_IN_FILE].size() > 0)
		{
			station = findStationById(lineSplited[STATION_ID_IN_FILE]);

			if (station == nullptr)
			{
				stationExist = false;
			}
			else
			{
				stationExist = true;
			}
		}
		else if (stationExist)
		{
			Station* connection = findStationById(lineSplited[STATION_CONNECTION_ID_IN_FILE]);

			if (connection != nullptr)
			{
				station->addConnection(new Connection(
					stoi(lineSplited[STATION_CONNECTION_JOURNEY_TIME_IN_FILE]),
					stod(lineSplited[STATION_CONNECTION_DISTANCE_IN_FILE]),
					connection
				));
			}
		}
	}
}

int DataLoader::getJourneyTimeBetweenStationsFormSavedData(string startStationId, string endStationId)
{
	return getJourneyTimeBetweenStationsFormSavedData(findStationById(startStationId), endStationId);
}

int DataLoader::getJourneyTimeBetweenStationsFormSavedData(Station* startStation, string endStationId)
{
	return startStation->getJourneyTime(endStationId);
}

void DataLoader::saveStationVectorToFile(string fileName)
{
	ofstream myFile;
	myFile.open(fileName);
	myFile << STATIONS_FILE_HEADER << endl;

	for (int i = 0; i < stationsVector->size(); i++)
	{
		string stationName = (*stationsVector)[i]->getName();
		size_t prefixBegin = (*stationsVector)[i]->getName().find(STATION_PREFIX);

		if (prefixBegin != string::npos)
		{
			stationName = (&stationName)->replace(prefixBegin, string::npos, "");
		}

		myFile << (*stationsVector)[i]->getId() << SEPERATOR;
		myFile << stationName << SEPERATOR;
		myFile << (*stationsVector)[i]->getLat() << SEPERATOR;
		myFile << (*stationsVector)[i]->getLon() << SEPERATOR;
		myFile << SEPERATOR << SEPERATOR << SEPERATOR << SEPERATOR << SEPERATOR << endl;
	}

	saveConnectionsToFile(fileName + CONNECTIONS_FILE_NAME + FILE_EXTENSION);
}

void DataLoader::saveLineVectorToFile(string fileName)
{
	ofstream myFile;
	myFile.open(fileName);
	myFile << LINE_FILE_HEADER << endl;

	for (int i = 0; i < lineVector.size(); i++)
	{
		int travelTime = 0;
		Station* lastStop = nullptr;

		for (int k = 0; k < lineVector[i]->getStops().size(); k++)
		{

			if (lastStop != nullptr)
			{
				string lineId = lineVector[i]->getStops()[k]->getId();
				travelTime += lastStop->getJourneyTime(lineId);
			}

			lastStop = lineVector[i]->getStops()[k];

			string stationName = lineVector[i]->getStops()[k]->getName();
			size_t prefixBegin = lineVector[i]->getStops()[k]->getName().find(STATION_PREFIX);

			if (prefixBegin != string::npos)
			{
				stationName = (&stationName)->replace(prefixBegin, string::npos, "");
			}

			myFile << lineVector[i]->getId() << SEPERATOR;
			myFile << lineVector[i]->getName() << SEPERATOR;
			myFile << lineVector[i]->getStops()[k]->getId() << SEPERATOR;
			myFile << stationName << SEPERATOR;
			myFile << travelTime << SEPERATOR << endl;
		}
	}

	saveTimetableToFile(fileName + TIMETABLE_FILE_NAME + FILE_EXTENSION);
}

vector<Station*>* DataLoader::getStationsVector()
{
	return stationsVector;
}

vector<Line*> DataLoader::getLineVector()
{
	return lineVector;
}

Json::Value DataLoader::readJSON(string& rootJSON)
{
	Json::Value root;
	Json::Reader reader;
	bool parsingSuccessful = reader.parse(rootJSON, root);

	if (!parsingSuccessful)
	{
		cout << PARSING_ERROR_MESSAGE << endl;
		return NULL;
	}
	else
	{
		return root;
	}
}

Station* DataLoader::findStationById(string& id)
{
	for (int i = 0; i < stationsVector->size(); i++)
	{
		if ((*stationsVector)[i]->getId()._Equal(id))
		{
			return (*stationsVector)[i];
		}
	}

	return nullptr;
}

Station* DataLoader::findStationByNLCCode(int NLCCode)
{
	for (int i = 0; i < stationsVector->size(); i++)
	{
		if ((*stationsVector)[i]->getNLCCode() == NLCCode)
		{
			return (*stationsVector)[i];
		}
	}

	return nullptr;
}

Line* DataLoader::findLineById(int& id)
{
	for (int i = 0; i < lineVector.size(); i++)
	{
		if (lineVector[i]->getId() == id)
		{
			return lineVector[i];
		}
	}

	return nullptr;
}

vector<pair<int, int>> DataLoader::getTimetableBetweenStationsFromAPI(string lineName, string startStationId, string endStationId)
{
	vector<pair<int, int>> result;
	//stringstream ss;
	//ss << TFL_BASE_URL << LINE_URL << lineName << LINE_TIMETABLE_URL << startStationId << LINE_TO_URL << endStationId << APP_ID_AND_KEY;
	//cpr::Response httpResponse = cpr::Get(cpr::Url{ ss.str() });
	//Json::Value root = readJSON(httpResponse.text);
	//root = root[JSON_TIMETABLE][JSON_ROUTES][JSON_FIRST_ELEMENT][JSON_SCHEDULES][JSON_FIRST_ELEMENT][JSON_JOURNEYS];

	//for (int i = 0; i < root.size(); i++)
	//{
	//	result.push_back(pair<int, int>(stoi(root[i][JSON_HOUR].asString()), stoi(root[i][JSON_MINUTE].asString())));
	//}

	return result;
}

int DataLoader::getJourneyTimeBetweenStationsFromAPI(string lineName, string startStationId, string endStationId)
{
	//stringstream ss;
	//ss << TFL_BASE_URL << LINE_URL << lineName << LINE_TIMETABLE_URL << startStationId << LINE_TO_URL << endStationId << APP_ID_AND_KEY;
	//cpr::Response httpResponse = cpr::Get(cpr::Url{ ss.str() });
	//Json::Value root = readJSON(httpResponse.text);

	//return root[JSON_TIMETABLE][JSON_ROUTES][JSON_FIRST_ELEMENT][JSON_STATION_INTERVALS][JSON_FIRST_ELEMENT][JSON_INTERVALS][JSON_FIRST_ELEMENT][JSON_ARRIVAL].asInt();
	return 0;
}

void DataLoader::saveConnectionsToFile(string fileName)
{
	ofstream myFile;
	myFile.open(fileName);
	myFile << CONNECTIONS_FILE_HEADER  << endl;

	for (int i = 0; i < stationsVector->size(); i++)
	{
		string stationName = (*stationsVector)[i]->getName();
		size_t prefixBegin = (*stationsVector)[i]->getName().find(STATION_PREFIX);

		if (prefixBegin != string::npos)
		{
			stationName = (&stationName)->replace(prefixBegin, string::npos, "");
		}

		myFile << (*stationsVector)[i]->getId() << SEPERATOR;
		myFile << stationName << SEPERATOR;
		myFile << (*stationsVector)[i]->getLat() << SEPERATOR;
		myFile << (*stationsVector)[i]->getLon() << SEPERATOR;
		myFile << SEPERATOR << SEPERATOR << SEPERATOR << SEPERATOR << SEPERATOR << endl;

		for (int k = 0; k < (*stationsVector)[i]->getConnections().size(); k++)
		{
			string connectionStationName = (*stationsVector)[i]->getConnections()[k]->getStation()->getName();
			size_t connectionPrefixBegin = (*stationsVector)[i]->getConnections()[k]->getStation()->getName().find(STATION_PREFIX);

			if (connectionPrefixBegin != string::npos)
			{
				connectionStationName = (&connectionStationName)->replace(connectionPrefixBegin, string::npos, "");
			}

			myFile << SEPERATOR << SEPERATOR << SEPERATOR << SEPERATOR;
			myFile << (*stationsVector)[i]->getConnections()[k]->getJourneyTime() << SEPERATOR;
			myFile << (*stationsVector)[i]->getConnections()[k]->getStation()->getId() << SEPERATOR;
			myFile << connectionStationName << SEPERATOR;
			myFile << (*stationsVector)[i]->getConnections()[k]->getStation()->getLat() << SEPERATOR;
			myFile << (*stationsVector)[i]->getConnections()[k]->getStation()->getLon() << SEPERATOR << endl;
		}
	}
}

void DataLoader::saveTimetableToFile(string fileName)
{
	ofstream myFile;
	myFile.open(fileName);
	myFile << TIMETABLE_FILE_HEADER << endl;

	for (int i = 0; i < lineVector.size(); i++)
	{
		for (int k = 0; k < lineVector[i]->getTimetable().size(); k++)
		{
			myFile << lineVector[i]->getId() << SEPERATOR;
			myFile << lineVector[i]->getTimetable()[k].first << SEPERATOR;
			myFile << lineVector[i]->getTimetable()[k].second << SEPERATOR << endl;
		}
	}
}
