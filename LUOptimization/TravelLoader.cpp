#include "TravelLoader.h"

using namespace std;

TravelLoader::TravelLoader()
{
	stationFlow = new vector<StationPlatformFlow*>();
	travelDestination = new vector<TravelDestination*>();
}

TravelLoader::~TravelLoader()
{
	for (int i = 0; i < stationFlow->size(); i++)
	{
		delete (*stationFlow)[i];
	}

	delete stationFlow;

	for (int i = 0; i < travelDestination->size(); i++)
	{
		delete (*travelDestination)[i];
	}

	delete travelDestination;
}

vector<Travel*>* TravelLoader::connectData(DataLoader& dataLoader)
{
	vector<Travel*>* travels = new vector<Travel*>();

	for (int i = 0; i < travelDestination->size(); i++)
	{
		cout << (*(*travelDestination)[i]->getStartStationName()) << endl;
		unordered_map<string, int> travelsMap = connectOneTimePeriod(FLOW_MORNING, FLOW_MORNIG_BEGIN, FLOW_MORNING_END, i);

		for (unordered_map<string, int>::iterator it = travelsMap.begin(); it != travelsMap.end(); ++it)
		{
			vector<string> keySplited;
			boost::split(keySplited, it->first, boost::algorithm::is_any_of(SEPERATOR));
			int hour = (stoi(keySplited[FLOW_MAP_KEY_TIME]) + FLOW_FIVE_O_CLOCK) / FLOW_GET_HOUR;

			if (hour >= FLOW_MIDNIGHT)
			{
				hour -= FLOW_MIDNIGHT;
			}

			int minute = (stoi(keySplited[FLOW_MAP_KEY_TIME]) + FLOW_FIVE_O_CLOCK) % FLOW_GET_HOUR * FLOW_GET_MINUTE;
			Travel::TravelBuilder* builder = new Travel::TravelBuilder();
			builder->setNumberOfPassengers(it->second);
			builder->setStartHour(hour);
			builder->setStartMinute(minute);
			builder->setStartStation(dataLoader.findStationByNLCCode(stoi(keySplited[FLOW_MAP_KEY_START_STATION_NLC_CODE])));
			builder->setEndStation(dataLoader.findStationByNLCCode(stoi(keySplited[FLOW_MAP_KEY_END_STATION_NLC_CODE])));
			travels->push_back(builder->build());

			delete builder;
		}

		travelsMap = connectOneTimePeriod(FLOW_AM_PEAK, FLOW_AM_PEAK_BEGIN, FLOW_AM_PEAK_END, i);

		for (unordered_map<string, int>::iterator it = travelsMap.begin(); it != travelsMap.end(); ++it)
		{
			vector<string> keySplited;
			boost::split(keySplited, it->first, boost::algorithm::is_any_of(SEPERATOR));
			int hour = (stoi(keySplited[FLOW_MAP_KEY_TIME]) + FLOW_FIVE_O_CLOCK) / FLOW_GET_HOUR;

			if (hour >= FLOW_MIDNIGHT)
			{
				hour -= FLOW_MIDNIGHT;
			}

			int minute = (stoi(keySplited[FLOW_MAP_KEY_TIME]) + FLOW_FIVE_O_CLOCK) % FLOW_GET_HOUR * FLOW_GET_MINUTE;
			Travel::TravelBuilder* builder = new Travel::TravelBuilder();
			builder->setNumberOfPassengers(it->second);
			builder->setStartHour(hour);
			builder->setStartMinute(minute);
			builder->setStartStation(dataLoader.findStationByNLCCode(stoi(keySplited[FLOW_MAP_KEY_START_STATION_NLC_CODE])));
			builder->setEndStation(dataLoader.findStationByNLCCode(stoi(keySplited[FLOW_MAP_KEY_END_STATION_NLC_CODE])));
			travels->push_back(builder->build());

			delete builder;
		}

		travelsMap = connectOneTimePeriod(FLOW_INTER_PEAK, FLOW_INTER_PEAK_BEGIN, FLOW_INTER_PEAK_END, i);

		for (unordered_map<string, int>::iterator it = travelsMap.begin(); it != travelsMap.end(); ++it)
		{
			vector<string> keySplited;
			boost::split(keySplited, it->first, boost::algorithm::is_any_of(SEPERATOR));
			int hour = (stoi(keySplited[FLOW_MAP_KEY_TIME]) + FLOW_FIVE_O_CLOCK) / FLOW_GET_HOUR;

			if (hour >= FLOW_MIDNIGHT)
			{
				hour -= FLOW_MIDNIGHT;
			}

			int minute = (stoi(keySplited[FLOW_MAP_KEY_TIME]) + FLOW_FIVE_O_CLOCK) % FLOW_GET_HOUR * FLOW_GET_MINUTE;
			Travel::TravelBuilder* builder = new Travel::TravelBuilder();
			builder->setNumberOfPassengers(it->second);
			builder->setStartHour(hour);
			builder->setStartMinute(minute);
			builder->setStartStation(dataLoader.findStationByNLCCode(stoi(keySplited[FLOW_MAP_KEY_START_STATION_NLC_CODE])));
			builder->setEndStation(dataLoader.findStationByNLCCode(stoi(keySplited[FLOW_MAP_KEY_END_STATION_NLC_CODE])));
			travels->push_back(builder->build());

			delete builder;
		}

		travelsMap = connectOneTimePeriod(FLOW_PM_PEAK, FLOW_PM_PEAK_BEGIN, FLOW_PM_PEAK_END, i);

		for (unordered_map<string, int>::iterator it = travelsMap.begin(); it != travelsMap.end(); ++it)
		{
			vector<string> keySplited;
			boost::split(keySplited, it->first, boost::algorithm::is_any_of(SEPERATOR));
			int hour = (stoi(keySplited[FLOW_MAP_KEY_TIME]) + FLOW_FIVE_O_CLOCK) / FLOW_GET_HOUR;

			if (hour >= FLOW_MIDNIGHT)
			{
				hour -= FLOW_MIDNIGHT;
			}

			int minute = (stoi(keySplited[FLOW_MAP_KEY_TIME]) + FLOW_FIVE_O_CLOCK) % FLOW_GET_HOUR * FLOW_GET_MINUTE;
			Travel::TravelBuilder* builder = new Travel::TravelBuilder();
			builder->setNumberOfPassengers(it->second);
			builder->setStartHour(hour);
			builder->setStartMinute(minute);
			builder->setStartStation(dataLoader.findStationByNLCCode(stoi(keySplited[FLOW_MAP_KEY_START_STATION_NLC_CODE])));
			builder->setEndStation(dataLoader.findStationByNLCCode(stoi(keySplited[FLOW_MAP_KEY_END_STATION_NLC_CODE])));
			travels->push_back(builder->build());

			delete builder;
		}

		travelsMap = connectOneTimePeriod(FLOW_EVENING, FLOW_EVENING_BEGIN, FLOW_EVENING_END, i);

		for (unordered_map<string, int>::iterator it = travelsMap.begin(); it != travelsMap.end(); ++it)
		{
			vector<string> keySplited;
			boost::split(keySplited, it->first, boost::algorithm::is_any_of(SEPERATOR));
			int hour = (stoi(keySplited[FLOW_MAP_KEY_TIME]) + FLOW_FIVE_O_CLOCK) / FLOW_GET_HOUR;

			if (hour >= FLOW_MIDNIGHT)
			{
				hour -= FLOW_MIDNIGHT;
			}

			int minute = (stoi(keySplited[FLOW_MAP_KEY_TIME]) + FLOW_FIVE_O_CLOCK) % FLOW_GET_HOUR * FLOW_GET_MINUTE;
			Travel::TravelBuilder* builder = new Travel::TravelBuilder();
			builder->setNumberOfPassengers(it->second);
			builder->setStartHour(hour);
			builder->setStartMinute(minute);
			builder->setStartStation(dataLoader.findStationByNLCCode(stoi(keySplited[FLOW_MAP_KEY_START_STATION_NLC_CODE])));
			builder->setEndStation(dataLoader.findStationByNLCCode(stoi(keySplited[FLOW_MAP_KEY_END_STATION_NLC_CODE])));
			travels->push_back(builder->build());

			delete builder;
		}

		travelsMap = connectOneTimePeriod(FLOW_LATE, FLOW_LATE_BEGIN, FLOW_LATE_END, i);

		for (unordered_map<string, int>::iterator it = travelsMap.begin(); it != travelsMap.end(); ++it)
		{
			vector<string> keySplited;
			boost::split(keySplited, it->first, boost::algorithm::is_any_of(SEPERATOR));
			int hour = (stoi(keySplited[FLOW_MAP_KEY_TIME]) + FLOW_FIVE_O_CLOCK) / FLOW_GET_HOUR;

			if (hour >= FLOW_MIDNIGHT)
			{
				hour -= FLOW_MIDNIGHT;
			}

			int minute = (stoi(keySplited[FLOW_MAP_KEY_TIME]) + FLOW_FIVE_O_CLOCK) % FLOW_GET_HOUR * FLOW_GET_MINUTE;
			Travel::TravelBuilder* builder = new Travel::TravelBuilder();
			builder->setNumberOfPassengers(it->second);
			builder->setStartHour(hour);
			builder->setStartMinute(minute);
			builder->setStartStation(dataLoader.findStationByNLCCode(stoi(keySplited[FLOW_MAP_KEY_START_STATION_NLC_CODE])));
			builder->setEndStation(dataLoader.findStationByNLCCode(stoi(keySplited[FLOW_MAP_KEY_END_STATION_NLC_CODE])));
			travels->push_back(builder->build());

			delete builder;
		}

		travelsMap = connectOneTimePeriod(FLOW_NIGHT, FLOW_NIGHT_BEGIN, FLOW_NIGHT_END, i);

		for (unordered_map<string, int>::iterator it = travelsMap.begin(); it != travelsMap.end(); ++it)
		{
			vector<string> keySplited;
			boost::split(keySplited, it->first, boost::algorithm::is_any_of(SEPERATOR));
			int hour = (stoi(keySplited[FLOW_MAP_KEY_TIME]) + FLOW_FIVE_O_CLOCK) / FLOW_GET_HOUR;

			if (hour >= FLOW_MIDNIGHT)
			{
				hour -= FLOW_MIDNIGHT;
			}

			int minute = (stoi(keySplited[FLOW_MAP_KEY_TIME]) + FLOW_FIVE_O_CLOCK) % FLOW_GET_HOUR * FLOW_GET_MINUTE;
			Travel::TravelBuilder* builder = new Travel::TravelBuilder();
			builder->setNumberOfPassengers(it->second);
			builder->setStartHour(hour);
			builder->setStartMinute(minute);
			builder->setStartStation(dataLoader.findStationByNLCCode(stoi(keySplited[FLOW_MAP_KEY_START_STATION_NLC_CODE])));
			builder->setEndStation(dataLoader.findStationByNLCCode(stoi(keySplited[FLOW_MAP_KEY_END_STATION_NLC_CODE])));
			travels->push_back(builder->build());

			delete builder;
		}

		travelsMap = connectOneTimePeriod(FLOW_EARLY, FLOW_EARLY_BEGIN, FLOW_EARLY_END, i);

		for (unordered_map<string, int>::iterator it = travelsMap.begin(); it != travelsMap.end(); ++it)
		{
			vector<string> keySplited;
			boost::split(keySplited, it->first, boost::algorithm::is_any_of(SEPERATOR));
			int hour = (stoi(keySplited[FLOW_MAP_KEY_TIME]) + FLOW_FIVE_O_CLOCK) / FLOW_GET_HOUR;
			
			if (hour >= FLOW_MIDNIGHT)
			{
				hour -= FLOW_MIDNIGHT;
			}

			int minute = (stoi(keySplited[FLOW_MAP_KEY_TIME]) + FLOW_FIVE_O_CLOCK) % FLOW_GET_HOUR * FLOW_GET_MINUTE;
			Travel::TravelBuilder* builder = new Travel::TravelBuilder();
			builder->setNumberOfPassengers(it->second);
			builder->setStartHour(hour);
			builder->setStartMinute(minute);
			builder->setStartStation(dataLoader.findStationByNLCCode(stoi(keySplited[FLOW_MAP_KEY_START_STATION_NLC_CODE])));
			builder->setEndStation(dataLoader.findStationByNLCCode(stoi(keySplited[FLOW_MAP_KEY_END_STATION_NLC_CODE])));
			travels->push_back(builder->build());

			delete builder;
		}
	}

	return travels;
}

void TravelLoader::loadStationFlow(string fileName)
{
	ifstream file(fileName);
	string line;
	getline(file, line);

	while (getline(file, line))
	{
		vector<string> lineSplited;
		boost::split(lineSplited, line, boost::algorithm::is_any_of(SEPERATOR));

		if (stationFlow->size() == 0 
			|| (*(*stationFlow)[stationFlow->size() - 1]->getNLCCode()) != stoi(lineSplited[STATION_FLOW_END_STATION_NLC_CODE_IN_FILE]))
		{
			stationFlow->push_back(new StationPlatformFlow(new int(stoi(lineSplited[STATION_FLOW_END_STATION_NLC_CODE_IN_FILE])),
				new string(lineSplited[STATION_FLOW_END_STATION_NAME_IN_FILE])));
			vector<int*>* station = (*stationFlow)[stationFlow->size() - 1]->getFlow();

			for (int i = 0; i < STATION_FLOW_FLOW_SIZE; i++)
			{
				station->push_back(new int(stoi(lineSplited[STATION_FLOW_FIRST_FLOW_RECORD + i])));
			}
		}
		else
		{
			vector<int*>* station = (*stationFlow)[stationFlow->size() - 1]->getFlow();

			for (int i = 0; i < STATION_FLOW_FLOW_SIZE; i++)
			{
				(*(*station)[i]) = (*(*station)[i]) + stoi(lineSplited[STATION_FLOW_FIRST_FLOW_RECORD + i]);
			}
		}
	}
}

void TravelLoader::loadTravelDestiantion(string fileName)
{
	ifstream file(fileName);
	string line;
	getline(file, line);

	while (getline(file, line))
	{
		vector<string> lineSplited;
		boost::split(lineSplited, line, boost::algorithm::is_any_of(SEPERATOR));
		vector<int*>* flow = new vector<int*>();

		for (int i = 0; i < TRAVEL_DESTINATION_FLOW_SIZE; i++)
		{
			flow->push_back(new int(stoi(lineSplited[TRAVEL_DESTINATION_FIRST_FLOW_RECORD_IN_FILE + i])));
		}

		Destination* destination = new Destination(new int(stoi(lineSplited[TRAVEL_DESTINATION_DESTIANTION_STATION_NLC_CODE_IN_FILE])),
			new string(lineSplited[TRAVEL_DESTINATION_DESTINATION_STATION_NAME_IN_FILE]), flow);
		
		if (travelDestination->size() != 0 && (*(*travelDestination)[travelDestination->size() - 1]->getStartNLCCode())
			== stoi(lineSplited[TRAVEL_DESTINATION_ORGIN_STATION_NLC_CODE_IN_FILE]))
		{
			(*travelDestination)[travelDestination->size() - 1]->getDestinations()->push_back(destination);
		}
		else
		{
			vector<Destination*>* destinationVector = new vector<Destination*>();
			destinationVector->push_back(destination);
			travelDestination->push_back(new TravelDestination(
				new int(stoi(lineSplited[TRAVEL_DESTINATION_ORGIN_STATION_NLC_CODE_IN_FILE])),
				new string(lineSplited[TRAVEL_DESTINATION_ORGIN_STATION_NAME_IN_FILE]), destinationVector
			));
		}
	}
}

void TravelLoader::saveTravelsToFile(vector<Travel*>* travels, string fileName)
{

	ofstream myFile;
	myFile.open(fileName);
	myFile << TRAVEL_FILE_HEADER << endl;

	for (int i = 0; i < travels->size(); i++)
	{
		myFile << (*travels)[i]->getNumberOfPassengers() << SEPERATOR;
		myFile << (*travels)[i]->getStartHour() << SEPERATOR;
		myFile << (*travels)[i]->getStartMinute() << SEPERATOR;
		myFile << (*travels)[i]->getStartStation()->getNLCCode() << SEPERATOR;
		myFile << (*travels)[i]->getStartStation()->getName() << SEPERATOR;
		myFile << (*travels)[i]->getEndStation()->getNLCCode() << SEPERATOR;
		myFile << (*travels)[i]->getEndStation()->getName() << SEPERATOR << endl;
	}
}

vector<Travel*>* TravelLoader::readTravelsFromFile(DataLoader& dataLoader, string fileName)
{
	vector<Travel*>* travels = new vector<Travel*>();
	ifstream file(fileName);
	string line;
	getline(file, line);

	while (getline(file, line))
	{
		vector<string> lineSplited;
		boost::split(lineSplited, line, boost::algorithm::is_any_of(SEPERATOR));

		Travel::TravelBuilder* builder = new Travel::TravelBuilder();
		builder->setNumberOfPassengers(stoi(lineSplited[TRAVELS_NUMBER_OF_PASSENGERS_IN_FILE]));
		builder->setStartHour(stoi(lineSplited[TRAVELS_START_HOUR_IN_FILE]));
		builder->setStartMinute(stoi(lineSplited[TRAVELS_START_MINUTE_IN_FILE]));
		builder->setStartStation(dataLoader.findStationByNLCCode(stoi(lineSplited[TRAVELS_START_STATION_NLC_CODE_IN_FILE])));
		builder->setEndStation(dataLoader.findStationByNLCCode(stoi(lineSplited[TRAVELS_END_STATION_NLC_CODE_IN_FILE])));

		try
		{
			travels->push_back(builder->build());
		}
		catch (const TravelException& e)
		{
			//TODO: Stations not set in Travel
			cout << "Travel builder: " << e.what() << endl;
		}

		delete builder;
	}

	sort(travels->begin(), travels->end(), [](const Travel* lhs, const Travel* rhs)
	{
		return *lhs < *rhs;
	});

	return travels;
}

vector<Travel*>* TravelLoader::readTravelsFromSmallFile(DataLoader& dataLoader, string fileName)
{
	unordered_map<int, Station*> stations;

	for (int i = 0; i < dataLoader.getStationsVector()->size(); i++)
	{
		stations.insert(pair<int, Station*>((*dataLoader.getStationsVector())[i]->getNLCCode(), (*dataLoader.getStationsVector())[i]));
	}

	vector<Travel*>* travels = new vector<Travel*>();
	ifstream file(fileName);
	string line;
	getline(file, line);

	while (getline(file, line))
	{
		vector<string> lineSplited;
		boost::split(lineSplited, line, boost::algorithm::is_any_of(SEPERATOR));
		unordered_map<int, Station*>::iterator startStation = stations.find(stoi(lineSplited[TRAVELS_START_STATION_NLC_CODE_IN_FILE]));
		unordered_map<int, Station*>::iterator endStation = stations.find(stoi(lineSplited[TRAVELS_END_STATION_NLC_CODE_IN_FILE]));

		if (startStation != stations.end() && endStation != stations.end())
		{
			Travel::TravelBuilder* builder = new Travel::TravelBuilder();
			builder->setNumberOfPassengers(stoi(lineSplited[TRAVELS_NUMBER_OF_PASSENGERS_IN_FILE]));
			builder->setStartHour(stoi(lineSplited[TRAVELS_START_HOUR_IN_FILE]));
			builder->setStartMinute(stoi(lineSplited[TRAVELS_START_MINUTE_IN_FILE]));
			builder->setStartStation(startStation->second);
			builder->setEndStation(endStation->second);
			travels->push_back(builder->build());

			delete builder;
		}
	}

	sort(travels->begin(), travels->end(), [](const Travel* lhs, const Travel* rhs)
		{
			return *lhs < *rhs;
		});

	return travels;
}

unordered_map<string, int> TravelLoader::connectOneTimePeriod(int currentFlow, int currentFlowPosition, int currentFlowEnd, int stationIterator)
{
	unordered_map<string, int> travelsMap;
	int countOfEmptyFlowPeriod = 0;

	while (countOfEmptyFlowPeriod != (*travelDestination)[stationIterator]->getDestinations()->size())
	{
		countOfEmptyFlowPeriod = 0;

		for (int k = 0; k < (*travelDestination)[stationIterator]->getDestinations()->size(); k++)
		{
			if ((*(*(*(*travelDestination)[stationIterator]->getDestinations())[k]->getFlow())[currentFlow]) <= 0)
			{
				countOfEmptyFlowPeriod++;
			}
			else
			{
				stringstream ss;
				ss << (*(*travelDestination)[stationIterator]->getStartNLCCode()) << SEPERATOR
					<< (*(*(*travelDestination)[stationIterator]->getDestinations())[k]->getDestiantionNLCCode()) << SEPERATOR
					<< currentFlowPosition;
				string mapKey = ss.str();
				unordered_map<string, int>::iterator it = travelsMap.find(mapKey);

				if (it == travelsMap.end())
				{
					travelsMap.insert(pair<string, int>(mapKey, 1));
				}
				else
				{
					travelsMap[mapKey] = travelsMap[mapKey]++;
				}

				if (currentFlowPosition != currentFlowEnd && (*(*(*stationFlow)[stationIterator]->getFlow())[currentFlowPosition]) == 0)
				{
					currentFlowPosition++;
				}
				
				(*(*(*(*travelDestination)[stationIterator]->getDestinations())[k]->getFlow())[currentFlow])--;
				(*(*(*stationFlow)[stationIterator]->getFlow())[currentFlowPosition])--;
			}
		}
	}

	return travelsMap;
}
