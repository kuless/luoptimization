#pragma once
#include <unordered_set>
#include "AbstractPopulationGenerator.h"
#include "IRandomNumberGenerator.h"
#include "Station.h"
#include "IDijkstra.h"
#include "AbstractNetworkBuilder.h"
#include "SingleCriteriaValue.h"
#include "MultiCriteriaValue.h"

template <class T>
class ShortDistancePopulationGenerator : public AbstractPopulationGenerator<T>
{
	public:
		ShortDistancePopulationGenerator(IRandomNumberGenerator* random, AbstractNetworkBuilder<T>* builder, IDijkstra* quasiShortestPath);
		~ShortDistancePopulationGenerator();

		std::vector<AbstractNetwork<T>*>* generatePopulation(std::vector<Station*>& stations, std::vector<Travel*>& travels, PopulationGeneratorInputParameters* populationGeneratorInputParameters) override;
		PopulationGenerationTechniqueEnum getType() override;

	private:
		IDijkstra* quasiShortestPath;

		Line* generateLine(std::vector<Station*>& stations, int lineNumber);
		void findAndDeleteShortestLine(std::vector<Line*>* lines);
		bool checkIfPercentOfStationsCovered(std::vector<Line*>* lines, int percentOfCoveredStations, int stationsInNetwork);
};
