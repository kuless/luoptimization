#include "StationPlatformFlow.h"

using namespace std;

StationPlatformFlow::StationPlatformFlow(int* NLCCode, string* name)
{
	this->NLCCode = NLCCode;
	this->name = name;
	flow = new vector<int*>();
}

StationPlatformFlow::~StationPlatformFlow()
{
	delete NLCCode;
	delete name;
	
	for (int i = 0; i < flow->size(); i++)
	{
		delete (*flow)[i];
	}

	delete flow;
}

int* StationPlatformFlow::getNLCCode()
{
	return NLCCode;
}

string* StationPlatformFlow::getName()
{
	return name;
}

vector<int*>* StationPlatformFlow::getFlow()
{
	return flow;
}

void StationPlatformFlow::setNLCCode(int& NLCCode)
{
	*(this->NLCCode) = NLCCode;
}

void StationPlatformFlow::setName(string& name)
{
	*(this->name) = name;
}

void StationPlatformFlow::setFlow(vector<int*>& flow)
{
	*(this->flow) = flow;
}
