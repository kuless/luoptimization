#pragma once
#include "ResultsWriter.h"
#include "MultiCriteriaValue.h"
#include "QualityOperator.h"

class ResultWriterMultiCriteria: public ResultsWriter<MultiCriteriaValue>
{
	public:
		ResultWriterMultiCriteria(int id, std::string fileName, std::vector<MultiCriteriaValue*>* trueParetoFornt, std::vector<QualityEnum> qualityTypes, bool saveValuesForTpf);
		~ResultWriterMultiCriteria();

		void saveResultsToFile(std::vector<AbstractNetwork<MultiCriteriaValue>*>* population, int generation, int numberOfGeneration) override;
		void saveBestResultToFile(std::vector<AbstractNetwork<MultiCriteriaValue>*>* population) override;

	protected:
		void saveTitlesToFile() override;

	private:
		QualityOperator* qualityOperator;
		std::vector<QualityEnum> qualityTypes;
		std::ofstream tpfFileWriter;
		bool saveValuesForTpf;

		void saveParetoFrontValues(std::vector<AbstractNetwork<MultiCriteriaValue>*>* population);
};