#include "FrequencyMutation.h"

using namespace std;

FrequencyMutation::FrequencyMutation(IRandomNumberGenerator* random): AbstractMutationOperator(random)
{
}

FrequencyMutation::~FrequencyMutation()
{
}

void FrequencyMutation::mutation(Line* lineForMutation)
{
	int timetableShift = random->getInt(0, RAND_MUTATION) - MAX_REMOVE_ADD_FREQENCE_MUTATION;

	vector<int> timetable = lineForMutation->getTimetableInMinutes();
	vector<int> travelsToRemove = vector<int>();

	for (int i = 0; i < timetable.size(); i++)
	{
		timetable[i] += timetableShift;

		if (timetable[i] < 0 || timetable[i] > DAY_IN_MINUTES)
		{
			travelsToRemove.push_back(i);
		}
	}

	for (int i = travelsToRemove.size() - 1; i >= 0; i--)
	{
		timetable.erase(timetable.begin() + travelsToRemove[i]);
	}

	lineForMutation->setTimetable(timetable);
}

MutationTechniqueEnum FrequencyMutation::getType()
{
	return MutationTechniqueEnum::FREQUENCY;
}
