#pragma once
#include <unordered_map>
#include <algorithm>
#include "Station.h"
#include "Line.h"
#include "IDijkstra.h"
#include "AbstractMutationOperator.h"
#include "DijkstraValue.h"

class LineMutation : public AbstractMutationOperator
{
	public:
		LineMutation(IRandomNumberGenerator* random, IDijkstra* quasiShortestPath);
		~LineMutation();
		void mutation(Line* lineForMutation) override;
		MutationTechniqueEnum getType() override;

	private:
		IDijkstra* quasiShortestPath;
};
