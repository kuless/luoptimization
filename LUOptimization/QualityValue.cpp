#include "QualityValue.h"

QualityValue::QualityValue(boost::multiprecision::uint128_t quality)
{
	qualityInt = quality;
	isQualityInt = true;
}

QualityValue::QualityValue(boost::multiprecision::mpf_float quality)
{
	qualityFloat = quality;
	isQualityInt = false;
}

QualityValue::~QualityValue()
{
}

std::string QualityValue::getQuality()
{
	if (isQualityInt)
	{
		return qualityInt.str();
	}
	else
	{
		return qualityFloat.str();
	}
}
