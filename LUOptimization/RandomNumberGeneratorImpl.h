#pragma once
#include "IRandomNumberGenerator.h"
#include <random>

class RandomNumberGeneratorImpl : public IRandomNumberGenerator
{
	public:
		RandomNumberGeneratorImpl();
		~RandomNumberGeneratorImpl();

		int getInt(int lowerLimit, int upperLimit) override;
		unsigned long long int getUnsignedLongLongInt(unsigned long long int lowerLimit, unsigned long long int upperLimit) override;
		IRandomNumberGenerator* copy() override;

	private:
		std::random_device rd;
		std::default_random_engine generator;
};