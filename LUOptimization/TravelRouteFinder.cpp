#include "TravelRouteFinder.h"

using namespace std;

template <class T>
TravelRouteFinder<T>::TravelRouteFinder(AbstractNetwork<T>* network)
{
	this->network = network;
	cache = new unordered_map<int, RouteCacheValue*>();
}

template <class T>
TravelRouteFinder<T>::~TravelRouteFinder()
{
	for (unordered_map<int, RouteCacheValue*>::iterator it = cache->begin(); it != cache->end(); ++it)
	{
		delete it->second;
	}

	delete cache;
}

template <class T>
void TravelRouteFinder<T>::setTravelsRoute(list<Travel*>* travels)
{
	for (list<Travel*>::iterator it = travels->begin(); it != travels->end(); ++it)
	{
		list<Route*>* route = dijkstra((*it)->getStartStation(), (*it)->getEndStation());
		(*it)->setRoutes(route);
	}
}

template <class T>
list<Route*>* TravelRouteFinder<T>::dijkstra(Station* start, Station* end)
{
	int key = start->getNLCCode() * 1000 + end->getNLCCode();
	unordered_map<int, RouteCacheValue*>::iterator it = cache->find(key);

	if (it == cache->end())
	{
		vector<DijkstraValueRoute*>* values = initDijkstraValueRoutes(start);
		unordered_map<pair<Station*, Line*>, DijkstraValueRoute*, PairHash>* visitedNodes = new unordered_map<pair<Station*, Line*>, DijkstraValueRoute*, PairHash>();

		while (!values->empty() && (*values)[values->size() - 1]->getValue() != INT_MAX)
		{
			DijkstraValueRoute* node = getNextNode(values, visitedNodes);
			vector<pair<Station*, bool>>* neighboringStationsOnLine = getNeighboringStationsOnLine(node);
			handleNeighbors(start, node, values, neighboringStationsOnLine);
			delete neighboringStationsOnLine;

			if (node->getStation() != start)
			{
				saveResult(start, node, visitedNodes);
			}

			sort(values->begin(), values->end(), [](const DijkstraValueRoute* lhs, const DijkstraValueRoute* rhs)
			{
				return lhs > rhs;
			});
		}

		for (int i = 0; i < values->size(); i++)
		{
			int cacheKey = start->getNLCCode() * 1000 + (*values)[i]->getStation()->getNLCCode();
			unordered_map<int, RouteCacheValue*>::iterator currentCacheValue = cache->find(cacheKey);

			if (currentCacheValue == cache->end())
			{
				cache->insert(
					pair<int, RouteCacheValue*>(
						cacheKey, new RouteCacheValue(new list<Route*>(), -1)
						)
				);
			}

			delete (*values)[i];
		}

		for (typename unordered_map<pair<Station*, Line*>, DijkstraValueRoute*, PairHash>::iterator it = visitedNodes->begin(); it != visitedNodes->end(); ++it)
		{
			delete it->second;
		}

		delete visitedNodes;
		delete values;
	}

	return searchForResult(key);
}

template <class T>
vector<DijkstraValueRoute*>* TravelRouteFinder<T>::initDijkstraValueRoutes(Station* start)
{
	vector<DijkstraValueRoute*>* values = new vector<DijkstraValueRoute*>();

	for (int i = 0; i < network->getLines()->size(); i++)
	{
		for (int k = 0; k < (*network->getLines())[i]->getStops().size(); k++)
		{
			if (start == (*network->getLines())[i]->getStops()[k])
			{
				values->push_back(new DijkstraValueRoute(0, (*network->getLines())[i]->getStops()[k], nullptr, k, (*network->getLines())[i], nullptr, false));
			}
			else
			{
				values->push_back(new DijkstraValueRoute(INT_MAX, (*network->getLines())[i]->getStops()[k], nullptr, k, (*network->getLines())[i], nullptr, false));
			}
		}
	}

	sort(values->begin(), values->end(), [](const DijkstraValueRoute* lhs, const DijkstraValueRoute* rhs)
	{
		return lhs > rhs;
	});

	return values;
}

template <class T>
DijkstraValueRoute* TravelRouteFinder<T>::getNextNode(vector<DijkstraValueRoute*>* values, unordered_map<pair<Station*, Line*>, DijkstraValueRoute*, PairHash>* visitedNodes)
{
	DijkstraValueRoute* node = (*values)[values->size() - 1];
	values->pop_back();
	visitedNodes->insert(pair<pair<Station*, Line*>, DijkstraValueRoute*>(pair<Station*, Line*>(node->getStation(), node->getLine()), node));
	return node;
}

template <class T>
vector<pair<Station*, bool>>* TravelRouteFinder<T>::getNeighboringStationsOnLine(DijkstraValueRoute* node)
{
	vector<pair<Station*, bool>>* neighboringStationsOnLine = new vector<pair<Station*, bool>>();

	if (node->getStationNumber() != 0)
	{
		neighboringStationsOnLine->push_back(pair<Station*, bool>(node->getLine()->getStops()[node->getStationNumber() - 1], false));
	}

	if (node->getStationNumber() != node->getLine()->getStops().size() - 1)
	{
		neighboringStationsOnLine->push_back(pair<Station*, bool>(node->getLine()->getStops()[node->getStationNumber() + 1], true));
	}

	return neighboringStationsOnLine;
}

template <class T>
void TravelRouteFinder<T>::handleNeighbors(Station* startStation, DijkstraValueRoute* node, vector<DijkstraValueRoute*>* values, vector<pair<Station*, bool>>* neighbors)
{
	for (int i = 0; i < values->size(); i++)
	{
		if (node->getLine() == (*values)[i]->getLine())
		{
			if ((*values)[i]->getStation() == (*neighbors)[0].first)
			{
				int nodeNewValue = node->getValue() + getDistance((*neighbors)[0], node);
				setNewValueIfPossible((*values)[i], node, nodeNewValue, (*neighbors)[0].second);
			}
			else if (neighbors->size() == 2 && (*values)[i]->getStation() == (*neighbors)[1].first)
			{
				int nodeNewValue = node->getValue() + getDistance((*neighbors)[1], node);
				setNewValueIfPossible((*values)[i], node, nodeNewValue, (*neighbors)[1].second);
			}
		}
		else if (isTransferPossible((*values)[i], node, startStation))
		{
			transferToDifferentLine((*values)[i], node);
		}
	}
}

template <class T>
int TravelRouteFinder<T>::getDistance(pair<Station*, bool> neighbor, DijkstraValueRoute* node)
{
	if (neighbor.second)
	{
		return node->getLine()->getJourneyTime()[node->getStationNumber()];
	}
	else
	{
		return node->getLine()->getJourneyTime()[node->getStationNumber() - 1];
	}
}

template <class T>
void TravelRouteFinder<T>::setNewValueIfPossible(DijkstraValueRoute* value, DijkstraValueRoute* node, int nodeNewValue, bool neighborMovement)
{
	if (nodeNewValue < value->getValue())
	{
		value->setPreviosStationLine(node->getLine());
		value->setPreviosStation(node->getStation());
		value->setValue(nodeNewValue);
		value->setForwardMovement(neighborMovement);
	}
}

template <class T>
bool TravelRouteFinder<T>::isTransferPossible(DijkstraValueRoute* value, DijkstraValueRoute* node, Station* startStation)
{
	return value->getStation() == node->getStation()
		&& value->getStation() != startStation
		&& node->getValue() + TRANSFER_PENALTY < value->getValue();
}

template <class T>
void TravelRouteFinder<T>::transferToDifferentLine(DijkstraValueRoute* value, DijkstraValueRoute* node)
{
	value->setPreviosStationLine(node->getLine());
	value->setPreviosStation(node->getStation());
	value->setValue(node->getValue() + TRANSFER_PENALTY);
}

template <class T>
void TravelRouteFinder<T>::saveResult(Station* startStation, DijkstraValueRoute* node, unordered_map<pair<Station*, Line*>, DijkstraValueRoute*, PairHash>* visitedNodes)
{
	int cacheKey = startStation->getNLCCode() * 1000 + node->getStation()->getNLCCode();
	list<Route*>* shortesPath = new list<Route*>();
	shortesPath->push_back(new Route(node->getStation(), node->getLine(), node->getForwardMovement()));
	Line* currentLine = node->getLine();

	for (
		unordered_map<pair<Station*, Line*>, DijkstraValueRoute*>::iterator helper = visitedNodes->find(pair<Station*, Line*>(node->getPreviosStation(), node->getPreviosStationLine()));
		helper != visitedNodes->end();
		helper = visitedNodes->find(pair<Station*, Line*>(helper->second->getPreviosStation(), helper->second->getPreviosStationLine()))
		)
	{
		if (currentLine != helper->second->getLine())
		{
			shortesPath->push_back(new Route(helper->second->getStation(), helper->second->getLine(), helper->second->getForwardMovement()));
			currentLine = helper->second->getLine();
		}
	}

	reverse(shortesPath->begin(), shortesPath->end());

	unordered_map<int, RouteCacheValue*>::iterator currentCacheValue = cache->find(cacheKey);

	if (currentCacheValue == cache->end())
	{
		cache->insert(
			pair<int, RouteCacheValue*>(
				cacheKey, new RouteCacheValue(shortesPath, node->getValue())
				)
		);
	}
	else if (currentCacheValue->second->getTravelTime() > node->getValue())
	{
		list<Route*>* routeToDelete = currentCacheValue->second->getRoute();
		currentCacheValue->second->setRoute(shortesPath);
		currentCacheValue->second->setTravelTime(node->getValue());
		deleteRoute(routeToDelete);
	}
	else
	{
		deleteRoute(shortesPath);
	}
}

template <class T>
void TravelRouteFinder<T>::deleteRoute(list<Route*>* route)
{
	for (list<Route*>::iterator it = route->begin(); it != route->end(); ++it)
	{
		delete* it;
	}

	delete route;
}

template <class T>
list<Route*>* TravelRouteFinder<T>::searchForResult(int key)
{
	unordered_map<int, RouteCacheValue*>::iterator it = cache->find(key);

	if (it == cache->end())
	{
		pair<int, RouteCacheValue*> result =
			pair<int, RouteCacheValue*>(key,
				new RouteCacheValue(new list<Route*>(), -1));

		cache->insert(result);
		return copyRoutes(result.second->getRoute());
	}
	else
	{
		return copyRoutes(it->second->getRoute());
	}
}

template <class T>
list<Route*>* TravelRouteFinder<T>::copyRoutes(list<Route*>* routes)
{
	list<Route*>* copy = new list<Route*>();

	for (list<Route*>::iterator it = routes->begin(); it != routes->end(); ++it)
	{
		copy->push_back(new Route(**it));
	}

	return copy;
}

template class TravelRouteFinder<SingleCriteriaValue>;
template class TravelRouteFinder<MultiCriteriaValue>;
