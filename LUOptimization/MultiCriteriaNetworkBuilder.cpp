#include "MultiCriteriaNetworkBuilder.h"

MultiCriteriaNetworkBuilder::MultiCriteriaNetworkBuilder() : AbstractNetworkBuilder<MultiCriteriaValue>()
{
}

MultiCriteriaNetworkBuilder::~MultiCriteriaNetworkBuilder()
{
}

AbstractNetwork<MultiCriteriaValue>* MultiCriteriaNetworkBuilder::createNewNetwork(std::vector<Line*>* lines)
{
	return new MultiCriteriaNetwork(lines);
}

AbstractNetworkBuilder<MultiCriteriaValue>* MultiCriteriaNetworkBuilder::copy()
{
	return new MultiCriteriaNetworkBuilder();
}
