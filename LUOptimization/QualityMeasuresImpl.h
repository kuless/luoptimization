#pragma once
#include <unordered_map>
#include "IQualityMeasures.h"
#include "AbstractQualityMeasure.h"
#include "HyperVolume.h"
#include "InvertedGenerationalDistance.h"
#include "QualityValue.h"

class QualityMeasuresImpl: public IQualityMeasures
{
	public:
		QualityMeasuresImpl(std::vector<MultiCriteriaValue*>* trueParetoFornt);
		~QualityMeasuresImpl();
		QualityValue* getQuality(std::vector<AbstractNetwork<MultiCriteriaValue>*>* population, QualityEnum qualityType) override;

	private:
		std::unordered_map<QualityEnum, AbstractQualityMeasure*> qualityMeasures;

		std::pair<QualityEnum, AbstractQualityMeasure*> getMapValue(AbstractQualityMeasure* qualityMeasure);
};
