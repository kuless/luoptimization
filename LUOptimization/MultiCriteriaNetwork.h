#pragma once
#include "AbstractNetwork.h"
#include "MultiCriteriaValue.h"

class MultiCriteriaNetwork: public AbstractNetwork<MultiCriteriaValue>
{
	public:
		MultiCriteriaNetwork(std::vector<Line*>* lines);
		MultiCriteriaNetwork(const AbstractNetwork<MultiCriteriaValue>& network);
		~MultiCriteriaNetwork();

		unsigned long long int getSelectionValue() override;

		AbstractNetwork<MultiCriteriaValue>* clone() override;
		AbstractNetwork<MultiCriteriaValue>* callChildConstructor(std::vector<Line*>* lines) override;
		AbstractNetwork<MultiCriteriaValue>* callChildConstructor(const AbstractNetwork<MultiCriteriaValue>& network) override;
};
