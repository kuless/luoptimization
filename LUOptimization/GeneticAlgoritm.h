#pragma once
#include <boost/stacktrace.hpp>
#include <vector>
#include <list>
#include <mutex>
#include <thread>
#include <array>
#include "Constants.h"
#include "Travel.h"
#include "Station.h"
#include "AbstractNetwork.h"
#include "PopulationGenerator.h"
#include "IRandomNumberGenerator.h"
#include "RandomNumberGeneratorImpl.h"
#include "AbstractEvaluationOperator.h"
#include "CrossoverOperators.h"
#include "MutationOperators.h"
#include "SelectionOperators.h"
#include "ResultsWriter.h"
#include "GaInputParameters.h"
#include "InitInputParameters.h"
#include "SingleCriteriaValue.h"
#include "MultiCriteriaValue.h"
#include "IDijkstra.h"
#include "DijkstraImpl.h"
#include "QuasiShortestPath.h"
#include "GaCommonComponents.h"
#include "PopulationGenerationTechniqueEnum.h"
#include "NotImplementedException.h"

template <class T>
class GeneticAlgoritm
{
	public:
		GeneticAlgoritm(int id, std::vector<Station*>* stationsVector, std::vector<Travel*>* travels, GaInputParameters* gaInputParameters, GaCommonComponents<T>* gaCommonComponents);
		virtual ~GeneticAlgoritm();

		void run();

	protected:
		int id;
		std::vector<Station*>* stationsVector;
		std::vector<Travel*>* travels;
		std::vector<AbstractNetwork<T>*>* population;
		IDijkstra* dijkstra;
		IDijkstra* quasiShortestPath;
		AbstractEvaluationOperator<T>* evaluationOperator;
		SelectionOperators<T>* selectionOperators;
		CrossoverOperators<T>* crossoverOperators;
		MutationOperators<T>* mutationOperators;
		ResultsWriter<T>* resultsWriter;
		IRandomNumberGenerator* random;
		RuntimeInputParameters* runtimeInputParameters;

		void evaluate(int numberOfThreads);

		virtual PopulationGenerator<T>* initPopulationGenerator(IDijkstra* dijkstra, IDijkstra* quasiShortestPath, PopulationGenerationTechniqueEnum populationGenerationTechnique);
		virtual void firstEvaluation(int numberOfThreads);
		virtual void nextEvaluations(int numberOfThreads);
		virtual void setNewPopulation(std::vector<AbstractNetwork<T>*>* newPopulation);

	private:
		void generatePopulation(PopulationGeneratorInputParameters* populationGeneratorInputParameters);
		std::vector<std::pair<AbstractNetwork<T>*, AbstractNetwork<T>*>*>* selection();
		std::vector<AbstractNetwork<T>*>* crossover(std::vector<std::pair<AbstractNetwork<T>*, AbstractNetwork<T>*>*>* pairs, int numberOfThreads);
		void crossoverThread(std::vector<std::pair<AbstractNetwork<T>*, AbstractNetwork<T>*>*>* pairs, std::vector<AbstractNetwork<T>*>* newPopulation, int begin, int end, std::mutex* mutexObj);
		void mutation(int numberOfThreads);
		void mutationThread(int begin, int end);
		void evaluateThread(int begin, int end, std::mutex* mutexObj);
};
