#include "RgaPopulationGenerator.h"

using namespace std;

template<class T>
RgaPopulationGenerator<T>::RgaPopulationGenerator(IRandomNumberGenerator* random, AbstractNetworkBuilder<T>* builder, IDijkstra* dijkstra): AbstractPopulationGenerator<T>(random, builder)
{
	this->dijkstra = dijkstra;
}

template<class T>
RgaPopulationGenerator<T>::~RgaPopulationGenerator()
{
}

template<class T>
vector<AbstractNetwork<T>*>* RgaPopulationGenerator<T>::generatePopulation(vector<Station*>& stations, vector<Travel*>& travels, PopulationGeneratorInputParameters* populationGeneratorInputParameters)
{
	vector<DemandMatrixElement*>* demandMatrix = createDemandMatrix(travels);
	vector<Line*>* lines = initLines(demandMatrix, populationGeneratorInputParameters->getNumberOfLines());

	return nullptr;
}

template<class T>
PopulationGenerationTechniqueEnum RgaPopulationGenerator<T>::getType()
{
	return PopulationGenerationTechniqueEnum::RGA;
}

template<class T>
vector<DemandMatrixElement*>* RgaPopulationGenerator<T>::createDemandMatrix(vector<Travel*>& travels)
{
	unordered_map<string, DemandMatrixElement*> resultMap = unordered_map<string, DemandMatrixElement*>();

	for (int i = 0; i < travels.size(); i++)
	{
		stringstream ss;
		ss << travels[i]->getStartStation()->getNLCCode() << SEPERATOR << travels[i]->getEndStation()->getNLCCode();
		string key = ss.str();
		unordered_map<string, DemandMatrixElement*>::iterator demand = resultMap.find(key);

		if (demand == resultMap.end())
		{
			DemandMatrixElement* demandMatrixElement = new DemandMatrixElement(travels[i]->getStartStation(), travels[i]->getEndStation(), travels[i]->getNumberOfPassengers());
			resultMap.insert(pair<string, DemandMatrixElement*>(key, demandMatrixElement));
		}
		else
		{
			demand->second->addPassengers(travels[i]->getNumberOfPassengers());
		}
	}

	vector<DemandMatrixElement*>* result = new vector<DemandMatrixElement*>();

	for (unordered_map<string, DemandMatrixElement*>::iterator it = resultMap.begin(); it != resultMap.end(); ++it)
	{
		result->push_back(it->second);
	}

	sort(
		result->begin(), result->end(),
			[](DemandMatrixElement* l, DemandMatrixElement* r) {
		return l->getPassengers() < r->getPassengers();
		}
	);

	return result;
}

template<class T>
vector<Line*>* RgaPopulationGenerator<T>::initLines(vector<DemandMatrixElement*>* demandMatrix, int numberOfLines)
{
	vector<Line*>* lines = new vector<Line*>();

	for (int i = 0; i < numberOfLines; i++)
	{
		vector<Station*>* stops = this->dijkstra->dijkstra(demandMatrix->back()->getStartStation(), demandMatrix->back()->getEndStation());
		Line* newLine = new Line(i + 1, "", stops);
		//TODO temp solution for timetable
		this->generateTimetableForNewLine(newLine);
		lines->push_back(newLine);
		demandMatrix->pop_back();
	}

	return lines;
}

template class RgaPopulationGenerator<SingleCriteriaValue>;
template class RgaPopulationGenerator<MultiCriteriaValue>;
