#pragma once
#include "Line.h"
#include "AbstractMutationOperator.h"

class FrequencyMutation : public AbstractMutationOperator
{
	public:
		FrequencyMutation(IRandomNumberGenerator* random);
		~FrequencyMutation();
		void mutation(Line* lineForMutation) override;
		MutationTechniqueEnum getType() override;

	private:

};
