#include "MultiCriteriaValue.h"

using namespace std;

MultiCriteriaValue::MultiCriteriaValue()
{
	passengersValue = INIT_VALUE;
	maintenanceValue = INIT_VALUE;
	paretoFront = INIT_VALUE;
	distance = INIT_VALUE;
}

MultiCriteriaValue::MultiCriteriaValue(const MultiCriteriaValue& multiCriteriaValue)
{
	passengersValue = multiCriteriaValue.passengersValue;
	maintenanceValue = multiCriteriaValue.maintenanceValue;
	paretoFront = INIT_VALUE;
	distance = INIT_VALUE;
}

MultiCriteriaValue::MultiCriteriaValue(unsigned long long int passengersValue, unsigned long long int maintenanceValue)
{
	this->passengersValue = passengersValue;
	this->maintenanceValue = maintenanceValue;
	paretoFront = INIT_VALUE;
	distance = INIT_VALUE;
}

MultiCriteriaValue::~MultiCriteriaValue()
{
}

bool MultiCriteriaValue::operator>(const MultiCriteriaValue& multiCriteriaValue) const
{
	return paretoFront < multiCriteriaValue.paretoFront || (paretoFront == multiCriteriaValue.paretoFront && distance > multiCriteriaValue.distance);
}

bool MultiCriteriaValue::operator<(const MultiCriteriaValue& multiCriteriaValue) const
{
	return paretoFront > multiCriteriaValue.paretoFront || (paretoFront == multiCriteriaValue.paretoFront && distance < multiCriteriaValue.distance);
}

bool MultiCriteriaValue::_Equal(MultiCriteriaValue& multiCriteriaValue)
{
	return passengersValue == multiCriteriaValue.passengersValue && maintenanceValue == multiCriteriaValue.maintenanceValue
		&& paretoFront == multiCriteriaValue.paretoFront && distance == multiCriteriaValue.distance;
}

bool MultiCriteriaValue::_EqualRawValue(MultiCriteriaValue& multiCriteriaValue)
{
	return passengersValue == multiCriteriaValue.passengersValue && maintenanceValue == multiCriteriaValue.maintenanceValue;
}

bool MultiCriteriaValue::doesItDominate(MultiCriteriaValue& multiCriteriaValue)
{
	return passengersValue >= multiCriteriaValue.passengersValue && maintenanceValue >= multiCriteriaValue.maintenanceValue &&
		!(passengersValue == multiCriteriaValue.passengersValue && maintenanceValue == multiCriteriaValue.maintenanceValue);
}

MultiCriteriaValue* MultiCriteriaValue::clone()
{
	MultiCriteriaValue* value = new MultiCriteriaValue();
	value->passengersValue = this->passengersValue;
	value->maintenanceValue = this->maintenanceValue;
	value->paretoFront = this->paretoFront;
	value->distance = this->distance;
	return value;
}

unsigned long long int MultiCriteriaValue::getPassengersValue()
{
	return passengersValue;
}

unsigned long long int MultiCriteriaValue::getMaintenanceValue()
{
	return maintenanceValue;
}

int MultiCriteriaValue::getParetoFront()
{
	return paretoFront;
}

unsigned long long int MultiCriteriaValue::getDistance()
{
	return distance;
}

void MultiCriteriaValue::setPassengersValue(unsigned long long int passengersValue)
{
	this->passengersValue = passengersValue;
}

void MultiCriteriaValue::setMaintenanceValue(unsigned long long int maintenanceValue)
{
	this->maintenanceValue = maintenanceValue;
}

void MultiCriteriaValue::setParetoFront(int paretoFront)
{
	this->paretoFront = paretoFront;
}

void MultiCriteriaValue::setDistance(unsigned long long int distance)
{
	this->distance = distance;
}

void MultiCriteriaValue::addPassengersValue(unsigned long long int passengersValue)
{
	this->passengersValue += passengersValue;
}

void MultiCriteriaValue::addMaintenanceValue(unsigned long long int maintenanceValue)
{
	this->maintenanceValue += maintenanceValue;
}

void MultiCriteriaValue::addDistance(unsigned long long int distance)
{
	this->distance += distance;
}
