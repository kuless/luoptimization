#include "DemandMatrixElement.h"

DemandMatrixElement::DemandMatrixElement(Station* startStation, Station* endStation, int passengers)
{
	this->startStation = startStation;
	this->endStation = endStation;
	this->passengers = passengers;
}

DemandMatrixElement::~DemandMatrixElement()
{
}

void DemandMatrixElement::addPassengers(int newPassengers)
{
	passengers += newPassengers;
}

Station* DemandMatrixElement::getStartStation()
{
	return startStation;
}

Station* DemandMatrixElement::getEndStation()
{
	return endStation;
}

int DemandMatrixElement::getPassengers()
{
	return passengers;
}
