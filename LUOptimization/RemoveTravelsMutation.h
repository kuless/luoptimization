#pragma once
#include "Line.h"
#include "AbstractMutationOperator.h"

class RemoveTravelsMutation : public AbstractMutationOperator
{
	public:
		RemoveTravelsMutation(IRandomNumberGenerator* random);
		~RemoveTravelsMutation();
		void mutation(Line* lineForMutation) override;
		MutationTechniqueEnum getType() override;

	private:

};
