#include "Nsgaii.h"

using namespace std;

Nsgaii::Nsgaii()
{
}

Nsgaii::~Nsgaii()
{
}

void Nsgaii::evaluate(vector<AbstractNetwork<MultiCriteriaValue>*>* population)
{
	vector<ParetoFront*>* nonDominationResult = fastNonDominatedSort(population);

	for (vector<ParetoFront*>::iterator it = nonDominationResult->begin(); it != nonDominationResult->end(); ++it)
	{
		if ((*it)->getFront()->size() > 2)
		{
			crowdingDistance(*it);
		}

		delete *it;
	}

	sort(population->begin(), population->end(), [](AbstractNetwork<MultiCriteriaValue>* left, AbstractNetwork<MultiCriteriaValue>* right)
	{
		return (left->getValue()->getParetoFront() == right->getValue()->getParetoFront() && left->getValue()->getMaintenanceValue() > right->getValue()->getMaintenanceValue())
			|| left->getValue()->getParetoFront() < right->getValue()->getParetoFront();
	});

	delete nonDominationResult;
}

vector<AbstractNetwork<MultiCriteriaValue>*>* Nsgaii::evaluate(vector<AbstractNetwork<MultiCriteriaValue>*>* oldPopulation, vector<AbstractNetwork<MultiCriteriaValue>*>* newPopulation)
{
	vector<AbstractNetwork<MultiCriteriaValue>*>* population = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	population->insert(population->end(), newPopulation->begin(), newPopulation->end());
	population->insert(population->end(), oldPopulation->begin(), oldPopulation->end());

	vector<ParetoFront*>* nonDominationResult = fastNonDominatedSort(population);
	delete population;
	vector<AbstractNetwork<MultiCriteriaValue>*>* resultPopulation = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	vector<ParetoFront*>::iterator it = nonDominationResult->begin();
	
	while (resultPopulation->size() + (*it)->getFront()->size() <= newPopulation->size())
	{
		if ((*it)->getFront()->size() > 2)
		{
			crowdingDistance(*it);
		}

		for (int i = 0; i < (*it)->getFront()->size(); i++)
		{
			AbstractNetwork<MultiCriteriaValue>* clone = (*it)->getFront()->at(i)->first->clone();
			clone->setValue((*it)->getFront()->at(i)->first->getValue()->clone());
			resultPopulation->push_back(clone);
		}

		delete* it;
		++it;
	}
	
	if ((*it)->getFront()->size() > 2)
	{
		crowdingDistance(*it);
	}
	
	(*it)->sortFront();

	for (int i = 0; resultPopulation->size() != newPopulation->size(); i++)
	{
		AbstractNetwork<MultiCriteriaValue>* clone = (*it)->getFront()->at(i)->first->clone();
		clone->setValue((*it)->getFront()->at(i)->first->getValue()->clone());
		resultPopulation->push_back(clone);
	}

	while (it != nonDominationResult->end())
	{
		delete *it;
		++it;
	}

	delete nonDominationResult;
	return resultPopulation;
}

ParetoFront* Nsgaii::getFirstFront(std::vector<AbstractNetwork<MultiCriteriaValue>*>* population)
{
	ParetoFront* populationAsParetoFront = new ParetoFront(population);
	ParetoFront* front = new ParetoFront();

	for (vector<pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>::iterator p = populationAsParetoFront->begin(); p != populationAsParetoFront->end(); ++p)
	{
		setDominationsForElement(populationAsParetoFront, *p);

		if ((*p)->second->getDominationCounter() == 0)
		{
			(*p)->first->getValue()->setParetoFront(FIRST_PARETO_FRONT);
			front->addNetwork((*p));
		}
	}

	populationAsParetoFront->clearFront();
	delete populationAsParetoFront;
	return front;
}

vector<ParetoFront*>* Nsgaii::fastNonDominatedSort(vector<AbstractNetwork<MultiCriteriaValue>*>* population)
{
	int frontNumber = FIRST_PARETO_FRONT;
	ParetoFront* front = getFirstFront(population);
	vector<ParetoFront*>* result = new vector<ParetoFront*>();

	while (!front->empty())
	{
		result->push_back(front);
		front = getNextFront(front, ++frontNumber);
	}
	
	delete front;
	return result;
}

void Nsgaii::setDominationsForElement(ParetoFront* populationAsParetoFront, pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>* element)
{
	for (vector<pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>::iterator q = populationAsParetoFront->begin(); q != populationAsParetoFront->end(); ++q)
	{
		if ((*q)->first->getValue()->doesItDominate(*element->first->getValue()))
		{
			element->second->addDomination((*q));
		}
		else if (element->first->getValue()->doesItDominate(*(*q)->first->getValue()))
		{
			element->second->addDominationCounter();
		}
	}
}

ParetoFront* Nsgaii::getNextFront(ParetoFront* front, int frontNumber)
{
	ParetoFront* nextFront = new ParetoFront();

	for (vector<pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>::iterator p = front->begin(); p != front->end(); ++p)
	{
		reduceDominationCounter(nextFront, *p, frontNumber);
	}

	return nextFront;
}

void Nsgaii::reduceDominationCounter(ParetoFront* nextFront, pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>* element, int frontNumber)
{
	for (vector<pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>::iterator q = element->second->getDominations()->begin(); q != element->second->getDominations()->end(); ++q)
	{
		(*q)->second->reduceDominationCounter();

		if ((*q)->second->getDominationCounter() == 0)
		{
			(*q)->first->getValue()->setParetoFront(frontNumber);
			nextFront->addNetwork(*q);
		}
	}
}

void Nsgaii::crowdingDistance(ParetoFront* nonDominatedFront)
{
	crowdingDistancePassengers(nonDominatedFront);
	crowdingDistanceMaintenance(nonDominatedFront);
}

void Nsgaii::crowdingDistancePassengers(ParetoFront* nonDominatedFront)
{
	sort(
		nonDominatedFront->begin(), nonDominatedFront->end(),
		[](pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>* l, pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>* r) {
			return l->first->getValue()->getPassengersValue() > r->first->getValue()->getPassengersValue();
		}
	);

	vector<pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>::iterator previosValue = nonDominatedFront->begin();
	(*previosValue)->first->getValue()->setDistance(ULLONG_MAX);
	vector<pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>::iterator currentValue = nonDominatedFront->begin();
	currentValue++;
	vector<pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>::iterator nextValue = nonDominatedFront->begin();
	nextValue++;
	nextValue++;

	while (nextValue != nonDominatedFront->end())
	{
		long long int distance = (*previosValue)->first->getValue()->getPassengersValue() - (*nextValue)->first->getValue()->getPassengersValue();
		(*currentValue)->first->getValue()->setDistance(distance);
		previosValue++;
		currentValue++;
		nextValue++;
	}

	(*currentValue)->first->getValue()->setDistance(ULLONG_MAX);
}

void Nsgaii::crowdingDistanceMaintenance(ParetoFront* nonDominatedFront)
{
	sort(
		nonDominatedFront->begin(), nonDominatedFront->end(),
		[](pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>* l, pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>* r) {
			return l->first->getValue()->getMaintenanceValue() > r->first->getValue()->getMaintenanceValue();
		}
	);

	vector<pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>::iterator previosValue = nonDominatedFront->begin();
	(*previosValue)->first->getValue()->setDistance(ULLONG_MAX);
	vector<pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>::iterator currentValue = nonDominatedFront->begin();
	currentValue++;
	vector<pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>::iterator nextValue = nonDominatedFront->begin();
	nextValue++;
	nextValue++;

	while (nextValue != nonDominatedFront->end())
	{
		if ((*currentValue)->first->getValue()->getDistance() != ULLONG_MAX) {
			long long int distance = (*previosValue)->first->getValue()->getMaintenanceValue() - (*nextValue)->first->getValue()->getMaintenanceValue();
			(*currentValue)->first->getValue()->addDistance(distance);
		}

		previosValue++;
		currentValue++;
		nextValue++;
	}

	(*currentValue)->first->getValue()->setDistance(ULLONG_MAX);
}
