#include "Solution.h"

using namespace std;

Solution::Solution()
{
}

Solution::~Solution()
{
}

void Solution::start()
{
	try
	{
		ifstream file(SOLUTION_FILE);
		string line;
		getline(file, line);

		if (line._Equal(GA_SOLUTION))
		{
			getline(file, line);
			batchSize = stoi(line);
			runSolution();
		}
		else if (line._Equal(TPF_SOLUTION))
		{
			createTrueParetoFront();
		}

		file.close();
	}
	catch (...)
	{
		cout << "Fail Solution::start" << endl;
		cout << boost::stacktrace::stacktrace() << endl;
	}
}

void Solution::createTrueParetoFront()
{
	TrueParetoFront trueParetoFront = TrueParetoFront();
	trueParetoFront.createTpf();
}

void Solution::runSolution()
{
	loadData();
	singleCriteriaGaCommonComponents = createSingleCriteriaGaCommonComponents();
	multiCriteriaGaCommonComponents = createMultiCriteriaGaCommonComponents();
	cout << STARTED << endl;
	ifstream file(PARAMS_FILE);
	string line;
	vector<string> lines;
	getline(file, line);

	while (getline(file, line))
	{
		lines.push_back(line);
	}

	int numberOfIterations = lines.size() / batchSize;

	if (lines.size() % batchSize != 0)
	{
		numberOfIterations++;
	}

	for (int i = 0; i < numberOfIterations; i++)
	{
		vector<GeneticAlgoritmSingleCriteria*>* gaSingleCriteria = new vector<GeneticAlgoritmSingleCriteria*>();
		vector<GeneticAlgoritmMultiCriteria*>* gaMultiCriteria = new vector<GeneticAlgoritmMultiCriteria*>();
		vector<thread*>* threads = new vector<thread*>();

		for (int k = i * batchSize; k < lines.size() && k < (i + 1) * batchSize; k++)
		{
			GaInputParameters* gaInputParameters = new GaInputParameters(lines[k]);

			if (gaInputParameters->getRuntimeInputParameters()->getSolutionType() == SolutionTypeEnum::SINGLE_CRITERIA)
			{
				createGaSingleCriteria(gaSingleCriteria, threads, gaInputParameters, k, singleCriteriaGaCommonComponents);
			}
			else if (gaInputParameters->getRuntimeInputParameters()->getSolutionType() == SolutionTypeEnum::NSGAII)
			{
				createGaMultiCriteria(gaMultiCriteria, threads, gaInputParameters, k, multiCriteriaGaCommonComponents);
			}
		}

		for (int k = 0; k < threads->size(); k++)
		{
			threads->at(k)->join();
			delete threads->at(k);
		}

		delete threads;

		for (int k = 0; k < gaSingleCriteria->size(); k++)
		{
			delete gaSingleCriteria->at(k);
		}

		delete gaSingleCriteria;

		for (int k = 0; k < gaMultiCriteria->size(); k++)
		{
			delete gaMultiCriteria->at(k);
		}

		delete gaMultiCriteria;
	}

	clearAfterGa();
	cout << FINISHED << endl;
}

void Solution::loadData()
{
	DataLoader* loader = new DataLoader();
	loader->addStationsToVectorFromFile(STATIONS_FILE);
	cout << STATIONS_FILE << SPACE << LOADED << endl;
	loader->addConnectionsToStationsFormSmallFile(STATIONS_CONNECTIONS_FILE);
	cout << STATIONS_CONNECTIONS_FILE << SPACE << LOADED << endl;
	stationsVector = loader->getStationsVector();
	TravelLoader* tLoader = new TravelLoader();
	travels = tLoader->readTravelsFromFile(*loader, FLOW_FILE);
	cout << FLOW_FILE << SPACE << LOADED << endl;
	TrueParetoFront* trueParetoFrontLoader = new TrueParetoFront();
	trueParetoFront = trueParetoFrontLoader->loadTTpf();
	cout << TPF_OUTPUT_FILE << SPACE << LOADED << endl;
	delete loader;
	delete tLoader;
	delete trueParetoFrontLoader;
}

void Solution::clearAfterGa()
{
	for (int i = 0; i < travels->size(); i++)
	{
		delete travels->at(i);
	}

	for (int i = 0; i < stationsVector->size(); i++)
	{
		delete stationsVector->at(i);
	}

	delete stationsVector;
	delete travels;
	delete singleCriteriaGaCommonComponents;
	delete multiCriteriaGaCommonComponents;
}

void Solution::createGaSingleCriteria(vector<GeneticAlgoritmSingleCriteria*>* gaSingleCriteria, vector<thread*>* threads, GaInputParameters* gaInputParameters, int id, GaCommonComponents<SingleCriteriaValue>* gaCommonComponents)
{
	GeneticAlgoritmSingleCriteria* ga = new GeneticAlgoritmSingleCriteria(id, stationsVector, travels, gaInputParameters, gaCommonComponents);
	gaSingleCriteria->push_back(ga);

	threads->push_back(new thread(&GeneticAlgoritmSingleCriteria::run, ref(*ga)));
}

void Solution::createGaMultiCriteria(vector<GeneticAlgoritmMultiCriteria*>* gaMultiCriteria, vector<thread*>* threads, GaInputParameters* gaInputParameters, int id, GaCommonComponents<MultiCriteriaValue>* gaCommonComponents)
{
	GeneticAlgoritmMultiCriteria* ga = new GeneticAlgoritmMultiCriteria(id, stationsVector, travels, gaInputParameters, gaCommonComponents, trueParetoFront);
	gaMultiCriteria->push_back(ga);

	threads->push_back(new thread(&GeneticAlgoritmMultiCriteria::run, ref(*ga)));
}

GaCommonComponents<SingleCriteriaValue>* Solution::createSingleCriteriaGaCommonComponents()
{
	DijkstraImpl* dijkstraImpl = new DijkstraImpl(stationsVector);
	QuasiShortestPath* quasiShortestPath = new QuasiShortestPath(stationsVector, dijkstraImpl);
	AbstractEvaluationOperator<SingleCriteriaValue>* evaluationOperator = new EvaluationSingleCriteria(travels);
	return new GaCommonComponents<SingleCriteriaValue>(dijkstraImpl, quasiShortestPath, evaluationOperator);
}

GaCommonComponents<MultiCriteriaValue>* Solution::createMultiCriteriaGaCommonComponents()
{
	DijkstraImpl* dijkstraImpl = new DijkstraImpl(stationsVector);
	QuasiShortestPath* quasiShortestPath = new QuasiShortestPath(stationsVector, dijkstraImpl);
	AbstractEvaluationOperator<MultiCriteriaValue>* evaluationOperator = new EvaluationMultiCriteria(travels);
	return new GaCommonComponents<MultiCriteriaValue>(dijkstraImpl, quasiShortestPath, evaluationOperator);
}
