#pragma once
#include <vector>
#include <unordered_map>
#include "Line.h"
#include "AbstractNetwork.h"
#include "DijkstraValue.h"
#include "IRandomNumberGenerator.h"
#include "AbstractMutationOperator.h"
#include "AllMutations.h"
#include "LineMutation.h"
#include "FrequencyMutation.h"
#include "ReverseMutation.h"
#include "AddTravelsMutation.h"
#include "RemoveTravelsMutation.h"
#include "MutationTechniqueEnum.h"
#include "QuasiShortestPath.h"
#include "SingleCriteriaValue.h"
#include "MultiCriteriaValue.h"

template <class T>
class MutationOperators
{
	public:
		MutationOperators(IRandomNumberGenerator* random, IDijkstra* quasiShortestPath, MutationTechniqueEnum mutationTechnique, int probability);
		~MutationOperators();

		void mutation(AbstractNetwork<T>* network);

	private:
		std::unordered_map<MutationTechniqueEnum, AbstractMutationOperator*> mutationOperators;
		MutationTechniqueEnum mutationTechnique;
		int probability;
		IRandomNumberGenerator* random;

		std::pair<MutationTechniqueEnum, AbstractMutationOperator*> getMapValue(AbstractMutationOperator* mutationOperator);
};
