#pragma once
#include "AbstractEvaluationOperator.h"
#include "SingleCriteriaValue.h"
#include "SingleCriteriaNetwork.h"
#include "NetworkEvaluationSingleCriteria.h"

class EvaluationSingleCriteria: public AbstractEvaluationOperator<SingleCriteriaValue>
{
	public:
		EvaluationSingleCriteria(std::vector<Travel*>* travels);
		~EvaluationSingleCriteria();

	protected:
		NetworkEvaluation<SingleCriteriaValue>* getNetworkEvaluation(std::list<Travel*>* travels, AbstractNetwork<SingleCriteriaValue>* network) override;
		void setValues(AbstractNetwork<SingleCriteriaValue>& network, SingleCriteriaValue& value) override;
};
