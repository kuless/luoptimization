#pragma once
#include <unordered_map>
#include "AbstractNetwork.h"
#include "SingleCriteriaValue.h"
#include "MultiCriteriaValue.h"

template <class T>
class EvaluationCache
{
	public:
		EvaluationCache();
		~EvaluationCache();

		std::pair<bool, T*> getValue(AbstractNetwork<T>& network);
		void addToCache(AbstractNetwork<T>* network, T* value);

	private:
		std::unordered_map<AbstractNetwork<T>, T*, NetworkHash<T>, NetworkEqual<T>>* cache;
};