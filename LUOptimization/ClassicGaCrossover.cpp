#include "ClassicGaCrossover.h"

using namespace std;

template <class T>
ClassicGaCrossover<T>::ClassicGaCrossover(IRandomNumberGenerator* random): AbstractCrossoverOperator<T>(random)
{
}

template <class T>
ClassicGaCrossover<T>::~ClassicGaCrossover()
{
}

template <class T>
pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* ClassicGaCrossover<T>::crossover(pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* pairToCrossover)
{
	pair<vector<Line*>*, vector<Line*>*> lines = pair<vector<Line*>*, vector<Line*>*>(new vector<Line*>(), new vector<Line*>());
	int randSize = this->getRandSize(pairToCrossover);

	//TODO: implement crossover method
	throw NotImplementedException("crossover not implemented");

	return new pair<AbstractNetwork<T>*, AbstractNetwork<T>*>(pairToCrossover->first->callChildConstructor(lines.first), pairToCrossover->first->callChildConstructor(lines.second));
}

template <class T>
CrossoverTechniqueEnum ClassicGaCrossover<T>::getType()
{
	return CrossoverTechniqueEnum::CLASSIC_GA;
}

template class ClassicGaCrossover<SingleCriteriaValue>;
template class ClassicGaCrossover<MultiCriteriaValue>;
