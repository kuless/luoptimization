#include "Connection.h"

Connection::Connection()
{
	journeyTime = 0;
	distance = 0.0;
	station = nullptr;
}

Connection::Connection(int journeyTime, double distance, Station* station)
{
	this->journeyTime = journeyTime;
	this->distance = distance;
	this->station = station;
}

Connection::~Connection()
{
}

int Connection::getJourneyTime()
{
	return journeyTime;
}

double Connection::getDistance()
{
	return distance;
}

Station* Connection::getStation()
{
	return station;
}

void Connection::setJourneyTime(int journeyTime)
{
	this->journeyTime = journeyTime;
}

void Connection::setDistance(double distance)
{
	this->distance = distance;
}

void Connection::setStation(Station* station)
{
	this->station = station;
}

size_t Connection::getHash() const
{
	size_t hash = station->getHash();
	boost::hash_combine(hash, journeyTime);
	boost::hash_combine(hash, distance);
	return hash;
}
