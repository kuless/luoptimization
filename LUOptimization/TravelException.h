#pragma once
#include <exception>

class TravelException : public std::exception
{
    public:
        explicit TravelException(const char* message) : std::exception(message) {};
        explicit TravelException(const std::string& message) : std::exception(message.c_str()) {};

};

