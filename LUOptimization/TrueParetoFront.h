#pragma once
#include <fstream>
#include <iostream>
#include <vector>
#include <boost/stacktrace.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string.hpp>
#include "Nsgaii.h"
#include "MultiCriteriaNetwork.h"
#include "MultiCriteriaValue.h"

class TrueParetoFront
{
	public:
		TrueParetoFront();
		~TrueParetoFront();

		void createTpf();
		std::vector<MultiCriteriaValue*>* loadTTpf();

	private:
};

