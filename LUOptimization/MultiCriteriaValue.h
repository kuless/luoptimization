#pragma once
#include "Constants.h"
#include <vector>

class MultiCriteriaValue
{
	public:
		MultiCriteriaValue();
		MultiCriteriaValue(const MultiCriteriaValue& multiCriteriaValue);
		MultiCriteriaValue(unsigned long long int passengersValue, unsigned long long int maintenanceValue);
		~MultiCriteriaValue();

		bool operator>(const MultiCriteriaValue& multiCriteriaValue) const;
		bool operator<(const MultiCriteriaValue& multiCriteriaValue) const;
		bool _Equal(MultiCriteriaValue& multiCriteriaValue);
		bool _EqualRawValue(MultiCriteriaValue& multiCriteriaValue);
		bool doesItDominate(MultiCriteriaValue& multiCriteriaValue);
		MultiCriteriaValue* clone();

		unsigned long long int getPassengersValue();
		unsigned long long int getMaintenanceValue();
		int getParetoFront();
		unsigned long long int getDistance();

		void setPassengersValue(unsigned long long int passengersValue);
		void setMaintenanceValue(unsigned long long int maintenanceValue);
		void setParetoFront(int paretoFront);
		void setDistance(unsigned long long int distance);

		void addPassengersValue(unsigned long long int passengersValue);
		void addMaintenanceValue(unsigned long long int maintenanceValue);
		void addDistance(unsigned long long int distance);

	private:
		unsigned long long int passengersValue;
		unsigned long long int maintenanceValue;
		int paretoFront;
		unsigned long long int distance;
};
