#include "EvaluationFailProtocol.h"


using namespace std;

template<class T>
EvaluationFailProtocol<T>::EvaluationFailProtocol(AbstractNetwork<T>* network, NetworkState* state): NetworkWriter()
{
	boost::uuids::random_generator generator;
	string uuid = boost::uuids::to_string(generator());
	cout << FAIL_PROTOCOL << uuid << endl;
	this->rootDirectory = FAIL_PROTOCOL_DIRECTORY;
	boost::filesystem::create_directory(this->rootDirectory);
	this->rootDirectory = this->rootDirectory + SLASH + uuid;
	boost::filesystem::create_directory(this->rootDirectory);
	this->fileWriter.open(this->rootDirectory + SLASH + FAIL_PROTOCOL_FILE);
	this->network = network;
	this->state = state;
}

template<class T>
EvaluationFailProtocol<T>::~EvaluationFailProtocol()
{
	this->fileWriter.close();
}

template<class T>
void EvaluationFailProtocol<T>::saveProtocol()
{
	this->fileWriter << PROTOCOL_REMAINING_TRAINS << this->state->getNumberOfTrainsOnTrack() << endl << endl;

	for (unordered_map<int, list<Train*>>::iterator itTrainsOnLine = this->state->getTrainsOnTrack()->begin(); itTrainsOnLine != this->state->getTrainsOnTrack()->end(); ++itTrainsOnLine)
	{
		for (list<Train*>::iterator itTrain = itTrainsOnLine->second.begin(); itTrain != itTrainsOnLine->second.end(); ++itTrain)
		{
			this->fileWriter << (*itTrain)->getTotalPassengers() << COMMA << (*itTrain)->getState() << COMMA << (*itTrain)->getCurrentTravelTime()
				<< COMMA << (*itTrain)->isForwardMovment() << COMMA << (*itTrain)->getLine()->getId() << COMMA << (*itTrain)->getCurrentStation()->getNLCCode() << endl;
		}
	}

	this->saveLineVectorToFile(*network->getLines(), this->rootDirectory + SLASH + LINES_FILE_NAME + CSV_FILE);
	this->saveTimetableToFile(*network->getLines(), this->rootDirectory + SLASH + TIMETABLE_FILE_NAME + CSV_FILE);
}

template class EvaluationFailProtocol<SingleCriteriaValue>;
template class EvaluationFailProtocol<MultiCriteriaValue>;
