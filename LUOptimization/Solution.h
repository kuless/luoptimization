#pragma once
#include <boost/stacktrace.hpp>
#include "DataLoader.h"
#include "TravelLoader.h"
#include "PopulationGenerator.h"
#include <iostream>
#include <unordered_map>
#include "Station.h"
#include "GeneticAlgoritmSingleCriteria.h"
#include "GeneticAlgoritmMultiCriteria.h"
#include "SingleCriteriaNetwork.h"
#include "SingleCriteriaValue.h"
#include "MultiCriteriaNetwork.h"
#include "MultiCriteriaValue.h"
#include "GaCommonComponents.h"
#include "TrueParetoFront.h"
#include <iterator>
#include <algorithm>
#include <thread>

class Solution
{
	public:
		Solution();
		~Solution();

		void start();

	private:
		std::vector<Station*>* stationsVector;
		std::vector<Travel*>* travels;
		GaCommonComponents<SingleCriteriaValue>* singleCriteriaGaCommonComponents;
		GaCommonComponents<MultiCriteriaValue>* multiCriteriaGaCommonComponents;
		std::vector<MultiCriteriaValue*>* trueParetoFront;
		int batchSize;

		void createTrueParetoFront();
		void runSolution();
		void loadData();
		void clearAfterGa();
		void createGaSingleCriteria(std::vector<GeneticAlgoritmSingleCriteria*>* gaSingleCriteria, std::vector<std::thread*>* threads, GaInputParameters* gaInputParameters, int id, GaCommonComponents<SingleCriteriaValue>* gaCommonComponents);
		void createGaMultiCriteria(std::vector<GeneticAlgoritmMultiCriteria*>* gaMultiCriteria, std::vector<std::thread*>* threads, GaInputParameters* gaInputParameters, int id, GaCommonComponents<MultiCriteriaValue>* gaCommonComponents);
		GaCommonComponents<SingleCriteriaValue>* createSingleCriteriaGaCommonComponents();
		GaCommonComponents<MultiCriteriaValue>* createMultiCriteriaGaCommonComponents();
};
