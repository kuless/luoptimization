#pragma once
#include <list>
#include "Route.h"

class RouteCacheValue
{
public:
	RouteCacheValue(std::list<Route*>* route, int travelTime);
	~RouteCacheValue();

	std::list<Route*>* getRoute();
	int getTravelTime();

	void setRoute(std::list<Route*>* route);
	void setTravelTime(int travelTime);

private:
	std::list<Route*>* route;
	int travelTime;
};
