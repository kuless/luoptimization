#include "MutationOperators.h"

using namespace std;

template <class T>
MutationOperators<T>::MutationOperators(IRandomNumberGenerator* random, IDijkstra* quasiShortestPath, MutationTechniqueEnum mutationTechnique, int probability)
{
	this->random = random;
	this->mutationTechnique = mutationTechnique;
	this->probability = probability;
	vector<AbstractMutationOperator*>* mutationOperatorsForAllClass = new vector<AbstractMutationOperator*>();
	mutationOperatorsForAllClass->push_back(new LineMutation(random->copy(), quasiShortestPath));
	mutationOperatorsForAllClass->push_back(new FrequencyMutation(random->copy()));
	mutationOperatorsForAllClass->push_back(new ReverseMutation(random->copy()));
	mutationOperatorsForAllClass->push_back(new AddTravelsMutation(random->copy()));
	mutationOperatorsForAllClass->push_back(new RemoveTravelsMutation(random->copy()));

	mutationOperators.insert(pair<MutationTechniqueEnum, AbstractMutationOperator*>(getMapValue(new AllMutations(random->copy(), mutationOperatorsForAllClass))));
	mutationOperators.insert(pair<MutationTechniqueEnum, AbstractMutationOperator*>(getMapValue(new LineMutation(random->copy(), quasiShortestPath))));
	mutationOperators.insert(pair<MutationTechniqueEnum, AbstractMutationOperator*>(getMapValue(new FrequencyMutation(random->copy()))));
	mutationOperators.insert(pair<MutationTechniqueEnum, AbstractMutationOperator*>(getMapValue(new ReverseMutation(random->copy()))));
	mutationOperators.insert(pair<MutationTechniqueEnum, AbstractMutationOperator*>(getMapValue(new AddTravelsMutation(random->copy()))));
	mutationOperators.insert(pair<MutationTechniqueEnum, AbstractMutationOperator*>(getMapValue(new RemoveTravelsMutation(random->copy()))));
}

template <class T>
MutationOperators<T>::~MutationOperators()
{
	delete random;

	for (unordered_map<MutationTechniqueEnum, AbstractMutationOperator*>::iterator it = mutationOperators.begin(); it != mutationOperators.end(); ++it)
	{
		delete it->second;
	}
}

template <class T>
void MutationOperators<T>::mutation(AbstractNetwork<T>* network)
{
	if (random->getInt(0, CERTAIN_EVENT) < probability)
	{
		Line* lineToMutation = (*network->getLines())[random->getInt(0, (int) network->getLines()->size())];
		mutationOperators.find(mutationTechnique)->second->mutation(lineToMutation);
	}
}

template <class T>
pair<MutationTechniqueEnum, AbstractMutationOperator*> MutationOperators<T>::getMapValue(AbstractMutationOperator* mutationOperator)
{
	return pair<MutationTechniqueEnum, AbstractMutationOperator*>(mutationOperator->getType(), mutationOperator);
}

template class MutationOperators<SingleCriteriaValue>;
template class MutationOperators<MultiCriteriaValue>;
