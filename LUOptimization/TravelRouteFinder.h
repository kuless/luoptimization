#pragma once
#include <vector>
#include <list>
#include <unordered_map>
#include "Station.h"
#include "Line.h"
#include "Route.h"
#include "AbstractNetwork.h"
#include "Travel.h"
#include "RouteCacheValue.h"
#include "DijkstraValueRoute.h"
#include "SingleCriteriaValue.h"
#include "MultiCriteriaValue.h"

template <class T>
class TravelRouteFinder
{
	public:
		TravelRouteFinder(AbstractNetwork<T>* network);
		~TravelRouteFinder();

		void setTravelsRoute(std::list<Travel*>* travels);

	private:
		AbstractNetwork<T>* network;
		std::unordered_map<int, RouteCacheValue*>* cache;

		struct PairHash {
			size_t operator () (const std::pair<Station*, Line*>& p) const {
				size_t hash = 0;
				boost::hash_combine(hash, p.first);
				boost::hash_combine(hash, p.second);
				return hash;
			}
		};

		std::list<Route*>* dijkstra(Station* start, Station* end);
		std::vector<DijkstraValueRoute*>* initDijkstraValueRoutes(Station* start);
		DijkstraValueRoute* getNextNode(std::vector<DijkstraValueRoute*>* values, std::unordered_map<std::pair<Station*, Line*>, DijkstraValueRoute*, PairHash>* visitedNodes);
		std::vector<std::pair<Station*, bool>>* getNeighboringStationsOnLine(DijkstraValueRoute* node);
		void handleNeighbors(Station* startStation, DijkstraValueRoute* node, std::vector<DijkstraValueRoute*>* values, std::vector<std::pair<Station*, bool>>* neighbors);
		int getDistance(std::pair<Station*, bool> neighbor, DijkstraValueRoute* node);
		void setNewValueIfPossible(DijkstraValueRoute* value, DijkstraValueRoute* node, int nodeNewValue, bool neighborMovement);
		bool isTransferPossible(DijkstraValueRoute* value, DijkstraValueRoute* node, Station* startStation);
		void transferToDifferentLine(DijkstraValueRoute* value, DijkstraValueRoute* node);
		void saveResult(Station* startStation, DijkstraValueRoute* node, std::unordered_map<std::pair<Station*, Line*>, DijkstraValueRoute*, PairHash>* visitedNodes);
		void deleteRoute(std::list<Route*>* route);
		std::list<Route*>* searchForResult(int key);
		std::list<Route*>* copyRoutes(std::list<Route*>* routes);
};
