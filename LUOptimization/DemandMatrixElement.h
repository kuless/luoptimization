#pragma once
#include "Station.h"

class DemandMatrixElement
{
	public:
		DemandMatrixElement(Station* startStation, Station* endStation, int passengers);
		~DemandMatrixElement();

		void addPassengers(int newPassengers);
		Station* getStartStation();
		Station* getEndStation();
		int getPassengers();

	private:
		Station* startStation;
		Station* endStation;
		int passengers;
};
