#pragma once
#include <string>

enum class SolutionTypeEnum
{
	SINGLE_CRITERIA, NSGAII
};

class SolutionType
{
	public:
		static const SolutionTypeEnum getValue(std::string value);
};
