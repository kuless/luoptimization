#pragma once
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <sstream>
#include <array>
#include <mutex>
#include "IDijkstra.h"
#include "DijkstraValue.h"
#include "Constants.h"

class DijkstraImpl: public IDijkstra
{
	public:
		DijkstraImpl(std::vector<Station*>* stations);
		~DijkstraImpl();
		std::vector<Station*>* dijkstra(Station* start, Station* end);

	private:
		std::unordered_map<std::string, std::vector<Station*>*>* cache;
		std::vector<Station*>* stations;
		std::mutex* mutexObj;

		std::vector<Station*>* createShortesPath(Station* start, std::string key);
		std::vector<DijkstraValue>* initValues(Station* start);
		void updateNodes(std::vector<DijkstraValue>* values, DijkstraValue& node);
		void saveShortesPath(int nlcCode, DijkstraValue& node, std::unordered_map<Station*, Station*>& visitedNodes);
		std::vector<Station*>* copy(std::vector<Station*>* element);
};
