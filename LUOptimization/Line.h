#pragma once
#include <iostream>
#include <unordered_set>
#include <unordered_map>
#include "Station.h"
#include "Constants.h"

class Line
{
	public:
		Line();
		Line(Line& line);
		Line(int id, std::string name, std::vector<Station*>* stops);
		Line(int id, std::string name, std::vector<Station*>* stops, std::vector<std::pair<int, int>>& timetable);
		Line(int id, std::string name, std::vector<Station*>* stops, std::vector<int>& timetable);
		Line(int id, std::string name, std::vector<Station*>* stops, std::vector<int>& journeyTime, std::vector<std::pair<int, int>>& timetable);
		~Line();

		bool _Equal(Line& line);
		bool _EqualStopsAndTimetable(Line& line);

		void addStop(Station* station, int journeyTime);
		void deleteLastStop();
		bool cointainsStop(Station* station);
		bool containsDuplicates();
		std::pair<int, int> getBeginAndEndOfDuplicates();
		Station* getNextStation(Station* station);
		std::vector<Station*>* getJunctionStations();
		void updateJourneyTimeAndLenght();
		void removeTimetableElement(int element);

		int getId();
		std::string getName();
		std::vector<Station*> getStops();
		std::vector<int> getJourneyTime();
		std::vector<std::pair<int, int>> getTimetable();
		std::vector<int> getTimetableInMinutes();
		double getLenght();

		void setId(int id);
		void setStops(std::vector<Station*>* stops);
		void setJourneyTime(std::vector<int> journeyTime);
		void setTimetable(std::vector<std::pair<int, int>>& timetable);
		void setTimetable(std::vector<int>& timetable);

	private:
		int id;
		std::string name;
		std::vector<Station*>* stops;
		std::vector<int> journeyTime;
		std::vector<int> timetable;
		double lenght;

		std::vector<std::pair<int, int>> convertTimetable(std::vector<int> timetable);
		std::vector<int> convertTimetable(std::vector<std::pair<int, int>> timetable);
};