#include "AbstractPopulationGenerator.h"

using namespace std;

template<class T>
AbstractPopulationGenerator<T>::AbstractPopulationGenerator(IRandomNumberGenerator* random, AbstractNetworkBuilder<T>* builder)
{
	this->builder = builder;
	this->random = random;
}

template<class T>
AbstractPopulationGenerator<T>::~AbstractPopulationGenerator()
{
	delete builder;
	delete random;
}

template<class T>
vector<AbstractNetwork<T>*>* AbstractPopulationGenerator<T>::generatePopulation(vector<Station*>& stations, vector<Travel*>& travels, PopulationGeneratorInputParameters* populationGeneratorInputParameters)
{
	throw NotImplementedException("generatePopulation not implemented");
}

template<class T>
PopulationGenerationTechniqueEnum AbstractPopulationGenerator<T>::getType()
{
	throw NotImplementedException("getType not implemented");
}

template<class T>
void AbstractPopulationGenerator<T>::generateTimetableForNewLine(Line* line)
{
	vector<int> timetable = vector<int>();
	set<int> tempTimetable = set<int>();

	int numberOfJourneys = this->random->getInt(0, DAY_IN_MINUTES);

	for (int n = 0; n < numberOfJourneys; n++)
	{
		int hour = -1;
		int timeIndicator = this->random->getInt(0, WHOLE_SPECTRUM);

		if (timeIndicator < HOUR_0)
		{
			hour = 0;
		}
		else if (timeIndicator >= HOUR_0 && timeIndicator < HOUR_1)
		{
			hour = 1;
		}
		else if (timeIndicator >= HOUR_1 && timeIndicator < HOUR_2)
		{
			hour = 2;
		}
		else if (timeIndicator >= HOUR_2 && timeIndicator < HOUR_3)
		{
			hour = 3;
		}
		else if (timeIndicator >= HOUR_3 && timeIndicator < HOUR_4)
		{
			hour = 4;
		}
		else if (timeIndicator >= HOUR_4 && timeIndicator < HOUR_5)
		{
			hour = 5;
		}
		else if (timeIndicator >= HOUR_5 && timeIndicator < HOUR_6)
		{
			hour = 6;
		}
		else if (timeIndicator >= HOUR_6 && timeIndicator < HOUR_7)
		{
			hour = 7;
		}
		else if (timeIndicator >= HOUR_7 && timeIndicator < HOUR_8)
		{
			hour = 8;
		}
		else if (timeIndicator >= HOUR_8 && timeIndicator < HOUR_9)
		{
			hour = 9;
		}
		else if (timeIndicator >= HOUR_9 && timeIndicator < HOUR_10)
		{
			hour = 10;
		}
		else if (timeIndicator >= HOUR_10 && timeIndicator < HOUR_11)
		{
			hour = 11;
		}
		else if (timeIndicator >= HOUR_11 && timeIndicator < HOUR_12)
		{
			hour = 12;
		}
		else if (timeIndicator >= HOUR_12 && timeIndicator < HOUR_13)
		{
			hour = 13;
		}
		else if (timeIndicator >= HOUR_13 && timeIndicator < HOUR_14)
		{
			hour = 14;
		}
		else if (timeIndicator >= HOUR_14 && timeIndicator < HOUR_15)
		{
			hour = 15;
		}
		else if (timeIndicator >= HOUR_15 && timeIndicator < HOUR_16)
		{
			hour = 16;
		}
		else if (timeIndicator >= HOUR_16 && timeIndicator < HOUR_17)
		{
			hour = 17;
		}
		else if (timeIndicator >= HOUR_17 && timeIndicator < HOUR_18)
		{
			hour = 18;
		}
		else if (timeIndicator >= HOUR_18 && timeIndicator < HOUR_19)
		{
			hour = 19;
		}
		else if (timeIndicator >= HOUR_19 && timeIndicator < HOUR_20)
		{
			hour = 20;
		}
		else if (timeIndicator >= HOUR_20 && timeIndicator < HOUR_21)
		{
			hour = 21;
		}
		else if (timeIndicator >= HOUR_21 && timeIndicator < HOUR_22)
		{
			hour = 22;
		}
		else if (timeIndicator >= HOUR_22 && timeIndicator < HOUR_23)
		{
			hour = 23;
		}

		tempTimetable.insert(hour * HOUR_IN_MINUTES + this->random->getInt(0, HOUR_IN_MINUTES));
	}

	for (set<int>::iterator it = tempTimetable.begin(); it != tempTimetable.end(); ++it)
	{
		timetable.push_back(*it);
	}

	sort(timetable.begin(), timetable.end());
	line->setTimetable(timetable);
}

template class AbstractPopulationGenerator<SingleCriteriaValue>;
template class AbstractPopulationGenerator<MultiCriteriaValue>;
