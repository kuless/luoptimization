#pragma once
#include <string>

enum class CrossoverTechniqueEnum
{
	ALL_LINES, CLASSIC_GA, CLASSIC_GA_WITH_SHORTEST_PATH, WHOLE_CHROMOSOME
};

class CrossoverTechnique
{
	public:
		static const CrossoverTechniqueEnum getValue(std::string value);
};
