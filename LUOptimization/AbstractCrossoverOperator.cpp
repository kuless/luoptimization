#include "AbstractCrossoverOperator.h"

using namespace std;

template <class T>
AbstractCrossoverOperator<T>::AbstractCrossoverOperator(IRandomNumberGenerator* random)
{
	this->random = random;
}

template <class T>
AbstractCrossoverOperator<T>::~AbstractCrossoverOperator()
{
	delete random;
}

template <class T>
pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* AbstractCrossoverOperator<T>::crossover(pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* pairToCrossover)
{
	throw NotImplementedException("crossover not implemented");
}

template <class T>
CrossoverTechniqueEnum AbstractCrossoverOperator<T>::getType()
{
	throw NotImplementedException("getType not implemented");
}

template <class T>
pair<vector<pair<int, int>>, vector<pair<int, int>>>* AbstractCrossoverOperator<T>::crossoverTimetable(vector<pair<int, int>> firstLineTimetable, vector<pair<int, int>> secondLineTimetable)
{
	int randSize = 0;

	if (firstLineTimetable.size() < secondLineTimetable.size())
	{
		randSize = firstLineTimetable.size();
	}
	else
	{
		randSize = secondLineTimetable.size();
	}

	int crossoverPoint = random->getInt(0, randSize);
	vector<pair<int, int>> firstLineNewTimetable;
	vector<pair<int, int>> secondLineNewTimetable;

	for (int i = 0; i < firstLineTimetable.size() || i < secondLineTimetable.size(); i++)
	{
		if (i < crossoverPoint)
		{
			firstLineNewTimetable.push_back(pair<int, int>(firstLineTimetable[i].first, firstLineTimetable[i].second));
			secondLineNewTimetable.push_back(pair<int, int>(secondLineTimetable[i].first, secondLineTimetable[i].second));
		}
		else
		{
			if (i < secondLineTimetable.size())
			{
				firstLineNewTimetable.push_back(pair<int, int>(secondLineTimetable[i].first, secondLineTimetable[i].second));
			}

			if (i < firstLineTimetable.size())
			{
				secondLineNewTimetable.push_back(pair<int, int>(firstLineTimetable[i].first, firstLineTimetable[i].second));
			}
		}
	}

	return new pair<vector<pair<int, int>>, vector<pair<int, int>>>(firstLineNewTimetable, secondLineNewTimetable);
}

template<class T>
int AbstractCrossoverOperator<T>::getMinRandSize(std::pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* pairToCrossover)
{
	int firstNetworkLines = pairToCrossover->first->getLines()->size();
	int secondNetworkLines = pairToCrossover->second->getLines()->size();
	return firstNetworkLines < secondNetworkLines ? firstNetworkLines : secondNetworkLines;
}

template <class T>
int AbstractCrossoverOperator<T>::getRandSize(pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* pairToCrossover)
{
	int firstNetworkLines = pairToCrossover->first->getLines()->size();
	int secondNetworkLines = pairToCrossover->second->getLines()->size();
	int additionalLines = random->getInt(0, abs(firstNetworkLines - secondNetworkLines) + 1);
	return firstNetworkLines < secondNetworkLines ? firstNetworkLines + additionalLines : secondNetworkLines + additionalLines;
}

template class AbstractCrossoverOperator<SingleCriteriaValue>;
template class AbstractCrossoverOperator<MultiCriteriaValue>;
