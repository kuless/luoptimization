#pragma once
#include "Station.h"

class DijkstraValue
{
	public:
		DijkstraValue(int value, Station* station, Station* previosStation);
		~DijkstraValue();

		bool operator<(const DijkstraValue& dijkstraValue) const;
		bool operator>(const DijkstraValue& dijkstraValue) const;
		bool operator<(const DijkstraValue* dijkstraValue) const;
		bool operator>(const DijkstraValue* dijkstraValue) const;

		int getValue();
		Station* getStation();
		Station* getPreviosStation();

		void setValue(int value);
		void setStation(Station* station);
		void setPreviosStation(Station* station);

	private:
		int value;
		Station* station;
		Station* previosStation;
};
