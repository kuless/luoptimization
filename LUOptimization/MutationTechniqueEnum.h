#pragma once
#include <string>

enum class MutationTechniqueEnum
{
	ALL, ADD_TRAVELS, FREQUENCY, LINE, REMOVE_TRAVELS, REVERSE
};

class MutationTechnique
{
	public:
		static const MutationTechniqueEnum getValue(std::string value);
};