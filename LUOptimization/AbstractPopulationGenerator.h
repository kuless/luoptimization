#pragma once
#include <set>
#include "IRandomNumberGenerator.h"
#include "Travel.h"
#include "IDijkstra.h"
#include "AbstractNetworkBuilder.h"
#include "AbstractNetwork.h"
#include "SingleCriteriaValue.h"
#include "MultiCriteriaValue.h"
#include "PopulationGeneratorInputParameters.h"
#include "PopulationGenerationTechniqueEnum.h"
#include "NotImplementedException.h"

template <class T>
class AbstractPopulationGenerator
{
	public:
		AbstractPopulationGenerator(IRandomNumberGenerator* random, AbstractNetworkBuilder<T>* builder);
		virtual ~AbstractPopulationGenerator();

		virtual std::vector<AbstractNetwork<T>*>* generatePopulation(std::vector<Station*>& stations, std::vector<Travel*>& travels, PopulationGeneratorInputParameters* populationGeneratorInputParameters);
		virtual PopulationGenerationTechniqueEnum getType();

	protected:
		IRandomNumberGenerator* random;
		AbstractNetworkBuilder<T>* builder;

		void generateTimetableForNewLine(Line* line);
};
