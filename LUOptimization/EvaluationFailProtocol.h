#pragma once
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/filesystem.hpp>
#include <iostream>
#include <fstream>
#include <list>
#include <unordered_map>
#include "Constants.h"
#include "AbstractNetwork.h"
#include "NetworkState.h"
#include "NetworkWriter.h"
#include "Train.h"

template <class T>
class EvaluationFailProtocol: public NetworkWriter
{
	public:
		EvaluationFailProtocol(AbstractNetwork<T>* network, NetworkState* state);
		~EvaluationFailProtocol();
		
		void saveProtocol();

	private:
		AbstractNetwork<T>* network;
		NetworkState* state;
		std::ofstream fileWriter;
		std::string rootDirectory;
};
