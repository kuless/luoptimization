#pragma once
#include "AbstractNetwork.h"
#include "IRandomNumberGenerator.h"
#include "NotImplementedException.h"
#include "CrossoverTechniqueEnum.h"
#include "SingleCriteriaValue.h"
#include "MultiCriteriaValue.h"

template <class T>
class AbstractCrossoverOperator
{
	public:
		AbstractCrossoverOperator(IRandomNumberGenerator* random);
		virtual ~AbstractCrossoverOperator();
		virtual std::pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* crossover(std::pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* pairToCrossover);
		virtual CrossoverTechniqueEnum getType();

	protected:
		IRandomNumberGenerator* random;

		std::pair<std::vector<std::pair<int, int>>, std::vector<std::pair<int, int>>>* crossoverTimetable(
			std::vector<std::pair<int, int>> firstLineTimetable, std::vector<std::pair<int, int>> secondLineTimetable
		);
		int getMinRandSize(std::pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* pairToCrossover);
		int getRandSize(std::pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* pairToCrossover);
};
