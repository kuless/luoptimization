#include "ResultWriterMultiCriteria.h"

using namespace std;

ResultWriterMultiCriteria::ResultWriterMultiCriteria(int id, string fileName, vector<MultiCriteriaValue*>* trueParetoFornt, vector<QualityEnum> qualityTypes, bool saveValuesForTpf): ResultsWriter<MultiCriteriaValue>(id, fileName)
{
	this->qualityOperator = new QualityOperator(trueParetoFornt);
	this->qualityTypes = qualityTypes;
	this->saveValuesForTpf = saveValuesForTpf;

	if (saveValuesForTpf)
	{
		this->tpfFileWriter.open(this->rootDirectory + this->fileName + TPF_FILE_NAME + FILE_EXTENSION);
		this->tpfFileWriter << TPF_FILE_HEADER << endl;
	}

	saveTitlesToFile();
}

ResultWriterMultiCriteria::~ResultWriterMultiCriteria()
{
	delete this->qualityOperator;

	if (this->saveValuesForTpf)
	{
		this->tpfFileWriter.close();
	}
}

void ResultWriterMultiCriteria::saveResultsToFile(vector<AbstractNetwork<MultiCriteriaValue>*>* population, int generation, int numberOfGeneration)
{
	string quality = this->qualityOperator->getQualites(population, this->qualityTypes);
	this->fileWriter << generation << SEPERATOR << quality << endl;
	cout << GA_ID << id << GENERATION << generation << SLASH << numberOfGeneration << SEPERATOR << quality << endl;
	
	if (this->saveValuesForTpf)
	{
		saveParetoFrontValues(population);
	}
}

void ResultWriterMultiCriteria::saveBestResultToFile(vector<AbstractNetwork<MultiCriteriaValue>*>* population)
{
	int resultId = FIRST_RESULT_ID;
	boost::filesystem::create_directory(this->rootDirectory + QUALITY_FOLDER);

	for (AbstractNetwork<MultiCriteriaValue>* network : *population)
	{
		if (network->getValue()->getParetoFront() == FIRST_PARETO_FRONT)
		{
			string networkFolder = this->rootDirectory + QUALITY_FOLDER + to_string(resultId);
			boost::filesystem::create_directory(networkFolder);
			saveLineVectorToFile(*network->getLines(), networkFolder + SLASH + this->fileName + LINES_FILE_NAME + FILE_EXTENSION);
			saveTimetableToFile(*network->getLines(), networkFolder + SLASH + this->fileName + TIMETABLE_FILE_NAME + FILE_EXTENSION);
			resultId++;
		}
	}

	resultId = FIRST_RESULT_ID;
	boost::filesystem::create_directory(this->rootDirectory + ELITIST_ARCHIVE_FOLDER);

	for (AbstractNetwork<MultiCriteriaValue>* network : *this->qualityOperator->getArchive())
	{
		string networkFolder = this->rootDirectory + ELITIST_ARCHIVE_FOLDER + to_string(resultId);
		boost::filesystem::create_directory(networkFolder);
		saveLineVectorToFile(*network->getLines(), networkFolder + SLASH + this->fileName + LINES_FILE_NAME + FILE_EXTENSION);
		saveTimetableToFile(*network->getLines(), networkFolder + SLASH + this->fileName + TIMETABLE_FILE_NAME + FILE_EXTENSION);
		resultId++;
	}
}

void ResultWriterMultiCriteria::saveTitlesToFile()
{
	this->fileWriter << RESULTS_FILE_HEADER_MULTI_CRITERIA << endl;
}

void ResultWriterMultiCriteria::saveParetoFrontValues(vector<AbstractNetwork<MultiCriteriaValue>*>* population)
{
	for (int i = 0; i < population->size(); i++)
	{
		this->tpfFileWriter << population->at(i)->getValue()->getPassengersValue() << SEPERATOR << population->at(i)->getValue()->getMaintenanceValue() << endl;
	}
}
