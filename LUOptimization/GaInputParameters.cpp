#include "GaInputParameters.h"

using namespace std;

GaInputParameters::GaInputParameters(string line)
{
	vector<string> lineSplited;
	boost::split(lineSplited, line, boost::algorithm::is_any_of(SEPERATOR));

	initInputParameters = new InitInputParameters();
	runtimeInputParameters = new RuntimeInputParameters();

	initInputParameters->setOutputFile(lineSplited[PARAMS_OUTPUT_FILE]);
	runtimeInputParameters->getPopulationGeneratorInputParameters()->setPopulationSize(stoi(lineSplited[PARAMS_POPULATION_SIZE]));
	initInputParameters->setCrossoverProbability(stoi(lineSplited[PARAMS_CROSSOVER_PROBABILITY]));
	initInputParameters->setMutationProbability(stoi(lineSplited[PARAMS_MUTATION_PROBABILITY]));
	runtimeInputParameters->setNumberOfGeneration(stoi(lineSplited[PARAMS_NUMBER_OF_GENERATIONS]));
	initInputParameters->setSelectionsSize(stoi(lineSplited[PARAMS_SELETION_SIZE]));
	initInputParameters->setRouletteSelection(lineSplited[PARAMS_ROULETTE_SELECTION]._Equal(TRUE_AS_TEXT));
	initInputParameters->setMutationTechnique(lineSplited[PARAMS_MUTATION_TECHNIQUE]);
	initInputParameters->setCrossoverTechnique(lineSplited[PARAMS_CROSSOVER_TECHNIQUE]);
	initInputParameters->setQualityTypes(lineSplited[PARAMS_QUALITY]);
	initInputParameters->setSaveValuesForTpf(lineSplited[PARAMS_SAVE_VALUES_FOR_TPF]._Equal(TRUE_AS_TEXT));
	runtimeInputParameters->getPopulationGeneratorInputParameters()->setNumberOfLines(stoi(lineSplited[PARAMS_NUMBER_OF_LINES]));
	runtimeInputParameters->setNumberOfThreads(stoi(lineSplited[PARAMS_NUMBER_OF_THREADS]));
	runtimeInputParameters->setSolutionType(lineSplited[PARAMS_SOLUTION_TYPE]);
	runtimeInputParameters->getPopulationGeneratorInputParameters()->setPopulationGenerationTechnique(lineSplited[PARAMS_POPULATION_GENERATOR_TECHNIQUE]);

	if (runtimeInputParameters->getPopulationGeneratorInputParameters()->getPopulationGenerationTechnique() != PopulationGenerationTechniqueEnum::RANDOM)
	{
		runtimeInputParameters->getPopulationGeneratorInputParameters()->setPercentOfCoveredStations(stoi(lineSplited[PARAMS_PERCENT_OF_COVERED_STATIONS]));
	}
}

GaInputParameters::~GaInputParameters()
{
	delete initInputParameters;
	delete runtimeInputParameters;
}

InitInputParameters* GaInputParameters::getInitInputParameters()
{
	return initInputParameters;
}

RuntimeInputParameters* GaInputParameters::getRuntimeInputParameters()
{
	return runtimeInputParameters;
}
