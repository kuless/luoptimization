#include "DijkstraValue.h"


DijkstraValue::DijkstraValue(int value, Station* station, Station* previosStation)
{
	this->value = value;
	this->station = station;
	this->previosStation = previosStation;
}

DijkstraValue::~DijkstraValue()
{
}

bool DijkstraValue::operator<(const DijkstraValue& dijkstraValue) const
{
	return value < dijkstraValue.value;
}

bool DijkstraValue::operator>(const DijkstraValue& dijkstraValue) const
{
	return value > dijkstraValue.value;
}

bool DijkstraValue::operator<(const DijkstraValue* dijkstraValue) const
{
	return value < dijkstraValue->value;
}

bool DijkstraValue::operator>(const DijkstraValue* dijkstraValue) const
{
	return value > dijkstraValue->value;
}

int DijkstraValue::getValue()
{
	return value;
}

Station* DijkstraValue::getStation()
{
	return station;
}

Station* DijkstraValue::getPreviosStation()
{
	return previosStation;
}

void DijkstraValue::setValue(int value)
{
	this->value = value;
}

void DijkstraValue::setStation(Station* station)
{
	this->station = station;
}

void DijkstraValue::setPreviosStation(Station* station)
{
	previosStation = station;
}
