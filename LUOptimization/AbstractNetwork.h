#pragma once
#include <vector>
#include <algorithm>
#include <boost/functional/hash.hpp>
#include "Line.h"
#include "SingleCriteriaValue.h"
#include "MultiCriteriaValue.h"
#include "NotImplementedException.h"

template <class T>
class AbstractNetwork
{
	public:
		AbstractNetwork(std::vector<Line*>* lines);
		AbstractNetwork(const AbstractNetwork<T>& network);
		virtual ~AbstractNetwork();

		bool operator>(const AbstractNetwork<T>& network) const;
		bool operator<(const AbstractNetwork<T>& network) const;
		bool operator==(const AbstractNetwork<T>& network) const;
		bool _Equal(const AbstractNetwork<T>& network) const;
		bool _Equal(const AbstractNetwork<T>* network) const;
		size_t getHash() const;
		
		std::vector<Line*>* getLines();
		T* getValue();
		virtual unsigned long long int getSelectionValue();

		virtual AbstractNetwork<T>* clone();
		virtual AbstractNetwork<T>* callChildConstructor(std::vector<Line*>* lines);
		virtual AbstractNetwork<T>* callChildConstructor(const AbstractNetwork<T>& network);

		void setValue(T* value);

	private:
		T* value;
		std::vector<Line*>* lines;
};

template <class T>
struct NetworkHash
{
	public:
		size_t operator()(const AbstractNetwork<T>& n) const;
		size_t operator()(const AbstractNetwork<T>* n) const;
};

template <class T>
struct NetworkEqual
{
	public:
		bool operator()(const AbstractNetwork<T>& l, const AbstractNetwork<T>& r) const;
		bool operator()(const AbstractNetwork<T>* l, const AbstractNetwork<T>* r) const;
};
