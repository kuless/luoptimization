#pragma once
#include "NetworkEvaluation.h"
#include "MultiCriteriaValue.h"
#include "MultiCriteriaNetwork.h"
#include "UniqueConnection.h"

class NetworkEvaluationMultiCriteria : public NetworkEvaluation<MultiCriteriaValue>
{
	public:
		NetworkEvaluationMultiCriteria(std::list<Travel*>* travels, MultiCriteriaNetwork* network);
		~NetworkEvaluationMultiCriteria();

	protected:
		void minuteBeginRating() override;
		void remainingPassengersRating(int numberOfPassengers) override;
		void afterDayRating() override;
		void transferPenaltyRating(int numberOfPassengers) override;
		void trainMoveRating(std::pair<int, int> trainUpdate) override;
		void newTrainsOnTrackRating() override;

	private:
		int maxTrainsOnTracks;
};
