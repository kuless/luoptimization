#include "NetworkState.h"

using namespace std;

NetworkState::NetworkState(list<Travel*>* travels, list<Train*>* trainsWaitingForDeparture, unordered_map<int, list<Train*>>* trainsOnTrack)
{
	this->travels = travels;
	this->trainsWaitingForDeparture = trainsWaitingForDeparture;
	numberOfTrainsOnTrack = 0;
	this->trainsOnTrack = trainsOnTrack;
	waitingPassangers = new list<Travel*>();
	notFittedPassangers = 0;
	numberOfWaitingPassangers = 0;
}

NetworkState::~NetworkState()
{
	for (list<Train*>::iterator itTrain = trainsWaitingForDeparture->begin(); itTrain != trainsWaitingForDeparture->end(); ++itTrain)
	{
		delete* itTrain;
	}

	delete trainsWaitingForDeparture;

	for (unordered_map<int, list<Train*>>::iterator itTrainsOnLine = trainsOnTrack->begin(); itTrainsOnLine != trainsOnTrack->end(); ++itTrainsOnLine)
	{
		for (list<Train*>::iterator itTrain = itTrainsOnLine->second.begin(); itTrain != itTrainsOnLine->second.end(); ++itTrain)
		{
			delete* itTrain;
		}
	}

	delete trainsOnTrack;

	for (list<Travel*>::iterator itTravel = waitingPassangers->begin(); itTravel != waitingPassangers->end(); ++itTravel)
	{
		delete* itTravel;
	}

	delete waitingPassangers;
	delete travels;
}

list<Travel*>* NetworkState::getTravels()
{
	return travels;
}

list<Train*>* NetworkState::getTrainsWaitingForDeparture()
{
	return trainsWaitingForDeparture;
}

int NetworkState::getNumberOfTrainsOnTrack()
{
	return numberOfTrainsOnTrack;
}

unordered_map<int, list<Train*>>* NetworkState::getTrainsOnTrack()
{
	return trainsOnTrack;
}

list<Travel*>* NetworkState::getWaitingPassangers()
{
	return waitingPassangers;
}

long NetworkState::getNumberOfWaitingPassangers()
{
	return numberOfWaitingPassangers;
}

long NetworkState::getNotFittedPassangers()
{
	return notFittedPassangers;
}

void NetworkState::setTravels(list<Travel*>* travels)
{
	this->travels = travels;
}

void NetworkState::setTrainsWaitingForDeparture(list<Train*>* trainsWaitingForDeparture)
{
	delete this->trainsWaitingForDeparture;
	this->trainsWaitingForDeparture = trainsWaitingForDeparture;
}

void NetworkState::setNumberOfTrainsOnTrack(int numberOfTrainsOnTrack)
{
	this->numberOfTrainsOnTrack;
}

void NetworkState::setTrainsOnTrack(unordered_map<int, list<Train*>>* trainsOnTrack)
{
	delete trainsOnTrack;
	this->trainsOnTrack = trainsOnTrack;
}

void NetworkState::setWaitingPassangers(list<Travel*>* waitingPassangers)
{
	delete waitingPassangers;
	this->waitingPassangers = waitingPassangers;
}

void NetworkState::setNotFittedPassangers(long notFittedPassangers)
{
	this->notFittedPassangers = notFittedPassangers;
}

void NetworkState::addWaitingPassangers(Travel* waitingPassangers)
{
	this->waitingPassangers->push_back(waitingPassangers);
	numberOfWaitingPassangers += waitingPassangers->getNumberOfPassengers();
}

void NetworkState::addWaitingPassangersOnFront(Travel* waitingPassangers)
{
	this->waitingPassangers->push_front(waitingPassangers);
	numberOfWaitingPassangers += waitingPassangers->getNumberOfPassengers();
}

void NetworkState::removeWaitingPassangers(Travel* waitingPassangers)
{
	this->waitingPassangers->remove(waitingPassangers);
	numberOfWaitingPassangers -= waitingPassangers->getNumberOfPassengers();
}

void NetworkState::addNotFittedPassangers(long notFittedPassangers)
{
	this->notFittedPassangers += notFittedPassangers;
}

void NetworkState::incrementNumberOfTrainsOnTrack()
{
	numberOfTrainsOnTrack++;
}

void NetworkState::reduceNumberOfTrainsOnTrack()
{
	numberOfTrainsOnTrack--;
}
