#include "RandomPopulationGenerator.h"

using namespace std;

template<class T>
RandomPopulationGenerator<T>::RandomPopulationGenerator(IRandomNumberGenerator* random, AbstractNetworkBuilder<T>* builder, IDijkstra* dijkstra): AbstractPopulationGenerator<T>(random, builder)
{
	this->dijkstra = dijkstra;
}

template<class T>
RandomPopulationGenerator<T>::~RandomPopulationGenerator()
{
}

template<class T>
vector<AbstractNetwork<T>*>* RandomPopulationGenerator<T>::generatePopulation(vector<Station*>& stations, vector<Travel*>& travels, PopulationGeneratorInputParameters* populationGeneratorInputParameters)
{
	vector<AbstractNetwork<T>*>* population = new vector<AbstractNetwork<T>*>();

	for (int i = 0; i < populationGeneratorInputParameters->getPopulationSize(); i++)
	{
		vector<Line*>* lines = new vector<Line*>();

		for (int k = 0; k < populationGeneratorInputParameters->getNumberOfLines(); k++)
		{
			int startStation = this->random->getInt(0, stations.size());
			int endStation = startStation;

			while (endStation == startStation)
			{
				endStation = this->random->getInt(0, stations.size());
			}

			vector<Station*>* stops = this->dijkstra->dijkstra(stations[startStation], stations[endStation]);
			Line* newLine = new Line(k + 1, "", stops);
			this->generateTimetableForNewLine(newLine);
			lines->push_back(newLine);
		}

		population->push_back(this->builder->createNewNetwork(lines));
	}

	return population;
}

template<class T>
PopulationGenerationTechniqueEnum RandomPopulationGenerator<T>::getType()
{
	return PopulationGenerationTechniqueEnum::RANDOM;
}

template class RandomPopulationGenerator<SingleCriteriaValue>;
template class RandomPopulationGenerator<MultiCriteriaValue>;
