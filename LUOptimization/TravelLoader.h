#pragma once
#include <iostream>
#include <vector>
#include <unordered_map>
#include <fstream>
#include <sstream>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string.hpp>
#include "StationPlatformFlow.h"
#include "TravelDestination.h"
#include "Constants.h"
#include "Travel.h"
#include "DataLoader.h"

class TravelLoader
{
	public:
		TravelLoader();
		~TravelLoader();
		std::vector<Travel*>* connectData(DataLoader& dataLoader);
		void loadStationFlow(std::string fileName);
		void loadTravelDestiantion(std::string fileName);
		void saveTravelsToFile(std::vector<Travel*>* travels, std::string fileName);
		std::vector<Travel*>* readTravelsFromFile(DataLoader& dataLoader, std::string fileName);

		std::vector<Travel*>* readTravelsFromSmallFile(DataLoader& dataLoader, std::string fileName);

	private:
		std::vector<StationPlatformFlow*>* stationFlow;
		std::vector<TravelDestination*>* travelDestination;

		std::unordered_map<std::string, int> connectOneTimePeriod(int currentFlow, int currentFlowPosition, int currentFlowEnd, int stationIterator);
};