#pragma once
#include <fstream>
#include <vector>
#include "Line.h"

class NetworkWriter
{
	public:
		NetworkWriter();
		virtual ~NetworkWriter();

		void saveLineVectorToFile(std::vector<Line*> lineVector, std::string fileName);
		void saveTimetableToFile(std::vector<Line*> lineVector, std::string fileName);

	private:
};
