#pragma once
#include <vector>
#include <unordered_map>
#include "Station.h"
#include "Line.h"
#include "AbstractNetwork.h"
#include "IRandomNumberGenerator.h"
#include "IDijkstra.h"
#include "AbstractCrossoverOperator.h"
#include "AllLinesCrossover.h"
#include "ClassicGaCrossover.h"
#include "ClassicGaWithShortestPathCrossover.h"
#include "WholeChromosomeCrossover.h"
#include "CrossoverTechniqueEnum.h"
#include "SingleCriteriaValue.h"
#include "MultiCriteriaValue.h"

template <class T>
class CrossoverOperators
{
	public:
		CrossoverOperators(IRandomNumberGenerator* random, CrossoverTechniqueEnum crossoverTechnique, int probability, IDijkstra* dijkstra);
		~CrossoverOperators();

		std::pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* crossover(std::pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* pairToCrossover);

	private:
		std::unordered_map<CrossoverTechniqueEnum, AbstractCrossoverOperator<T>*> crossoverOperators;
		CrossoverTechniqueEnum crossoverTechnique;
		int probability;
		IRandomNumberGenerator* random;

		std::pair<CrossoverTechniqueEnum, AbstractCrossoverOperator<T>*> getMapValue(AbstractCrossoverOperator<T>* crossoverOperator);
};
