#include "CrossoverTechniqueEnum.h"

using namespace std;

const CrossoverTechniqueEnum CrossoverTechnique::getValue(string value)
{
	if (value.compare("All lines") == 0)
	{
		return CrossoverTechniqueEnum::ALL_LINES;
	}
	else if (value.compare("Classic GA") == 0)
	{
		return CrossoverTechniqueEnum::CLASSIC_GA;
	}
	else if (value.compare("Classic GA with shortest path") == 0)
	{
		return CrossoverTechniqueEnum::CLASSIC_GA_WITH_SHORTEST_PATH;
	}
	else if (value.compare("Whole chromosome") == 0)
	{
		return CrossoverTechniqueEnum::WHOLE_CHROMOSOME;
	}
}
