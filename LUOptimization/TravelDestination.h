#pragma once
#include <string>
#include <vector>
#include "Destination.h"

class TravelDestination
{
	public:
		TravelDestination(int* startNLCCode, std::string* startStationName, std::vector<Destination*>* destinations);
		~TravelDestination();
		int* getStartNLCCode();
		std::string* getStartStationName();
		std::vector<Destination*>* getDestinations();

	private:
		int* startNLCCode;
		std::string* startStationName;
		std::vector<Destination*>* destinations;
};