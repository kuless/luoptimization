#include "QualityOperator.h"

using namespace std;

QualityOperator::QualityOperator(std::vector<MultiCriteriaValue*>* trueParetoFornt)
{
	this->qualityMeasures = new QualityMeasuresImpl(trueParetoFornt);
	this->elitistArchive = new ElitistArchive(new QualityMeasuresImpl(trueParetoFornt));
}

QualityOperator::~QualityOperator()
{
	delete this->qualityMeasures;
	delete this->elitistArchive;
}

string QualityOperator::getQualites(vector<AbstractNetwork<MultiCriteriaValue>*>* population, vector<QualityEnum> qualityTypes)
{
	this->elitistArchive->addToArchive(population);
	vector<promise<QualityValue*>> promises;
	vector<future<QualityValue*>> futures;
	vector<thread> threads;

	for (int i = 0; i < qualityTypes.size(); i++)
	{
		promises.push_back(promise<QualityValue*>());
		futures.push_back(promises[i * 2].get_future());
		threads.push_back(thread(&QualityOperator::setQualityMeasuresPromise, this, move(promises[i * 2]), ref(population), qualityTypes[i]));

		promises.push_back(promise<QualityValue*>());
		futures.push_back(promises[i * 2 + 1].get_future());
		threads.push_back(thread(&QualityOperator::setElitistArchivePromise, this, move(promises[i * 2 + 1]), qualityTypes[i]));
	}

	string result = "";

	for (int i = 0; i < futures.size(); i++)
	{
		QualityValue* qualityValue = futures[i].get();
		result = result + qualityValue->getQuality() + ";";
		delete qualityValue;
	}

	for (int i = 0; i < threads.size(); i++)
	{
		threads[i].join();
	}

	result.pop_back();
	return result;
}

vector<AbstractNetwork<MultiCriteriaValue>*>* QualityOperator::getArchive()
{
	return this->elitistArchive->getArchive();
}

void QualityOperator::setQualityMeasuresPromise(promise<QualityValue*>&& promise, vector<AbstractNetwork<MultiCriteriaValue>*>* population, QualityEnum qualityType)
{
	promise.set_value(this->qualityMeasures->getQuality(population, qualityType));
}

void QualityOperator::setElitistArchivePromise(promise<QualityValue*>&& promise, QualityEnum qualityType)
{
	promise.set_value(this->elitistArchive->getQuality(qualityType));
}
