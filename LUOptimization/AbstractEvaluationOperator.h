#pragma once
#include <vector>
#include <list>
#include <mutex>
#include "EvaluationCache.h"
#include "AbstractNetwork.h"
#include "NetworkEvaluation.h"
#include "TravelRouteFinder.h"
#include "Train.h"
#include "NotImplementedException.h"
#include "SingleCriteriaValue.h"
#include "MultiCriteriaValue.h"

template<class T>
class AbstractEvaluationOperator
{
	public:
		AbstractEvaluationOperator(std::vector<Travel*>* travels);
		virtual ~AbstractEvaluationOperator();

		void evaluate(AbstractNetwork<T>* network, std::mutex* mutex);

	protected:
		virtual NetworkEvaluation<T>* getNetworkEvaluation(std::list<Travel*>* travels, AbstractNetwork<T>* network);
		virtual void setValues(AbstractNetwork<T>& network, T& value);

	private:
		EvaluationCache<T>* cache;
		std::vector<Travel*>* travels;

		void addToCache(AbstractNetwork<T>* network, T* value);
		std::list<Travel*>* getCopyOfTravels();
};
