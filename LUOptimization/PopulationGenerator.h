#pragma once
#include <vector>
#include "IRandomNumberGenerator.h"
#include "Travel.h"
#include "AbstractNetwork.h"
#include "IDijkstra.h"
#include "Constants.h"
#include "SingleCriteriaValue.h"
#include "MultiCriteriaValue.h"
#include "AbstractNetworkBuilder.h"
#include "PopulationGeneratorInputParameters.h"
#include "PopulationGenerationTechniqueEnum.h"
#include "AbstractPopulationGenerator.h"
#include "RandomPopulationGenerator.h"
#include "ShortDistancePopulationGenerator.h"
#include "RgaPopulationGenerator.h"
#include "NotImplementedException.h"

template <class T>
class PopulationGenerator
{
	public:
		PopulationGenerator(IRandomNumberGenerator* random, IDijkstra* dijkstra, IDijkstra* quasiShortestPath, AbstractNetworkBuilder<T>* builder, PopulationGenerationTechniqueEnum populationGenerationTechnique);
		~PopulationGenerator();
		std::vector<AbstractNetwork<T>*>* generatePopulation(std::vector<Station*>& stations, std::vector<Travel*>& travels, PopulationGeneratorInputParameters* populationGeneratorInputParameters);

	private:
		IRandomNumberGenerator* random;
		IDijkstra* dijkstra;
		IDijkstra* quasiShortestPath;
		AbstractNetworkBuilder<T>* builder;
		std::unordered_map<PopulationGenerationTechniqueEnum, AbstractPopulationGenerator<T>*> populationGenerators;
		PopulationGenerationTechniqueEnum populationGenerationTechnique;

		std::pair<PopulationGenerationTechniqueEnum, AbstractPopulationGenerator<T>*> getMapValue(AbstractPopulationGenerator<T>* populationGenerator);
};
