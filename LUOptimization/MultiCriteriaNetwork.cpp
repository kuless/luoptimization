#include "MultiCriteriaNetwork.h"

using namespace std;

MultiCriteriaNetwork::MultiCriteriaNetwork(vector<Line*>* lines): AbstractNetwork<MultiCriteriaValue>(lines)
{
}

MultiCriteriaNetwork::MultiCriteriaNetwork(const AbstractNetwork<MultiCriteriaValue>& network): AbstractNetwork<MultiCriteriaValue>(network)
{
}

MultiCriteriaNetwork::~MultiCriteriaNetwork()
{
}

unsigned long long int MultiCriteriaNetwork::getSelectionValue()
{
	//TODO: after MultiCriteriaValue implemetation, implement this method
	return 0;
}

AbstractNetwork<MultiCriteriaValue>* MultiCriteriaNetwork::clone()
{
	return new MultiCriteriaNetwork(*this);
}

AbstractNetwork<MultiCriteriaValue>* MultiCriteriaNetwork::callChildConstructor(vector<Line*>* lines)
{
	return new MultiCriteriaNetwork(lines);
}

AbstractNetwork<MultiCriteriaValue>* MultiCriteriaNetwork::callChildConstructor(const AbstractNetwork<MultiCriteriaValue>& network)
{
	return new MultiCriteriaNetwork(network);
}
