#pragma once
#include "GeneticAlgoritm.h"
#include "SingleCriteriaValue.h"
#include "ResultWriterSingleCriteria.h"
#include "SingleCriteriaNetworkBuilder.h"
#include "EvaluationSingleCriteria.h"

class GeneticAlgoritmSingleCriteria: public GeneticAlgoritm<SingleCriteriaValue>
{
	public:
		GeneticAlgoritmSingleCriteria(int id, std::vector<Station*>* stationsVector, std::vector<Travel*>* travels, GaInputParameters* gaInputParameters, GaCommonComponents<SingleCriteriaValue>* gaCommonComponents);
		~GeneticAlgoritmSingleCriteria();

	protected:
		PopulationGenerator<SingleCriteriaValue>* initPopulationGenerator(IDijkstra* dijkstra, IDijkstra* quasiShortestPath, PopulationGenerationTechniqueEnum populationGenerationTechnique) override;
		void firstEvaluation(int numberOfThreads) override;
		void nextEvaluations(int numberOfThreads) override;
		void setNewPopulation(std::vector<AbstractNetwork<SingleCriteriaValue>*>* newPopulation) override;
};
