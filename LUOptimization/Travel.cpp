#include "Travel.h"

using namespace std;

Travel::Travel(Travel& travel)
{
	numberOfPassengers = travel.numberOfPassengers;
	startHour = travel.startHour;
	startMinute = travel.startMinute;
	startInMinutes = travel.startInMinutes;
	startStation = travel.startStation;
	endStation = travel.endStation;

	routes = new list<Route*>();

	for (list<Route*>::iterator it = travel.routes->begin(); it != travel.routes->end(); ++it)
	{
		routes->push_back(new Route(**it));
	}
}

Travel::Travel(int numberOfPassengers, int startHour, int startMinute, Station* startStation, Station* endStation)
{
	this->numberOfPassengers = numberOfPassengers;
	this->startHour = startHour;
	this->startMinute = startMinute;
	this->startStation = startStation;
	startInMinutes = startHour * HOUR_IN_MINUTES + startMinute;
	this->endStation = endStation;

	routes = new list<Route*>();
}

Travel::~Travel()
{
	for (list<Route*>::iterator it = routes->begin(); it != routes->end(); ++it)
	{
		delete *it;
	}

	delete routes;
}

bool Travel::operator<(const Travel& travel) const
{
	return startInMinutes < travel.startInMinutes;
}

int Travel::getNumberOfPassengers()
{
	return numberOfPassengers;
}

int Travel::getStartHour()
{
	return startHour;
}

int Travel::getStartMinute()
{
	return startMinute;
}

int Travel::getStartInMinutes()
{
	return startInMinutes;
}

Station* Travel::getStartStation()
{
	return startStation;
}

Station* Travel::getEndStation()
{
	return endStation;
}

list<Route*>* Travel::getRoutes()
{
	return routes;
}

void Travel::updateAfterComplitionPartOfTheJourney()
{
	Route* route = routes->front();
	startStation = route->getEndStation();
	routes->pop_front();
	delete route;
}

void Travel::setRoutes(list<Route*>* routes)
{
	for (list<Route*>::iterator it = this->routes->begin(); it != this->routes->end(); ++it)
	{
		delete* it;
	}

	delete this->routes;

	this->routes = routes;
}

Travel* Travel::splitPassengers(int reducePassengersInCurrentObj)
{
	Travel* splitedTravel = new Travel(*this);
	if (numberOfPassengers < reducePassengersInCurrentObj)
	{
		throw TravelException("Received value smaller than the current numberOfPassengers");
	}
	else
	{
		splitedTravel->numberOfPassengers = reducePassengersInCurrentObj;
		this->numberOfPassengers -= reducePassengersInCurrentObj;
	}
	return splitedTravel;
}

Travel::TravelBuilder::TravelBuilder()
{
	this->startStation = nullptr;
	this->endStation = nullptr;
}

Travel::TravelBuilder::~TravelBuilder()
{
}

Travel* Travel::TravelBuilder::build()
{
	if (startStation == nullptr || endStation == nullptr)
	{
		throw TravelException("Stations not set in Travel");
	}
	else
	{
		return new Travel(numberOfPassengers, startHour, startMinute, startStation, endStation);
	}
}

void Travel::TravelBuilder::setNumberOfPassengers(int numberOfPassengers)
{
	this->numberOfPassengers = numberOfPassengers;
}

void Travel::TravelBuilder::setStartHour(int hour)
{
	this->startHour = hour;
}

void Travel::TravelBuilder::setStartMinute(int minute)
{
	this->startMinute = minute;
}

void Travel::TravelBuilder::setStartStation(Station* startStation)
{
	this->startStation = startStation;
}

void Travel::TravelBuilder::setEndStation(Station* endStation)
{
	this->endStation = endStation;
}
