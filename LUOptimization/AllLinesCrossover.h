#pragma once
#include "AbstractCrossoverOperator.h"
#include "AbstractNetwork.h"
#include "IRandomNumberGenerator.h"
#include "SingleCriteriaValue.h"
#include "MultiCriteriaValue.h"

template<class T>
class AllLinesCrossover : public AbstractCrossoverOperator<T>
{
	public:
		AllLinesCrossover(IRandomNumberGenerator* random);
		~AllLinesCrossover();
		std::pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* crossover(std::pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* pairToCrossover) override;
		CrossoverTechniqueEnum getType() override;

	private:
		std::vector<int> getCommonJunctions(std::vector<Station*>* firstGenLineJunctionStations, std::vector<Station*>* secondGenLineJucntionStations);
};
