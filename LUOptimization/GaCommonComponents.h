#pragma once
#include "DijkstraImpl.h"
#include "QuasiShortestPath.h"
#include "AbstractEvaluationOperator.h"

template <class T>
class GaCommonComponents
{
	public:
		GaCommonComponents(DijkstraImpl* dijkstraImpl, QuasiShortestPath* quasiShortestPath, AbstractEvaluationOperator<T>* evaluationOperator);
		~GaCommonComponents();

		DijkstraImpl* getDijkstraImpl();
		QuasiShortestPath* getQuasiShortestPath();
		AbstractEvaluationOperator<T>* getEvaluationOperator();

	private:
		DijkstraImpl* dijkstraImpl;
		QuasiShortestPath* quasiShortestPath;
		AbstractEvaluationOperator<T>* evaluationOperator;
};
