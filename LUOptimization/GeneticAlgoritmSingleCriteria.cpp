#include "GeneticAlgoritmSingleCriteria.h"

using namespace std;

GeneticAlgoritmSingleCriteria::GeneticAlgoritmSingleCriteria(int id, vector<Station*>* stationsVector, vector<Travel*>* travels, GaInputParameters* gaInputParameters, GaCommonComponents<SingleCriteriaValue>* gaCommonComponents): GeneticAlgoritm<SingleCriteriaValue>(id, stationsVector, travels, gaInputParameters, gaCommonComponents)
{
	this->selectionOperators = new SelectionOperators<SingleCriteriaValue>(this->random->copy(), gaInputParameters->getInitInputParameters()->getRouletteSelection(), gaInputParameters->getInitInputParameters()->getSelectionsSize());
	this->crossoverOperators = new CrossoverOperators<SingleCriteriaValue>(this->random->copy(), gaInputParameters->getInitInputParameters()->getCrossoverTechnique(), gaInputParameters->getInitInputParameters()->getCrossoverProbability(), dijkstra);
	this->mutationOperators = new MutationOperators<SingleCriteriaValue>(this->random->copy(), quasiShortestPath, gaInputParameters->getInitInputParameters()->getMutationTechnique(), gaInputParameters->getInitInputParameters()->getMutationProbability());
	this->resultsWriter = new ResultWriterSingleCriteria(id, gaInputParameters->getInitInputParameters()->getOutputFile());
}

GeneticAlgoritmSingleCriteria::~GeneticAlgoritmSingleCriteria()
{
}

PopulationGenerator<SingleCriteriaValue>* GeneticAlgoritmSingleCriteria::initPopulationGenerator(IDijkstra* dijkstra, IDijkstra* quasiShortestPath, PopulationGenerationTechniqueEnum populationGenerationTechnique)
{
	return new PopulationGenerator<SingleCriteriaValue>(this->random->copy(), dijkstra, quasiShortestPath, new SingleCriteriaNetworkBuilder(), populationGenerationTechnique);
}

void GeneticAlgoritmSingleCriteria::firstEvaluation(int numberOfThreads)
{
	evaluate(numberOfThreads);
	cout << id << " evaluated" << endl;
}

void GeneticAlgoritmSingleCriteria::nextEvaluations(int numberOfThreads)
{
	evaluate(numberOfThreads);
	cout << id << " evaluate done" << endl;
}

void GeneticAlgoritmSingleCriteria::setNewPopulation(std::vector<AbstractNetwork<SingleCriteriaValue>*>* newPopulation)
{
	for (int i = 0; i < population->size(); i++)
	{
		delete (*population)[i];
	}

	delete population;
	population = newPopulation;
}
