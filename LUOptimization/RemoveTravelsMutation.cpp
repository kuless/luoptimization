#include "RemoveTravelsMutation.h"

RemoveTravelsMutation::RemoveTravelsMutation(IRandomNumberGenerator* random): AbstractMutationOperator(random)
{
}

RemoveTravelsMutation::~RemoveTravelsMutation()
{
}

void RemoveTravelsMutation::mutation(Line* lineForMutation)
{
	int travelsToRemove = random->getInt(0, MAX_REMOVE_ADD_FREQENCE_MUTATION);

	for (int i = 0; i < travelsToRemove; i++)
	{
		int toRemove = random->getInt(0, lineForMutation->getTimetable().size());
		lineForMutation->removeTimetableElement(toRemove);
	}
}

MutationTechniqueEnum RemoveTravelsMutation::getType()
{
	return MutationTechniqueEnum::REMOVE_TRAVELS;
}
