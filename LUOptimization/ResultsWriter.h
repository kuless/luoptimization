#pragma once
#include <fstream>
#include <vector>
#include <boost/filesystem.hpp>
#include "Line.h"
#include "AbstractNetwork.h"
#include "SingleCriteriaValue.h"
#include "MultiCriteriaValue.h"
#include "NetworkWriter.h"
#include "NotImplementedException.h"

template <class T>
class ResultsWriter: public NetworkWriter
{
	public:
		ResultsWriter(int id, std::string fileName);
		virtual ~ResultsWriter();

		virtual void saveResultsToFile(std::vector<AbstractNetwork<T>*>* population, int generation, int numberOfGeneration);
		virtual void saveBestResultToFile(std::vector<AbstractNetwork<T>*>* population);
	
	protected:
		int id;
		std::string fileName;
		std::string rootDirectory;
		std::ofstream fileWriter;

		virtual void saveTitlesToFile();
};

