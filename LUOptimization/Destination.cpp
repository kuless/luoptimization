#include "Destination.h"

using namespace std;

Destination::Destination(int* destinationNLCCode, string* destinationStationName, vector<int*>* flow)
{
	this->destinationNLCCode = destinationNLCCode;
	this->destinationStationName = destinationStationName;
	this->flow = flow;
}

Destination::~Destination()
{
	delete destinationNLCCode;
	delete destinationStationName;

	for (int i = 0; i < flow->size(); i++)
	{
		delete (*flow)[i];
	}

	delete flow;
}

int* Destination::getDestiantionNLCCode()
{
	return destinationNLCCode;
}

string* Destination::getDestinationStationName()
{
	return destinationStationName;
}

vector<int*>* Destination::getFlow()
{
	return flow;
}
