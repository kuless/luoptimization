#pragma once
#include <vector>
#include "FrontDomination.h"
#include "MultiCriteriaValue.h"

class ParetoFront
{
	public:
		ParetoFront();
		ParetoFront(std::vector<AbstractNetwork<MultiCriteriaValue>*>* population);
		~ParetoFront();

		bool _Equal(ParetoFront& paretoFront);

		std::vector<std::pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>* getFront();
		void addNetwork(std::pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>* network);
		bool empty();
		std::vector<std::pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>::iterator begin();
		std::vector<std::pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>::iterator end();
		void sortFront();
		void clearFront();

	private:
		std::vector<std::pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>* front;
};
