#pragma once
#include "PopulationGeneratorInputParameters.h"
#include "SolutionTypeEnum.h"

class RuntimeInputParameters
{
	public:
		RuntimeInputParameters();
		RuntimeInputParameters(RuntimeInputParameters& runtimeInputParameters);
		~RuntimeInputParameters();

		void setPopulationGeneratorInputParameters(PopulationGeneratorInputParameters* populationGeneratorInputParameters);
		void setNumberOfGeneration(int numberOfGeneration);
		void setNumberOfThreads(int numberOfThreads);
		void setSolutionType(std::string solutionType);

		PopulationGeneratorInputParameters* getPopulationGeneratorInputParameters();
		int getNumberOfGeneration();
		int getNumberOfThreads();
		SolutionTypeEnum getSolutionType();

	private:
		PopulationGeneratorInputParameters* populationGeneratorInputParameters;
		int numberOfGeneration;
		int numberOfThreads;
		SolutionTypeEnum solutionType;
};
