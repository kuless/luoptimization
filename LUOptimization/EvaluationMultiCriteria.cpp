#include "EvaluationMultiCriteria.h"

EvaluationMultiCriteria::EvaluationMultiCriteria(std::vector<Travel*>* travels) : AbstractEvaluationOperator<MultiCriteriaValue>(travels)
{
}

EvaluationMultiCriteria::~EvaluationMultiCriteria()
{
}

NetworkEvaluation<MultiCriteriaValue>* EvaluationMultiCriteria::getNetworkEvaluation(std::list<Travel*>* travels, AbstractNetwork<MultiCriteriaValue>* network)
{
    return new NetworkEvaluationMultiCriteria(travels, dynamic_cast<MultiCriteriaNetwork*>(network));
}

void EvaluationMultiCriteria::setValues(AbstractNetwork<MultiCriteriaValue>& network, MultiCriteriaValue& value)
{
    network.setValue(new MultiCriteriaValue(value));
}
