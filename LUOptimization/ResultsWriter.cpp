#include "ResultsWriter.h"

using namespace std;

template <class T>
ResultsWriter<T>::ResultsWriter(int id, string fileName): NetworkWriter()
{
	this->id = id;
	this->fileName = fileName;
	boost::filesystem::create_directory(CURRENT_DIRECTORY + fileName);
	this->rootDirectory = CURRENT_DIRECTORY + fileName + SLASH;
	this->fileWriter.open(this->rootDirectory + fileName + FILE_EXTENSION);
}

template<class T>
ResultsWriter<T>::~ResultsWriter()
{
	fileWriter.close();
}

template<class T>
void ResultsWriter<T>::saveResultsToFile(vector<AbstractNetwork<T>*>* population, int generation, int numberOfGeneration)
{
	throw NotImplementedException("saveResultsToFile not implemented");
}

template<class T>
void ResultsWriter<T>::saveBestResultToFile(vector<AbstractNetwork<T>*>* population)
{
	throw NotImplementedException("saveBestResultToFile not implemented");
}

template <class T>
void ResultsWriter<T>::saveTitlesToFile()
{
	throw NotImplementedException("saveTitlesToFile not implemented");
}

template class ResultsWriter<SingleCriteriaValue>;
template class ResultsWriter<MultiCriteriaValue>;
