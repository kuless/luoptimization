#include "InvertedGenerationalDistance.h"

using namespace std;

InvertedGenerationalDistance::InvertedGenerationalDistance(vector<MultiCriteriaValue*>* trueParetoFornt): AbstractQualityMeasure()
{
    vector<MultiCriteriaValue*>* copy = new vector<MultiCriteriaValue*>();

    for (int i = 0; i < trueParetoFornt->size(); i++)
    {
        copy->push_back(new MultiCriteriaValue(*trueParetoFornt->at(i)));
    }

    this->trueParetoFornt = copy;
}

InvertedGenerationalDistance::~InvertedGenerationalDistance()
{
    for (int i = 0; i < trueParetoFornt->size(); i++)
    {
        delete trueParetoFornt->at(i);
    }

    delete trueParetoFornt;
}

QualityValue* InvertedGenerationalDistance::getQuality(vector<AbstractNetwork<MultiCriteriaValue>*>* population)
{
    boost::multiprecision::mpf_float  quality = 0;
    quality.precision(10);

    for (int i = 0; i < trueParetoFornt->size(); i++)
    {
        boost::multiprecision::mpf_float distance = getDistance(trueParetoFornt->at(i), population->at(0)->getValue());

        for (int k = 1; k < population->size() && population->at(k)->getValue()->getParetoFront() == FIRST_PARETO_FRONT; k++)
        {
            boost::multiprecision::mpf_float distanceForNextObj = getDistance(trueParetoFornt->at(i), population->at(k)->getValue());

            if (distanceForNextObj < distance)
            {
                distance = distanceForNextObj;
            }
        }

        quality += distance;
    }


    return new QualityValue(quality / trueParetoFornt->size());
}

QualityEnum InvertedGenerationalDistance::getType()
{
    return QualityEnum::IGD;
}

boost::multiprecision::mpf_float  InvertedGenerationalDistance::getDistance(MultiCriteriaValue* trueParetoFrontValue, MultiCriteriaValue* populationValue)
{
    boost::multiprecision::mpf_float  distance = 0;
    distance.precision(10);

    if (trueParetoFrontValue->getMaintenanceValue() > populationValue->getMaintenanceValue())
    {
        distance += boost::multiprecision::mpf_float(trueParetoFrontValue->getMaintenanceValue() - populationValue->getMaintenanceValue())
            * boost::multiprecision::mpf_float(trueParetoFrontValue->getMaintenanceValue() - populationValue->getMaintenanceValue());
    }
    else
    {
        distance += boost::multiprecision::mpf_float(populationValue->getMaintenanceValue() - trueParetoFrontValue->getMaintenanceValue())
            * boost::multiprecision::mpf_float(populationValue->getMaintenanceValue() - trueParetoFrontValue->getMaintenanceValue());
    }

    if (trueParetoFrontValue->getPassengersValue() > populationValue->getPassengersValue())
    {
        distance += boost::multiprecision::mpf_float(trueParetoFrontValue->getPassengersValue() - populationValue->getPassengersValue())
            * boost::multiprecision::mpf_float(trueParetoFrontValue->getPassengersValue() - populationValue->getPassengersValue());
    }
    else
    {
        distance += 
            boost::multiprecision::mpf_float(populationValue->getPassengersValue() - trueParetoFrontValue->getPassengersValue())
            * boost::multiprecision::mpf_float(populationValue->getPassengersValue() - trueParetoFrontValue->getPassengersValue());
    }
    
    if (distance == 0)
    {
        return 0;
    }
    else
    {
        return boost::multiprecision::sqrt(distance);
    }
}
