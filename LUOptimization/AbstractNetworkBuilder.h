#pragma once
#include "AbstractNetwork.h"
#include "SingleCriteriaValue.h"
#include "MultiCriteriaValue.h"
#include "NotImplementedException.h"

template <class T>
class AbstractNetworkBuilder
{
	public:
		AbstractNetworkBuilder();
		virtual ~AbstractNetworkBuilder();

		virtual AbstractNetwork<T>* createNewNetwork(std::vector<Line*>* lines);
		virtual AbstractNetworkBuilder<T>* copy();
};
