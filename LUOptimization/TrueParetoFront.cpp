#include "TrueParetoFront.h"

using namespace std;

TrueParetoFront::TrueParetoFront()
{
}

TrueParetoFront::~TrueParetoFront()
{
}

void TrueParetoFront::createTpf()
{
	vector<AbstractNetwork<MultiCriteriaValue>*>* solutions = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	ifstream file(TPF_INPUT_FILE);
	string line;
	getline(file, line);

	while (getline(file, line))
	{
		vector<string> lineSplited;
		boost::split(lineSplited, line, boost::algorithm::is_any_of(SEPERATOR));
		MultiCriteriaValue* value = new MultiCriteriaValue(stoull(lineSplited[TPF_PASSENGER_VALUE]), stoull(lineSplited[TPF_MAINTENANCE_VALUE]));
		MultiCriteriaNetwork* network = new MultiCriteriaNetwork(new vector<Line*>());
		network->setValue(value);
		solutions->push_back(network);
	}

	Nsgaii* nsgaii = new Nsgaii();
	ParetoFront* firstFront = nsgaii->getFirstFront(solutions);
	ofstream myFile;
	myFile.open(TPF_OUTPUT_FILE);

	for (int i = 0; i < firstFront->getFront()->size(); i++)
	{
		myFile << firstFront->getFront()->at(i)->first->getValue()->getPassengersValue() << SEPERATOR << firstFront->getFront()->at(i)->first->getValue()->getMaintenanceValue() << endl;
	}

	myFile.close();
	delete firstFront;
	delete nsgaii;

	for (int i = 0; i < solutions->size(); i++)
	{
		delete solutions->at(i);
	}

	delete solutions;
}

vector<MultiCriteriaValue*>* TrueParetoFront::loadTTpf()
{
	vector<MultiCriteriaValue*>* trueParetoFront = new vector<MultiCriteriaValue*>();
	ifstream file(TPF_OUTPUT_FILE);
	string line;
	getline(file, line);

	while (getline(file, line))
	{
		vector<string> lineSplited;
		boost::split(lineSplited, line, boost::algorithm::is_any_of(SEPERATOR));
		trueParetoFront->push_back(new MultiCriteriaValue(stoull(lineSplited[TPF_PASSENGER_VALUE]), stoull(lineSplited[TPF_MAINTENANCE_VALUE])));
	}

	return trueParetoFront;
}
