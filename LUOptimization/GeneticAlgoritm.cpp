#include "GeneticAlgoritm.h"

using namespace std;

template <class T>
GeneticAlgoritm<T>::GeneticAlgoritm(int id, vector<Station*>* stationsVector, vector<Travel*>* travels, GaInputParameters* gaInputParameters, GaCommonComponents<T>* gaCommonComponents)
{
	this->id = id;
	this->population = nullptr;
	this->dijkstra = gaCommonComponents->getDijkstraImpl();
	this->quasiShortestPath = gaCommonComponents->getQuasiShortestPath();
	this->selectionOperators = nullptr;
	this->crossoverOperators = nullptr;
	this->evaluationOperator = gaCommonComponents->getEvaluationOperator();
	this->mutationOperators = nullptr;
	this->resultsWriter = nullptr;
	this->stationsVector = stationsVector;
	this->travels = travels;
	this->runtimeInputParameters = new RuntimeInputParameters(*gaInputParameters->getRuntimeInputParameters());
	this->random = new RandomNumberGeneratorImpl();
}

template <class T>
GeneticAlgoritm<T>::~GeneticAlgoritm()
{
	delete selectionOperators;
	delete crossoverOperators;
	delete mutationOperators;
	delete resultsWriter;
	delete runtimeInputParameters;
	delete random;
}

template<class T>
void GeneticAlgoritm<T>::run()
{
	try
	{
		generatePopulation(this->runtimeInputParameters->getPopulationGeneratorInputParameters());
		cout << id << " generated population" << endl;
		firstEvaluation(this->runtimeInputParameters->getNumberOfThreads());

		resultsWriter->saveResultsToFile(population, 0, this->runtimeInputParameters->getNumberOfGeneration());

		for (int i = 0; i < this->runtimeInputParameters->getNumberOfGeneration(); i++)
		{
			setNewPopulation(crossover(selection(), this->runtimeInputParameters->getNumberOfThreads()));
			cout << id << " crossover done" << endl;
			mutation(this->runtimeInputParameters->getNumberOfThreads());
			cout << id << " mutation done" << endl;
			nextEvaluations(this->runtimeInputParameters->getNumberOfThreads());
			resultsWriter->saveResultsToFile(population, i + 1, this->runtimeInputParameters->getNumberOfGeneration());
		}

		resultsWriter->saveBestResultToFile(population);
	
		for (int i = 0; i < population->size(); i++)
		{
			delete (*population)[i];
		}

		delete population;
	}
	catch (...)
	{
		cout << "Fail GeneticAlgoritm::run" << endl;
		cout << boost::stacktrace::stacktrace() << endl;
	}
}

template <class T>
void GeneticAlgoritm<T>::generatePopulation(PopulationGeneratorInputParameters* populationGeneratorInputParameters)
{
	PopulationGenerator<T>* generator = initPopulationGenerator(dijkstra, quasiShortestPath, this->runtimeInputParameters->getPopulationGeneratorInputParameters()->getPopulationGenerationTechnique());
	population = generator->generatePopulation((*stationsVector), (*travels), populationGeneratorInputParameters);
	delete generator;
}

template <class T>
void GeneticAlgoritm<T>::evaluate(int numberOfThreads)
{
	mutex* mutexObj = new mutex();

	int sizeOfThread = population->size() / numberOfThreads;
	vector<thread> threads = vector<thread>();

	for (int i = 0; i < numberOfThreads - 1; i++)
	{
		threads.push_back(thread(&GeneticAlgoritm<T>::evaluateThread, this, sizeOfThread * i, sizeOfThread * i + sizeOfThread, ref(mutexObj)));
	}

	threads.push_back(thread(&GeneticAlgoritm<T>::evaluateThread, this, sizeOfThread * (numberOfThreads - 1), population->size(), ref(mutexObj)));

	for (int i = 0; i < numberOfThreads; i++)
	{
		threads[i].join();
	}

	delete mutexObj;
}

template <class T>
void GeneticAlgoritm<T>::evaluateThread(int begin, int end, mutex* mutexObj)
{
	try
	{
		for (int i = begin; i < end; i++)
		{
			evaluationOperator->evaluate((*population)[i], mutexObj);
		}
	}
	catch (...)
	{
		cout << "Fail GeneticAlgoritm::evaluateThread" << endl;
		cout << boost::stacktrace::stacktrace() << endl;
	}
}

template <class T>
vector<pair<AbstractNetwork<T>*, AbstractNetwork<T>*>*>* GeneticAlgoritm<T>::selection()
{
	return selectionOperators->selection(population);
}

template <class T>
vector<AbstractNetwork<T>*>* GeneticAlgoritm<T>::crossover(vector<pair<AbstractNetwork<T>*, AbstractNetwork<T>*>*>* pairs, int numberOfThreads)
{
	vector<AbstractNetwork<T>*>* newPopulation = new vector<AbstractNetwork<T>*>();
	int sizeOfThread = pairs->size() / numberOfThreads;
	vector<thread> threads = vector<thread>();
	mutex* mutexObj = new mutex();

	for (int i = 0; i < numberOfThreads - 1; i++)
	{
		threads.push_back(thread(&GeneticAlgoritm<T>::crossoverThread, this, ref(pairs), ref(newPopulation), sizeOfThread * i, sizeOfThread * i + sizeOfThread, ref(mutexObj)));
	}

	threads.push_back(thread(&GeneticAlgoritm<T>::crossoverThread, this, ref(pairs), ref(newPopulation), sizeOfThread * (numberOfThreads - 1), pairs->size(), ref(mutexObj)));

	for (int i = 0; i < numberOfThreads; i++)
	{
		threads[i].join();
	}

	delete mutexObj;

	for (int i = 0; i < pairs->size(); i++)
	{
		delete (*pairs)[i];
	}

	delete pairs;

	return newPopulation;
}

template <class T>
void GeneticAlgoritm<T>::crossoverThread(vector<pair<AbstractNetwork<T>*, AbstractNetwork<T>*>*>* pairs, vector<AbstractNetwork<T>*>* newPopulation, int begin, int end, mutex* mutexObj)
{
	try
	{
		for (int i = begin; i < end; i++)
		{
			pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* newPair = crossoverOperators->crossover((*pairs)[i]);
			mutexObj->lock();
			newPopulation->push_back(newPair->first);
			newPopulation->push_back(newPair->second);
			mutexObj->unlock();
			delete newPair;
		}
	}
	catch (...)
	{
		cout << "Fail GeneticAlgoritm::crossoverThread" << endl;
		cout << boost::stacktrace::stacktrace() << endl;
	}
}

template <class T>
void GeneticAlgoritm<T>::mutation(int numberOfThreads)
{
	mutex* mutexObj = new mutex();
	int sizeOfThread = population->size() / numberOfThreads;
	vector<thread> threads = vector<thread>();

	for (int i = 0; i < numberOfThreads - 1; i++)
	{
		threads.push_back(thread(&GeneticAlgoritm<T>::mutationThread, this, sizeOfThread * i, sizeOfThread * i + sizeOfThread));
	}

	threads.push_back(thread(&GeneticAlgoritm<T>::mutationThread, this, sizeOfThread * (numberOfThreads - 1), population->size()));

	for (int i = 0; i < numberOfThreads; i++)
	{
		threads[i].join();
	}

	delete mutexObj;
}

template <class T>
void GeneticAlgoritm<T>::mutationThread(int begin, int end)
{
	try
	{
		for (int i = begin; i < end; i++)
		{
			mutationOperators->mutation((*population)[i]);
		}
	}
	catch (...)
	{
		cout << "Fail GeneticAlgoritm::mutationThread" << endl;
		cout << boost::stacktrace::stacktrace() << endl;
	}
}

template<class T>
PopulationGenerator<T>* GeneticAlgoritm<T>::initPopulationGenerator(IDijkstra* dijkstra, IDijkstra* quasiShortestPath, PopulationGenerationTechniqueEnum populationGenerationTechnique)
{
	throw NotImplementedException("initPopulationGenerator not implemented");
}

template<class T>
void GeneticAlgoritm<T>::firstEvaluation(int numberOfThreads)
{
	throw NotImplementedException("firstEvaluation not implemented");
}

template<class T>
void GeneticAlgoritm<T>::nextEvaluations(int numberOfThreads)
{
	throw NotImplementedException("nextEvaluations not implemented");
}

template<class T>
void GeneticAlgoritm<T>::setNewPopulation(std::vector<AbstractNetwork<T>*>* newPopulation)
{
	throw NotImplementedException("setNewPopulation not implemented");
}

template class GeneticAlgoritm<SingleCriteriaValue>;
template class GeneticAlgoritm<MultiCriteriaValue>;
