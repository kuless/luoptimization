#pragma once
#include "MultiCriteriaValue.h"
#include "ParetoFront.h"
#include "FrontDomination.h"
#include <mutex>

class Nsgaii
{
	public:
		Nsgaii();
		~Nsgaii();
		void evaluate(std::vector<AbstractNetwork<MultiCriteriaValue>*>* population);
		std::vector<AbstractNetwork<MultiCriteriaValue>*>* evaluate(std::vector<AbstractNetwork<MultiCriteriaValue>*>* oldPopulation, std::vector<AbstractNetwork<MultiCriteriaValue>*>* newPopulation);
		ParetoFront* getFirstFront(std::vector<AbstractNetwork<MultiCriteriaValue>*>* population);

	private:
		std::vector<ParetoFront*>* fastNonDominatedSort(std::vector<AbstractNetwork<MultiCriteriaValue>*>* population);
		void setDominationsForElement(ParetoFront* populationAsParetoFront, std::pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>* element);
		ParetoFront* getNextFront(ParetoFront* front, int frontNumber);
		void reduceDominationCounter(ParetoFront* nextFront, std::pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>* element, int frontNumber);
		void crowdingDistance(ParetoFront* nonDominatedFront);
		void crowdingDistancePassengers(ParetoFront* nonDominatedFront);
		void crowdingDistanceMaintenance(ParetoFront* nonDominatedFront);
};
