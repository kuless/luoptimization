#ifndef ABSTRACT_MUTATION_OPERATOR_HEADER
#define ABSTRACT_MUTATION_OPERATOR_HEADER

#pragma 
#include "Line.h"
#include "IRandomNumberGenerator.h"
#include "NotImplementedException.h"
#include "MutationTechniqueEnum.h"

class AbstractMutationOperator
{
	public:
		AbstractMutationOperator(IRandomNumberGenerator* random);
		virtual ~AbstractMutationOperator();
		virtual void mutation(Line* lineForMutation);
		virtual MutationTechniqueEnum getType();

	protected:
		IRandomNumberGenerator* random;
};
#endif
