#include "EvaluationSingleCriteria.h"

using namespace std;

EvaluationSingleCriteria::EvaluationSingleCriteria(vector<Travel*>* travels): AbstractEvaluationOperator<SingleCriteriaValue>(travels)
{
}

EvaluationSingleCriteria::~EvaluationSingleCriteria()
{
}

NetworkEvaluation<SingleCriteriaValue>* EvaluationSingleCriteria::getNetworkEvaluation(list<Travel*>* travels, AbstractNetwork<SingleCriteriaValue>* network)
{
    return new NetworkEvaluationSingleCriteria(travels, dynamic_cast<SingleCriteriaNetwork*>(network));
}

void EvaluationSingleCriteria::setValues(AbstractNetwork<SingleCriteriaValue>& network, SingleCriteriaValue& value)
{
    network.setValue(new SingleCriteriaValue(value));
}
