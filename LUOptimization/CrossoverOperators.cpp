#include "CrossoverOperators.h"

using namespace std;

template <class T>
CrossoverOperators<T>::CrossoverOperators(IRandomNumberGenerator* random, CrossoverTechniqueEnum crossoverTechnique, int probability, IDijkstra* dijkstra)
{
	this->random = random;
	this->crossoverTechnique = crossoverTechnique;
	this->probability = probability;
	crossoverOperators.insert(pair<CrossoverTechniqueEnum, AbstractCrossoverOperator<T>*>(getMapValue(new AllLinesCrossover<T>(random->copy()))));
	crossoverOperators.insert(pair<CrossoverTechniqueEnum, AbstractCrossoverOperator<T>*>(getMapValue(new WholeChromosomeCrossover<T>(random->copy()))));
	// TODO: Uncomment after implementation
	// crossoverOperators.insert(pair<CrossoverTechniqueEnum, AbstractCrossoverOperator<T>*>(getMapValue(new ClassicGaCrossover<T>(random->copy()))));
	crossoverOperators.insert(pair<CrossoverTechniqueEnum, AbstractCrossoverOperator<T>*>(getMapValue(new ClassicGaWithShortestPathCrossover<T>(random->copy(), dijkstra))));
}

template <class T>
CrossoverOperators<T>::~CrossoverOperators()
{
	delete random;

	for (typename unordered_map<CrossoverTechniqueEnum, AbstractCrossoverOperator<T>*>::iterator it = this->crossoverOperators.begin(); it != this->crossoverOperators.end(); ++it)
	{
		delete it->second;
	}
}

template <class T>
pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* CrossoverOperators<T>::crossover(pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* pairToCrossover)
{
	pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* newPair = nullptr;

	if (random->getInt(0, CERTAIN_EVENT) < probability)
	{
		newPair = this->crossoverOperators.find(crossoverTechnique)->second->crossover(pairToCrossover);
	}
	else
	{
		newPair = new pair<AbstractNetwork<T>*, AbstractNetwork<T>*>(pairToCrossover->first->clone(), pairToCrossover->second->clone());
	}

	return newPair;
}

template <class T>
pair<CrossoverTechniqueEnum, AbstractCrossoverOperator<T>*> CrossoverOperators<T>::getMapValue(AbstractCrossoverOperator<T>* crossoverOperator)
{
	return pair<CrossoverTechniqueEnum, AbstractCrossoverOperator<T>*>(crossoverOperator->getType(), crossoverOperator);
}

template class CrossoverOperators<SingleCriteriaValue>;
template class CrossoverOperators<MultiCriteriaValue>;
