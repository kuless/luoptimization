#include "DijkstraCache.h"

DijkstraCache::DijkstraCache()
{
	dijkstraCache = new std::unordered_map<std::string, std::vector<Station*>>();
}

DijkstraCache::~DijkstraCache()
{
	delete dijkstraCache;
}
