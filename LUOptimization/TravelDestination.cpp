#include "TravelDestination.h"

using namespace std;

TravelDestination::TravelDestination(int* startNLCCode, string* startStationName, vector<Destination*>* destinations)
{
	this->startNLCCode = startNLCCode;
	this->startStationName = startStationName;
	this->destinations = destinations;
}

TravelDestination::~TravelDestination()
{
	delete startNLCCode;
	delete startStationName;

	for (int i = 0; i < destinations->size(); i++)
	{
		delete (*destinations)[i];
	}

	delete destinations;
}

int* TravelDestination::getStartNLCCode()
{
	return startNLCCode;
}

string* TravelDestination::getStartStationName()
{
	return startStationName;
}

vector<Destination*>* TravelDestination::getDestinations()
{
	return destinations;
}
