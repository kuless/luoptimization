#include "LineMutation.h"

using namespace std;

LineMutation::LineMutation(IRandomNumberGenerator* random, IDijkstra* quasiShortestPath): AbstractMutationOperator(random)
{
	this->quasiShortestPath = quasiShortestPath;
}

LineMutation::~LineMutation()
{
}

void LineMutation::mutation(Line* lineForMutation)
{
	vector<Station*>* junctionStations = lineForMutation->getJunctionStations();

	if (junctionStations->size() >= 2)
	{
		int startNodeToMutation = random->getInt(0, (int)junctionStations->size() - 1);

		vector<Station*>* mutationSubline = quasiShortestPath->dijkstra(
			junctionStations->at(startNodeToMutation), junctionStations->at(startNodeToMutation + 1)
		);

		vector<Station*> stops = lineForMutation->getStops();
		vector<Station*> backupVector = stops;
		vector<Station*> endOfLine;
		bool endStationToMutationNotPassed = true;

		if (!mutationSubline->empty())
		{
			for (int i = stops.size() - 1; stops[i] != junctionStations->at(startNodeToMutation); i--)
			{
				if (endStationToMutationNotPassed)
				{
					endOfLine.push_back(stops[i]);

					if (stops[i] == junctionStations->at(startNodeToMutation + 1))
					{
						endStationToMutationNotPassed = false;
					}
				}

				stops.pop_back();
			}

			stops.pop_back();

			for (int i = 0; i < mutationSubline->size(); i++)
			{
				stops.push_back(mutationSubline->at(i));
			}

			for (int i = endOfLine.size() - 2; i >= 0; i--)
			{
				stops.push_back(endOfLine[i]);
			}

			lineForMutation->setStops(new vector<Station*>(stops));

			if (lineForMutation->containsDuplicates())
			{
				lineForMutation->setStops(new vector<Station*>(backupVector));
			}
			else
			{
				lineForMutation->updateJourneyTimeAndLenght();
			}
		}

		delete mutationSubline;
	}

	delete junctionStations;
}

MutationTechniqueEnum LineMutation::getType()
{
	return MutationTechniqueEnum::LINE;
}
