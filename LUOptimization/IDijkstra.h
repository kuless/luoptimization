#pragma once
#include <vector>
#include "Station.h"
#include "NotImplementedException.h"

class IDijkstra
{
	public:
		IDijkstra();
		virtual ~IDijkstra();
		virtual std::vector<Station*>* dijkstra(Station* start, Station* end);

	private:
};
