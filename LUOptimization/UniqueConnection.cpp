#include "UniqueConnection.h"

using namespace std;

UniqueConnection::UniqueConnection(string firstStationId, string secondStationId, double distance)
{
	if (0 < firstStationId.compare(secondStationId))
	{
		this->firstStationId = firstStationId;
		this->secondStationId = secondStationId;
	}
	else
	{
		this->firstStationId = secondStationId;
		this->secondStationId = firstStationId;
	}

	this->distance = distance;
}

UniqueConnection::~UniqueConnection()
{
}

bool UniqueConnection::operator==(const UniqueConnection* uniqueConnection) const
{
	return _Equal(*uniqueConnection);
}

bool UniqueConnection::operator==(const UniqueConnection& uniqueConnection) const
{
	return _Equal(uniqueConnection);
}

bool UniqueConnection::_Equal(const UniqueConnection& uniqueConnection) const
{
	return firstStationId._Equal(uniqueConnection.firstStationId)
		&& secondStationId._Equal(uniqueConnection.secondStationId)
		&& distance == uniqueConnection.distance;
}

size_t UniqueConnection::getHash() const
{
	size_t res = 17;
	res = res * 31 + hash<string>()(firstStationId);
	res = res * 31 + hash<string>()(secondStationId);
	res = res * 31 + hash<double>()(distance);
	return res;
}

std::string UniqueConnection::getFirstStationId()
{
	return firstStationId;
}

std::string UniqueConnection::getSecondStationId()
{
	return secondStationId;
}

double UniqueConnection::getDistance()
{
	return distance;
}

size_t UniqueConnectionHash::operator()(const UniqueConnection* uc) const
{
	return uc->getHash();
}

size_t UniqueConnectionHash::operator()(const UniqueConnection& uc) const
{
	return uc.getHash();
}

bool UniqueConnectionEqual::operator()(const UniqueConnection* l, const UniqueConnection* r) const
{
	return l->_Equal(*r);
}
