#pragma once
#include "AbstractCrossoverOperator.h"
#include "AbstractNetwork.h"
#include "IRandomNumberGenerator.h"
#include "SingleCriteriaValue.h"
#include "MultiCriteriaValue.h"

template <class T>
class WholeChromosomeCrossover : public AbstractCrossoverOperator<T>
{
	public:
		WholeChromosomeCrossover(IRandomNumberGenerator* random);
		~WholeChromosomeCrossover();
		std::pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* crossover(std::pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* pairToCrossover) override;
		CrossoverTechniqueEnum getType() override;

	private:

};
