#pragma once
#include "NetworkEvaluation.h"
#include "SingleCriteriaValue.h"
#include "SingleCriteriaNetwork.h"

class NetworkEvaluationSingleCriteria: public NetworkEvaluation<SingleCriteriaValue>
{
	public:
		NetworkEvaluationSingleCriteria(std::list<Travel*>* travels, SingleCriteriaNetwork* network);
		~NetworkEvaluationSingleCriteria();

	protected:
		void minuteBeginRating() override;
		void remainingPassengersRating(int numberOfPassengers) override;
		void afterDayRating() override;
		void transferPenaltyRating(int numberOfPassengers) override;
		void trainMoveRating(std::pair<int, int> trainUpdate) override;
		void newTrainsOnTrackRating() override;
};
