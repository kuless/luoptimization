#include "PopulationGenerator.h"

using namespace std;

template <class T>
PopulationGenerator<T>::PopulationGenerator(IRandomNumberGenerator* random, IDijkstra* dijkstra, IDijkstra* quasiShortestPath, AbstractNetworkBuilder<T>* builder, PopulationGenerationTechniqueEnum populationGenerationTechnique)
{
	this->random = random;
	this->dijkstra = dijkstra;
	this->quasiShortestPath = quasiShortestPath;
	this->builder = builder;
	this->populationGenerationTechnique = populationGenerationTechnique;

	populationGenerators.insert(pair<PopulationGenerationTechniqueEnum, AbstractPopulationGenerator<T>*>(getMapValue(new RandomPopulationGenerator<T>(random->copy(), builder->copy(), dijkstra))));
	populationGenerators.insert(pair<PopulationGenerationTechniqueEnum, AbstractPopulationGenerator<T>*>(getMapValue(new ShortDistancePopulationGenerator<T>(random->copy(), builder->copy(), quasiShortestPath))));
	populationGenerators.insert(pair<PopulationGenerationTechniqueEnum, AbstractPopulationGenerator<T>*>(getMapValue(new RgaPopulationGenerator<T>(random->copy(), builder->copy(), dijkstra))));
}

template <class T>
PopulationGenerator<T>::~PopulationGenerator()
{
	delete random;
	delete builder;

	for (typename unordered_map<PopulationGenerationTechniqueEnum, AbstractPopulationGenerator<T>*>::iterator it = this->populationGenerators.begin(); it != this->populationGenerators.end(); ++it)
	{
		delete it->second;
	}
}

template <class T>
vector<AbstractNetwork<T>*>* PopulationGenerator<T>::generatePopulation(vector<Station*>& stations, vector<Travel*>& travels, PopulationGeneratorInputParameters* populationGeneratorInputParameters)
{
	return this->populationGenerators.find(populationGenerationTechnique)->second->generatePopulation(stations, travels, populationGeneratorInputParameters);
}

template<class T>
std::pair<PopulationGenerationTechniqueEnum, AbstractPopulationGenerator<T>*> PopulationGenerator<T>::getMapValue(AbstractPopulationGenerator<T>* populationGenerator)
{
	return pair<PopulationGenerationTechniqueEnum, AbstractPopulationGenerator<T>*>(populationGenerator->getType(), populationGenerator);
}

template class PopulationGenerator<SingleCriteriaValue>;
template class PopulationGenerator<MultiCriteriaValue>;
