#pragma once
#include "AbstractEvaluationOperator.h"
#include "MultiCriteriaValue.h"
#include "NetworkEvaluationMultiCriteria.h"

class EvaluationMultiCriteria: public AbstractEvaluationOperator<MultiCriteriaValue>
{
	public:
		EvaluationMultiCriteria(std::vector<Travel*>* travels);
		~EvaluationMultiCriteria();

	protected:
		NetworkEvaluation<MultiCriteriaValue>* getNetworkEvaluation(std::list<Travel*>* travels, AbstractNetwork<MultiCriteriaValue>* network) override;
		void setValues(AbstractNetwork<MultiCriteriaValue>& network, MultiCriteriaValue& value) override;
};