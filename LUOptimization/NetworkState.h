#pragma once
#include <list>
#include <unordered_map>
#include "Train.h"
#include "Travel.h"

class NetworkState
{
	public:
		NetworkState(std::list<Travel*>* travels, std::list<Train*>* trainsWaitingForDeparture, std::unordered_map<int, std::list<Train*>>* trainsOnTrack);
		~NetworkState();

		std::list<Travel*>* getTravels();
		std::list<Train*>* getTrainsWaitingForDeparture();
		int getNumberOfTrainsOnTrack();
		std::unordered_map<int, std::list<Train*>>* getTrainsOnTrack();
		std::list<Travel*>* getWaitingPassangers();
		long getNumberOfWaitingPassangers();
		long getNotFittedPassangers();

		void setTravels(std::list<Travel*>* travels);
		void setTrainsWaitingForDeparture(std::list<Train*>* trainsWaitingForDeparture);
		void setNumberOfTrainsOnTrack(int numberOfTrainsOnTrack);
		void setTrainsOnTrack(std::unordered_map<int, std::list<Train*>>* trainsOnTrack);
		void setWaitingPassangers(std::list<Travel*>* waitingPassangers);
		void setNotFittedPassangers(long notFittedPassangers);

		void addWaitingPassangers(Travel* waitingPassangers);
		void addWaitingPassangersOnFront(Travel* waitingPassangers);
		void removeWaitingPassangers(Travel* waitingPassangers);
		void addNotFittedPassangers(long notFittedPassangers);
		void incrementNumberOfTrainsOnTrack();
		void reduceNumberOfTrainsOnTrack();

	private:
		std::list<Travel*>* travels;
		std::list<Train*>* trainsWaitingForDeparture;
		int numberOfTrainsOnTrack;
		std::unordered_map<int, std::list<Train*>>* trainsOnTrack;
		std::list<Travel*>* waitingPassangers;
		long numberOfWaitingPassangers;
		long notFittedPassangers;
};