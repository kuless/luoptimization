#include "NetworkEvaluation.h"

using namespace std;

template <class T>
NetworkEvaluation<T>::NetworkEvaluation(list<Travel*>* travels, AbstractNetwork<T>* network)
{
	this->network = network;
	vector<Line*>* lines = network->getLines();
	unordered_map<int, list<Train*>>* trainsOnTrack = new unordered_map<int, list<Train*>>();

	for (int i = 0; i < lines->size(); i++)
	{
		trainsOnTrack->insert(pair<int, list<Train*>>((*lines)[i]->getId(), list<Train*>()));
	}

	state = new NetworkState(travels, getAllTrains(lines), trainsOnTrack);
	value = new T();
}

template <class T>
NetworkEvaluation<T>::~NetworkEvaluation()
{
	delete state;
}

template <class T>
T* NetworkEvaluation<T>::evaluate()
{
	for (int i = 0; evaluationShouldContinue(i); i++)
	{
		minuteBeginRating();
		addStartingTravelPassangers(i);
		updateTrainsStatus();
		addTrainsOnTrack(i);
		updatePassangers();
	}

	afterDayRating();
	deleteRemainingPassengers();
	return value;
}

template<class T>
void NetworkEvaluation<T>::minuteBeginRating()
{
	throw NotImplementedException("minuteBeginRating not implemented");
}

template<class T>
void NetworkEvaluation<T>::remainingPassengersRating(int numberOfPassengers)
{
	throw NotImplementedException("remainingPassengersRating not implemented");
}

template<class T>
void NetworkEvaluation<T>::afterDayRating()
{
	throw NotImplementedException("afterDayRating not implemented");
}

template<class T>
void NetworkEvaluation<T>::transferPenaltyRating(int numberOfPassengers)
{
	throw NotImplementedException("transferPenaltyRating not implemented");
}

template<class T>
void NetworkEvaluation<T>::trainMoveRating(pair<int, int> trainUpdate)
{
	throw NotImplementedException("trainMoveRating not implemented");
}

template<class T>
void NetworkEvaluation<T>::newTrainsOnTrackRating()
{
	throw NotImplementedException("newTrainsOnTrackRating not implemented");
}

template <class T>
void NetworkEvaluation<T>::addStartingTravelPassangers(int minute)
{
	while (!state->getTravels()->empty() && state->getTravels()->front()->getStartInMinutes() == minute)
	{
		Travel* travel = state->getTravels()->front();
		state->getTravels()->pop_front();

		if (travel->getRoutes()->size() == 0)
		{
			increaseValueByTravel(travel);
		}
		else
		{
			state->addWaitingPassangers(travel);
		}
	}
}

template <class T>
list<Train*>* NetworkEvaluation<T>::getAllTrains(vector<Line*>* lines)
{
	list<Train*>* trains = new list<Train*>();

	for (int i = 0; i < lines->size(); i++)
	{
		vector<int> timetable = (*lines)[i]->getTimetableInMinutes();

		for (int k = 0; k < timetable.size(); k++)
		{
			trains->push_back(new Train(TRAIN_CAPASITY, (*lines)[i], timetable[k]));
		}
	}

	trains->sort([](const Train* l, const Train* r) { return *l < *r; });;
	return trains;
}

template <class T>
void NetworkEvaluation<T>::deleteRemainingPassengers()
{
	while (!state->getWaitingPassangers()->empty())
	{
		remainingPassengersRating(state->getWaitingPassangers()->front()->getNumberOfPassengers());
		delete state->getWaitingPassangers()->front();
		state->getWaitingPassangers()->pop_front();
	}
}

template<class T>
bool NetworkEvaluation<T>::evaluationShouldContinue(int dayMinute)
{
	if (dayMinute < DAY_IN_MINUTES)
	{
		return true;
	}
	else if (dayMinute < TWO_DAYS_IN_MINUTES)
	{
		if (state->getNumberOfTrainsOnTrack() != 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		EvaluationFailProtocol<T>* evaluationFailProtocol = new EvaluationFailProtocol<T>(network, state);
		evaluationFailProtocol->saveProtocol();
		delete evaluationFailProtocol;
		return false;
	}
}

template <class T>
void NetworkEvaluation<T>::increaseValueByTravel(Travel* travel)
{
	state->addNotFittedPassangers(travel->getNumberOfPassengers());
	delete travel;
}

template <class T>
void NetworkEvaluation<T>::updateTrainsStatus()
{
	list<pair<int, Train*>>* trainsToDelete = new list<pair<int, Train*>>();

	for (unordered_map<int, list<Train*>>::iterator itTrainsOnLine = state->getTrainsOnTrack()->begin(); itTrainsOnLine != state->getTrainsOnTrack()->end(); ++itTrainsOnLine)
	{
		list<Train*> trainsOnLine = itTrainsOnLine->second;

		for (list<Train*>::iterator itTrains = trainsOnLine.begin(); itTrains != trainsOnLine.end(); ++itTrains)
		{
			Train* train = *itTrains;
			pair<int, int> trainUpdate = train->update();
			trainMoveRating(trainUpdate);

			if (trainUpdate.first != TRAIN_MOVE)
			{
				list<Travel*> exitingPassangers = train->getPassengersExitingTrain();

				for (list<Travel*>::iterator itPassangers = exitingPassangers.begin(); itPassangers != exitingPassangers.end(); ++itPassangers)
				{
					if ((*itPassangers)->getRoutes()->size() == 0)
					{
						delete* itPassangers;
					}
					else
					{
						state->addWaitingPassangers(*itPassangers);
						transferPenaltyRating((*itPassangers)->getNumberOfPassengers());
					}
				}

				if (trainUpdate.first == TRAIN_FINISH)
				{
					(*trainsToDelete).push_back(pair<int, Train*>(itTrainsOnLine->first, train));
				}
			}
		}
	}

	for (list<pair<int, Train*>>::iterator itTrains = (*trainsToDelete).begin(); itTrains != (*trainsToDelete).end(); ++itTrains)
	{
		deleteTrainOnTrack(*itTrains);
	}

	delete trainsToDelete;
}

template <class T>
void NetworkEvaluation<T>::deleteTrainOnTrack(pair<int, Train*> train)
{
	state->getTrainsOnTrack()->find(train.first)->second.remove(train.second);
	delete train.second;
	state->reduceNumberOfTrainsOnTrack();
}

template <class T>
void NetworkEvaluation<T>::addTrainsOnTrack(int minute)
{
	while (!state->getTrainsWaitingForDeparture()->empty() && state->getTrainsWaitingForDeparture()->front()->getDeparture() == minute)
	{
		state->getTrainsOnTrack()->find(state->getTrainsWaitingForDeparture()->front()->getLine()->getId())->second.push_back(state->getTrainsWaitingForDeparture()->front());
		state->getTrainsWaitingForDeparture()->pop_front();
		state->incrementNumberOfTrainsOnTrack();
	}

	newTrainsOnTrackRating();
}

template <class T>
void NetworkEvaluation<T>::updatePassangers()
{
	list<Travel*>* travelsToDelete = new list<Travel*>();
	list<Travel*>* travelsToAdd = new list<Travel*>();

	for (list<Travel*>::iterator itPassangers = state->getWaitingPassangers()->begin(); itPassangers != state->getWaitingPassangers()->end(); ++itPassangers)
	{
		bool passangersNotAdded = true;
		Travel* nextPassanger = (*itPassangers);
		Route* nextPassangerFirstRouteStep = nextPassanger->getRoutes()->front();
		list<Train*> trainsOnPassangersLine = state->getTrainsOnTrack()->find(nextPassangerFirstRouteStep->getLine()->getId())->second;

		for (list<Train*>::iterator itTrains = trainsOnPassangersLine.begin(); passangersNotAdded && itTrains != trainsOnPassangersLine.end(); ++itTrains)
		{
			Train* nextTrain = (*itTrains);

			if (nextTrain->getState() != TRAIN_MOVE && !nextTrain->isFull()
				&& nextPassanger->getStartStation() == nextTrain->getCurrentStation()
				&& nextPassangerFirstRouteStep->getLine() == nextTrain->getLine()
				&& nextPassangerFirstRouteStep->isForwardMovement() == nextTrain->isForwardMovment())
			{
				Travel* notFittedPassangers = nextTrain->addPassangers(nextPassanger);
				travelsToDelete->push_back(nextPassanger);
				passangersNotAdded = false;

				if (notFittedPassangers != nullptr)
				{
					travelsToAdd->push_back(notFittedPassangers);
				}
			}
		}
	}

	for (list<Travel*>::iterator itPassangers = (*travelsToDelete).begin(); itPassangers != (*travelsToDelete).end(); ++itPassangers)
	{
		state->removeWaitingPassangers(*itPassangers);
	}

	for (list<Travel*>::iterator itPassangers = (*travelsToAdd).begin(); itPassangers != (*travelsToAdd).end(); ++itPassangers)
	{
		state->addWaitingPassangersOnFront(*itPassangers);
	}

	delete travelsToDelete;
	delete travelsToAdd;
}

template class NetworkEvaluation<SingleCriteriaValue>;
template class NetworkEvaluation<MultiCriteriaValue>;
