#pragma once
#include <vector>
#include "AbstractNetwork.h"
#include "MultiCriteriaValue.h"

class FrontDomination
{
	public:
		FrontDomination();
		~FrontDomination();

		bool _Equal(FrontDomination& frontDomination);

		std::vector<std::pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>* getDominations();
		int getDominationCounter();

		void addDomination(std::pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>* network);
		void addDominationCounter();
		void reduceDominationCounter();

	private:
		std::vector<std::pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>* dominations;
		int dominationCounter;
};
