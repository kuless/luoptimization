#pragma once
#include <vector>
#include <set>
#include <array>
#include "AbstractPopulationGenerator.h"
#include "IRandomNumberGenerator.h"
#include "IDijkstra.h"
#include "AbstractNetworkBuilder.h"
#include "SingleCriteriaValue.h"
#include "MultiCriteriaValue.h"
#include "Constants.h"

template<class T>
class RandomPopulationGenerator: public AbstractPopulationGenerator<T>
{
	public:
		RandomPopulationGenerator(IRandomNumberGenerator* random, AbstractNetworkBuilder<T>* builder, IDijkstra* dijkstra);
		~RandomPopulationGenerator();

		std::vector<AbstractNetwork<T>*>* generatePopulation(std::vector<Station*>& stations, std::vector<Travel*>& travels, PopulationGeneratorInputParameters* populationGeneratorInputParameters) override;
		PopulationGenerationTechniqueEnum getType() override;

	private:
		IDijkstra* dijkstra;
};
