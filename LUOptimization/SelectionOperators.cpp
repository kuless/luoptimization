#include "SelectionOperators.h"

using namespace std;


template <class T>
SelectionOperators<T>::SelectionOperators(IRandomNumberGenerator* random, bool useRoulette, int selectionsSize)
{
	this->random = random;
	this->useRoulette = useRoulette;
	this->selectionsSize = selectionsSize;
}

template<class T>
SelectionOperators<T>::~SelectionOperators()
{
	delete random;
}

template <class T>
vector<pair<AbstractNetwork<T>*, AbstractNetwork<T>*>*>* SelectionOperators<T>::selection(vector<AbstractNetwork<T>*>* population)
{
	if (this->useRoulette)
	{
		return roulette(population, selectionsSize);
	}
	else
	{
		return tournament(population, selectionsSize);
	}
}

template <class T>
vector<pair<AbstractNetwork<T>*, AbstractNetwork<T>*>*>* SelectionOperators<T>::tournament(vector<AbstractNetwork<T>*>* population, int size)
{
	vector<pair<AbstractNetwork<T>*, AbstractNetwork<T>*>*>* pairs = new vector<pair<AbstractNetwork<T>*, AbstractNetwork<T>*>*>();

	for (int i = 0; i < population->size() / 2; i++)
	{
		vector<AbstractNetwork<T>*> networks1;
		vector<AbstractNetwork<T>*> networks2;

		for (int j = 0; j < size; j++)
		{
			networks1.push_back((*population)[random->getInt(0, (int)population->size())]);
			networks2.push_back((*population)[random->getInt(0, (int)population->size())]);
		}

		sort(networks1.begin(), networks1.end(), [](const AbstractNetwork<T>* lhs, const AbstractNetwork<T>* rhs)
		{
			return *lhs < *rhs;
		});

		sort(networks2.begin(), networks2.end(), [](const AbstractNetwork<T>* lhs, const AbstractNetwork<T>* rhs)
		{
			return *lhs < *rhs;
		});

		pairs->push_back(new pair<AbstractNetwork<T>*, AbstractNetwork<T>*>(networks1[0], networks2[0]));
	}

	return pairs;
}

template <class T>
vector<pair<AbstractNetwork<T>*, AbstractNetwork<T>*>*>* SelectionOperators<T>::roulette(vector<AbstractNetwork<T>*>* population, int size)
{
	vector<pair<AbstractNetwork<T>*, AbstractNetwork<T>*>*>* pairs = new vector<pair<AbstractNetwork<T>*, AbstractNetwork<T>*>*>();

	for (int i = 0; i < population->size() / 2; i++)
	{
		vector<AbstractNetwork<T>*> networks1;
		vector<AbstractNetwork<T>*> networks2;

		for (int j = 0; j < size; j++)
		{
			int firstPorter = random->getInt(0, (int)population->size());
			networks1.push_back((*population)[firstPorter]);
			int secondPorter = random->getInt(0, (int)population->size());
			networks2.push_back((*population)[secondPorter]);
		}


		sort(networks1.begin(), networks1.end(), [](const AbstractNetwork<T>* lhs, const AbstractNetwork<T>* rhs)
		{
			return *lhs > * rhs;
		});

		sort(networks2.begin(), networks2.end(), [](const AbstractNetwork<T>* lhs, const AbstractNetwork<T>* rhs)
		{
			return *lhs > * rhs;
		});

		unsigned long long int* tabFirst = new unsigned long long int[size];
		unsigned long long int* tabSecond = new unsigned long long int[size];
		unsigned long long int first = networks1[0]->getSelectionValue() * 2;
		unsigned long long int second = networks2[0]->getSelectionValue() * 2;
		tabFirst[0] = first;
		tabSecond[0] = second;

		for (int j = 1; j < size; j++)
		{
			first += networks1[j]->getSelectionValue();
			tabFirst[j] = first;
			second += networks2[j]->getSelectionValue();
			tabSecond[j] = second;
		}

		unsigned long long int firstRoulette = random->getUnsignedLongLongInt(0, first);
		unsigned long long int secondRoulette = random->getUnsignedLongLongInt(0, second);
		bool firstSelected = false;
		bool secondSelected = false;
		AbstractNetwork<T>* firstPorter = nullptr;
		AbstractNetwork<T>* secondPorter = nullptr;
		int lastIndex = size - 1;

		for (int j = 0; j < size; j++)
		{
			if (!firstSelected && tabFirst[j] >= firstRoulette)
			{
				firstSelected = true;
				int cellToSelect = lastIndex - j;
				firstPorter = networks1[cellToSelect];
			}

			if (!secondSelected && tabSecond[j] >= secondRoulette)
			{
				secondSelected = true;
				int cellToSelect = lastIndex - j;
				secondPorter = networks2[cellToSelect];
			}
		}

		delete tabFirst;
		delete tabSecond;
		pairs->push_back(new pair<AbstractNetwork<T>*, AbstractNetwork<T>*>(firstPorter, secondPorter));
	}

	return pairs;
}

template class SelectionOperators<SingleCriteriaValue>;
template class SelectionOperators<MultiCriteriaValue>;
