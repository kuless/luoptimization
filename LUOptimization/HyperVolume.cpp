#include "HyperVolume.h"

using namespace std;

HyperVolume::HyperVolume(): AbstractQualityMeasure()
{
}

HyperVolume::~HyperVolume()
{
}

QualityValue* HyperVolume::getQuality(vector<AbstractNetwork<MultiCriteriaValue>*>* population)
{
    boost::multiprecision::uint128_t quality = 0;
    unsigned long long int lastY = ULLONG_MAX;

    for (int i = 0; i < population->size() && population->at(i)->getValue()->getParetoFront() == FIRST_PARETO_FRONT; i++)
    {
        boost::multiprecision::uint128_t a = boost::multiprecision::uint128_t(lastY - population->at(i)->getValue()->getMaintenanceValue());
        lastY = population->at(i)->getValue()->getMaintenanceValue();
        boost::multiprecision::uint128_t b = boost::multiprecision::uint128_t(ULLONG_MAX - population->at(i)->getValue()->getPassengersValue());
        quality += a * b;
    }

    return new QualityValue(quality);
}

QualityEnum HyperVolume::getType()
{
    return QualityEnum::HV;
}
