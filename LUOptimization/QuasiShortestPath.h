#pragma once
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <algorithm>
#include <mutex>
#include "IDijkstra.h"
#include "Constants.h"
#include "DijkstraValue.h"

class QuasiShortestPath : public IDijkstra
{
	public:
		QuasiShortestPath(std::vector<Station*>* stations, IDijkstra* dijkstraImpl);
		~QuasiShortestPath();

		std::vector<Station*>* dijkstra(Station* start, Station* end) override;

	private:
		std::unordered_map<std::string, std::vector<Station*>*>* cache;
		IDijkstra* dijkstraImpl;
		std::vector<Station*>* stations;
		std::mutex* mutexObj;

		void findPathsAndSaveInCache(Station* start, Station* end, Station* notMeasuredForStartStation, std::unordered_set<Station*, StationHash, StationEqual>* lineSet);
		std::vector<Station*>* createQuasiShortestPath(Station* start, Station* end, std::string key);
		std::vector<DijkstraValue>* initValues(Station* start);
		void updateNodes(std::vector<DijkstraValue>* values, DijkstraValue& node, Station* start, Station* notMeasuredForStartStation);
		void saveShortesPath(Station* start, DijkstraValue& node, std::unordered_map<Station*, Station*>& visitedNodes, std::unordered_set<Station*, StationHash, StationEqual>* lineSet);
		void saveToShortResult(std::string key);
		std::vector<Station*>* copy(std::vector<Station*>* element);
		std::unordered_set<Station*, StationHash, StationEqual>* convertLineToSet(std::vector<Station*>* line);
};
