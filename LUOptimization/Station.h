class Connection;

#pragma once
#include <iostream>
#include <vector>
#include <boost/functional/hash.hpp>
#include "Connection.h"

class Station
{
	public:
		Station(std::string id, std::string name, double lat, double lon);
		Station(std::string id, std::string name, double lat, double lon, std::vector<Connection*> connections);
		Station(std::string id, std::string name, double lat, double lon, int NLCCode);
		~Station();

		Station* addConnection(Connection* connection);

		bool containsConnection(Station& station);
		bool _Equal(const Station& station) const;
		size_t getHash() const;

		bool operator==(const Station& station) const;

		std::string getId();
		std::string getName();
		int getNLCCode();
		double getLat();
		double getLon();
		std::vector<Connection*> getConnections();
		Connection* getConnection(std::string& stationId);
		int getJourneyTime(std::string& stationId);

		void setNLCCode(int NLCCode);

	private:
		std::string id;
		std::string name;
		int NLCCode;
		double lat;
		double lon;
		std::vector<Connection*> connections;
};

struct StationHash
{
	public:
		size_t operator()(const Station& n) const;
		size_t operator()(const Station* n) const;
};

struct StationEqual
{
	public:
		bool operator()(const Station* r, const Station* l) const;
};
