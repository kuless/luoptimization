#pragma once
#include "DijkstraValue.h"
#include "Line.h"

class DijkstraValueRoute : public DijkstraValue
{
public:
	DijkstraValueRoute(int value, Station* station, Station* previosStation, int stationNumber, Line* line, Line* previosStationLine, bool forwardMovement);
	~DijkstraValueRoute();

	Line* getLine();
	Line* getPreviosStationLine();
	bool getForwardMovement();
	int getStationNumber();

	void setPreviosStationLine(Line* previosStationLine);
	void setForwardMovement(bool forwardMovement);

private:
	Line* line;
	Line* previosStationLine;
	bool forwardMovement;
	int stationNumber;
};
