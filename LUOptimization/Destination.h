#pragma once
#include <vector>
#include <string>
class Destination
{

	public:
		Destination(int* destinationNLCCode, std::string* destinationStationName, std::vector<int*>* flow);
		~Destination();
		int* getDestiantionNLCCode();
		std::string* getDestinationStationName();
		std::vector<int*>* getFlow();

	private:
		int* destinationNLCCode;
		std::string* destinationStationName;
		std::vector<int*>* flow;
};