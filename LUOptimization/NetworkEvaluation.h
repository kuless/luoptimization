#pragma once
#include <list>
#include <unordered_map>
#include "AbstractNetwork.h"
#include "Train.h"
#include "Travel.h"
#include "NetworkState.h"
#include "SingleCriteriaValue.h"
#include "MultiCriteriaValue.h"
#include "EvaluationFailProtocol.h"
#include "NotImplementedException.h"

template <class T>
class NetworkEvaluation
{
	public:
		NetworkEvaluation(std::list<Travel*>* travels, AbstractNetwork<T>* network);
		virtual ~NetworkEvaluation();

		T* evaluate();

	protected:
		AbstractNetwork<T>* network;
		NetworkState* state;
		T* value;

		virtual void minuteBeginRating();
		virtual void remainingPassengersRating(int numberOfPassengers);
		virtual void afterDayRating();
		virtual void transferPenaltyRating(int numberOfPassengers);
		virtual void trainMoveRating(std::pair<int, int> trainUpdate);
		virtual void newTrainsOnTrackRating();

	private:
		void addStartingTravelPassangers(int minute);
		void increaseValueByTravel(Travel* travel);
		void updateTrainsStatus();
		void addTrainsOnTrack(int minute);
		void updatePassangers();
		void deleteTrainOnTrack(std::pair<int, Train*> train);
		std::list<Train*>* getAllTrains(std::vector<Line*>* lines);
		void deleteRemainingPassengers();
		bool evaluationShouldContinue(int dayMinute);
};
