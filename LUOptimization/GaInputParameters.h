#pragma once
#include <string>
#include <vector>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string.hpp>
#include "InitInputParameters.h"
#include "RuntimeInputParameters.h"
#include "PopulationGeneratorInputParameters.h"
#include "SolutionTypeEnum.h"
#include "Constants.h"

class GaInputParameters
{
	public:
		GaInputParameters(std::string line);
		~GaInputParameters();

		InitInputParameters* getInitInputParameters();
		RuntimeInputParameters* getRuntimeInputParameters();

	private:
		InitInputParameters* initInputParameters;
		RuntimeInputParameters* runtimeInputParameters;
};
