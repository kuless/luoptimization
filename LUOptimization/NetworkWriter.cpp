#include "NetworkWriter.h"

using namespace std;

NetworkWriter::NetworkWriter()
{
}

NetworkWriter::~NetworkWriter()
{
}

void NetworkWriter::saveLineVectorToFile(std::vector<Line*> lineVector, std::string fileName)
{
	ofstream myFile;
	myFile.open(fileName);
	myFile << LINE_FILE_HEADER << endl;

	for (int i = 0; i < lineVector.size(); i++)
	{
		int travelTime = 0;
		Station* lastStop = nullptr;

		for (int k = 0; k < lineVector[i]->getStops().size(); k++)
		{
			if (lastStop != nullptr)
			{
				string lineId = lineVector[i]->getStops()[k]->getId();
				travelTime += lastStop->getJourneyTime(lineId);
			}

			lastStop = lineVector[i]->getStops()[k];

			size_t prefixBegin = lineVector[i]->getStops()[k]->getName().find(STATION_PREFIX);

			myFile << lineVector[i]->getId() << SEPERATOR;
			myFile << lineVector[i]->getName() << SEPERATOR;
			myFile << lineVector[i]->getStops()[k]->getId() << SEPERATOR;
			myFile << travelTime << SEPERATOR << endl;
		}
	}

	myFile.close();
}

void NetworkWriter::saveTimetableToFile(std::vector<Line*> lineVector, std::string fileName)
{
	ofstream myFile;
	myFile.open(fileName);
	myFile << TIMETABLE_FILE_HEADER << endl;

	for (int i = 0; i < lineVector.size(); i++)
	{
		for (int k = 0; k < lineVector[i]->getTimetable().size(); k++)
		{
			myFile << lineVector[i]->getId() << SEPERATOR;
			myFile << lineVector[i]->getTimetable()[k].first << SEPERATOR;
			myFile << lineVector[i]->getTimetable()[k].second << SEPERATOR << endl;
		}
	}

	myFile.close();
}
