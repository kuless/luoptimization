#include "RandomNumberGeneratorImpl.h"

using namespace std;

RandomNumberGeneratorImpl::RandomNumberGeneratorImpl(): IRandomNumberGenerator()
{
	generator = default_random_engine(rd());
}

RandomNumberGeneratorImpl::~RandomNumberGeneratorImpl()
{
}

int RandomNumberGeneratorImpl::getInt(int lowerLimit, int upperLimit)
{
	if (lowerLimit == upperLimit)
	{
		return lowerLimit;
	}
	else
	{
		uniform_int_distribution<int> distribution(lowerLimit, upperLimit - 1);
		return distribution(generator);
	}
}

unsigned long long int RandomNumberGeneratorImpl::getUnsignedLongLongInt(unsigned long long int lowerLimit, unsigned long long int upperLimit)
{
	if (lowerLimit == upperLimit)
	{
		return lowerLimit;
	}
	else
	{
		uniform_int_distribution<unsigned long long int> distribution(lowerLimit, upperLimit - 1);
		return distribution(generator);
	}
}

IRandomNumberGenerator* RandomNumberGeneratorImpl::copy()
{
	return new RandomNumberGeneratorImpl();
}
