#include "ClassicGaWithShortestPathCrossover.h"

using namespace std;

template <class T>
ClassicGaWithShortestPathCrossover<T>::ClassicGaWithShortestPathCrossover(IRandomNumberGenerator* random, IDijkstra* dijkstra): AbstractCrossoverOperator<T>(random)
{
	this->dijkstra = dijkstra;
}

template <class T>
ClassicGaWithShortestPathCrossover<T>::~ClassicGaWithShortestPathCrossover()
{
}

template <class T>
pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* ClassicGaWithShortestPathCrossover<T>::crossover(pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* pairToCrossover)
{
	pair<vector<Line*>*, vector<Line*>*> lines = pair<vector<Line*>*, vector<Line*>*>(new vector<Line*>(), new vector<Line*>());
	int randSize = this->getMinRandSize(pairToCrossover);

	for (int i = 0; i < randSize; i++)
	{
		int iteratorForSecondNetwork = pairToCrossover->second->getLines()->size() - i - 1;
		ClassicGaWithShortestPathCrossover<T>::ClassicGAResult* stationsCrossover = classicGA(pair<Line*, Line*>(pairToCrossover->first->getLines()->at(i), pairToCrossover->second->getLines()->at(iteratorForSecondNetwork)));
		pair<vector<pair<int, int>>, vector<pair<int, int>>>* timetableCrossover
			= this->crossoverTimetable(pairToCrossover->first->getLines()->at(i)->getTimetable(), pairToCrossover->second->getLines()->at(iteratorForSecondNetwork)->getTimetable());
		vector<Station*>* middlePartOfFirstLine = this->dijkstra->dijkstra(stationsCrossover->leftPartOfFirstLine->at(stationsCrossover->leftPartOfFirstLine->size() - 1), stationsCrossover->rightPartOfSecondLine->at(0));
		vector<Station*>* firstLine = stationsCrossover->getFirstLine(middlePartOfFirstLine, pairToCrossover->first->getLines()->at(i));
		delete middlePartOfFirstLine;
		vector<Station*>* middlePartOfSecondLine = this->dijkstra->dijkstra(stationsCrossover->leftPartOfSecondLine->at(stationsCrossover->leftPartOfSecondLine->size() - 1), stationsCrossover->rightPartOfFirstLine->at(0));
		vector<Station*>* secondLine = stationsCrossover->getSecondLine(middlePartOfSecondLine, pairToCrossover->second->getLines()->at(iteratorForSecondNetwork));
		delete middlePartOfSecondLine;
		lines.first->push_back(createLine(i, firstLine, timetableCrossover->first, pairToCrossover->first->getLines()->at(i)));
		lines.second->push_back(createLine(i, secondLine, timetableCrossover->second, pairToCrossover->second->getLines()->at(iteratorForSecondNetwork)));
		delete stationsCrossover;
		delete timetableCrossover;
	}

	for (int i = pairToCrossover->second->getLines()->size(); i < randSize; i++)
	{
		lines.first->push_back(new Line(*pairToCrossover->second->getLines()->at(i)));
		lines.first->at(i)->setId(i);
	}

	for (int i = randSize; i < pairToCrossover->first->getLines()->size(); i++)
	{
		lines.second->push_back(new Line(*pairToCrossover->first->getLines()->at(i)));
		lines.second->at(i)->setId(i);
	}

	return new pair<AbstractNetwork<T>*, AbstractNetwork<T>*>(pairToCrossover->first->callChildConstructor(lines.first), pairToCrossover->first->callChildConstructor(lines.second));
}

template <class T>
CrossoverTechniqueEnum ClassicGaWithShortestPathCrossover<T>::getType()
{
	return CrossoverTechniqueEnum::CLASSIC_GA_WITH_SHORTEST_PATH;
}

template<class T>
typename ClassicGaWithShortestPathCrossover<T>::ClassicGAResult* ClassicGaWithShortestPathCrossover<T>::classicGA(pair<Line*, Line*> pairToCrossover)
{
	ClassicGaWithShortestPathCrossover<T>::ClassicGAResult* result = new ClassicGaWithShortestPathCrossover<T>::ClassicGAResult();
	int firstLineBreakingPoint = this->random->getInt(1, pairToCrossover.first->getStops().size() - 1);
	int secondLineBreakingPoint = this->random->getInt(1, pairToCrossover.second->getStops().size() - 1);

	for (int i = 0; i < firstLineBreakingPoint; i++)
	{
		result->leftPartOfFirstLine->push_back(pairToCrossover.first->getStops()[i]);
	}

	for (int i = firstLineBreakingPoint; i < pairToCrossover.first->getStops().size(); i++)
	{
		result->rightPartOfFirstLine->push_back(pairToCrossover.first->getStops()[i]);
	}

	for (int i = 0; i < secondLineBreakingPoint; i++)
	{
		result->leftPartOfSecondLine->push_back(pairToCrossover.second->getStops()[i]);
	}

	for (int i = secondLineBreakingPoint; i < pairToCrossover.second->getStops().size(); i++)
	{
		result->rightPartOfSecondLine->push_back(pairToCrossover.second->getStops()[i]);
	}

	return result;
}

template<class T>
Line* ClassicGaWithShortestPathCrossover<T>::createLine(int id, vector<Station*>* stops, vector<pair<int, int>>& timetable, Line* orginalLine)
{
	Line* line = new Line(id, "", stops, timetable);

	while (line->containsDuplicates())
	{
		pair<int, int> beginAndEndOfDuplicates = line->getBeginAndEndOfDuplicates();
		vector<Station*>* middlePartOfLine = this->dijkstra->dijkstra(line->getStops().at(beginAndEndOfDuplicates.first), line->getStops().at(beginAndEndOfDuplicates.second));
		vector<Station*>* newStops = new vector<Station*>();

		for (int i = 0; i < beginAndEndOfDuplicates.first; i++)
		{
			newStops->push_back(line->getStops().at(i));
		}

		for (int i = 0; i < middlePartOfLine->size(); i++)
		{
			newStops->push_back(middlePartOfLine->at(i));
		}

		for (int i = beginAndEndOfDuplicates.second + 1; i < line->getStops().size(); i++)
		{
			newStops->push_back(line->getStops().at(i));
		}

		line->setStops(newStops);
		delete middlePartOfLine;
	}

	if (line->getStops().size() <= 1)
	{
		vector<Station*>* newStops = new vector<Station*>();

		for (int i = 0; i < orginalLine->getStops().size(); i++)
		{
			newStops->push_back(orginalLine->getStops()[i]);
		}

		line->setStops(newStops);
	}

	return line;
}

template <class T>
ClassicGaWithShortestPathCrossover<T>::ClassicGAResult::ClassicGAResult()
{
	leftPartOfFirstLine = new vector<Station*>();
	rightPartOfFirstLine = new vector<Station*>();
	leftPartOfSecondLine = new vector<Station*>();
	rightPartOfSecondLine = new vector<Station*>();
}

template <class T>
ClassicGaWithShortestPathCrossover<T>::ClassicGAResult::~ClassicGAResult()
{
	delete leftPartOfFirstLine;
	delete rightPartOfFirstLine;
	delete leftPartOfSecondLine;
	delete rightPartOfSecondLine;
}

template <class T>
vector<Station*>* ClassicGaWithShortestPathCrossover<T>::ClassicGAResult::getFirstLine(vector<Station*>* middlePart, Line* orginalLine)
{
	vector<Station*>* line = new vector<Station*>();

	for (int i = 0; i < leftPartOfFirstLine->size(); i++)
	{
		line->push_back(leftPartOfFirstLine->at(i));
	}

	if (middlePart != nullptr)
	{
		for (int i = 1; i < middlePart->size() - 1; i++)
		{
			line->push_back(middlePart->at(i));
		}
	}

	for (int i = 0; i < rightPartOfSecondLine->size(); i++)
	{
		line->push_back(rightPartOfSecondLine->at(i));
	}

	if (line->size() <= 1)
	{
		delete line;
		line = new vector<Station*>();

		for (int i = 0; i < orginalLine->getStops().size(); i++)
		{
			line->push_back(orginalLine->getStops()[i]);
		}
	}

	return line;
}

template <class T>
vector<Station*>* ClassicGaWithShortestPathCrossover<T>::ClassicGAResult::getSecondLine(vector<Station*>* middlePart, Line* orginalLine)
{
	vector<Station*>* line = new vector<Station*>();

	for (int i = 0; i < leftPartOfSecondLine->size(); i++)
	{
		line->push_back(leftPartOfSecondLine->at(i));
	}

	if (middlePart != nullptr)
	{
		for (int i = 1; i < middlePart->size() - 1; i++)
		{
			line->push_back(middlePart->at(i));
		}
	}

	for (int i = 0; i < rightPartOfFirstLine->size(); i++)
	{
		line->push_back(rightPartOfFirstLine->at(i));
	}

	if (line->size() <= 1)
	{
		delete line;
		line = new vector<Station*>();

		for (int i = 0; i < orginalLine->getStops().size(); i++)
		{
			line->push_back(orginalLine->getStops()[i]);
		}
	}

	return line;
}

template class ClassicGaWithShortestPathCrossover<SingleCriteriaValue>;
template class ClassicGaWithShortestPathCrossover<MultiCriteriaValue>;
