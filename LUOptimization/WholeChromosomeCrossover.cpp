#include "WholeChromosomeCrossover.h"

using namespace std;

template <class T>
WholeChromosomeCrossover<T>::WholeChromosomeCrossover(IRandomNumberGenerator* random): AbstractCrossoverOperator<T>(random)
{
}

template <class T>
WholeChromosomeCrossover<T>::~WholeChromosomeCrossover()
{
}

template <class T>
pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* WholeChromosomeCrossover<T>::crossover(pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* pairToCrossover)
{
	int randSize = this->getMinRandSize(pairToCrossover);

	int crossoverPoint = this->random->getInt(0, randSize);
	vector<Line*>* firstNewGenLines = new vector<Line*>();
	vector<Line*>* secondNewGenLines = new vector<Line*>();

	for (int i = 0; i < pairToCrossover->first->getLines()->size() || i < pairToCrossover->second->getLines()->size(); i++)
	{
		if (i < crossoverPoint)
		{
			firstNewGenLines->push_back(new Line(*(*pairToCrossover->first->getLines())[i]));
			firstNewGenLines->at(i)->setId(i);
			secondNewGenLines->push_back(new Line(*(*pairToCrossover->second->getLines())[i]));
			secondNewGenLines->at(i)->setId(i);
		}
		else
		{
			if (i < pairToCrossover->second->getLines()->size())
			{
				firstNewGenLines->push_back(new Line(*(*pairToCrossover->second->getLines())[i]));
				firstNewGenLines->at(i)->setId(i);
			}

			if (i < pairToCrossover->first->getLines()->size())
			{
				secondNewGenLines->push_back(new Line(*(*pairToCrossover->first->getLines())[i]));
				secondNewGenLines->at(i)->setId(i);
			}
		}
	}

	return new pair<AbstractNetwork<T>*, AbstractNetwork<T>*>(pairToCrossover->first->callChildConstructor(firstNewGenLines), pairToCrossover->first->callChildConstructor(secondNewGenLines));
}

template <class T>
CrossoverTechniqueEnum WholeChromosomeCrossover<T>::getType()
{
	return CrossoverTechniqueEnum::WHOLE_CHROMOSOME;
}

template class WholeChromosomeCrossover<SingleCriteriaValue>;
template class WholeChromosomeCrossover<MultiCriteriaValue>;
