#include "DijkstraImpl.h"

using namespace std;

DijkstraImpl::DijkstraImpl(vector<Station*>* stations): IDijkstra()
{
	this->stations = stations;
	this->cache = new unordered_map<string, vector<Station*>*>();
	this->mutexObj = new mutex();
}

DijkstraImpl::~DijkstraImpl()
{
	for (unordered_map<string, vector<Station*>*>::iterator it = this->cache->begin(); it != this->cache->end(); ++it)
	{
		delete it->second;
	}

	delete this->cache;
	delete mutexObj;
}

vector<Station*>* DijkstraImpl::dijkstra(Station* start, Station* end)
{
	stringstream ss;
	ss << start->getNLCCode() << SEPERATOR << end->getNLCCode();
	this->mutexObj->lock();
	unordered_map<string, vector<Station*>*>::iterator it = this->cache->find(ss.str());
	this->mutexObj->unlock();
	return it == this->cache->end() ? createShortesPath(start, ss.str()) : copy(it->second);
}

vector<Station*>* DijkstraImpl::createShortesPath(Station* start, string key)
{
	vector<DijkstraValue>* values = initValues(start);

	unordered_map<Station*, Station*> visitedNodes;

	while (!values->empty())
	{
		sort(values->begin(), values->end(), [](const DijkstraValue& lhs, const DijkstraValue& rhs)
		{
			return lhs > rhs;
		});

		DijkstraValue node = values->at(values->size() - 1);
		values->pop_back();
		visitedNodes.insert(pair<Station*, Station*>(node.getStation(), node.getPreviosStation()));
		updateNodes(values, node);
		saveShortesPath(start->getNLCCode(), node, visitedNodes);
	}

	delete values;
	this->mutexObj->lock();
	vector<Station*>* result = this->cache->find(key)->second;
	this->mutexObj->unlock();
	return copy(result);
}

vector<DijkstraValue>* DijkstraImpl::initValues(Station* start)
{
	vector<DijkstraValue>* values = new vector<DijkstraValue>();

	for (int i = 0; i < this->stations->size(); i++)
	{
		if (start == this->stations->at(i))
		{
			values->push_back(DijkstraValue(0, this->stations->at(i), nullptr));
		}
		else
		{
			values->push_back(DijkstraValue(INT_MAX, this->stations->at(i), nullptr));
		}
	}

	return values;
}

void DijkstraImpl::updateNodes(vector<DijkstraValue>* values, DijkstraValue& node)
{
	for (int i = 0; i < values->size(); i++)
	{
		string stationId = values->at(i).getStation()->getId();
		int distance = node.getStation()->getJourneyTime(stationId);

		if (distance != -1 && values->at(i).getValue() > node.getValue() + distance)
		{
			values->at(i).setPreviosStation(node.getStation());
			values->at(i).setValue(node.getValue() + distance);
		}
	}
}

void DijkstraImpl::saveShortesPath(int nlcCode, DijkstraValue& node, unordered_map<Station*, Station*>& visitedNodes)
{
	stringstream cashKey;
	cashKey << nlcCode << SEPERATOR << node.getStation()->getNLCCode();
	vector<Station*>* shortesPath = new vector<Station*>();
	shortesPath->push_back(node.getStation());

	for (Station* helper = node.getPreviosStation(); helper != nullptr; helper = visitedNodes.find(helper)->second)
	{
		shortesPath->push_back(helper);
	}

	reverse(shortesPath->begin(), shortesPath->end());
	this->mutexObj->lock();
	pair<unordered_map<string, vector<Station*>*>::iterator, bool> elementAdded = this->cache->insert(pair<string, vector<Station*>*>(cashKey.str(), shortesPath));
	this->mutexObj->unlock();

	if (!elementAdded.second)
	{
		delete shortesPath;
	}
}

vector<Station*>* DijkstraImpl::copy(vector<Station*>* element)
{
	vector<Station*>* copy = new vector<Station*>();

	for (int i = 0; i < element->size(); i++)
	{
		copy->push_back(element->at(i));
	}

	return copy;
}
