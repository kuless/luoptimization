#include "ParetoFront.h"

using namespace std;

ParetoFront::ParetoFront()
{
	front = new vector<pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>();
}

ParetoFront::ParetoFront(vector<AbstractNetwork<MultiCriteriaValue>*>* population)
{
	front = new vector<pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>();

	for (vector<AbstractNetwork<MultiCriteriaValue>*>::iterator it = population->begin(); it != population->end(); ++it)
	{
		front->push_back(new pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>(*it, new FrontDomination()));
	}
}

ParetoFront::~ParetoFront()
{
	for (vector<pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>::iterator it = front->begin(); it != front->end(); ++it)
	{
		delete (*it)->second;
		delete *it;
	}

	delete front;
}

bool ParetoFront::_Equal(ParetoFront& paretoFront)
{
	if (front->size() != paretoFront.front->size())
	{
		return false;
	}

	for (int i = 0; i < front->size(); i++)
	{
		if (!(front->at(i)->first->getValue()->_Equal(*paretoFront.front->at(i)->first->getValue())
			&& front->at(i)->second->_Equal(*paretoFront.front->at(i)->second)))
		{
			return false;
		}
	}

	return true;
}

vector<pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>* ParetoFront::getFront()
{
	return front;
}

void ParetoFront::addNetwork(pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>* network)
{
	front->push_back(network);
}

bool ParetoFront::empty()
{
	return front->empty();
}

vector<pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>::iterator ParetoFront::begin()
{
	return front->begin();
}

vector<pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>::iterator ParetoFront::end()
{
	return front->end();
}

void ParetoFront::sortFront()
{
	sort(front->begin(), front->end(), [](pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>* l, pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>* r)
	{
		return l->first->getValue()->getParetoFront() < r->first->getValue()->getParetoFront() ||
			(l->first->getValue()->getParetoFront() == r->first->getValue()->getParetoFront() && l->first->getValue()->getDistance() > r->first->getValue()->getDistance());
	});
}

void ParetoFront::clearFront()
{
	front->clear();
}
