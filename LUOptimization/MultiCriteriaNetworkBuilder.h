#pragma once
#include "AbstractNetworkBuilder.h"
#include "MultiCriteriaNetwork.h"

class MultiCriteriaNetworkBuilder : public AbstractNetworkBuilder<MultiCriteriaValue>
{
	public:
		MultiCriteriaNetworkBuilder();
		~MultiCriteriaNetworkBuilder();

		AbstractNetwork<MultiCriteriaValue>* createNewNetwork(std::vector<Line*>* lines) override;
		AbstractNetworkBuilder<MultiCriteriaValue>* copy() override;
};
