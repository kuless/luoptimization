#include "QualityMeasuresImpl.h"

using namespace std;

QualityMeasuresImpl::QualityMeasuresImpl(vector<MultiCriteriaValue*>* trueParetoFornt)
{
	qualityMeasures.insert(getMapValue(new HyperVolume()));
	qualityMeasures.insert(getMapValue(new InvertedGenerationalDistance(trueParetoFornt)));
}

QualityMeasuresImpl::~QualityMeasuresImpl()
{
	for (unordered_map<QualityEnum, AbstractQualityMeasure*>::iterator it = this->qualityMeasures.begin(); it != this->qualityMeasures.end(); ++it)
	{
		delete it->second;
	}
}

QualityValue* QualityMeasuresImpl::getQuality(vector<AbstractNetwork<MultiCriteriaValue>*>* population, QualityEnum qualityType)
{
	return qualityMeasures.find(qualityType)->second->getQuality(population);
}

pair<QualityEnum, AbstractQualityMeasure*> QualityMeasuresImpl::getMapValue(AbstractQualityMeasure* qualityMeasure)
{
	return pair<QualityEnum, AbstractQualityMeasure*>(qualityMeasure->getType(), qualityMeasure);
}
