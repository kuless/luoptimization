#include "FrontDomination.h"

using namespace std;

FrontDomination::FrontDomination()
{
	dominations = new vector<pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>();
	dominationCounter = INIT_VALUE;
}

FrontDomination::~FrontDomination()
{
	delete dominations;
}

bool FrontDomination::_Equal(FrontDomination& frontDomination)
{
	if (dominationCounter != frontDomination.dominationCounter || dominations->size() != frontDomination.dominations->size())
	{
		return false;
	}

	for (int i = 0; i < dominations->size(); i++)
	{
		if (dominations->at(i)->first->_Equal(*frontDomination.dominations->at(i)->first))
		{
			return false;
		}
	}

	return true;
}

vector<pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>*>* FrontDomination::getDominations()
{
	return dominations;
}

int FrontDomination::getDominationCounter()
{
	return dominationCounter;
}

void FrontDomination::addDomination(pair<AbstractNetwork<MultiCriteriaValue>*, FrontDomination*>* network)
{
	dominations->push_back(network);
}

void FrontDomination::addDominationCounter()
{
	dominationCounter++;
}

void FrontDomination::reduceDominationCounter()
{
	dominationCounter--;
}
