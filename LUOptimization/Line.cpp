#include "Line.h"

using namespace std;

Line::Line()
{
	id = 0;
	stops = new vector<Station*>();
}

Line::Line(Line& line)
{
	id = line.id;
	name = line.name;
	stops = new vector<Station*>();

	for (int i = 0; i < line.stops->size(); i++)
	{
		stops->push_back(line.stops->at(i));
	}

	journeyTime = line.journeyTime;
	lenght = line.lenght;
	timetable = line.timetable;
}

Line::Line(int id, string name, vector<Station*>* stops)
{
	this->id = id;
	this->name = name;
	this->stops = stops;
	updateJourneyTimeAndLenght();
}

Line::Line(int id, string name, vector<Station*>* stops, vector<pair<int, int>>& timetable)
{
	this->id = id;
	this->name = name;
	this->stops = stops;
	this->timetable = convertTimetable(timetable);
	updateJourneyTimeAndLenght();
}

Line::Line(int id, string name, vector<Station*>* stops, vector<int>& timetable)
{
	this->id = id;
	this->name = name;
	this->stops = stops;
	this->timetable = timetable;
	updateJourneyTimeAndLenght();
}

Line::Line(int id, string name, vector<Station*>* stops, vector<int>& journeyTime, vector<pair<int, int>>& timetable)
{
	this->id = id;
	this->name = name;
	this->stops = stops;
	this->journeyTime = journeyTime;
	this->timetable = convertTimetable(timetable);
}

Line::~Line()
{
	delete stops;
}

bool Line::_Equal(Line& line)
{
	if (id == line.id && name._Equal(line.name))
	{
		return _EqualStopsAndTimetable(line);
	}
	else
	{
		return false;
	}
}

bool Line::_EqualStopsAndTimetable(Line& line)
{
	if (stops->size() == line.stops->size() && journeyTime.size() == line.journeyTime.size() && timetable.size() == line.timetable.size())
	{
		for (int i = 0; i < line.stops->size(); i++)
		{
			if (stops->at(i) != line.stops->at(i))
			{
				return false;
			}
		}

		for (int i = 0; i < journeyTime.size(); i++)
		{
			if (journeyTime[i] != line.journeyTime[i])
			{
				return false;
			}
		}

		for (int i = 0; i < timetable.size(); i++)
		{
			if (timetable[i] != line.timetable[i])
			{
				return false;
			}
		}

		return true;
	}
	else
	{
		return false;
	}
}

void Line::addStop(Station* station, int journeyTime)
{
	stops->push_back(station);
	this->journeyTime.push_back(journeyTime);
}

void Line::deleteLastStop()
{
	if (stops->size() != 0)
	{
		stops->pop_back();
	}
	
	if (journeyTime.size() != 0)
	{
		journeyTime.pop_back();
	}
}

bool Line::cointainsStop(Station* station)
{
	for (int i = 0; i < stops->size(); i++)
	{
		if (stops->at(i)->_Equal(*station))
		{
			return true;
		}
	}

	return false;
}

bool Line::containsDuplicates()
{
	unordered_set<Station*, StationHash, StationEqual> elements = unordered_set<Station*, StationHash, StationEqual>();

	for (int i = 0; i < stops->size(); i++)
	{
		pair<unordered_set<Station*>::iterator, bool> elementAdded = elements.insert(stops->at(i));

		if (!elementAdded.second)
		{
			return true;
		}
	}

	return false;
}

pair<int, int> Line::getBeginAndEndOfDuplicates()
{
	unordered_map<Station*, int, StationHash, StationEqual> allDuplicants;
	pair<int, int> result = pair<int, int>(INT_MAX, INT_MAX);

	for (int i = 0; i < stops->size(); i++)
	{
		unordered_map<Station*, int, StationHash, StationEqual>::iterator station = allDuplicants.find(stops->at(i));

		if (station == allDuplicants.end())
		{
			allDuplicants.insert(pair<Station*, int>(stops->at(i), i));
		}
		else
		{
			if (station->second < result.first)
			{
				result.first = station->second;
			}

			result.second = i;
		}
	}

	return result;
}

Station* Line::getNextStation(Station* station)
{
	for (int i = 0; i < stops->size(); i++)
	{
		if (stops->at(i) == station && i + 1 != stops->size())
		{
			return stops->at(i + 1);
		}
	}

	return nullptr;
}

vector<Station*>* Line::getJunctionStations()
{
	vector<Station*>* result = new vector<Station*>();

	for (int i = 0; i < stops->size(); i++)
	{
		if (stops->at(i)->getConnections().size() > 2)
		{
			result->push_back(stops->at(i));
		}
	}

	return result;
}

void Line::updateJourneyTimeAndLenght()
{
	vector<int> journeyTime = vector<int>();
	lenght = 0.0;

	for (int i = 0; i < stops->size() - 1; i++)
	{
		string nextStationId = stops->at(i + 1)->getId();
		Connection* connection = stops->at(i)->getConnection(nextStationId);
		
		if (connection == nullptr)
		{
			//TODO: add logging
			if (!stops->at(i)->getId()._Equal(nextStationId))
			{
				cout << "Error on updateJourneyTimeAndLenght in Line: " << id << " connection beetween start station: " << stops->at(i)->getId() << " and destination station: " << nextStationId << " does not exist" << endl;
			}
			journeyTime.push_back(1);
			lenght += 1.0;
		}
		else
		{
			journeyTime.push_back(connection->getJourneyTime());
			lenght += connection->getDistance();
		}
	}

	this->journeyTime = journeyTime;
}

int Line::getId()
{
	return id;
}

string Line::getName()
{
	return name;
}

vector<Station*> Line::getStops()
{
	return *stops;
}

vector<int> Line::getJourneyTime()
{
	return journeyTime;
}

vector<pair<int, int>> Line::getTimetable()
{
	return convertTimetable(timetable);
}

vector<int> Line::getTimetableInMinutes()
{
	return timetable;
}

double Line::getLenght()
{
	return lenght;
}

void Line::setId(int id)
{
	this->id = id;
}

void Line::setStops(vector<Station*>* stops)
{
	delete this->stops;
	this->stops = stops;
	updateJourneyTimeAndLenght();
}

void Line::setJourneyTime(vector<int> journeyTime)
{
	this->journeyTime = journeyTime;
}

void Line::setTimetable(vector<pair<int, int>>& timetable)
{
	this->timetable = convertTimetable(timetable);
}

void Line::setTimetable(vector<int>& timetable)
{
	this->timetable = timetable;
}

void Line::removeTimetableElement(int element)
{
	if (element < timetable.size())
	{
		timetable.erase(timetable.begin() + element);
	}
}

vector<pair<int, int>> Line::convertTimetable(vector<int> timetable)
{
	vector<pair<int, int>> convertedTimetable = vector<pair<int, int>>();

	for (int i = 0; i < timetable.size(); i++)
	{
		convertedTimetable.push_back(pair<int, int>(timetable[i] / HOUR_IN_MINUTES, timetable[i] % HOUR_IN_MINUTES));
	}

	return convertedTimetable;
}

vector<int> Line::convertTimetable(vector<pair<int, int>> timetable)
{
	vector<int> convertedTimetable = vector<int>();

	for (int i = 0; i < timetable.size(); i++)
	{
		convertedTimetable.push_back(timetable[i].first * HOUR_IN_MINUTES + timetable[i].second);
	}

	return convertedTimetable;
}
