#pragma once
#include <string>

class UniqueConnection
{
	public:
		UniqueConnection(std::string firstStationId, std::string secondStationId, double distance);
		~UniqueConnection();
		
		bool operator==(const UniqueConnection* uniqueConnection) const;
		bool operator==(const UniqueConnection& uniqueConnection) const;
		bool _Equal(const UniqueConnection& uniqueConnection) const;
		size_t getHash() const;

		std::string getFirstStationId();
		std::string getSecondStationId();
		double getDistance();

	private:
		std::string firstStationId;
		std::string secondStationId;
		double distance;
};

struct UniqueConnectionHash
{
	public:
		size_t operator()(const UniqueConnection* uc) const;
		size_t operator()(const UniqueConnection& uc) const;
};


struct UniqueConnectionEqual
{
	public:
		bool operator()(const UniqueConnection* l, const UniqueConnection* r) const;
};