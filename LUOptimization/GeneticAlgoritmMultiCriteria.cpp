#include "GeneticAlgoritmMultiCriteria.h"

using namespace std;

GeneticAlgoritmMultiCriteria::GeneticAlgoritmMultiCriteria(int id, vector<Station*>* stationsVector, vector<Travel*>* travels, GaInputParameters* gaInputParameters, GaCommonComponents<MultiCriteriaValue>* gaCommonComponents, vector<MultiCriteriaValue*>* trueParetoFront): GeneticAlgoritm<MultiCriteriaValue>(id, stationsVector, travels, gaInputParameters, gaCommonComponents)
{
	this->selectionOperators = new SelectionOperators<MultiCriteriaValue>(this->random->copy(), gaInputParameters->getInitInputParameters()->getRouletteSelection(), gaInputParameters->getInitInputParameters()->getSelectionsSize());
	this->crossoverOperators = new CrossoverOperators<MultiCriteriaValue>(this->random->copy(), gaInputParameters->getInitInputParameters()->getCrossoverTechnique(), gaInputParameters->getInitInputParameters()->getCrossoverProbability(), dijkstra);
	this->mutationOperators = new MutationOperators<MultiCriteriaValue>(this->random->copy(), quasiShortestPath, gaInputParameters->getInitInputParameters()->getMutationTechnique(), gaInputParameters->getInitInputParameters()->getMutationProbability());
	this->resultsWriter = new ResultWriterMultiCriteria(id, gaInputParameters->getInitInputParameters()->getOutputFile(), trueParetoFront, gaInputParameters->getInitInputParameters()->getQualityTypes(), gaInputParameters->getInitInputParameters()->getSaveValuesForTpf());
	this->nsgaii = new Nsgaii();
	this->oldPopulation = new vector<AbstractNetwork<MultiCriteriaValue>*>();
}

GeneticAlgoritmMultiCriteria::~GeneticAlgoritmMultiCriteria()
{
	for (vector<AbstractNetwork<MultiCriteriaValue>*>::iterator it = oldPopulation->begin(); it != oldPopulation->end(); ++it)
	{
		delete (*it);
	}

	delete oldPopulation;
	delete nsgaii;
}

PopulationGenerator<MultiCriteriaValue>* GeneticAlgoritmMultiCriteria::initPopulationGenerator(IDijkstra* dijkstra, IDijkstra* quasiShortestPath, PopulationGenerationTechniqueEnum populationGenerationTechnique)
{
	return new PopulationGenerator<MultiCriteriaValue>(this->random->copy(), dijkstra, quasiShortestPath, new MultiCriteriaNetworkBuilder(), populationGenerationTechnique);
}

void GeneticAlgoritmMultiCriteria::firstEvaluation(int numberOfThreads)
{
	evaluate(numberOfThreads);
	cout << id << " evaluated" << endl;
	nsgaii->evaluate(population);
	cout << id << " nsgaii evaluated" << endl;
}

void GeneticAlgoritmMultiCriteria::nextEvaluations(int numberOfThreads)
{
	evaluate(numberOfThreads);
	cout << id << " evaluate done" << endl;
	evaluateNsgaii();
	cout << id << " nsgaii evaluate done" << endl;
}

void GeneticAlgoritmMultiCriteria::setNewPopulation(vector<AbstractNetwork<MultiCriteriaValue>*>* newPopulation)
{
	for (vector<AbstractNetwork<MultiCriteriaValue>*>::iterator it = oldPopulation->begin(); it != oldPopulation->end(); ++it)
	{
		delete (*it);
	}

	delete oldPopulation;
	oldPopulation = population;
	population = newPopulation;
}

void GeneticAlgoritmMultiCriteria::evaluateNsgaii()
{
	vector<AbstractNetwork<MultiCriteriaValue>*>* newPopultaion = nsgaii->evaluate(oldPopulation, population);
	for (vector<AbstractNetwork<MultiCriteriaValue>*>::iterator it = population->begin(); it != population->end(); ++it)
	{
		delete (*it);
	}

	delete population;
	population = newPopultaion;
}
