#include "ShortDistancePopulationGenerator.h"

using namespace std;

template<class T>
ShortDistancePopulationGenerator<T>::ShortDistancePopulationGenerator(IRandomNumberGenerator* random, AbstractNetworkBuilder<T>* builder, IDijkstra* quasiShortestPath): AbstractPopulationGenerator<T>(random, builder)
{
	this->quasiShortestPath = quasiShortestPath;
}

template<class T>
ShortDistancePopulationGenerator<T>::~ShortDistancePopulationGenerator()
{
}

template<class T>
vector<AbstractNetwork<T>*>* ShortDistancePopulationGenerator<T>::generatePopulation(vector<Station*>& stations, vector<Travel*>& travels, PopulationGeneratorInputParameters* populationGeneratorInputParameters)
{
	vector<AbstractNetwork<T>*>* population = new vector<AbstractNetwork<T>*>();

	for (int i = 0; i < populationGeneratorInputParameters->getPopulationSize(); i++)
	{
		vector<Line*>* lines = new vector<Line*>();

		for (int k = 0; k < populationGeneratorInputParameters->getNumberOfLines(); k++)
		{
			lines->push_back(generateLine(stations, k + 1));
		}

		int lineNumberIndex = populationGeneratorInputParameters->getNumberOfLines();

		while (!checkIfPercentOfStationsCovered(lines, populationGeneratorInputParameters->getPercentOfCoveredStations(), stations.size()))
		{
			findAndDeleteShortestLine(lines);
			lineNumberIndex++;
			lines->push_back(generateLine(stations, lineNumberIndex));
			lineNumberIndex++;
			lines->push_back(generateLine(stations, lineNumberIndex));
		}

		population->push_back(this->builder->createNewNetwork(lines));
	}

	return population;
}

template<class T>
PopulationGenerationTechniqueEnum ShortDistancePopulationGenerator<T>::getType()
{
	return PopulationGenerationTechniqueEnum::SHORT_DISTANCE;
}

template<class T>
Line* ShortDistancePopulationGenerator<T>::generateLine(vector<Station*>& stations, int lineNumber)
{
	vector<Station*>* stops = new vector<Station*>();

	while (stops->empty())
	{
		int startStation = this->random->getInt(0, stations.size());
		int endStation = startStation;

		while (endStation == startStation)
		{
			endStation = this->random->getInt(0, stations.size());
		}

		delete stops;
		stops = this->quasiShortestPath->dijkstra(stations[startStation], stations[endStation]);
	}

	Line* newLine = new Line(lineNumber, "", stops);
	this->generateTimetableForNewLine(newLine);
	return newLine;
}

template<class T>
void ShortDistancePopulationGenerator<T>::findAndDeleteShortestLine(vector<Line*>* lines)
{
	vector<Line*>::iterator shortestLineIndex;
	int shortestLineLength = INT_MAX;

	for (vector<Line*>::iterator it = lines->begin(); it != lines->end(); ++it)
	{
		if ((*it)->getStops().size() < shortestLineLength)
		{
			shortestLineIndex = it;
			shortestLineLength = (*it)->getStops().size();
		}
	}

	delete *shortestLineIndex;
	lines->erase(shortestLineIndex);
}

template<class T>
bool ShortDistancePopulationGenerator<T>::checkIfPercentOfStationsCovered(vector<Line*>* lines, int percentOfCoveredStations, int stationsInNetwork)
{
	unordered_set<Station*, StationHash, StationEqual> elements = unordered_set<Station*, StationHash, StationEqual>();

	for (int i = 0; i < lines->size(); i++)
	{
		Line* line = lines->at(i);

		for (int k = 0; k < line->getStops().size(); k++)
		{
			elements.insert(line->getStops()[k]);
		}
	}

	return elements.size() * 100 / stationsInNetwork >= percentOfCoveredStations;
}

template class ShortDistancePopulationGenerator<SingleCriteriaValue>;
template class ShortDistancePopulationGenerator<MultiCriteriaValue>;
