#include "AllLinesCrossover.h"

using namespace std;

template<class T>
AllLinesCrossover<T>::AllLinesCrossover(IRandomNumberGenerator* random): AbstractCrossoverOperator<T>(random)
{
}

template<class T>
AllLinesCrossover<T>::~AllLinesCrossover()
{
}

template<class T>
pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* AllLinesCrossover<T>::crossover(pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* pairToCrossover)
{
	int randSize = this->getRandSize(pairToCrossover);

	vector<Line*>* firstNewGenLines = new vector<Line*>();
	vector<Line*>* secondNewGenLines = new vector<Line*>();
	int numberOfLinesMuteted = this->random->getInt(0, randSize + 1);

	for (int g = 0; g < numberOfLinesMuteted; g++)
	{
		Line* firstGenLine = (*pairToCrossover->first->getLines())[this->random->getInt(0, (int)pairToCrossover->first->getLines()->size())];
		Line* secondGenLine = (*pairToCrossover->second->getLines())[this->random->getInt(0, (int)pairToCrossover->second->getLines()->size())];
		vector<Station*>* firstGenLineJunctionStations = firstGenLine->getJunctionStations();
		vector<Station*>* secondGenLineJucntionStations = secondGenLine->getJunctionStations();
		vector<int> commonJunctions = getCommonJunctions(firstGenLineJunctionStations, secondGenLineJucntionStations);
		delete secondGenLineJucntionStations;
		pair<vector<pair<int, int>>, vector<pair<int, int>>>* newTimetables
			= this->crossoverTimetable(firstGenLine->getTimetable(), secondGenLine->getTimetable());

		if (commonJunctions.size() > 0)
		{
			vector<Station*>* firstLineStationsSequence = new vector<Station*>();
			vector<Station*>* secondLineStationsSequence = new vector<Station*>();
			Station* crossoverStation = (*firstGenLineJunctionStations)[commonJunctions[this->random->getInt(0, (int)commonJunctions.size())]];
			bool crossoverStationMatchedInFirstStation = false;
			bool crossoverStationMatchedInSecondStation = false;
			int firstGenIterator = 0;
			int secondGenIterator = 0;

			while (firstGenIterator < firstGenLine->getStops().size() || secondGenIterator < secondGenLine->getStops().size())
			{
				if (crossoverStationMatchedInFirstStation && crossoverStationMatchedInSecondStation)
				{
					if (secondGenIterator < secondGenLine->getStops().size())
					{
						firstLineStationsSequence->push_back(secondGenLine->getStops()[secondGenIterator]);
						secondGenIterator++;
					}

					if (firstGenIterator < firstGenLine->getStops().size())
					{
						secondLineStationsSequence->push_back(firstGenLine->getStops()[firstGenIterator]);
						firstGenIterator++;
					}
				}

				if (!crossoverStationMatchedInFirstStation)
				{
					if (firstGenLine->getStops()[firstGenIterator] == crossoverStation)
					{
						crossoverStationMatchedInFirstStation = true;
					}

					firstLineStationsSequence->push_back(firstGenLine->getStops()[firstGenIterator]);
					firstGenIterator++;
				}

				if (!crossoverStationMatchedInSecondStation)
				{
					if (secondGenLine->getStops()[secondGenIterator] == crossoverStation)
					{
						crossoverStationMatchedInSecondStation = true;
					}

					secondLineStationsSequence->push_back(secondGenLine->getStops()[secondGenIterator]);
					secondGenIterator++;
				}
			}

			Line* firstNewLine = new Line(g, "", firstLineStationsSequence, newTimetables->first);
			Line* secondNewLine = new Line(g, "", secondLineStationsSequence, newTimetables->second);

			if (firstLineStationsSequence->size() < 2 || secondLineStationsSequence->size() < 2
				|| firstNewLine->containsDuplicates() || secondNewLine->containsDuplicates())
			{
				delete firstNewLine;
				delete secondNewLine;
				Line* firstLineCopy = new Line(*firstGenLine);
				firstLineCopy->setId(g);
				firstLineCopy->setTimetable(newTimetables->first);
				firstNewGenLines->push_back(firstLineCopy);
				Line* secondLineCopy = new Line(*secondGenLine);
				secondLineCopy->setId(g);
				secondLineCopy->setTimetable(newTimetables->second);
				secondNewGenLines->push_back(secondLineCopy);
			}
			else
			{
				firstNewGenLines->push_back(firstNewLine);
				secondNewGenLines->push_back(secondNewLine);
			}
		}
		else
		{
			Line* firstLineCopy = new Line(*firstGenLine);
			firstLineCopy->setId(g);
			firstLineCopy->setTimetable(newTimetables->first);
			firstNewGenLines->push_back(firstLineCopy);
			Line* secondLineCopy = new Line(*secondGenLine);
			secondLineCopy->setId(g);
			secondLineCopy->setTimetable(newTimetables->second);
			secondNewGenLines->push_back(secondLineCopy);
		}

		delete newTimetables;
		delete firstGenLineJunctionStations;
	}

	for (int g = numberOfLinesMuteted; g < randSize; g++)
	{
		Line* firstGenLine = (*pairToCrossover->first->getLines())[this->random->getInt(0, (int)pairToCrossover->first->getLines()->size())];
		Line* secondGenLine = (*pairToCrossover->second->getLines())[this->random->getInt(0, (int)pairToCrossover->second->getLines()->size())];
		Line* firstLineCopy = new Line(*firstGenLine);
		firstLineCopy->setId(g);
		firstNewGenLines->push_back(firstLineCopy);
		Line* secondLineCopy = new Line(*secondGenLine);
		secondLineCopy->setId(g);
		secondNewGenLines->push_back(secondLineCopy);
		g++;

		if (g < randSize)
		{
			Line* firstGenLine = (*pairToCrossover->first->getLines())[this->random->getInt(0, (int)pairToCrossover->first->getLines()->size())];
			Line* secondGenLine = (*pairToCrossover->second->getLines())[this->random->getInt(0, (int)pairToCrossover->second->getLines()->size())];
			Line* firstLineCopy = new Line(*firstGenLine);
			firstLineCopy->setId(g);
			secondNewGenLines->push_back(firstLineCopy);
			Line* secondLineCopy = new Line(*secondGenLine);
			secondLineCopy->setId(g);
			firstNewGenLines->push_back(secondLineCopy);
		}
	}

	return new pair<AbstractNetwork<T>*, AbstractNetwork<T>*>(pairToCrossover->first->callChildConstructor(firstNewGenLines), pairToCrossover->first->callChildConstructor(secondNewGenLines));
}

template<class T>
CrossoverTechniqueEnum AllLinesCrossover<T>::getType()
{
	return CrossoverTechniqueEnum::ALL_LINES;
}

template<class T>
std::vector<int> AllLinesCrossover<T>::getCommonJunctions(std::vector<Station*>* firstGenLineJunctionStations, std::vector<Station*>* secondGenLineJucntionStations)
{
	vector<int> commonJunctions;

	for (int k = 0; k < firstGenLineJunctionStations->size(); k++)
	{
		for (int n = 0; n < secondGenLineJucntionStations->size(); n++)
		{
			if ((*firstGenLineJunctionStations)[k] == (*secondGenLineJucntionStations)[n])
			{
				commonJunctions.push_back(k);
			}
		}
	}

	return commonJunctions;
}

template class AllLinesCrossover<SingleCriteriaValue>;
template class AllLinesCrossover<MultiCriteriaValue>;
