#include "RouteCacheValue.h"

using namespace std;

RouteCacheValue::RouteCacheValue(list<Route*>* route, int travelTime)
{
	this->route = route;
	this->travelTime = travelTime;
}

RouteCacheValue::~RouteCacheValue()
{
	for (list<Route*>::iterator it = route->begin(); it != route->end(); ++it)
	{
		delete* it;
	}

	delete route;
}

list<Route*>* RouteCacheValue::getRoute()
{
	return route;
}

int RouteCacheValue::getTravelTime()
{
	return travelTime;
}

void RouteCacheValue::setRoute(list<Route*>* route)
{
	this->route = route;
}

void RouteCacheValue::setTravelTime(int travelTime)
{
	this->travelTime = travelTime;
}
