#pragma once
#include "Constants.h"

class SingleCriteriaValue
{
	public:
		SingleCriteriaValue();
		SingleCriteriaValue(const SingleCriteriaValue& singleCriteriaValue);
		SingleCriteriaValue(unsigned long long int value, int notFittedPassangers, int numberOfCrossingTrainsThreshold);
		~SingleCriteriaValue();

		bool operator>(const SingleCriteriaValue& singleCriteriaValue) const;
		bool operator<(const SingleCriteriaValue& singleCriteriaValue) const;

		void setValue(unsigned long long int value);
		void setNotFittedPassangers(long notFittedPassangers);
		void setNumberOfCrossingTrainsThreshold(long numberOfCrossingTrainsThreshold);

		unsigned long long int getValue();
		long getNotFittedPassangers();
		long getNumberOfCrossingTrainsThreshold();

		void addValue(unsigned long long int value);
		void addNumberOfCrossingTrainsThreshold();

	private:
		unsigned long long int value;
		long notFittedPassangers;
		long numberOfCrossingTrainsThreshold;
};
