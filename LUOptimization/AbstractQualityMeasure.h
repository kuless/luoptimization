#pragma once
#include "QualityValue.h"
#include "Constants.h"
#include "MultiCriteriaNetwork.h"
#include "NotImplementedException.h"
#include "QualityEnum.h"

class AbstractQualityMeasure
{
	public:
		AbstractQualityMeasure();
		virtual ~AbstractQualityMeasure();
		virtual QualityValue* getQuality(std::vector<AbstractNetwork<MultiCriteriaValue>*>* population);
		virtual QualityEnum getType();
};
