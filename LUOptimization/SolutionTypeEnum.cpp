#include "SolutionTypeEnum.h"

using namespace std;

const SolutionTypeEnum SolutionType::getValue(string value)
{
	if (value.compare("Single criteria") == 0)
	{
		return SolutionTypeEnum::SINGLE_CRITERIA;
	}
	else if (value.compare("NSGA-II") == 0)
	{
		return SolutionTypeEnum::NSGAII;
	}
}
