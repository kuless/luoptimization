#pragma once
#include <algorithm>
#include "Line.h"
#include "AbstractMutationOperator.h"

class AddTravelsMutation : public AbstractMutationOperator
{
	public:
		AddTravelsMutation(IRandomNumberGenerator* random);
		~AddTravelsMutation();
		void mutation(Line* lineForMutation) override;
		MutationTechniqueEnum getType() override;

	private:

};
