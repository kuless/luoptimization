#include "ElitistArchive.h"

using namespace std;

ElitistArchive::ElitistArchive(IQualityMeasures* qualityMeasure)
{
	this->qualityMeasure = qualityMeasure;
	this->nsgaii = new Nsgaii();
	this->archive = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	this->fullArchive = new unordered_set<AbstractNetwork<MultiCriteriaValue>*, NetworkHash<MultiCriteriaValue>, NetworkEqual<MultiCriteriaValue>>();
}

ElitistArchive::~ElitistArchive()
{
	delete this->qualityMeasure;
	delete this->nsgaii;

	for (int i = 0; i < this->archive->size(); i++)
	{
		delete this->archive->at(i);
	}

	delete this->archive;

	for (unordered_set<AbstractNetwork<MultiCriteriaValue>*, NetworkHash<MultiCriteriaValue>, NetworkEqual<MultiCriteriaValue>>::iterator it = this->fullArchive->begin(); it != this->fullArchive->end(); ++it)
	{
		delete* it;
	}

	delete this->fullArchive;
}

QualityValue* ElitistArchive::getQuality(QualityEnum qualityType)
{
	return this->qualityMeasure->getQuality(this->archive, qualityType);
}

void ElitistArchive::addToArchive(std::vector<AbstractNetwork<MultiCriteriaValue>*>* population)
{
	vector<AbstractNetwork<MultiCriteriaValue>*>* joinedPopulation = new vector<AbstractNetwork<MultiCriteriaValue>*>();
	joinedPopulation->insert(joinedPopulation->end(), this->archive->begin(), this->archive->end());

	for (int i = 0; i < population->size() && population->at(i)->getValue()->getParetoFront() == FIRST_PARETO_FRONT; i++)
	{
		bool isPresent = this->fullArchive->contains(population->at(i));

		if (!isPresent)
		{
			AbstractNetwork<MultiCriteriaValue>* network = population->at(i)->clone();
			network->setValue(population->at(i)->getValue()->clone());
			this->fullArchive->insert(network);
			joinedPopulation->push_back(network);
		}
	}

	ParetoFront* firstFront = this->nsgaii->getFirstFront(joinedPopulation);
	delete joinedPopulation;
	vector<AbstractNetwork<MultiCriteriaValue>*>* newArchive = new vector<AbstractNetwork<MultiCriteriaValue>*>();

	for (int i = 0; i < firstFront->getFront()->size(); i++)
	{
		AbstractNetwork<MultiCriteriaValue>* network = firstFront->getFront()->at(i)->first->clone();
		network->setValue(firstFront->getFront()->at(i)->first->getValue()->clone());
		newArchive->push_back(network);
	}

	for (int i = 0; i < this->archive->size(); i++)
	{
		delete this->archive->at(i);
	}

	delete this->archive;
	this->archive = newArchive;

	sort(archive->begin(), archive->end(), [](AbstractNetwork<MultiCriteriaValue>* left, AbstractNetwork<MultiCriteriaValue>* right)
	{
		return left->getValue()->getMaintenanceValue() > right->getValue()->getMaintenanceValue();
	});
}

vector<AbstractNetwork<MultiCriteriaValue>*>* ElitistArchive::getArchive()
{
	return this->archive;
}
