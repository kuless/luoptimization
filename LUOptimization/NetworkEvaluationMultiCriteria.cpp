#include "NetworkEvaluationMultiCriteria.h"

using namespace std;

NetworkEvaluationMultiCriteria::NetworkEvaluationMultiCriteria(list<Travel*>* travels, MultiCriteriaNetwork* network): NetworkEvaluation<MultiCriteriaValue>(travels, network)
{
	this->maxTrainsOnTracks = 0;
}

NetworkEvaluationMultiCriteria::~NetworkEvaluationMultiCriteria()
{
}

void NetworkEvaluationMultiCriteria::minuteBeginRating()
{
	this->value->addPassengersValue(this->state->getNumberOfWaitingPassangers());
}

void NetworkEvaluationMultiCriteria::remainingPassengersRating(int numberOfPassengers)
{
	this->value->addPassengersValue(numberOfPassengers * NO_ROUTE_PENALTY);
}

void NetworkEvaluationMultiCriteria::afterDayRating()
{
	this->value->addPassengersValue(this->state->getNotFittedPassangers() * NO_ROUTE_PENALTY);
	
	double lenght = 0.0;
	double drivenLenght = 0.0;
	unordered_set<Station*, StationHash, StationEqual> amountOfOperatedStations;
	unordered_set<UniqueConnection*, UniqueConnectionHash, UniqueConnectionEqual> connections;

	for (int i = 0; this->network->getLines()->size() > i; i++)
	{
		Line* line = this->network->getLines()->at(i);
		drivenLenght += line->getLenght() * line->getTimetableInMinutes().size() * 2;

		for (int k = 0; k < line->getStops().size() - 1; k++)
		{
			Station* station = line->getStops().at(k);
			amountOfOperatedStations.insert(station);
			string nextStationId = line->getStops().at(k + 1)->getId();
			Connection* connection = station->getConnection(nextStationId);
			UniqueConnection* uniqueConnection = new UniqueConnection(station->getId(), nextStationId, connection->getDistance());
			pair<unordered_set<UniqueConnection*>::iterator, bool> isInserted = connections.insert(uniqueConnection);

			if (!isInserted.second)
			{
				delete uniqueConnection;
			}
		}

		amountOfOperatedStations.insert(line->getStops().at(line->getStops().size() - 1));
	}

	for (unordered_set<UniqueConnection*>::iterator it = connections.begin(); it != connections.end(); ++it)
	{
		lenght += (*it)->getDistance();
		delete *it;
	}

	this->value->addMaintenanceValue(amountOfOperatedStations.size() * MAINTENANCE_STATION_COSTS);
	this->value->addMaintenanceValue(lenght * MAINTENANCE_TRACKS_COSTS);
	this->value->addMaintenanceValue(drivenLenght * MAINTENANCE_DRIVING_COSTS);
	this->value->addMaintenanceValue(this->maxTrainsOnTracks * MAINTENANCE_TRAIN_COSTS);
}

void NetworkEvaluationMultiCriteria::transferPenaltyRating(int numberOfPassengers)
{
	this->value->addPassengersValue(numberOfPassengers * TRANSFER_PENALTY);
}

void NetworkEvaluationMultiCriteria::trainMoveRating(pair<int, int> trainUpdate)
{
	this->value->addPassengersValue(trainUpdate.second);
}

void NetworkEvaluationMultiCriteria::newTrainsOnTrackRating()
{
	if (this->maxTrainsOnTracks < this->state->getNumberOfTrainsOnTrack())
	{
		this->maxTrainsOnTracks = this->state->getNumberOfTrainsOnTrack();
	}
}
