#include "SingleCriteriaNetworkBuilder.h"

SingleCriteriaNetworkBuilder::SingleCriteriaNetworkBuilder(): AbstractNetworkBuilder<SingleCriteriaValue>()
{
}

SingleCriteriaNetworkBuilder::~SingleCriteriaNetworkBuilder()
{
}

AbstractNetwork<SingleCriteriaValue>* SingleCriteriaNetworkBuilder::createNewNetwork(std::vector<Line*>* lines)
{
	return new SingleCriteriaNetwork(lines);
}

AbstractNetworkBuilder<SingleCriteriaValue>* SingleCriteriaNetworkBuilder::copy()
{
	return new SingleCriteriaNetworkBuilder();
}
