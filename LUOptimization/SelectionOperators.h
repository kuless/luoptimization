#pragma once
#include <vector>
#include "AbstractNetwork.h"
#include "SingleCriteriaValue.h"
#include "MultiCriteriaValue.h"
#include "IRandomNumberGenerator.h"

template <class T>
class SelectionOperators
{
	public:
		SelectionOperators(IRandomNumberGenerator* random, bool useRoulette, int selectionsSize);
		~SelectionOperators();

		std::vector<std::pair<AbstractNetwork<T>*, AbstractNetwork<T>*>*>* selection(std::vector<AbstractNetwork<T>*>* population);

	private:
		bool useRoulette;
		int selectionsSize;
		IRandomNumberGenerator* random;

		std::vector<std::pair<AbstractNetwork<T>*, AbstractNetwork<T>*>*>* tournament(std::vector<AbstractNetwork<T>*>* population, int size);
		std::vector<std::pair<AbstractNetwork<T>*, AbstractNetwork<T>*>*>* roulette(std::vector<AbstractNetwork<T>*>* population, int size);
};

