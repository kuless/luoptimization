#pragma once
#include "ResultsWriter.h"
#include "SingleCriteriaValue.h"
#include "Constants.h"

class ResultWriterSingleCriteria: public ResultsWriter<SingleCriteriaValue>
{
	public:
		ResultWriterSingleCriteria(int id, std::string fileName);

		void saveResultsToFile(std::vector<AbstractNetwork<SingleCriteriaValue>*>* population, int generation, int numberOfGeneration) override;
		void saveBestResultToFile(std::vector<AbstractNetwork<SingleCriteriaValue>*>* population) override;

	protected:
		void saveTitlesToFile() override;
};