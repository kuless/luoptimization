#include "EvaluationCache.h"

using namespace std;

template <class T>
EvaluationCache<T>::EvaluationCache()
{
	this->cache = new unordered_map<AbstractNetwork<T>, T*, NetworkHash<T>, NetworkEqual<T>>();
}

template <class T>
EvaluationCache<T>::~EvaluationCache()
{
	for (typename unordered_map<AbstractNetwork<T>, T*>::iterator it = cache->begin(); it != cache->end(); ++it)
	{
		delete it->second;
	}

	delete this->cache;
}

template <class T>
pair<bool, T*> EvaluationCache<T>::getValue(AbstractNetwork<T>& network)
{
	typename unordered_map<AbstractNetwork<T>, T*>::iterator it = this->cache->find(network);

	if (it == this->cache->end())
	{
		return pair<bool, T*>(false, nullptr);
	}
	else
	{
		return pair<bool, T*>(true, it->second);
	}
}

template <class T>
void EvaluationCache<T>::addToCache(AbstractNetwork<T>* network, T* value)
{
	(*cache)[*network] = value;
}

template class EvaluationCache<SingleCriteriaValue>;
template class EvaluationCache<MultiCriteriaValue>;
