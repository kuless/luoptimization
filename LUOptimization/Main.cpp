#include "Solution.h"

using namespace std;

int main() 
{
	Solution* solution = new Solution();
	solution->start();
	delete solution;

	cout << endl;
	return 0;
}
