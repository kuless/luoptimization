#pragma once
#include <string>
#include <vector>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string.hpp>
#include "SolutionTypeEnum.h"
#include "QualityEnum.h"
#include "CrossoverTechniqueEnum.h"
#include "MutationTechniqueEnum.h"
#include "Constants.h"

class InitInputParameters
{
	public:
		InitInputParameters();
		~InitInputParameters();

		void setOutputFile(std::string outputFile);
		void setCrossoverProbability(int crossoverProbability);
		void setMutationProbability(int mutationProbability);
		void setSelectionsSize(int selectionsSize);
		void setRouletteSelection(bool rouletteSelection);
		void setMutationTechnique(std::string mutationTechnique);
		void setCrossoverTechnique(std::string crossoverTechnique);
		void setSolutionType(std::string solutionType);
		void setQualityTypes(std::string qualityTypes);
		void setSaveValuesForTpf(bool saveValuesForTpf);

		std::string getOutputFile();
		int getCrossoverProbability();
		int getMutationProbability();
		int getSelectionsSize();
		bool getRouletteSelection();
		MutationTechniqueEnum getMutationTechnique();
		CrossoverTechniqueEnum getCrossoverTechnique();
		SolutionTypeEnum getSolutionType();
		std::vector<QualityEnum> getQualityTypes();
		bool getSaveValuesForTpf();

	private:
		std::string outputFile;
		int crossoverProbability;
		int mutationProbability;
		int selectionsSize;
		bool rouletteSelection;
		MutationTechniqueEnum mutationTechnique;
		CrossoverTechniqueEnum crossoverTechnique;
		SolutionTypeEnum solutionType;
		std::vector<QualityEnum> qualityTypes;
		bool saveValuesForTpf;
};
