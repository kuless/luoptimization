#include "Train.h"

using namespace std;

Train::Train(int capacity, Line* line, int departure)
{
	totalPassengers = 0;
	passengers = list<Travel*>();
	this->capacity = capacity;
	this->line = line;
	currentStation = line->getStops()[0];
	state = TRAIN_ON_STATION;
	currentTravelTime = 0;
	this->departure = departure;
	forwardMovment = true;
}

Train::~Train()
{
	while (!passengers.empty())
	{
		delete passengers.front();
		passengers.pop_front();
	}
}

bool Train::operator<(const Train& train) const
{
	return departure < train.departure;
}

Travel* Train::addPassangers(Travel* passangers)
{
	int newTotalPassengers = totalPassengers + passangers->getNumberOfPassengers();

	if (newTotalPassengers <= capacity)
	{
		totalPassengers = newTotalPassengers;
		this->passengers.push_back(passangers);
		return nullptr;
	}
	else
	{
		try
		{
			Travel* notFittedPassangers = passangers->splitPassengers(newTotalPassengers - capacity);
			totalPassengers = capacity;
			this->passengers.push_back(passangers);
			return notFittedPassangers;
		}
		catch (const TravelException& e)
		{
			throw TravelException("Received value smaller than the current numberOfPassengers");
		}
	}
}

bool Train::isFull()
{
	return totalPassengers == capacity;
}

pair<int, int> Train::update()
{
	int trainValue = totalPassengers;
	state = TRAIN_STATE_NOT_SET;
	currentTravelTime++;

	if (forwardMovment)
	{
		handleForwardMovement();
	}
	else
	{
		handleBackwardMovement();
	}

	return pair<int, int>(state, trainValue);
}

list<Travel*> Train::getPassengersExitingTrain()
{
	list<Travel*> exiting = list<Travel*>();
	
	for (list<Travel*>::iterator it = passengers.begin(); it != passengers.end(); it++)
	{
		if ((*it)->getRoutes()->front()->getEndStation() == currentStation)
		{
			(*it)->updateAfterComplitionPartOfTheJourney();
			totalPassengers -= (*it)->getNumberOfPassengers();
			exiting.push_back(*it);
		}
	}

	for (list<Travel*>::iterator it = exiting.begin(); it != exiting.end(); it++)
	{
		passengers.remove(*it);
	}
	
	return exiting;
}

int Train::getDeparture()
{
	return departure;
}

Line* Train::getLine()
{
	return line;
}

Station* Train::getCurrentStation()
{
	return currentStation;
}

int Train::getState()
{
	return state;
}

bool Train::isForwardMovment()
{
	return forwardMovment;
}

int Train::getTotalPassengers()
{
	return totalPassengers;
}

int Train::getCurrentTravelTime()
{
	return currentTravelTime;
}

list<Travel*> Train::getPassengers()
{
	return passengers;
}

void Train::handleForwardMovement()
{
	int travelTimeToCurrentStation = 0;
	int journeyTimeVectorSize = line->getJourneyTime().size();

	for (int i = -1; state == TRAIN_STATE_NOT_SET && i < journeyTimeVectorSize; i++)
	{
		if (i != -1)
		{
			travelTimeToCurrentStation += line->getJourneyTime()[i];
		}

		if (travelTimeToCurrentStation == currentTravelTime)
		{
			if (i == journeyTimeVectorSize - 1)
			{
				state = TRAIN_CHANGING_DIRECTION;
				currentTravelTime = 0;
				forwardMovment = false;
			}
			else
			{
				state = TRAIN_ON_STATION;
			}

			currentStation = line->getStops()[i + 1];
		}
		else if (travelTimeToCurrentStation < currentTravelTime)
		{
			if (i != journeyTimeVectorSize - 1 && travelTimeToCurrentStation + line->getJourneyTime()[i + 1] > currentTravelTime)
			{
				state = TRAIN_MOVE;
			}
			else if (i == journeyTimeVectorSize - 1)
			{
				state = TRAIN_MOVE;
			}
		}
	}
}

void Train::handleBackwardMovement()
{
	int travelTimeToCurrentStation = 0;
	int journeyTimeVectorSize = line->getJourneyTime().size();

	for (int i = journeyTimeVectorSize; state == TRAIN_STATE_NOT_SET && i >= 0; i--)
	{
		if (i != journeyTimeVectorSize)
		{
			travelTimeToCurrentStation += line->getJourneyTime()[i];
		}

		if (travelTimeToCurrentStation == currentTravelTime)
		{
			if (i == 0)
			{
				state = TRAIN_FINISH;
			}
			else
			{
				state = TRAIN_ON_STATION;
			}

			currentStation = line->getStops()[i];
		}
		else if (travelTimeToCurrentStation < currentTravelTime)
		{
			if (i != 0 && travelTimeToCurrentStation + line->getJourneyTime()[i - 1] > currentTravelTime)
			{
				state = TRAIN_MOVE;
			}
			else if (i == 0)
			{
				state = TRAIN_MOVE;
			}
		}
	}
}
