#include "AbstractNetwork.h"

using namespace std;

template<class T>
AbstractNetwork<T>::AbstractNetwork(vector<Line*>* lines)
{
	this->lines = lines;
	value = nullptr;
}

template<class T>
AbstractNetwork<T>::AbstractNetwork(const AbstractNetwork<T>& network)
{
	lines = new vector<Line*>();

	for (int i = 0; i < network.lines->size(); i++)
	{
		lines->push_back(new Line((*(*network.lines)[i])));
	}

	value = nullptr;
}

template<class T>
AbstractNetwork<T>::~AbstractNetwork()
{
	for (int i = 0; i < lines->size(); i++)
	{
		delete (*lines)[i];
	}

	delete lines;

	if (value != nullptr)
	{
		delete value;
	}
}

template<class T>
bool AbstractNetwork<T>::operator>(const AbstractNetwork<T>& network) const
{
	return value > network.value;
}

template<class T>
bool AbstractNetwork<T>::operator<(const AbstractNetwork<T>& network) const
{
	return value < network.value;
}

template<class T>
bool AbstractNetwork<T>::operator==(const AbstractNetwork<T>& network) const
{
	return _Equal(network);
}

template<class T>
bool AbstractNetwork<T>::_Equal(const AbstractNetwork<T>& network) const
{
	if (lines->size() == network.lines->size())
	{
		bool* equalLines = new bool[network.lines->size()];

		for (int i = 0; i < network.lines->size(); i++)
		{
			equalLines[i] = false;
		}

		for (int i = 0; i < lines->size(); i++)
		{
			bool equalLineNotFound = true;

			for (int k = 0; equalLineNotFound && k < network.lines->size(); k++)
			{
				if (!equalLines[k] && (*lines)[i]->_EqualStopsAndTimetable(*(*network.lines)[k]))
				{
					equalLineNotFound = false;
					equalLines[k] = true;
				}
			}

			if (equalLineNotFound)
			{
				delete equalLines;
				return false;
			}
		}

		delete equalLines;
		return true;
	}
	else
	{
		return false;
	}
}

template<class T>
bool AbstractNetwork<T>::_Equal(const AbstractNetwork<T>* network) const
{
	return _Equal(*network);
}

template<class T>
size_t AbstractNetwork<T>::getHash() const
{
	vector<size_t> linesInHash;

	for (int i = 0; i < lines->size(); i++)
	{
		size_t hash = 0;
		boost::hash_combine(hash, lines->at(i)->getStops());
		boost::hash_combine(hash, lines->at(i)->getTimetable());
		linesInHash.push_back(hash);
	}

	sort(linesInHash.begin(), linesInHash.end());
	size_t result = 0;

	for (int i = 0; i < linesInHash.size(); i++)
	{
		boost::hash_combine(result, linesInHash[i]);
	}

	return result;
}

template<class T>
vector<Line*>* AbstractNetwork<T>::getLines()
{
	return lines;
}

template<class T>
T* AbstractNetwork<T>::getValue()
{
	return value;
}

template<class T>
unsigned long long int AbstractNetwork<T>::getSelectionValue()
{
	throw NotImplementedException("getSelectionValue not implemented");
}

template<class T>
AbstractNetwork<T>* AbstractNetwork<T>::clone()
{
	throw NotImplementedException("clone not implemented");
}

template<class T>
AbstractNetwork<T>* AbstractNetwork<T>::callChildConstructor(vector<Line*>* lines)
{
	throw NotImplementedException("callChildConstructor not implemented");
}

template<class T>
AbstractNetwork<T>* AbstractNetwork<T>::callChildConstructor(const AbstractNetwork<T>& network)
{
	throw NotImplementedException("callChildConstructor not implemented");
}

template<class T>
void AbstractNetwork<T>::setValue(T* value)
{
	this->value = value;
}

template<class T>
size_t NetworkHash<T>::operator()(const AbstractNetwork<T>& n) const
{
	return n.getHash();
}

template<class T>
size_t NetworkHash<T>::operator()(const AbstractNetwork<T>* n) const
{
	return n->getHash();
}

template<class T>
bool NetworkEqual<T>::operator()(const AbstractNetwork<T>& l, const AbstractNetwork<T>& r) const
{
	return l._Equal(r);
}

template<class T>
bool NetworkEqual<T>::operator()(const AbstractNetwork<T>* l, const AbstractNetwork<T>* r) const
{
	return l->_Equal(r);
}

template class AbstractNetwork<SingleCriteriaValue>;
template class AbstractNetwork<MultiCriteriaValue>;
template struct NetworkHash<SingleCriteriaValue>;
template struct NetworkHash<MultiCriteriaValue>;
template struct NetworkEqual<SingleCriteriaValue>;
template struct NetworkEqual<MultiCriteriaValue>;
