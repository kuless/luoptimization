#pragma once
#include "AbstractNetworkBuilder.h"
#include "SingleCriteriaNetwork.h"

class SingleCriteriaNetworkBuilder : public AbstractNetworkBuilder<SingleCriteriaValue>
{
	public:
		SingleCriteriaNetworkBuilder();
		~SingleCriteriaNetworkBuilder();

		AbstractNetwork<SingleCriteriaValue>* createNewNetwork(std::vector<Line*>* lines) override;
		AbstractNetworkBuilder<SingleCriteriaValue>* copy() override;
};
