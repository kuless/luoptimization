class Station;

#pragma once
#include "Station.h"
#include <boost/functional/hash.hpp>

class Connection
{
	public:
		Connection();
		Connection(int journeyTime, double distance, Station* station);
		~Connection();

		int getJourneyTime();
		double getDistance();
		Station* getStation();

		void setJourneyTime(int journeyTime);
		void setDistance(double distance);
		void setStation(Station* station);

		size_t getHash() const;

	private:
		int journeyTime;
		double distance;
		Station* station;
};
