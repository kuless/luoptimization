#pragma once
#include <string>

enum class QualityEnum
{
	HV, IGD
};

class Quality
{
	public:
		static const QualityEnum getValue(std::string value);
};
