#include "MutationTechniqueEnum.h"

using namespace std;

const MutationTechniqueEnum MutationTechnique::getValue(string value)
{
	if (value.compare("All") == 0)
	{
		return MutationTechniqueEnum::ALL;
	}
	else if (value.compare("Add travels") == 0)
	{
		return MutationTechniqueEnum::ADD_TRAVELS;
	}
	else if (value.compare("Frequency") == 0)
	{
		return MutationTechniqueEnum::FREQUENCY;
	}
	else if (value.compare("Line") == 0)
	{
		return MutationTechniqueEnum::LINE;
	}
	else if (value.compare("Remove travels") == 0)
	{
		return MutationTechniqueEnum::REMOVE_TRAVELS;
	}
	else if (value.compare("Reverse") == 0)
	{
		return MutationTechniqueEnum::REVERSE;
	}
}
