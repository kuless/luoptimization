#pragma once
#include "NotImplementedException.h"
#include <boost/multiprecision/cpp_int.hpp> 

class IRandomNumberGenerator
{
	public:
		IRandomNumberGenerator();
		virtual ~IRandomNumberGenerator();

		virtual int getInt(int lowerLimit, int upperLimit);
		virtual unsigned long long int getUnsignedLongLongInt(unsigned long long int lowerLimit, unsigned long long int upperLimit);
		virtual boost::multiprecision::uint128_t getUint128(boost::multiprecision::uint128_t lowerLimit, boost::multiprecision::uint128_t upperLimit);
		virtual IRandomNumberGenerator* copy();
};