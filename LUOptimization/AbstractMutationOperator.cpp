#include "AbstractMutationOperator.h"

AbstractMutationOperator::AbstractMutationOperator(IRandomNumberGenerator* random)
{
	this->random = random;
}

AbstractMutationOperator::~AbstractMutationOperator()
{
	delete random;
}

void AbstractMutationOperator::mutation(Line* lineForMutation)
{
	throw NotImplementedException("mutation not implemented");
}

MutationTechniqueEnum AbstractMutationOperator::getType()
{
	throw NotImplementedException("getType not implemented");
}
