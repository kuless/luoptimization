#include "NetworkEvaluationSingleCriteria.h"

using namespace std;

NetworkEvaluationSingleCriteria::NetworkEvaluationSingleCriteria(list<Travel*>* travels, SingleCriteriaNetwork* network): NetworkEvaluation<SingleCriteriaValue>(travels, network)
{
}

NetworkEvaluationSingleCriteria::~NetworkEvaluationSingleCriteria()
{
}

void NetworkEvaluationSingleCriteria::minuteBeginRating()
{
	this->value->addValue(this->state->getNumberOfWaitingPassangers());
}

void NetworkEvaluationSingleCriteria::remainingPassengersRating(int numberOfPassengers)
{
	this->value->addValue(numberOfPassengers * NO_ROUTE_PENALTY);
}

void NetworkEvaluationSingleCriteria::afterDayRating()
{
	this->value->addValue(this->state->getNotFittedPassangers() * NO_ROUTE_PENALTY);
}

void NetworkEvaluationSingleCriteria::transferPenaltyRating(int numberOfPassengers)
{
	this->value->addValue(numberOfPassengers * TRANSFER_PENALTY);
}

void NetworkEvaluationSingleCriteria::trainMoveRating(pair<int, int> trainUpdate)
{
	this->value->addValue(trainUpdate.second);
}

void NetworkEvaluationSingleCriteria::newTrainsOnTrackRating()
{
	if (state->getTrainsOnTrack()->size() > TRAINS_THRESHOLD)
	{
		this->value->addNumberOfCrossingTrainsThreshold();
		this->value->addValue((state->getTrainsOnTrack()->size() - TRAINS_THRESHOLD) * CROSSING_TRAINS_THRESHOLD_PENALTY);
	}
}
