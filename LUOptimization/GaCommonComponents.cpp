#include "GaCommonComponents.h"

template<class T>
GaCommonComponents<T>::GaCommonComponents(DijkstraImpl* dijkstraImpl, QuasiShortestPath* quasiShortestPath, AbstractEvaluationOperator<T>* evaluationOperator)
{
	this->dijkstraImpl = dijkstraImpl;
	this->quasiShortestPath = quasiShortestPath;
	this->evaluationOperator = evaluationOperator;
}

template<class T>
GaCommonComponents<T>::~GaCommonComponents()
{
	delete dijkstraImpl;
	delete quasiShortestPath;
	delete evaluationOperator;
}

template<class T>
DijkstraImpl* GaCommonComponents<T>::getDijkstraImpl()
{
	return dijkstraImpl;
}

template<class T>
QuasiShortestPath* GaCommonComponents<T>::getQuasiShortestPath()
{
	return quasiShortestPath;
}

template<class T>
AbstractEvaluationOperator<T>* GaCommonComponents<T>::getEvaluationOperator()
{
	return evaluationOperator;
}

template class GaCommonComponents<SingleCriteriaValue>;
template class GaCommonComponents<MultiCriteriaValue>;
