#pragma once
#include <vector>
#include "GeneticAlgoritm.h"
#include "MultiCriteriaValue.h"
#include "MultiCriteriaNetworkBuilder.h"
#include "ResultWriterMultiCriteria.h"
#include "EvaluationMultiCriteria.h"
#include "Nsgaii.h"

class GeneticAlgoritmMultiCriteria: public GeneticAlgoritm<MultiCriteriaValue>
{
	public:
		GeneticAlgoritmMultiCriteria(int id, std::vector<Station*>* stationsVector, std::vector<Travel*>* travels, GaInputParameters* gaInputParameters, GaCommonComponents<MultiCriteriaValue>* gaCommonComponents, std::vector<MultiCriteriaValue*>* trueParetoFront);
		~GeneticAlgoritmMultiCriteria();

	protected:
		PopulationGenerator<MultiCriteriaValue>* initPopulationGenerator(IDijkstra* dijkstra, IDijkstra* quasiShortestPath, PopulationGenerationTechniqueEnum populationGenerationTechnique) override;
		void firstEvaluation(int numberOfThreads) override;
		void nextEvaluations(int numberOfThreads) override;
		void setNewPopulation(std::vector<AbstractNetwork<MultiCriteriaValue>*>* newPopulation) override;

	private:
		void evaluateNsgaii();

		std::vector<AbstractNetwork<MultiCriteriaValue>*>* oldPopulation;
		Nsgaii* nsgaii;
};
