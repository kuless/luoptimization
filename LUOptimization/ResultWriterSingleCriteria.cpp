#include "ResultWriterSingleCriteria.h"

using namespace std;

ResultWriterSingleCriteria::ResultWriterSingleCriteria(int id, string fileName): ResultsWriter<SingleCriteriaValue>(id, fileName)
{
	saveTitlesToFile();
}

void ResultWriterSingleCriteria::saveResultsToFile(vector<AbstractNetwork<SingleCriteriaValue>*>* population, int generation, int numberOfGeneration)
{
	long long int sum = 0;
	long long int min = LLONG_MAX;
	long long int max = LLONG_MIN;
	int notFittedPassangersInMin = 0;
	int numberOfCrossingTrainsThresholdInMin = 0;

	for (AbstractNetwork<SingleCriteriaValue>* network : (*population))
	{
		sum += network->getValue()->getValue();

		if (network->getValue()->getValue() > max)
		{
			max = network->getValue()->getValue();
		}

		if (network->getValue()->getValue() < min)
		{
			min = network->getValue()->getValue();
			notFittedPassangersInMin = network->getValue()->getNotFittedPassangers();
			numberOfCrossingTrainsThresholdInMin = network->getValue()->getNumberOfCrossingTrainsThreshold();
		}
	}

	this->fileWriter << generation << SEPERATOR << min << SEPERATOR << (sum / population->size())
		<< SEPERATOR << max << SEPERATOR << notFittedPassangersInMin << SEPERATOR << numberOfCrossingTrainsThresholdInMin << endl;
	cout << GA_ID << id << GENERATION << generation << SLASH << numberOfGeneration << SEPERATOR << min << SEPERATOR
		<< (sum / population->size()) << SEPERATOR << max << SEPERATOR << notFittedPassangersInMin << SEPERATOR
		<< numberOfCrossingTrainsThresholdInMin << endl;
}

void ResultWriterSingleCriteria::saveBestResultToFile(vector<AbstractNetwork<SingleCriteriaValue>*>* population)
{
	long long int min = LLONG_MAX;
	AbstractNetwork<SingleCriteriaValue>* best = nullptr;

	for (AbstractNetwork<SingleCriteriaValue>* network : (*population))
	{
		if (network->getValue()->getValue() < min)
		{
			min = network->getValue()->getValue();
			best = network;
		}
	}

	saveLineVectorToFile(*best->getLines(), this->rootDirectory + this->fileName + LINES_FILE_NAME + FILE_EXTENSION);
	saveTimetableToFile(*best->getLines(), this->rootDirectory + this->fileName + TIMETABLE_FILE_NAME + FILE_EXTENSION);
}

void ResultWriterSingleCriteria::saveTitlesToFile()
{
	this->fileWriter << RESULTS_FILE_HEADER_SINGLE_CRITERIA << endl;
}
