#include "SingleCriteriaNetwork.h"

using namespace std;

SingleCriteriaNetwork::SingleCriteriaNetwork(vector<Line*>* lines): AbstractNetwork<SingleCriteriaValue>(lines)
{
}

SingleCriteriaNetwork::SingleCriteriaNetwork(const AbstractNetwork<SingleCriteriaValue>& network): AbstractNetwork<SingleCriteriaValue>(network)
{
}

SingleCriteriaNetwork::~SingleCriteriaNetwork()
{
}

unsigned long long int SingleCriteriaNetwork::getSelectionValue()
{
    return this->getValue()->getValue();
}

AbstractNetwork<SingleCriteriaValue>* SingleCriteriaNetwork::clone()
{
    return new SingleCriteriaNetwork(*this);
}

AbstractNetwork<SingleCriteriaValue>* SingleCriteriaNetwork::callChildConstructor(vector<Line*>* lines)
{
    return new SingleCriteriaNetwork(lines);
}

AbstractNetwork<SingleCriteriaValue>* SingleCriteriaNetwork::callChildConstructor(const AbstractNetwork<SingleCriteriaValue>& network)
{
    return new SingleCriteriaNetwork(network);
}
