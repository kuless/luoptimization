#include "QuasiShortestPath.h"

using namespace std;

QuasiShortestPath::QuasiShortestPath(vector<Station*>* stations, IDijkstra* dijkstraImpl): IDijkstra()
{
	this->stations = stations;
	this->mutexObj = new mutex();
	this->dijkstraImpl = dijkstraImpl;
	this->cache = new unordered_map<string, vector<Station*>*>();
}

QuasiShortestPath::~QuasiShortestPath()
{
	for (unordered_map<string, vector<Station*>*>::iterator it = this->cache->begin(); it != this->cache->end(); ++it)
	{
		delete it->second;
	}

	delete this->cache;
	delete mutexObj;
}

vector<Station*>* QuasiShortestPath::dijkstra(Station* start, Station* end)
{
	stringstream ss;
	ss << start->getNLCCode() << SEPERATOR << end->getNLCCode();
	this->mutexObj->lock();
	unordered_map<string, vector<Station*>*>::iterator it = this->cache->find(ss.str());
	this->mutexObj->unlock();
	return it == this->cache->end() ? createQuasiShortestPath(start, end, ss.str()) : copy(it->second);
}

vector<Station*>* QuasiShortestPath::createQuasiShortestPath(Station* start, Station* end, string key)
{
	vector<Station*>* dijkstraImplResult = this->dijkstraImpl->dijkstra(start, end);

	if (dijkstraImplResult->size() >= 2)
	{
		unordered_set<Station*, StationHash, StationEqual>* lineSet = convertLineToSet(dijkstraImplResult);
		findPathsAndSaveInCache(dijkstraImplResult->at(0), dijkstraImplResult->at(dijkstraImplResult->size() - 1), dijkstraImplResult->at(1), lineSet);
		delete lineSet;
		stringstream ss;
		ss << start->getNLCCode() << SEPERATOR << dijkstraImplResult->at(1)->getNLCCode();
		saveToShortResult(ss.str());
	}
	else
	{
		saveToShortResult(key);
	}

	delete dijkstraImplResult;
	this->mutexObj->lock();
	vector<Station*>* result = this->cache->find(key)->second;
	this->mutexObj->unlock();
	return copy(result);
}

vector<DijkstraValue>* QuasiShortestPath::initValues(Station* start)
{
	vector<DijkstraValue>* values = new vector<DijkstraValue>();;

	for (int i = 0; i < this->stations->size(); i++)
	{
		if (start == this->stations->at(i))
		{
			values->push_back(DijkstraValue(0, this->stations->at(i), nullptr));
		}
		else
		{
			values->push_back(DijkstraValue(INT_MAX, this->stations->at(i), nullptr));
		}
	}

	return values;
}

void QuasiShortestPath::updateNodes(vector<DijkstraValue>* values, DijkstraValue& node, Station* start, Station* notMeasuredForStartStation)
{
	for (int i = 0; i < values->size(); i++)
	{
		string stationId = values->at(i).getStation()->getId();
		int distance = node.getStation()->getJourneyTime(stationId);

		if (
			distance != -1 && values->at(i).getValue() > node.getValue() + distance
			&& (node.getStation() != start || (node.getStation() == start && values->at(i).getStation() != notMeasuredForStartStation))
			)
		{
			values->at(i).setPreviosStation(node.getStation());
			values->at(i).setValue(node.getValue() + distance);
		}
	}
}

void QuasiShortestPath::saveShortesPath(Station* start, DijkstraValue& node, unordered_map<Station*, Station*>& visitedNodes, unordered_set<Station*, StationHash, StationEqual>* lineSet)
{
	if (lineSet->find(node.getStation()) != lineSet->end())
	{
		stringstream cashKey;
		cashKey << start->getNLCCode() << SEPERATOR << node.getStation()->getNLCCode();
		vector<Station*>* result = new vector<Station*>();
		result->push_back(node.getStation());

		for (Station* helper = node.getPreviosStation(); helper != nullptr; helper = visitedNodes.find(helper)->second)
		{
			result->push_back(helper);
		}

		if (result->at(result->size() - 1) == start)
		{
			reverse(result->begin(), result->end());
		}
		else
		{
			result->clear();
		}
		

		this->mutexObj->lock();
		pair<unordered_map<string, vector<Station*>*>::iterator, bool> elementAdded = this->cache->insert(pair<string, vector<Station*>*>(cashKey.str(), result));
		this->mutexObj->unlock();

		if (!elementAdded.second)
		{
			delete result;
		}
	}
}

void QuasiShortestPath::saveToShortResult(string key)
{
	vector<Station*>* noRoute = new vector<Station*>();
	this->mutexObj->lock();
	pair<unordered_map<string, vector<Station*>*>::iterator, bool> elementAdded = this->cache->insert(pair<string, vector<Station*>*>(key, noRoute));
	this->mutexObj->unlock();

	if (!elementAdded.second)
	{
		delete noRoute;
	}
}

void QuasiShortestPath::findPathsAndSaveInCache(Station* start, Station* end, Station* notMeasuredForStartStation, unordered_set<Station*, StationHash, StationEqual>* lineSet)
{
	vector<DijkstraValue>* values = initValues(start);
	unordered_map<Station*, Station*> visitedNodes;

	while (!values->empty())
	{
		sort(values->begin(), values->end(), [](const DijkstraValue& lhs, const DijkstraValue& rhs)
		{
			return lhs > rhs;
		});

		DijkstraValue node = values->at(values->size() - 1);
		values->pop_back();
		visitedNodes.insert(pair<Station*, Station*>(node.getStation(), node.getPreviosStation()));
		updateNodes(values, node, start, notMeasuredForStartStation);
		saveShortesPath(start, node, visitedNodes, lineSet);
	}

	delete values;
}

vector<Station*>* QuasiShortestPath::copy(vector<Station*>* element)
{
	vector<Station*>* copy = new vector<Station*>();

	for (int i = 0; i < element->size(); i++)
	{
		copy->push_back((*element)[i]);
	}

	return copy;
}

unordered_set<Station*, StationHash, StationEqual>* QuasiShortestPath::convertLineToSet(vector<Station*>* line)
{
	unordered_set<Station*, StationHash, StationEqual>* lineSet = new unordered_set<Station*, StationHash, StationEqual>();

	for (int i = 0; i < line->size(); i++)
	{
		lineSet->insert(line->at(i));
	}

	return lineSet;
}
