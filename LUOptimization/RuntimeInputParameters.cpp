#include "RuntimeInputParameters.h"

using namespace std;

RuntimeInputParameters::RuntimeInputParameters()
{
	populationGeneratorInputParameters = new PopulationGeneratorInputParameters();
}

RuntimeInputParameters::RuntimeInputParameters(RuntimeInputParameters& runtimeInputParameters)
{
	populationGeneratorInputParameters = new PopulationGeneratorInputParameters(*runtimeInputParameters.getPopulationGeneratorInputParameters());
	numberOfGeneration = runtimeInputParameters.getNumberOfGeneration();
	numberOfThreads = runtimeInputParameters.getNumberOfThreads();
	solutionType = runtimeInputParameters.getSolutionType();
}

RuntimeInputParameters::~RuntimeInputParameters()
{
	delete populationGeneratorInputParameters;
}

void RuntimeInputParameters::setPopulationGeneratorInputParameters(PopulationGeneratorInputParameters* populationGeneratorInputParameters)
{
	this->populationGeneratorInputParameters = populationGeneratorInputParameters;
}

void RuntimeInputParameters::setNumberOfGeneration(int numberOfGeneration)
{
	this->numberOfGeneration = numberOfGeneration;
}

void RuntimeInputParameters::setNumberOfThreads(int numberOfThreads)
{
	this->numberOfThreads = numberOfThreads;
}

void RuntimeInputParameters::setSolutionType(string solutionType)
{
	this->solutionType = SolutionType::getValue(solutionType);
}

PopulationGeneratorInputParameters* RuntimeInputParameters::getPopulationGeneratorInputParameters()
{
	return populationGeneratorInputParameters;
}

int RuntimeInputParameters::getNumberOfGeneration()
{
	return numberOfGeneration;
}

int RuntimeInputParameters::getNumberOfThreads()
{
	return numberOfThreads;
}

SolutionTypeEnum RuntimeInputParameters::getSolutionType()
{
	return solutionType;
}
