#include "AllMutations.h"

using namespace std;

AllMutations::AllMutations(IRandomNumberGenerator* random, vector<AbstractMutationOperator*>* mutationOperators): AbstractMutationOperator(random)
{
	this->mutationOperators = mutationOperators;
}

AllMutations::~AllMutations()
{
	for (vector<AbstractMutationOperator*>::iterator it = mutationOperators->begin(); it != mutationOperators->end(); ++it)
	{
		delete *it;
	}

	delete mutationOperators;
}

void AllMutations::mutation(Line* lineForMutation)
{
	int mutationOption = random->getInt(0, mutationOperators->size());
	return mutationOperators->at(mutationOption)->mutation(lineForMutation);
}

MutationTechniqueEnum AllMutations::getType()
{
	return MutationTechniqueEnum::ALL;
}
