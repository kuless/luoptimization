#include "QualityEnum.h"

const QualityEnum Quality::getValue(std::string value)
{
	if (value.compare("HV") == 0 || value.compare("HyperVolume") == 0)
	{
		return QualityEnum::HV;
	}
	else if (value.compare("IGD") == 0 || value.compare("InvertedGenerationalDistance") == 0)
	{
		return QualityEnum::IGD;
	}
}
