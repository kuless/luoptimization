#include "AbstractQualityMeasure.h"

using namespace std;

AbstractQualityMeasure::AbstractQualityMeasure()
{
}

AbstractQualityMeasure::~AbstractQualityMeasure()
{
}

QualityValue* AbstractQualityMeasure::getQuality(vector<AbstractNetwork<MultiCriteriaValue>*>* population)
{
	throw NotImplementedException("getQuality not implemented");
}

QualityEnum AbstractQualityMeasure::getType()
{
	throw NotImplementedException("getType not implemented");
}
