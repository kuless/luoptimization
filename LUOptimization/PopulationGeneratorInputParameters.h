#pragma once
#include "PopulationGenerationTechniqueEnum.h"

class PopulationGeneratorInputParameters
{
	public:
		PopulationGeneratorInputParameters();
		PopulationGeneratorInputParameters(PopulationGeneratorInputParameters& populationGeneratorInputParameters);
		~PopulationGeneratorInputParameters();

		void setPopulationSize(int populationSize);
		void setNumberOfLines(int numberOfLines);
		void setPercentOfCoveredStations(int percentOfCoveredStations);
		void setPopulationGenerationTechnique(std::string populationGenerationTechnique);

		int getPopulationSize();
		int getNumberOfLines();
		int getPercentOfCoveredStations();
		PopulationGenerationTechniqueEnum getPopulationGenerationTechnique();

	private:
		int populationSize;
		int numberOfLines;
		int percentOfCoveredStations;
		PopulationGenerationTechniqueEnum populationGenerationTechnique;
};
