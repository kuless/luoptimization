#pragma once
#include "Line.h"
#include "AbstractMutationOperator.h"

class ReverseMutation : public AbstractMutationOperator
{
	public:
		ReverseMutation(IRandomNumberGenerator* random);
		~ReverseMutation();
		void mutation(Line* lineForMutation) override;
		MutationTechniqueEnum getType() override;

	private:

};
