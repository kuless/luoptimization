#pragma once
#include <vector>
#include <unordered_map>
#include "Station.h"

class DijkstraCache
{
	public:
		DijkstraCache();
		~DijkstraCache();

	private:
		std::unordered_map<std::string, std::vector<Station*>>* dijkstraCache;
};
