#pragma once
#include <vector>
#include "Line.h"
#include "AbstractMutationOperator.h"
#include "CrossoverTechniqueEnum.h"

class AllMutations : public AbstractMutationOperator
{
	public:
		AllMutations(IRandomNumberGenerator* random, std::vector<AbstractMutationOperator*>* mutationOperators);
		~AllMutations();
		void mutation(Line* lineForMutation) override;
		MutationTechniqueEnum getType() override;

	private:
		std::vector<AbstractMutationOperator*>* mutationOperators;
};
