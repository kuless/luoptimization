#pragma once
#pragma warning(disable:4146)
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/gmp.hpp>

class QualityValue
{
	public:
		QualityValue(boost::multiprecision::uint128_t quality);
		QualityValue(boost::multiprecision::mpf_float quality);
		~QualityValue();

		std::string getQuality();

	private:
		boost::multiprecision::uint128_t qualityInt;
		boost::multiprecision::mpf_float qualityFloat;
		bool isQualityInt;
};
