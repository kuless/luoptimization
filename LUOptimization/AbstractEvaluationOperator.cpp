#include "AbstractEvaluationOperator.h"

using namespace std;

template<class T>
AbstractEvaluationOperator<T>::AbstractEvaluationOperator(vector<Travel*>* travels)
{
	this->travels = travels;
	cache = new EvaluationCache<T>();
}

template<class T>
AbstractEvaluationOperator<T>::~AbstractEvaluationOperator()
{
	delete cache;
}

template<class T>
void AbstractEvaluationOperator<T>::evaluate(AbstractNetwork<T>* network, mutex* mutex)
{
	mutex->lock();
	pair<bool, T*> cacheValue = cache->getValue(*network);
	mutex->unlock();

	if (cacheValue.first)
	{
		setValues(*network, *cacheValue.second);
	}
	else
	{
		list<Travel*>* copyOfTravels = getCopyOfTravels();
		TravelRouteFinder<T>* travelRouteFinder = new TravelRouteFinder<T>(network);
		travelRouteFinder->setTravelsRoute(copyOfTravels);
		delete travelRouteFinder;
		NetworkEvaluation<T>* networkStatus = getNetworkEvaluation(copyOfTravels, network);
		T* value = networkStatus->evaluate();
		delete networkStatus;
		setValues(*network, *value);
		mutex->lock();
		addToCache(network, value);
		mutex->unlock();
	}
}

template<class T>
void AbstractEvaluationOperator<T>::addToCache(AbstractNetwork<T>* network, T* value)
{
	pair<bool, T*> cacheValue = cache->getValue(*network);

	if (cacheValue.first)
	{
		cache->addToCache(network->clone(), value);
	}
	else
	{
		delete value;
	}
}

template<class T>
list<Travel*>* AbstractEvaluationOperator<T>::getCopyOfTravels()
{
	list<Travel*>* copy = new list<Travel*>();

	for (int i = 0; i < travels->size(); i++)
	{
		copy->push_back(new Travel(*(*travels)[i]));
	}

	return copy;
}

template<class T>
NetworkEvaluation<T>* AbstractEvaluationOperator<T>::getNetworkEvaluation(list<Travel*>* travels, AbstractNetwork<T>* network)
{
	throw NotImplementedException("getNetworkEvaluation not implemented");
}

template<class T>
void AbstractEvaluationOperator<T>::setValues(AbstractNetwork<T>& network, T& value)
{
	throw NotImplementedException("setValues not implemented");
}

template class AbstractEvaluationOperator<SingleCriteriaValue>;
template class AbstractEvaluationOperator<MultiCriteriaValue>;
