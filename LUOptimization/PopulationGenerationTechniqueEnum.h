#pragma once
#include <string>

enum class PopulationGenerationTechniqueEnum
{
	RANDOM, SHORT_DISTANCE, RGA
};

class PopulationGenerationTechnique
{
	public:
		static const PopulationGenerationTechniqueEnum getValue(std::string value);
};
