#include "AbstractNetworkBuilder.h"

template<class T>
AbstractNetworkBuilder<T>::AbstractNetworkBuilder()
{
}

template<class T>
AbstractNetworkBuilder<T>::~AbstractNetworkBuilder()
{
}

template<class T>
AbstractNetwork<T>* AbstractNetworkBuilder<T>::createNewNetwork(std::vector<Line*>* lines)
{
	throw NotImplementedException("createNewNetwork not implemented");
}

template<class T>
AbstractNetworkBuilder<T>* AbstractNetworkBuilder<T>::copy()
{
	throw NotImplementedException("copy not implemented");
}

template class AbstractNetworkBuilder<SingleCriteriaValue>;
template class AbstractNetworkBuilder<MultiCriteriaValue>;
