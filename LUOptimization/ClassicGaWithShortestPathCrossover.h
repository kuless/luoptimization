#pragma once
#include "AbstractCrossoverOperator.h"
#include "AbstractNetwork.h"
#include "IRandomNumberGenerator.h"
#include "IDijkstra.h"
#include "SingleCriteriaValue.h"
#include "MultiCriteriaValue.h"

template <class T>
class ClassicGaWithShortestPathCrossover : public AbstractCrossoverOperator<T>
{
	public:
		ClassicGaWithShortestPathCrossover(IRandomNumberGenerator* random, IDijkstra* dijkstra);
		~ClassicGaWithShortestPathCrossover();
		std::pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* crossover(std::pair<AbstractNetwork<T>*, AbstractNetwork<T>*>* pairToCrossover) override;
		CrossoverTechniqueEnum getType() override;

	protected:
		struct ClassicGAResult
		{
			ClassicGAResult();
			~ClassicGAResult();
			std::vector<Station*>* leftPartOfFirstLine;
			std::vector<Station*>* rightPartOfFirstLine;
			std::vector<Station*>* leftPartOfSecondLine;
			std::vector<Station*>* rightPartOfSecondLine;

			std::vector<Station*>* getFirstLine(std::vector<Station*>* middlePart, Line* orginalLine);
			std::vector<Station*>* getSecondLine(std::vector<Station*>* middlePart, Line* orginalLine);
		};

	private:
		IDijkstra* dijkstra;

		ClassicGaWithShortestPathCrossover::ClassicGAResult* classicGA(std::pair<Line*, Line*> pairToCrossover);
		Line* createLine(int id, std::vector<Station*>* stops, std::vector<std::pair<int, int>>& timetable, Line* orginalLine);
};
