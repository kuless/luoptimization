#pragma once
//#include <cpr/cpr.h>
#include "json/json.h"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string.hpp>
#include "Constants.h"
#include "Station.h"
#include "Line.h"
#include "Travel.h"

class DataLoader 
{
	
	public:
		DataLoader();
		~DataLoader();

		std::string* getLineDetailsFromAPI(std::string lineName);
		void addStationsToVectorFromAPI(std::string& rootJSON);
		void addLinesToVectorFromAPI(std::string& rootJSON);

		void addStationsToVectorFromFile(std::string fileName);
		void addConnectionsToStationsFormFile(std::string fileName);
		void addLineToVectorFromFile(std::string fileName);
		void addTimetableToVectorFromFile(std::string fileName);
		void addTravelsToVectorFormFile(std::string fileName);

		void addConnectionsToStationsFormSmallFile(std::string fileName);

		int getJourneyTimeBetweenStationsFormSavedData(std::string startStationId, std::string endStationId);
		int getJourneyTimeBetweenStationsFormSavedData(Station* startStationId, std::string endStationId);

		void saveStationVectorToFile(std::string fileName);
		void saveLineVectorToFile(std::string fileName);

		std::vector<Station*>* getStationsVector();
		std::vector<Line*> getLineVector();

		Station* findStationById(std::string& id);
		Station* findStationByNLCCode(int NLCCode);
		Line* findLineById(int& id);

	private:
		std::vector<Station*>* stationsVector;
		std::vector<Line*> lineVector;

		Json::Value readJSON(std::string& rootJSON);

		std::vector<std::pair<int, int>> getTimetableBetweenStationsFromAPI(std::string lineName, std::string startStationId, std::string endStationId);
		int getJourneyTimeBetweenStationsFromAPI(std::string linaName, std::string startStationId, std::string endStationId);

		void saveConnectionsToFile(std::string fileName);
		void saveTimetableToFile(std::string fileName);
};