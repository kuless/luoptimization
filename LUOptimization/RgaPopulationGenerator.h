#pragma once
#include <string>
#include <unordered_map>
#include <vector>
#include <algorithm>
#include "AbstractPopulationGenerator.h"
#include "IRandomNumberGenerator.h"
#include "IDijkstra.h"
#include "AbstractNetworkBuilder.h"
#include "SingleCriteriaValue.h"
#include "MultiCriteriaValue.h"
#include "DemandMatrixElement.h"

template <class T>
class RgaPopulationGenerator : public AbstractPopulationGenerator<T>
{
	public:
		RgaPopulationGenerator(IRandomNumberGenerator* random, AbstractNetworkBuilder<T>* builder, IDijkstra* dijkstra);
		~RgaPopulationGenerator();

		std::vector<AbstractNetwork<T>*>* generatePopulation(std::vector<Station*>& stations, std::vector<Travel*>& travels, PopulationGeneratorInputParameters* populationGeneratorInputParameters) override;
		PopulationGenerationTechniqueEnum getType() override;

	private:
		IDijkstra* dijkstra;

		std::vector<DemandMatrixElement*>* createDemandMatrix(std::vector<Travel*>& travels);
		std::vector<Line*>* initLines(std::vector<DemandMatrixElement*>* demandMatrix, int numberOfLines);
};
