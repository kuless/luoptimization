#include "PopulationGenerationTechniqueEnum.h"

const PopulationGenerationTechniqueEnum PopulationGenerationTechnique::getValue(std::string value)
{
	if (value.compare("Random") == 0)
	{
		return PopulationGenerationTechniqueEnum::RANDOM;
	}
	else if (value.compare("Shortest distance") == 0)
	{
		return PopulationGenerationTechniqueEnum::SHORT_DISTANCE;
	}
	else if (value.compare("RGA") == 0)
	{
		return PopulationGenerationTechniqueEnum::RGA;
	}
}
