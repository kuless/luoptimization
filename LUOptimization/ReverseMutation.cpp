#include "ReverseMutation.h"

using namespace std;

ReverseMutation::ReverseMutation(IRandomNumberGenerator* random): AbstractMutationOperator(random)
{
}

ReverseMutation::~ReverseMutation()
{
}

void ReverseMutation::mutation(Line* lineForMutation)
{
	vector<Station*> reversedLine = lineForMutation->getStops();
	vector<int> reversedJourneyTime = lineForMutation->getJourneyTime();
	reverse(reversedLine.begin(), reversedLine.end());
	reverse(reversedJourneyTime.begin(), reversedJourneyTime.end());
	lineForMutation->setStops(new vector<Station*>(reversedLine));
	lineForMutation->setJourneyTime(reversedJourneyTime);
}

MutationTechniqueEnum ReverseMutation::getType()
{
	return MutationTechniqueEnum::REVERSE;
}
