#include "Route.h"


Route::Route(Station* endStation, Line* line, bool forwardMovement)
{
	this->endStation = endStation;
	this->line = line;
	this->forwardMovement = forwardMovement;
}

Route::Route(Route& route)
{
	this->endStation = route.endStation;
	this->line = route.line;
	this->forwardMovement = route.forwardMovement;
}

Route::~Route()
{
}

Station* Route::getEndStation()
{
	return endStation;
}

Line* Route::getLine()
{
	return line;
}

bool Route::isForwardMovement()
{
	return forwardMovement;
}
