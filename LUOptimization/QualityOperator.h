#pragma once
#include <vector>
#include <thread>
#include <future>
#include "MultiCriteriaValue.h"
#include "QualityMeasuresImpl.h"
#include "ElitistArchive.h"
#include "QualityEnum.h"

class QualityOperator
{
	public:
		QualityOperator(std::vector<MultiCriteriaValue*>* trueParetoFornt);
		~QualityOperator();

		std::string getQualites(std::vector<AbstractNetwork<MultiCriteriaValue>*>* population, std::vector<QualityEnum> qualityTypes);
		std::vector<AbstractNetwork<MultiCriteriaValue>*>* getArchive();

	private:
		QualityMeasuresImpl* qualityMeasures;
		ElitistArchive* elitistArchive;

		void setQualityMeasuresPromise(std::promise<QualityValue*>&& promise, std::vector<AbstractNetwork<MultiCriteriaValue>*>* population, QualityEnum qualityType);
		void setElitistArchivePromise(std::promise<QualityValue*>&& promise, QualityEnum qualityType);
};
