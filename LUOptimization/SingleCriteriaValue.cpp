#include "SingleCriteriaValue.h"

SingleCriteriaValue::SingleCriteriaValue()
{
	value = INIT_VALUE;
	notFittedPassangers = INIT_VALUE;
	numberOfCrossingTrainsThreshold = INIT_VALUE;
}

SingleCriteriaValue::SingleCriteriaValue(const SingleCriteriaValue& singleCriteriaValue)
{
	value = singleCriteriaValue.value;
	notFittedPassangers = singleCriteriaValue.notFittedPassangers;
	numberOfCrossingTrainsThreshold = singleCriteriaValue.numberOfCrossingTrainsThreshold;
}

SingleCriteriaValue::SingleCriteriaValue(unsigned long long int value, int notFittedPassangers, int numberOfCrossingTrainsThreshold)
{
	this->value = value;
	this->notFittedPassangers = notFittedPassangers;
	this->numberOfCrossingTrainsThreshold = numberOfCrossingTrainsThreshold;
}

SingleCriteriaValue::~SingleCriteriaValue()
{
}

bool SingleCriteriaValue::operator>(const SingleCriteriaValue& singleCriteriaValue) const
{
	return this->value > singleCriteriaValue.value;
}

bool SingleCriteriaValue::operator<(const SingleCriteriaValue& singleCriteriaValue) const
{
	return this->value < singleCriteriaValue.value;
}

void SingleCriteriaValue::setValue(unsigned long long int value)
{
	this->value = value;
}

void SingleCriteriaValue::setNotFittedPassangers(long notFittedPassangers)
{
	this->notFittedPassangers = notFittedPassangers;
}

void SingleCriteriaValue::setNumberOfCrossingTrainsThreshold(long numberOfCrossingTrainsThreshold)
{
	this->numberOfCrossingTrainsThreshold = numberOfCrossingTrainsThreshold;
}

unsigned long long int SingleCriteriaValue::getValue()
{
	return value;
}

long SingleCriteriaValue::getNotFittedPassangers()
{
	return notFittedPassangers;
}

long SingleCriteriaValue::getNumberOfCrossingTrainsThreshold()
{
	return numberOfCrossingTrainsThreshold;
}

void SingleCriteriaValue::addValue(unsigned long long int value)
{
	this->value += value;
}

void SingleCriteriaValue::addNumberOfCrossingTrainsThreshold()
{
	numberOfCrossingTrainsThreshold++;
}
