#pragma once
#include <vector>
#include "AbstractNetwork.h"
#include "MultiCriteriaValue.h"
#include "QualityValue.h"
#include "QualityEnum.h"
#include "NotImplementedException.h"

class IQualityMeasures
{
	public:
		IQualityMeasures();
		virtual ~IQualityMeasures();
		virtual QualityValue* getQuality(std::vector<AbstractNetwork<MultiCriteriaValue>*>* population, QualityEnum qualityType);
};
