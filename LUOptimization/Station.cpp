#include "Station.h"

using namespace std;

Station::Station(string id, string name, double lat, double lon)
{
	this->id = id;
	this->name = name;
	this->lat = lat;
	this->lon = lon;
	this->connections;
}

Station::Station(string id, string name, double lat, double lon, vector<Connection*> connections)
{
	this->id = id;
	this->name = name;
	this->lat = lat;
	this->lon = lon;
	this->connections = connections;
}

Station::Station(string id, string name, double lat, double lon, int NLCCode)
{
	this->id = id;
	this->name = name;
	this->lat = lat;
	this->lon = lon;
	this->connections;
	this->NLCCode = NLCCode;
}

Station::~Station()
{
	for (int i = 0; i < connections.size(); i++)
	{
		delete connections[i];
	}
}

bool Station::containsConnection(Station& station)
{
	if (connections.empty())
	{	
		return false;
	}
	else
	{
		for (int i = 0; i < connections.size(); i++)
		{
			if (connections[i]->getStation()->_Equal(station))
			{
				return true;
			}
		}

		return false;
	}
}

bool Station::_Equal(const Station& station) const
{
	return id._Equal(station.id) && name._Equal(station.name) && lat == station.lat && lon == station.lon;
}

size_t Station::getHash() const
{
	size_t hash = 0;
	boost::hash_combine(hash, id);
	boost::hash_combine(hash, name);
	boost::hash_combine(hash, lat);
	boost::hash_combine(hash, lon);
	boost::hash_combine(hash, NLCCode);

	for (int i = 0; i < connections.size(); i++)
	{
		Connection* connection = connections[i];
		boost::hash_combine(hash, connection->getDistance());
		boost::hash_combine(hash, connection->getJourneyTime());
		boost::hash_combine(hash, connection->getStation());
	}

	return hash;
}

bool Station::operator==(const Station& station) const
{
	return _Equal(station);
}

int Station::getJourneyTime(string& stationId)
{
	Connection* connection = getConnection(stationId);
	return connection == nullptr ? -1 : connection->getJourneyTime();
}

void Station::setNLCCode(int NLCCode)
{
	this->NLCCode = NLCCode;
}

Station* Station::addConnection(Connection* connection)
{
	if (containsConnection(*connection->getStation()))
	{
		return NULL;
	}
	else
	{
		connections.push_back(connection);
		return connection->getStation();
	}
}

string Station::getId()
{
	return id;
}

string Station::getName()
{
	return name;
}

int Station::getNLCCode()
{
	return NLCCode;
}

double Station::getLat()
{
	return lat;
}

double Station::getLon()
{
	return lon;
}

vector<Connection*> Station::getConnections()
{
	return connections;
}

Connection* Station::getConnection(std::string& stationId)
{
	for (int i = 0; i < connections.size(); i++)
	{
		if (connections[i]->getStation()->getId()._Equal(stationId))
		{
			return connections[i];
		}
	}

	return nullptr;
}

size_t StationHash::operator()(const Station& n) const
{
	return n.getHash();
}

size_t StationHash::operator()(const Station* n) const
{
	return n->getHash();
}

bool StationEqual::operator()(const Station* l, const Station* r) const
{
	return l->_Equal(*r);
}
